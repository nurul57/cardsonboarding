package android.form.avss.prepaidcard;

import android.form.avss.prepaidcard.database.MyDBHelper;
import android.form.avss.prepaidcard.ui.activities.LoginActivity;
import android.form.avss.prepaidcard.ui.adapters.ImagesName;
import android.form.avss.prepaidcard.ui.custom_classes.CustomTextInputLayout;
import android.form.avss.prepaidcard.ui.custom_classes.InputFilters;
import android.form.avss.prepaidcard.ui.custom_classes.RegularExpression;
import android.os.Build;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }


    @Test public void getOS() {
        System.out.print(Arrays.toString(Build.VERSION_CODES.class.getFields()));
    }

    @Test
    public void addressSpecialCharacter() {
        String address = " 1jhas";
        System.out.print(address.matches(RegularExpression.MULTIPLE_SPACE));
    }


}


