package android.form.avss.prepaidcard.helper_classes;

import android.content.Context;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public final class CreateKycRequest {
    private String bdeNo;
    @Deprecated
    public CreateKycRequest(Context context, String bdeNo) {
        this.bdeNo = bdeNo;
    }

    public CreateKycRequest(String bdeNo) {
        this.bdeNo = bdeNo;
    }

    /**
     * @param uid
     * @param bio
     * @param _imei - IMEI NUMBER OF DEVICE
     * @throws IOException
     * @throws GeneralSecurityException
     */
    public String createRequest(String uid,HashMap<String, String> bio,String _imei)
    {
         SimpleDateFormat ddmm = new SimpleDateFormat("MMdd", Locale.getDefault());
        Date date = new Date();
        String ra = "F";
        String rc = "Y";
        String de = "N";
        String lr = "N";
        String tid;
        tid = "AXB" + _imei.substring(_imei.length()-12);
        String agentId = bdeNo;
        String pi = "N";
        String pa = "N";
        String pfa = "N";
        String pbio = "Y";
        String bt = "FMR";
        String pin = "N";
        String otp = "N";
        String pfr = "N";

        return "<KycRequest uid=\"" + uid + "\" ra=\"" + ra + "\" rc=\"" + rc + "\" de=\"" + de + "\" lr=\"" + lr + "\" req_id=\"" + ddmm.format(date) + "\" txn_date=\"" + ddmm.format(date) + "\" tid=\"" + tid + "\" agentid=\"" + agentId + "\" pfr=\"" + pfr + "\">" +
                "<Uses pi=\"" + pi + "\" pa=\"" + pa + "\" pfa=\"" + pfa + "\" bio=\"" + pbio + "\" bt=\"" + bt + "\" pin=\"" + pin + "\" otp=\"" + otp + "\" />" +
                "<Meta udc=\"" + tid + "\" " + bio.get("DeviceInfo") + " />" +
                bio.get("Skey") +
                bio.get("Data") +
                bio.get("Hmac") +
                "</KycRequest>" + "#" + bio.get("DeviceInfo");
    }
}
