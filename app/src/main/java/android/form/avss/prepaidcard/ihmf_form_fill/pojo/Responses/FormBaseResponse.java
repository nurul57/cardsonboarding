package android.form.avss.prepaidcard.ihmf_form_fill.pojo.Responses;

import com.google.gson.annotations.SerializedName;

public class FormBaseResponse<T> {
    @SerializedName("da")
    private T data;
    @SerializedName("em")
    private String error_msg;
    @SerializedName("ss")
    private Boolean status;

    public T getData() {
        return data;
    }

    public String getError_msg() {
        return error_msg;
    }

    public Boolean isSuccess() {
        return status;
    }

    public String getToken() {
        return token;
    }

    @SerializedName("tn")
    private String token;










}
