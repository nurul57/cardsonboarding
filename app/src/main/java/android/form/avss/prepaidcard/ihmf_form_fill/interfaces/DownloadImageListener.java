package android.form.avss.prepaidcard.ihmf_form_fill.interfaces;

import android.graphics.Bitmap;

public interface DownloadImageListener {
    void onSuccess(Bitmap bitmap);
    void onFailure(String msg);
}
