package android.form.avss.prepaidcard.web_services.requests;

import android.form.avss.prepaidcard.web_services.responses.BaseResponse;

import com.google.gson.annotations.SerializedName;

public class LoginRequest extends BaseResponse {
    @SerializedName("un")
    private String userName;
    @SerializedName("pd")
    private String userPassword;

    @SerializedName("cl")
    private String crashListReport;
    @SerializedName("lts")
    private String timeStamp;
    @SerializedName("apv")
    private String appVersion;
    @SerializedName("hh")
    private String appHash;


    private String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    private String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }


    public String getCrashListReport() {
        return crashListReport;
    }

    public void setCrashListReport(String crashListReport) {
        this.crashListReport = crashListReport;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getAppHash() {
        return appHash;
    }

    public void setAppHash(String appHash) {
        this.appHash = appHash;
    }

    public String toString() {
        return "User Name : " + getUserName() + "\n User Password : " + getUserPassword() + "\n App Version : " + getAppVersion()
                + "\n Time Stamp : " + getTimeStamp() + "\n App Hash : " + getAppHash();
    }
}
