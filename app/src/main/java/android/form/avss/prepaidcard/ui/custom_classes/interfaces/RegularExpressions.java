package android.form.avss.prepaidcard.ui.custom_classes.interfaces;

public interface RegularExpressions {
    String IFSC_EXPRESSION ="^[A-Z]{4}[0]{1}[0-9]{6}$";
    String PAN_EXPRESSION = "^[A-Z]{3}[P][A-Z][0-9]{4}[A-Z]$";
    String MOBILE_NO_EXPRESSION = "^[6-9]{1}[0-9]{9}$";
    String EMAIL_EXPRESSION = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
            "\\@" + "[a-zA-Z0-9]{0,64}" + "(" + "\\." + "[a-zA-Z0-9]{0,25}" + ")+";
    String ADDRESS = "^([a-zA-Z0-9\\.\\,\\#\\-\\/]*\\s)*[a-zA-Z0-9\\.\\,\\#\\-\\/]*$";

    String MULTIPLE_SPACE = "^\\S+(\\s\\S+)*$";
}
