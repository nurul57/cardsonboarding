package android.form.avss.prepaidcard.web_services;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class BasicInterceptor implements Interceptor {
    private String authToken;
    private String webFunctionName;

    BasicInterceptor(String token, String webFunctionName) {
        this.authToken = token;
        this.webFunctionName = webFunctionName;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();

        Request.Builder builder = original.newBuilder()
                .header("tn", authToken)
                .header("fn", webFunctionName);

        Request request = builder.build();
        return chain.proceed(request);
    }
}
