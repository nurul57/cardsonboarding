package android.form.avss.prepaidcard.ihmf_form_fill.pojo.Requests;

import com.google.gson.annotations.SerializedName;

public class MarkedDataRequest {


    @SerializedName("formGroupId")
    private String formId;

    @SerializedName("refId")
    private String referenceId;

    @SerializedName("type")
    private String type;


    public void setFormId(String formId) {
        this.formId = formId;
    }

    public void setReferenceId(String refId) {
        this.referenceId = refId;
    }

    public void setType(String type) {
        this.type = type;
    }
}
