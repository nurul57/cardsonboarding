package android.form.avss.prepaidcard.ihmf_form_fill.ui.Adapters;

import android.form.avss.prepaidcard.ihmf_form_fill.ui.Fragments.FormFillFragmentForm;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class FormFillFragmentPagerAdapter extends FragmentPagerAdapter {

    private SparseArray<FormFillFragmentForm> sparseArray;
    private List<Fragment> fragmentList;

    public FormFillFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
        fragmentList = new ArrayList<>();
        sparseArray = new SparseArray<>();
    }

    public void addFragment(Fragment fragment) {
        fragmentList.add(fragment);
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }


    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

    @NonNull
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        sparseArray.put(position, (FormFillFragmentForm) fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        sparseArray.remove(position);
        super.destroyItem(container, position, object);
    }

    public FormFillFragmentForm getRegisteredFragment(int pos) {
        return sparseArray.get(pos);
    }
}
