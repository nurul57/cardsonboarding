package android.form.avss.prepaidcard.web_services.responses;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class LoginResponse {
    @SerializedName("da")
    public String data;

    public static class LoginData  extends BaseResponse<LoginResponse.LoginData> {
        @SerializedName("sslt")
        private ArrayList<String> serviceStatusList = new ArrayList<>();

        public ArrayList<String> getServiceStatusList() {
            return serviceStatusList;
        }

        public void setServiceStatusList(ArrayList<String> serviceStatusList) {
            this.serviceStatusList = serviceStatusList;
        }

        public boolean isServiceListEmpty() {
            if (serviceStatusList == null) return true;
            return serviceStatusList.isEmpty();
        }

        public int size() {
            return serviceStatusList == null || serviceStatusList.isEmpty() ? 0 : serviceStatusList.size();
        }
    }
}
