package android.form.avss.prepaidcard.ui.activities;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.form.avss.prepaidcard.BuildConfig;
import android.form.avss.prepaidcard.R;
import android.form.avss.prepaidcard.interfaces.Keys;
import android.form.avss.prepaidcard.receivers.NetworkReceiver;
import android.form.avss.prepaidcard.ui.custom_classes.CustomTextInputLayout;
import android.form.avss.prepaidcard.utils.RootUtil;
import android.graphics.Color;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ScrollView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import static android.form.avss.prepaidcard.receivers.NetworkReceiver.register;
import static android.form.avss.prepaidcard.receivers.NetworkReceiver.unregister;

public class BaseActivity extends AppCompatActivity implements Keys {
    protected NetworkReceiver receiver;

    protected String[] permissionList = {
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.CAMERA,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    public String[] cameraPermission = {
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

	protected SimpleDateFormat format;

	public boolean isEmpty(String msg){
		return TextUtils.isEmpty(msg);
	}

	public boolean isNotEmpty(String msg){
		return !TextUtils.isEmpty(msg);
	}


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        if (RootUtil.isDeviceRooted()) {
//            ApplicationUtil.dialog(this).show();
//            return;
//        }

        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(Color.parseColor("#ba1655"));

        format = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

        receiver = new NetworkReceiver();
        receiver.setOnStateChangeListener(new NetworkReceiver.OnStateChange() {
            @Override
            public void onChange(BroadcastReceiver receiver, boolean isConnected) {
                if (isFinishing()) return;

                Snackbar snackbar = showNetworkSnackBar(isConnected);
                if (snackbar != null)
                    snackbar.show();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        register(this, receiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));



        if (!BuildConfig.DEBUG && RootUtil.isDeviceRooted()) {
            AlertDialog alertDialog = new AlertDialog.Builder(BaseActivity.this).create();
            alertDialog.setTitle(R.string.app_name);
            alertDialog.setMessage("This application is not allowed to run on rooted device");
            alertDialog.setCancelable(false);
            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            alertDialog.show();
        }
    }

    public void hideSoftKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(Objects.requireNonNull(this.getCurrentFocus()).getWindowToken(), 0);
        }
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public void hideSoftKeyboard(View view) {
        if (view == null) {
            view = this.getCurrentFocus();
        }
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            if (view != null) {
                inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    protected Snackbar showNetworkSnackBar(boolean isConnected) {
        if (isFinishing()) return null;
        final String msg = isConnected ? "Internet Connection found...." : "No Internet Connection....";
        int duration = isConnected ? Snackbar.LENGTH_SHORT : Snackbar.LENGTH_INDEFINITE;
        final Snackbar snackbar = Snackbar.make(Objects.requireNonNull(findViewById(android.R.id.content)), msg, duration);
        snackbar.setAction(isConnected ? "Close" : "Retry", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkReceiver.isConnected(v.getContext()))
                    snackbar.dismiss();
                else {
                    Snackbar sb = showNetworkSnackBar(NetworkReceiver.isConnected(v.getContext()));
                    if (sb != null)
                        sb.show();
                }
            }
        });
        return snackbar;
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregister(this, receiver);
    }


    public void toast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    public void toast(@StringRes int resId) {
        Toast.makeText(this, getString(resId), Toast.LENGTH_LONG).show();
    }

    // todo: to scroll page for mandatory fields if there aren't field in fragment

    /**
     * Used to scroll to the given view.
     *
     * @param scrollViewParent Parent ScrollView
     * @param view             View to which we need to scroll.
     */
    public void scrollToView(final ScrollView scrollViewParent, final View view) {
        // Get deepChild Offset
        Point childOffset = new Point();
        getDeepChildOffset(scrollViewParent, view.getParent(), view, childOffset);
        // Scroll to child.
        scrollViewParent.smoothScrollTo(0, childOffset.y);
    }

    /**
     * Used to get deep child offset.
     * <p/>
     * 1. We need to scroll to child in scrollview, but the child may not the direct child to scrollview.
     * 2. So to get correct child position to scroll, we need to iterate through all of its parent views till the main parent.
     *
     * @param mainParent        Main Top parent.
     * @param parent            Parent.
     * @param child             Child.
     * @param accumulatedOffset Accumulated Offset.
     */
    public void getDeepChildOffset(final ViewGroup mainParent, final ViewParent parent,
                                   final View child, final Point accumulatedOffset) {
        ViewGroup parentGroup = (ViewGroup) parent;
        accumulatedOffset.x += child.getLeft();
        accumulatedOffset.y += child.getTop();
        if (parentGroup.equals(mainParent)) {
            return;
        }
        getDeepChildOffset(mainParent, parentGroup.getParent(), parentGroup, accumulatedOffset);
    }

    public void automaticallyScrollToField(List<Object> mandatoryFieldList, ScrollView sv) {
        if (!mandatoryFieldList.isEmpty()) {
            for (Object object : mandatoryFieldList) {
                if (object instanceof CustomTextInputLayout) {
                    CustomTextInputLayout tv = (CustomTextInputLayout) object;
                    if (tv.isFieldEmpty()) {
                        scrollToView(sv, tv);
                        break;
                    }
                }
            }
        }
    }


}
