package android.form.avss.prepaidcard.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public final class NetworkReceiver extends BroadcastReceiver {
    private OnStateChange mListener;
    private boolean mFirstAlert = true;

    public static void register (Context ctx, NetworkReceiver receiver, IntentFilter filter) {
        if (ctx == null || receiver == null) return;
        ctx.registerReceiver(receiver, filter);
    }

    public static void unregister (Context ctx, NetworkReceiver receiver) {
        if (ctx == null || receiver == null) return;
        ctx.unregisterReceiver(receiver);
    }

    @Override
    public void onReceive(Context ctx, Intent intent) {
            // check if broadcast is from network broadcast receiver or not. If not, then return.
        if (intent == null || intent.getAction() == null ||
		        !intent.getAction().equalsIgnoreCase(ConnectivityManager.CONNECTIVITY_ACTION))
            return;

        ConnectivityManager manager = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);

        // If failed to get instance of manager...
        if ( manager == null ) return;
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();

        // Now we start check network state
        boolean isConnected = false;

		// Check if any provider is ava ilable and whether is connected...
        isConnected = networkInfo != null && networkInfo.isConnected();

		// This is used to check if it is first all or not.
	    // if so, then check network available or not.
        // if network is not available then show message and disable this block.
        // If not then skip first alert as having network initially is good idea.
        if ( mFirstAlert ) {
            if ( isConnected )
                return;
            else
                mFirstAlert = false;
        }

		// Call Network state update if listener is assigned.
        if ( mListener != null )
            mListener.onChange(this, isConnected);
    }

    public void setOnStateChangeListener(OnStateChange l) {
        this.mListener = l;
    }

    public static boolean isConnected(Context ctx) {
        if (ctx == null) return false;

        boolean isConnected = false;

        ConnectivityManager manager = (ConnectivityManager)
                ctx.getSystemService(Context.CONNECTIVITY_SERVICE);

        // If failed to get instance of manager...
        if (manager == null) return false;

        NetworkInfo networkInfo = manager.getActiveNetworkInfo();

//        If no network info object not found....
        if (networkInfo == null) return false;

        isConnected = networkInfo.isConnected();
        return isConnected;
    }


    public static boolean isConnecting(Context ctx) {
        if (ctx == null) return false;

        boolean isConnected = false;
        ConnectivityManager manager = (ConnectivityManager)
                ctx.getSystemService(Context.CONNECTIVITY_SERVICE);

        // If failed to get instance of manager...
        if (manager == null) return false;

        NetworkInfo networkInfo = manager.getActiveNetworkInfo();

//        If no network info object not found....
        if (networkInfo == null) return false;

        boolean isConnectingOrConnected = networkInfo.isConnectedOrConnecting();
        isConnected = networkInfo.isConnected();

        // isConnecting  ===== isConnected ===== Result
        //   true               true             false
        //   false              false            false
        //   true               false            true <== That's we want.
        return isConnected != isConnectingOrConnected;
    }

    public interface OnStateChange {
        void onChange(BroadcastReceiver receiver, boolean isConnected);
    }
}
