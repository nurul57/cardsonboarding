package android.form.avss.prepaidcard.ihmf_form_fill.ui.Fragments;


import android.content.Context;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.controls.Control;
import android.form.avss.prepaidcard.ihmf_form_fill.ui.Activities.FormBaseActivity;
import android.form.avss.prepaidcard.ihmf_form_fill.ui.Activities.FormFillActivity;
import android.form.avss.prepaidcard.ihmf_form_fill.util.MySharePreference;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.RecyclerView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;

/**
 * A simple {@link Fragment} subclass.
 */
public class FormBaseFragment extends Fragment {


    private FormFillActivity activity;
    public static Control mControl = Control.NONE;


    public File getFormImagesFileDir(){
        if(getActivity() instanceof FormBaseActivity){
            return  ((FormBaseActivity)getActivity()).getFormImagesFilesDirectory();
        }
        else return null;
    }



    public MySharePreference getSPrefenrece(){
        if(getActivity() instanceof FormBaseActivity){
            return  ((FormBaseActivity)getActivity()).getSPreference();
        }
        return new MySharePreference(getContext());
    }


    public void showShortToast(String mssg){
        Toast.makeText(getContext(), mssg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (FormFillActivity) context;

    }


    protected DrawerLayout getDrawer() {
        return activity.drawer;
    }

    @Override
    public void onDetach() {
        activity = null;
        super.onDetach();

    }


}
