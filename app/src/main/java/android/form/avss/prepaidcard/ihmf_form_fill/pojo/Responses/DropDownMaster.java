package android.form.avss.prepaidcard.ihmf_form_fill.pojo.Responses;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DropDownMaster {
    @SerializedName("ddLabelId")
    private String ddLabelId;

    @SerializedName("dataList")
    private List<String> dataList;


    public String getDdLabelId() {
        return ddLabelId;
    }

    public List<String> getDataList() {
        return dataList;
    }


    public void setDdLabelId(String ddLabelId) {
        this.ddLabelId = ddLabelId;
    }

    public void setDataList(List<String> dataList) {
        this.dataList = dataList;
    }
}
