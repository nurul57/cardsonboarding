package android.form.avss.prepaidcard.ihmf_form_fill.pojo.controls;


import android.form.avss.prepaidcard.ihmf_form_fill.TempPackage.GroupControls;

public interface iHMFGroupListener<T> {

    GroupControls getGroupControl(String groupId);

    void handleGroup(int viewId, String groupId, T isChecked);
}

