package android.form.avss.prepaidcard.ihmf_form_fill.pojo.controls;

import com.google.gson.annotations.SerializedName;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

public class TextControl extends BaseControl {

    public static final int DEFAULT_FONT_SIZE = 12;
    // used for validation

    public static final int DEFAULT_BOX_COUNT = 0;

    public static final int DEFAULT_BOX_FRACTION = 0;


    @SerializedName("boxTextAlignment")
    private String boxTextAlignment = "";

    @SerializedName("boxCount")
    private Integer boxCount = DEFAULT_BOX_COUNT;

    @SerializedName("dataType")
    private String dataType;

    @SerializedName("fontSize")
    private float fontSize = DEFAULT_FONT_SIZE;

    @SerializedName("accessLock")
    private String accessLock;

    @SerializedName("boxGap")
    private Integer boxGap;

//    @SerializedName("fontFamily")
//    private transient String fontFamily;

    @SerializedName("fontColor")
    private transient String fontColor;

//    @SerializedName("fontBold")
//    private transient String fontBold;

//    @SerializedName("fontItalic")
//    private transient String fontItalic;

    @SerializedName("borderWeight")
    private Integer borderWeight = DEFAULT_INT_DIMEN;

    @SerializedName("charLength")
    private String charLength;

    @SerializedName("minChar")
    private String minChar;

//    @SerializedName("fontUnderLine")
//    private transient String fontUnderLine;

    @SerializedName("textAlignment")
    private transient String textAlignment;

    @SerializedName("masking")
    private transient String masking;

    @SerializedName("amountDisplayType")
    private String amountDisplayType;

    @SerializedName("dateFormat")
    private String dateFormat;

    @SerializedName("unlimitedChar")
    private transient String unlimitedChar;

    @SerializedName("totalLines")
    private transient String totalLines;

    @SerializedName("manual")
    private transient String manual;

    @SerializedName("formula")
    private transient String formula;

    @SerializedName("validateformula")
    private String validateformula;

    @SerializedName("usedIn")
    private transient String usedIn;

    @SerializedName("companyPublisherIds")
    private transient String companyPublisherIds;

    @SerializedName("fractionalDigits")
    private transient Integer fractionalDigits = DEFAULT_BOX_FRACTION ;

    @SerializedName("ddLabelId")
    private  String ddLabelId;

    @SerializedName("ddLabelName")
    private String ddLabelName;

    @SerializedName("dependentId")
    private transient String dependentId;

    @SerializedName("dependentIdsName")
    private transient String dependentIdsName;

    @SerializedName("actionType")
    private transient String actionType;

    @SerializedName("multiSelect")
    private transient String multiSelect;

    @SerializedName("dropdownType")
    private transient String dropdownType;

    @SerializedName("dropdownLock")
    private transient String dropdownLock;

    @SerializedName("validation")
    private String validation;

    @SerializedName("dropdownArrayList")
    private List<String> dropdownArrayList;

    @SerializedName("isCapital")
    private String isCapital = "0";

    @SerializedName("intrmdtPageMappingId")
    private transient String intrmdtPageMappingId;

    @SerializedName("otpValidate")
    private transient String otpValidate;

    @SerializedName("otpImageX")
    private transient String otpImageX;

    @SerializedName("signatureMappingId")
    private transient String signatureMappingId;

    @SerializedName("ctrlUsedInSigns")
    private transient String ctrlUsedInSigns;

    @SerializedName("bothAreSame")
    private transient String bothAreSame;

    @SerializedName("textSpacing")
    private transient String textSpacing;

    @SerializedName("historyCollection")
    private transient String historyCollection;

    @SerializedName("idUsedInFormula")
    private transient String idUsedInFormula;

    @SerializedName("validationFailed")
    private transient String validationFailed;

    @SerializedName("isWidthSetManually")
    private transient String isWidthSetManually;

    @SerializedName("boxBorderWidth")
    private transient String boxBorderWidth;

    @SerializedName("printWithBorder")
    private transient String printWithBorder;

    @SerializedName("boxCollection")
    private transient List<String> boxCollection;

    @SerializedName("isOther")
    private transient String isOther;

    @SerializedName("formFeeId")
    private transient String formFeeId;

    @SerializedName("regex")
    private String regex ;


    @SerializedName("infRegex")
    private String inputFilter;

    public String getRegex() {
        String pattern;
        try {
            pattern = URLDecoder.decode(regex, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return null;
        }
        return pattern;
    }

    public String getInputFilterRegex() {
        String pattern;
        try {
            pattern = URLDecoder.decode(inputFilter, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return null;
        }
        return pattern;
    }

    public String getAmountDisplayType() {
        return amountDisplayType;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public Integer getFontSize() {
        return (int)fontSize;
    }


    public Integer getScaledFontSize(){
        float x = fontSize * getRatio().y;

        return (int)x;
    }


    public boolean isValid() {
        return dataType == null || getActualText().matches(dataType);
    }


    public String getDataType() {
        return dataType == null ? "" : dataType;
    }



    public Integer getBorderWeight() {
        return borderWeight;
    }

    public void setBorderWeight(Integer borderWeight) {
        this.borderWeight = borderWeight;
    }

    public Integer getBoxCount() {
        return boxCount;
    }

    public void setBoxCount(Integer boxCount) {
        this.boxCount = boxCount;
    }


    public boolean isEmpty() {
        return boxCount == null || boxCount == DEFAULT_BOX_COUNT;
    }

    public int getFractionalDigits() {
        return fractionalDigits == null ? DEFAULT_BOX_FRACTION : fractionalDigits;
    }


    public int getBoxGap() {
        return boxGap == null ? DEFAULT_BOX_FRACTION : boxGap;
    }


    public boolean isCapital() {
        return Integer.parseInt(isCapital) == 1;
    }

    public String getDdLabelId() {
        return ddLabelId;
    }

    public String getDdLabelName() {
        return ddLabelName;
    }
}
