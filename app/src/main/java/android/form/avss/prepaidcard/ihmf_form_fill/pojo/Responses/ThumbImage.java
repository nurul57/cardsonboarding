package android.form.avss.prepaidcard.ihmf_form_fill.pojo.Responses;

public class ThumbImage{
    private String thumbUrl;
    private boolean isSelected = false;


    public String getThumbUrl() {
        return thumbUrl;
    }

    public void setThumbUrl(String thumbUrl) {
        this.thumbUrl = thumbUrl;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
