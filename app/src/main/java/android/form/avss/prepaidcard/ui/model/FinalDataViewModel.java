package android.form.avss.prepaidcard.ui.model;

public class FinalDataViewModel {

	private EkycOptionViewModel ekycOptionViewModel;
	private PanDataViewModel panDataViewModel;
	private CardDetailViewModel cardDetailViewModel;

	public FinalDataViewModel() {}

	public EkycOptionViewModel getEkycOptionViewModel() {
		return ekycOptionViewModel;
	}

	public void setEkycOptionViewModel(EkycOptionViewModel ekycOptionViewModel) {
		this.ekycOptionViewModel = ekycOptionViewModel;
	}

	public PanDataViewModel getPanDataViewModel() {
		return panDataViewModel;
	}

	public void setPanDataViewModel(PanDataViewModel panDataViewModel) {
		this.panDataViewModel = panDataViewModel;
	}

	public CardDetailViewModel getCardDetailViewModel() {
		return cardDetailViewModel;
	}

	public void setCardDetailViewModel(CardDetailViewModel cardDetailViewModel) {
		this.cardDetailViewModel = cardDetailViewModel;
	}
}
