package android.form.avss.prepaidcard.ui.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PanDataViewModel implements Serializable {
	@SerializedName("pn")
    private String panNumber;
	@SerializedName("aci")
	private String agriCultureIncome;
	@SerializedName("naci")
	private String nonAgriCultureIncome;
	@SerializedName("ar")
	private String acknowledgementNumber;
	@SerializedName("ad")
    private String applicationDate;
	@SerializedName("ic")
    private String isIncomeMoreThan5Lac;
	@SerializedName("ispn")
    private String isPanAvailable;

    public String getPanNumber() {
        return panNumber;
    }

    public void setPanNumber(String panNumber) {
        this.panNumber = panNumber;
    }

    public String getAgriCultureIncome() {
        return agriCultureIncome;
    }

    public void setAgriCultureIncome(String agriCultureIncome) {
        this.agriCultureIncome = agriCultureIncome;
    }

    public String getNonAgriCultureIncome() {
        return nonAgriCultureIncome;
    }

    public void setNonAgriCultureIncome(String nonAgriCultureIncome) {
        this.nonAgriCultureIncome = nonAgriCultureIncome;
    }

    public String getAcknowledgementNumber() {
        return acknowledgementNumber;
    }

    public void setAcknowledgementNumber(String acknowledgementNumber) {
        this.acknowledgementNumber = acknowledgementNumber;
    }

    public String getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(String applicationDate) {
        this.applicationDate = applicationDate;
    }

	/*public boolean isIncomeMoreThan5Lac() {
		return isIncomeMoreThan5Lac;
	}*/

	public void setIncomeMoreThan5Lac(boolean incomeMoreThan5Lac) {
        if(incomeMoreThan5Lac) {
            isIncomeMoreThan5Lac = "Y";
        } else {
            isIncomeMoreThan5Lac = "N";
        }
	}

	/*public boolean isPanAvailable() {
        return isPanAvailable;
    }*/

    public void setPanAvailable(boolean panAvailable) {
        if(panAvailable) {
            isPanAvailable = "Y";
        } else {
            isPanAvailable = "N";
        }
    }
}
