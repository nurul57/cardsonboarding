package android.form.avss.prepaidcard.web_services.pojo;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ImageOption implements Serializable {

    @SerializedName("doc_ttl")
    private String tittle;
    @SerializedName("doc_nm")
    private String docName;
    @SerializedName("doc_min")
    private String  min;
    @SerializedName("doc_max")
    private String  max;
    @SerializedName("doc_isadr")
    private String isAadhar;
    @SerializedName("doc_issh")
    private String isShow;
    @SerializedName("doc_ism")
    private String isMandatory;

    private boolean isSelectedItem = false;
    private boolean isDataCaptured = false;

    public static final String TRUE ="true";
    public static final String FALSE ="false";

    public ImageOption() {}

    public ImageOption(String tittle) {
        this.tittle = tittle;
    }

    public ImageOption(String tittle, String min, String max) {
        this.tittle = tittle;
        this.min = min;
        this.max = max;
    }

    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public String getDocName() {
        return docName;
    }

    public void setDocName(String docName) {
        this.docName = docName;
    }

    public boolean isDocNameEmpty() {
        return TextUtils.isEmpty(getDocName());
    }

    public String getMin() {
        return min;
    }

    public int getMinCount() {
        if (TextUtils.isEmpty(getMin())) return  -1;
        return Integer.parseInt(getMin());
    }

    public boolean isMinCountZero() {
        return TextUtils.isEmpty(getMin());
    }

    public void setMin(String min) {
        this.min = min;
    }

    public String getMax() {
        return max;
    }

    public int getMaxCount() {
        if (TextUtils.isEmpty(getMax())) return  -1;
        return Integer.parseInt(getMax());
    }
    public boolean isMaxCountZero() {
        return TextUtils.isEmpty(getMax());
    }

    public void setMax(String max) {
        this.max = max;
    }

    public String getIsShow() {
        return isShow;
    }

    public void setIsShow(String isShow) {
        this.isShow = isShow;
    }

    public String getIsMandatory() {
        return isMandatory;
    }

    public void setIsMandatory(String isMandatory) {
        this.isMandatory = isMandatory;
    }

    public boolean isShow() {
        return TRUE.equalsIgnoreCase(getIsShow());
    }

    public boolean isHide() {
        return FALSE.equalsIgnoreCase(getIsShow());
    }

    public boolean isMandatory() {
        return TRUE.equalsIgnoreCase(getIsMandatory());
    }

    public boolean isNonMandatory() {
        return FALSE.equalsIgnoreCase(getIsMandatory());
    }

    public String getIsAadhar() {
        return isAadhar;
    }

    public void setIsAadhar(String isAadhar) {
        this.isAadhar = isAadhar;
    }

    public boolean isAadhaar() {
        return TRUE.equalsIgnoreCase(getIsAadhar());
    }


    public boolean isSelectedItem() {
        return isSelectedItem;
    }

    public void setSelectedItem(boolean selectedItem) {
        isSelectedItem = selectedItem;
    }

    public boolean isDataCaptured() {
        return isDataCaptured;
    }

    public void setDataCaptured(boolean dataCaptured) {
        isDataCaptured = dataCaptured;
    }
}
