package android.form.avss.prepaidcard.web_services.requests;

import com.google.gson.annotations.SerializedName;

public class FinalDataSubmitRequest extends BaseRequest {

    @SerializedName("da")
    private String finalSubmit;

    public String getFinalSubmit() {
        return finalSubmit;
    }

    public void setFinalSubmit(String finalSubmit) {
        this.finalSubmit = finalSubmit;
    }
}
