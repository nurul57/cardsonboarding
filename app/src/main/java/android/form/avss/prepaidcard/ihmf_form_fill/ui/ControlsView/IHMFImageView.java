package android.form.avss.prepaidcard.ihmf_form_fill.ui.ControlsView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.form.avss.prepaidcard.R;
import android.form.avss.prepaidcard.ihmf_form_fill.util.DragTouchListener;
import android.form.avss.prepaidcard.ihmf_form_fill.interfaces.FocusListener;
import android.form.avss.prepaidcard.ihmf_form_fill.interfaces.OnLongPressListener;
import android.form.avss.prepaidcard.ihmf_form_fill.interfaces.iHMFView;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.controls.ImageControl;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.controls.iHMFGroupListener;
import android.form.avss.prepaidcard.ihmf_form_fill.util.ConstantsUtils;
import android.form.avss.prepaidcard.ihmf_form_fill.util.ImageUtil;
import android.form.avss.prepaidcard.ihmf_form_fill.util.MySharePreference;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.widget.AppCompatImageView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Toast;

import com.google.gson.JsonObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import static android.content.Context.MODE_PRIVATE;


public class IHMFImageView extends AppCompatImageView implements iHMFView<ImageControl> {

//    public static final int ROTATE_90 = 90;
//    private static final int ROTATE_180 = 180;
//    private static final int ROTATE_270 = 270;
    private static final OnTouchListener DRAG_TOUCH_LISTENER = new DragTouchListener();
    private ImageControl mControl;
    private OnTouchListener mTouchListener;
    private Drawable mErrorDrawable;
    private Drawable mDefaultDrawable;

    private iHMFGroupListener<Bitmap> mGroupListener;
//    private GroupControls mGroup;

    private OnLongPressListener mLongPressListener;
    private MySharePreference mSharePreference;


    public IHMFImageView(Context context) {
        this(context,null);
    }

    public IHMFImageView(Context context, AttributeSet attrs) {
        this(context, attrs,-1);
    }
    public IHMFImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        if (getContext() instanceof OnLongPressListener)
            mLongPressListener = (OnLongPressListener) getContext();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        if (getControl().isMarkedControl()) {
            setMeasuredDimension(getControl().getScaleWidth().intValue(), getControl().getScaleHeight().intValue());
        } else
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }



    @Override
    public void resizeView(int resizeType) {
        if (resizeType == ConstantsUtils.RESIZE_TYPE_INCREASE) {
            this.setScaleX((this.getScaleX() + 0.05f));
            this.setScaleY((this.getScaleY() + 0.05f));
        } else {
            if (this.getScaleX() > 0.3f) {
                this.setScaleX((this.getScaleX() - 0.05f));
                this.setScaleY((this.getScaleY() - 0.05f));
            } else
                Toast.makeText(getContext(), R.string.size_cant_reduce_further, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public ImageControl getControl() {
        if (mControl == null)
            mControl = new ImageControl();

        return mControl;
    }

    private JsonObject jsonObject;

    @Override
    public void setControl(ImageControl control, JsonObject jsonObject, FocusListener focusListener) {

        this.mControl = control;
        this.jsonObject = jsonObject;

        getControl();

        setX(mControl.getScaleX());
        setY(mControl.getScaleY());

        String data = mControl.getData();

        if (data != null && !data.isEmpty())
            setImageBitmap(ImageUtil.decodeImage(data));
        else {
            if(!TextUtils.isEmpty(getControl().getImagePath())){
                String loaclImagePath = getImagePath();
                if(!TextUtils.isEmpty(loaclImagePath)){
                    Bitmap bitmap = BitmapFactory.decodeFile(loaclImagePath);
                    setImageBitmap(bitmap);
                }
            }
        }

        if (!mControl.isMarkedControl()) {
            setOnLongClickListener(new OnLongClickListener() {
                @SuppressLint("ClickableViewAccessibility")
                @Override
                public boolean onLongClick(View v) {
                    if (mTouchListener == null)
                        mTouchListener = DRAG_TOUCH_LISTENER;
                    IHMFImageView.this.setOnTouchListener(mTouchListener);

                    if (mLongPressListener != null) {
                        mLongPressListener.onLongPressed(IHMFImageView.this);
                        return true;
                    }


                    return false;
                }
            });
        }
    }

    public String getImagePath(){
        File file = new File(getControlImagesFilesDirectory(),new MySharePreference(getContext()).getFillingFormId() + "_" + getControl().getImagePath() + ".png");
        if(!file.exists()) return  null;
        return file.getAbsolutePath();
    }

    @SuppressWarnings("all")
    public File getFormDataFileDir(){
        File formDatafile;
        formDatafile = getContext().getApplicationContext().getDir(new MySharePreference(getContext()).getFillingFormId() + "_FORM_DATA", MODE_PRIVATE);
        if(!formDatafile.exists()) formDatafile.mkdirs();
        return formDatafile;
    }

    @SuppressWarnings("all")
    public File getControlImagesFilesDirectory(){

        File imageDatafile = new File(getFormDataFileDir(), "CONTROL_IMAGES");
        if(!imageDatafile.exists()) imageDatafile.mkdirs();

        return imageDatafile;
    }

    public void removeOnTouchListener() {
        setOnTouchListener(null);
        mTouchListener = null;
    }

    @Override
    public View getView() {
        return this;
    }


    @Override
    public boolean save() {
        Drawable d = getDrawable();
        if (getPreference().shouldCheckMandatoryFields() && getControl().isMandatory() && d == null) {
            post(new Runnable() {
                @Override
                public void run() {
                    IHMFImageView.this.setErrorDrawable();
                    IHMFImageView.this.getControl().setData(null);
                }
            });

            return false;
        }
        if (d == null) return true;

        BitmapDrawable bd = (BitmapDrawable) d;

        String imagebase64 = ImageUtil.encodeImage(bd.getBitmap(), 70);
        getControl().setData(imagebase64);

        if (jsonObject != null)
            jsonObject.addProperty("imageData", imagebase64);

        return true;

    }


    public void setOnGroupListener(iHMFGroupListener<Bitmap> mListener) {
        this.mGroupListener = mListener;
    }

//    public GroupControls getGroup() {
//        if (mGroup == null)
//            mGroup = mGroupListener == null ? null : mGroupListener.getGroupControl(getControl().getGroupId());
//
//        return mGroup;
//    }


    @Override
    public boolean isValid() {
        return getDrawable() != null;
    }

    @Override
    public MySharePreference getPreference() {
        if(mSharePreference == null){
            mSharePreference = new MySharePreference(getContext());
        }
        return mSharePreference;
    }


    public void setErrorDrawable() {

        if (mErrorDrawable == null)
            mErrorDrawable = getResources().getDrawable(R.drawable.image_background_error);

        setBackground(mErrorDrawable);
    }

    public void setDefaultDrawable() {

        if (mDefaultDrawable == null)
            mDefaultDrawable = getResources().getDrawable(R.drawable.image_background_default);

        setBackground(mDefaultDrawable);
    }


    public void setImage(String imagePath) {

        if (TextUtils.isEmpty(imagePath)) return;

        File f = new File(imagePath);

        if (!f.exists() || !f.isFile())
            return;

        InputStream is = null;
        try {
            is = new FileInputStream(f);
            setImageDrawable(new BitmapDrawable(getResources(), is));

            is.close();
        } catch (IOException e) {
            try {
                if (is != null)
                    is.close();
            } catch (IOException ignore) {}
        }


    }

    public void setImage(Uri uri) {
        setImageURI(uri);
    }


//    public void setImage(String path, Bitmap bitmap) {
//
//        if (!TextUtils.isEmpty(path)) {
//            int rotation = ImageUtil.getCameraPhotoOrientation(null, path);
//
//            if (rotation != 0)
//                bitmap = ImageUtil.fixOrientation(bitmap, rotation);
//        }
//        setImageBitmap(bitmap);
//    }


    @Override
    public void setImageBitmap(Bitmap bm) {
        super.setImageBitmap(bm);
        if (bm != null)
            setDefaultDrawable();

        if (getControl().hasGroup() && mGroupListener != null)
            mGroupListener.handleGroup(getControl().getId(), getControl().getGroupId(), bm);

    }

}
