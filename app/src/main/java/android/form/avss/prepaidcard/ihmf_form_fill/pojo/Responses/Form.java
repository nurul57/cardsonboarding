package android.form.avss.prepaidcard.ihmf_form_fill.pojo.Responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Form {

    public static final int TRUE = 1;
    public static final int FALSE = 0;


    @SerializedName("isImagesUpdated")
    private transient Boolean isImagesUpdated = false;

    @SerializedName("publisherFormId")
    private transient String pdfID;

    @SerializedName("fillingId")
    private String fillingID;


    @SerializedName("title")
    private String title;

    @SerializedName("urlCode")
    private transient String urlCode;


    @SerializedName("isStampBarQRCode")
    private transient Integer isStampBarQRCode;

    @SerializedName("autoFormNo")
    private transient Integer autoFormNo;

    @SerializedName("disabledToolList")
    private transient String disabledToolList;

    @SerializedName("pdfUrl")
    private transient String pdfUrl;

    @SerializedName("thumImageUrls")
    private List<String> thumbnailsURL;

    @SerializedName("pngImageUrls")
    private List<String> pngImageUrls;


    @Expose
    @SerializedName("pageList")
    private List<Page> pages;


    public Boolean getIsImagesUpdated() {
        return isImagesUpdated;
    }

    private HashMap<String, String> mappingValue;

    public String getPdfID() {
        return pdfID;
    }

    public void setPdfID(String pdfID) {
        this.pdfID = pdfID;
    }

    public String getFillingID() {
        return fillingID;
    }

    public void setFillingID(String fillingID) {
        this.fillingID = fillingID;
    }

    public Integer getIsStampBarQRCode() {
        return isStampBarQRCode;
    }

    public void setIsStampBarQRCode(Integer isStampBarQRCode) {
        this.isStampBarQRCode = isStampBarQRCode;
    }

    public Integer getAutoFormNo() {
        return autoFormNo;
    }

    public void setAutoFormNo(Integer autoFormNo) {
        this.autoFormNo = autoFormNo;
    }

    public String getDisabledToolList() {
        return disabledToolList;
    }

    public void setDisabledToolList(String disabledToolList) {
        this.disabledToolList = disabledToolList;
    }

    public String getPdfUrl() {
        return pdfUrl;
    }

    public void setPdfUrl(String pdfUrl) {
        this.pdfUrl = pdfUrl;
    }

    public List<String> getThumbnailsURL() {
        return thumbnailsURL;
    }

    public void setThumbnailsURL(List<String> thumbnailsURL) {
        this.thumbnailsURL = thumbnailsURL;
    }

    public List<String> getPngImageUrls() {
        return pngImageUrls;
    }

    public void setPngImageUrls(List<String> pngImageUrls) {
        this.pngImageUrls = pngImageUrls;
    }

    public List<Page> getPages() {
        return pages;
    }


    public String getTitle() {
        return title;
    }

    public String getUrlCode() {
        return urlCode;
    }


    public void clear() {
        if (pages != null)
            pages.clear();
    }


    public void add(Page page) {
        if (pages == null)
            pages = new ArrayList<>();

        pages.add(page);
    }

    public void setPages(List<Page> pages) {
        this.pages = pages;
    }

    public boolean hasPages() {
        return pages != null && !pages.isEmpty();
    }
//    public boolean hasThumbnail(){return thumbnailsURL != null && !thumbnailsURL.isEmpty();}



    public boolean containsMapper(String key) {
        return mappingValue != null && mappingValue.containsKey(key);
    }

    public String getMappingValue(String key, String defaultValue) {
        return (mappingValue == null || !containsMapper(key)) ? defaultValue : mappingValue.get(key);

    }

    public String getMappingValue(String key) {
        return getMappingValue(key, "");
    }



    public void setMappingValue(HashMap<String,String> value){
        this.mappingValue =value;
    }

}
