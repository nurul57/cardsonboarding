package android.form.avss.prepaidcard.ui.adapters;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

public class ImagesName {

    public static final String AADHAAR_IMAGE = "/aadhaar.jpg";


    public static final String APPLICANT_PHOTO = "Applicant_Photo";
    public static final String ID_PROOF = "Id_Proof";
    public static final String ADDRESS_PROOF = "Address_Proof";
    public static final String PAN_PROOF = "Pan_Proof";
    private static List<String> listOfImages = new ArrayList<>();

    static {
        listOfImages.add(ImagesName.APPLICANT_PHOTO);
        listOfImages.add(ImagesName.ID_PROOF);
        listOfImages.add(ImagesName.ADDRESS_PROOF);
        listOfImages.add(ImagesName.PAN_PROOF);
    }

    public static int imageSize() {
        return listOfImages.isEmpty() ? 0 : listOfImages.size();
    }

    public static List<String> getListOfImages() {
        return listOfImages;
    }

    public static void removeImage(int index) {
        if (listOfImages.isEmpty()) return;
        listOfImages.remove(index);
    }

    public static boolean removeImageName(String imageName) {
        if (listOfImages.isEmpty()) return false;
        int index = listOfImages.indexOf(imageName);
        if (index == -1) { return false; }
        listOfImages.remove(index);
        return true;
    }

    public static void removeImage(String imageName) {
        if (listOfImages.isEmpty()) return;
        int index = listOfImages.indexOf(imageName);
        if (index == -1) { return; }
        listOfImages.remove(index);
    }

    public static void addImageName(String imageName) {
        if (!listOfImages.contains(imageName)) {
            listOfImages.add(imageName);
        }
    }

}
