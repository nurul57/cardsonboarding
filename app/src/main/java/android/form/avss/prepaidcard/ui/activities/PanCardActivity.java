package android.form.avss.prepaidcard.ui.activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.form.avss.prepaidcard.R;
import android.form.avss.prepaidcard.ihmf_form_fill.TempPackage.MainActivity;
import android.form.avss.prepaidcard.ihmf_form_fill.util.MySharePreference;
import android.form.avss.prepaidcard.ui.custom_classes.CustomButton;
import android.form.avss.prepaidcard.ui.custom_classes.CustomTextInputLayout;
import android.form.avss.prepaidcard.helper_classes.Constant;
import android.form.avss.prepaidcard.ui.model.CardDetailViewModel;
import android.form.avss.prepaidcard.ui.model.EkycOptionViewModel;
import android.form.avss.prepaidcard.ui.model.FinalDataViewModel;
import android.form.avss.prepaidcard.ui.model.PanDataViewModel;
import android.form.avss.prepaidcard.utils.UtilityClass;
import android.form.avss.prepaidcard.web_services.model.Services;
import android.form.avss.prepaidcard.web_services.requests.FinalDataSubmitRequest;
import android.form.avss.prepaidcard.web_services.requests.PanVerificationRequest;
import android.form.avss.prepaidcard.web_services.responses.FinalDataSubmitResponse;
import android.form.avss.prepaidcard.web_services.responses.PanVerificationResponse;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.Calendar;


public class PanCardActivity extends BaseActivity implements RadioGroup.OnCheckedChangeListener, View.OnClickListener, Services.ValidatePan, Services.FinalDataSubmit {
	private RelativeLayout rlPanAvailable, rlNoPanAvailable, rlIncomeMoreThan5Lac;
	private RadioGroup rgPanOption;
	private CustomTextInputLayout tvPanNo, tvAgricultureIncome, tvNonAgricultureIncome, tvApplicationDate, tvAcknowledgementNumber;
	private CustomButton btnValidate;
	private ImageView btnNext;
	private ImageView btnPrevious;
	private double income5Lakh = 500000.00D;
	private boolean isPanVerified = false;
	private PanDataViewModel model;
	private FinalDataViewModel finalDataModel;
	private EkycOptionViewModel ekycModel;
	private String token;
	private String bdeno;
	private ProgressDialog progressDialog;
	private String ekycOrNonEKycData;
	private Constant constant;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pancard);
		model = new PanDataViewModel();

		if (getIntent() == null) {
			toast("Something went wrong");
			finish();
			return;
		}

		token = getIntent().getStringExtra(TOKEN);
		bdeno = getIntent().getStringExtra(BDE_NO);
		ekycOrNonEKycData = getIntent().getStringExtra(EKYC_OPTION_DATA);
		ekycModel = Constant.getInstance().fromJson(ekycOrNonEKycData, EkycOptionViewModel.class);

		finalDataModel = new FinalDataViewModel();
		model = new PanDataViewModel();
		constant = Constant.getInstance();

		initViews();
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		int id = group.getCheckedRadioButtonId();
		rlPanAvailable.setVisibility(id == R.id.rbYes ? View.VISIBLE : View.GONE);
		rlNoPanAvailable.setVisibility(id == R.id.rbNo ? View.VISIBLE : View.GONE);
	}

	@Override
	public void onClick(View v) {
		switch ( v.getId() ) {
			case R.id.btn_pan_validate:
				if (!tvPanNo.matchPanPattern()) {
					toast(R.string.msg_pan);
					return;
				}

				PanVerificationRequest _panRequest = new PanVerificationRequest();
				_panRequest.setPanCardNo(tvPanNo.getString());
				_panRequest.setName(UtilityClass.getName(ekycModel.getApplicantFirstName(),
						ekycModel.getApplicantMiddleName(), ekycModel.getApplicantLastName()));
				_panRequest.setBdeNo(bdeno);

				progressDialog = new ProgressDialog(PanCardActivity.this);
				progressDialog.setTitle("Service Call in Progress");
				progressDialog.setMessage("Logging in...");
				progressDialog.show();
				new Services().validatePan(_panRequest, PanCardActivity.this, token);
				break;

			case R.id.et_applicationDate:
				Calendar c = Calendar.getInstance();
				DatePickerDialog dd = new DatePickerDialog(PanCardActivity.this, new DatePickerDialog.OnDateSetListener() {
					@Override
					public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
						String dateOfString = dayOfMonth + "-" + (month + 1) + "-" + year;
						tvApplicationDate.setText(dateOfString);
					}
				}, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE));
				dd.getDatePicker().setMaxDate(System.currentTimeMillis());
				dd.show();
				break;


			case R.id.btn_next:
				if (validateMandatoryFields()) {
					setData();
					String finalPanData = null;
					finalPanData = Constant.getInstance().toJson(model);

					if ( isEmpty(finalPanData) ) {
						toast("Something went wrong");
						return;
					}

					Intent intent = getIntent();
					try {
						finalDataModel.setEkycOptionViewModel(constant.fromJson(intent.getStringExtra(EKYC_OPTION_DATA), EkycOptionViewModel.class));
						finalDataModel.setPanDataViewModel(constant.fromJson(intent.getStringExtra(PAN_DATA), PanDataViewModel.class));
						finalDataModel.setCardDetailViewModel(constant.fromJson(intent.getStringExtra(CARD_DATA), CardDetailViewModel.class));
						token = intent.getStringExtra(TOKEN);
						callFinalDataSubmitService(constant.toJson(finalDataModel));

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				break;

			case R.id.btn_previous:
				final AlertDialog alertDialog = new AlertDialog.Builder(PanCardActivity.this)
						.setTitle(R.string.alert)
						.setMessage(R.string.data_loss)
						.setCancelable(true)
						.create();

				alertDialog.setButton(
						DialogInterface.BUTTON_POSITIVE, "CONTINUE",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								alertDialog.dismiss();
								finish();
							}
						});

				alertDialog.setButton(
						DialogInterface.BUTTON_NEGATIVE, "CANCEL",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								alertDialog.dismiss();
							}
						});

				alertDialog.show();
				break;
		}
	}

	private void setData() {
		if (model == null) return;

		model.setPanAvailable(rgPanOption.getCheckedRadioButtonId() == R.id.rbYes);
		if (rgPanOption.getCheckedRadioButtonId() == R.id.rbYes) {

			model.setPanNumber(tvPanNo.getString());
		} else {

			model.setAgriCultureIncome(tvAgricultureIncome.getString());
			model.setNonAgriCultureIncome(tvNonAgricultureIncome.getString());
			model.setIncomeMoreThan5Lac(isIncomeMoreThan5Lac());
			if (rlIncomeMoreThan5Lac.getVisibility() == View.VISIBLE) {

				model.setApplicationDate(tvApplicationDate.getString());
				model.setAcknowledgementNumber(tvAcknowledgementNumber.getString());
			}
		}
	}

	private boolean validateMandatoryFields() {
		boolean temp = true;
		if (rgPanOption.getCheckedRadioButtonId() == R.id.rbYes) {
			if (tvPanNo.isFieldEmpty()) {

				temp = false;
			} else if (!tvPanNo.matchPanPattern()) {

				tvPanNo.setErrorMessage(R.string.msg_pan);
				temp = false;
			}
			if (!isPanVerified) {

				UtilityClass.longToast(PanCardActivity.this, "Your pan is not verified");
			}
			if (!isPanVerified) temp = false;
		} else {

			if (tvAgricultureIncome.isFieldEmpty()) temp = false;
			if (tvNonAgricultureIncome.isFieldEmpty()) temp = false;

			if (isIncomeMoreThan5Lac()) {

				if (tvApplicationDate.isFieldEmpty()) temp = false;
				if (tvAcknowledgementNumber.isFieldEmpty()) temp = false;
				if (tvAcknowledgementNumber.getLength() < 15) {
					temp = false;
					tvAcknowledgementNumber.setErrorMessage(R.string.err_ac_no);
				}
			}
		}
		return temp;
	}

	@Override
	public void panSuccessful(PanVerificationResponse.PanVerification data) {
		progressDialog.dismiss();
        if (data.isSuccess()) {
            isPanVerified = data.isSuccess();
            rlNoPanAvailable.setVisibility(isPanVerified ? View.GONE : View.VISIBLE);
            toast("Pan Validation Successful");
        } else {
            rlNoPanAvailable.setVisibility(isPanVerified ? View.VISIBLE : View.GONE);
            toast(data.msg());
        }
		token = data.token();
	}

	@Override
	public void panFailed(boolean status, String error) {

	}

	private boolean isIncomeMoreThan5Lac() {
		if (tvAgricultureIncome.isEmpty() || tvNonAgricultureIncome.isEmpty()) return false;
		double totalIncome = Double.parseDouble(tvAgricultureIncome.getString()) +
				Double.parseDouble(tvNonAgricultureIncome.getString());
		return totalIncome > income5Lakh;
	}

	// showing alert dialog
	public void showAlert(){
		AlertDialog.Builder builder = new AlertDialog.Builder(PanCardActivity.this);
		builder.setTitle("Alert!");
		builder.setMessage("Upload PAN Card Image");
		builder.setPositiveButton("Upload", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Toast.makeText(getApplicationContext(), "Will Implement...", Toast.LENGTH_SHORT).show();
			}
		});

		AlertDialog alertDialog = builder.create();
		alertDialog.show();
	}

	private void initViews() {
		rgPanOption = (RadioGroup) findViewById(R.id.rgPanOption);

		tvPanNo = (CustomTextInputLayout) findViewById(R.id.tvPanNo);
		tvAgricultureIncome = (CustomTextInputLayout) findViewById(R.id.tv_agriculture_income);
		tvNonAgricultureIncome = (CustomTextInputLayout) findViewById(R.id.tv_nonAgricultureIncome);
		tvApplicationDate = (CustomTextInputLayout) findViewById(R.id.tv_application_date);
		tvAcknowledgementNumber = (CustomTextInputLayout) findViewById(R.id.tv_acknowledgement_no);

		btnValidate = (CustomButton) findViewById(R.id.btn_pan_validate);
		btnNext = (ImageView) findViewById(R.id.btn_next);
		btnPrevious = (ImageView) findViewById(R.id.btn_previous);

		rlPanAvailable = (RelativeLayout) findViewById(R.id.rlPanAvailable);
		rlNoPanAvailable = (RelativeLayout) findViewById(R.id.rlNoPanAvailable);
		rlIncomeMoreThan5Lac = (RelativeLayout) findViewById(R.id.rlMoreThan5Lac);

		tvPanNo.setButtonVisibility(btnValidate, 10);

		setVisibleLayout(tvAgricultureIncome, tvNonAgricultureIncome);
		setVisibleLayout(tvNonAgricultureIncome, tvAgricultureIncome);

		setInputFiltersOnFields();
		setListeners();
		setFocusChangeListeners();

	}


	private void callFinalDataSubmitService(String finalRequest) {
		if (isEmpty(finalRequest)){
			toast("final data is empty");
			return;
		}

		FinalDataSubmitRequest submitRequest = new FinalDataSubmitRequest();
		submitRequest.setBdeNo(getIntent().getStringExtra(BDE_NO));
		submitRequest.setFinalSubmit(finalRequest);

		new Services().finalSubmitData(submitRequest, PanCardActivity.this, token);
	}

	@Override
	public void finalDataSubmitSuccessful(FinalDataSubmitResponse.FinalSubmit data) {
		if (data.isSuccess()) {
			toast("data is successfully submitted");

			//todo : bypass
			Intent intent = new Intent(PanCardActivity.this, MainActivity.class);

			MySharePreference preference = new MySharePreference(PanCardActivity.this);
			preference.setBdeNumber(getIntent().getStringExtra(BDE_NO));
			preference.setFormId("1113");
			preference.setReferenceID(data.getReferenceId());
			preference.setToken(data.getToken());

//			intent.putExtra(BDE_NO, getIntent().getStringExtra(BDE_NO));
//			intent.putExtra(FORM_ID, "1113");
//			intent.putExtra(TOKEN, data.getToken());
//			intent.putExtra(REFERENCE_ID, data.getReferenceId());

			startActivity(intent);
			finish();
		}
	}

	@Override
	public void finalDataSubmitFailed(boolean status, String error) {
		toast(error);
	}

	private void setVisibleLayout(CustomTextInputLayout source, final CustomTextInputLayout destination) {
		if (source == null || destination == null || rlIncomeMoreThan5Lac == null) return;
		source.getCustomEditText().addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (s.length() > 0 && destination.getLength() > 0) {
					if (isEmpty(s.toString()) || destination.isEmpty()) return;

					double sum = Double.parseDouble(s.toString()) + Double.parseDouble(destination.getString());
					rlIncomeMoreThan5Lac.setVisibility(sum >= income5Lakh ? View.VISIBLE : View.GONE);
				}
			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		});
	}

	private void setInputFiltersOnFields() {
		tvPanNo.setAlphaNumericInputFilter(10);
	}

	private void setFocusChangeListeners() {
		tvPanNo.setFocusChangeListenerPan();
		tvAgricultureIncome.setFocusChangeListener();
		tvNonAgricultureIncome.setFocusChangeListener();
		tvApplicationDate.setFocusChangeListener();
		tvAcknowledgementNumber.setFocusChangeListener();
	}

	private void setListeners() {
		rgPanOption.setOnCheckedChangeListener(this);
		tvApplicationDate.getCustomEditText().setOnClickListener(this);
		btnValidate.setOnClickListener(this);
	}

}
