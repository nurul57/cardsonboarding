package android.form.avss.prepaidcard.ihmf_form_fill.interfaces;

import android.widget.EditText;

/**
 * Created by Neom on 14/07/17.
 */

/**
 * This interface is callback for validation library which perform specific task as per validation.
 * This is created by Sahil/Neom.
 */
public interface MyEventListener {
     void myEvent(boolean valid, EditText et);
}
