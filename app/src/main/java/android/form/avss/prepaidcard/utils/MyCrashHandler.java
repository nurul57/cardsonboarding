package android.form.avss.prepaidcard.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PermissionInfo;
import android.form.avss.prepaidcard.BuildConfig;
import android.form.avss.prepaidcard.R;
import android.form.avss.prepaidcard.database.MyDBHelper;
import android.form.avss.prepaidcard.database.pojo.ServiceSession;
import android.form.avss.prepaidcard.ui.activities.LoginActivity;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;


public class MyCrashHandler implements Thread.UncaughtExceptionHandler {

    private static final String FILE_NAME = "crash_log.txt";
    private Context ctx;
    private static final String CRASH_FLAG = "CRASH-f";

    public MyCrashHandler(@NonNull Context ctx) {
        this.ctx = ctx.getApplicationContext();
    }

    private static File getFile(Context ctx) {

        return ctx == null ? null : new File(ctx.getFilesDir(), FILE_NAME);
    }

    @Override
    public void uncaughtException(Thread t, Throwable e) {

        BufferedOutputStream out = null;
        FileOutputStream fos = null;

//        long start = System.currentTimeMillis();
        try {

//            fos = ctx.openFileOutput(FILE_NAME, Context.MODE_APPEND);

            File file = getFile(ctx);

            if (file == null) return;

            String old = getLog(ctx);

            fos = new FileOutputStream(file.getAbsolutePath());

            out = new BufferedOutputStream(fos);


            if (!TextUtils.isEmpty(old))
                out.write(old.getBytes());

            String imei = getImei(ctx);
            String ver = ctx.getString(R.string.version_no) ;

            String permissionDetail = permissionDetail(ctx);


            String bde = null, formID = null;
            MyDBHelper db = MyDBHelper.getInstance(ctx);

            ServiceSession session = db.session();

            bde = session.getId();
            formID = session.getFormId();


            Date date = new Date();
            String header = "\n==================== HEADER <" + date.toString() + ">=================\n";
            String footer = "==================== FOOTER <" + date.toString() + ">=================\n";


            String msg = header +
                    "\n========= BASIC INFO ===============\n" +
                    "\n Imei : " + imei + "\n" +
                    "\n BD Number : " + bde +
                    "\n FORM ID : " + formID +
                    "\n================APP LEVEL ===============\n" +
                    "Message\n" + e.getMessage()
                    + "Cause\n" +
                    e.toString() + "\n";


            StringBuffer buf = new StringBuffer(msg);


            for (StackTraceElement ste : e.getStackTrace())
                buf.append("\t").append(ste.toString()).append("=LINE=").append(ste.getLineNumber()).append("=LINE=").append("\n");


            buf.append("\n").append(footer);

            buf.append(deviceDetail(ver));
            buf.append(permissionDetail);
            out.write(buf.toString().getBytes());

            out.close();

        } catch (Exception e1) {

            if (out != null) {
                try {
                    out.close();
                } catch (Exception e2) {
//                    e2.printStackTrace();
                }
            }
        }

//        long end = System.currentTimeMillis();

//        Log.d("Duration",String.valueOf(end- start));

        Intent i = new Intent(ctx, LoginActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.putExtra(CRASH_FLAG, true);
        ctx.startActivity(i);

        ctx = null;

        System.exit(2);

    }


    private String deviceDetail(String ver) {
        StringBuilder builder = new StringBuilder();

        builder.append("\n== Detail ==\n");
        builder.append("Flavor Type : ").append(BuildConfig.FLAVOR).append("\n"); // added on 03-08-2018. This is used only in Salary as flavor exist here.
        builder.append("App ver : ").append(ver).append("\n");
        builder.append("Brand :").append(Build.BRAND).append("\n");
        builder.append("Model : ").append(Build.DEVICE).append("\n");
        builder.append("manuf: ").append(Build.MANUFACTURER).append("\n");
        builder.append("OS VERSION : ").append(Build.VERSION.RELEASE).append("\n");
        builder.append("SDK INT : ").append(Build.VERSION.SDK_INT).append("\n");
        builder.append("CODE NAME : ").append(Build.VERSION.CODENAME);
        builder.append("\n== Detail ==\n");

        return builder.toString();
    }


    private String permissionDetail(Context ctx) {


        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) return "\n Lower than M \n";

        if (ctx == null) return "Context is null";

        StringBuilder builder = new StringBuilder();
        PackageManager pm = ctx.getPackageManager();

        builder.append("\n== Permission Detail ===").append("\n");


        try {
            PackageInfo packageInfo = pm.getPackageInfo(ctx.getPackageName(), PackageManager.GET_PERMISSIONS);

            String[] pinfo = packageInfo.requestedPermissions;

            if (pinfo == null) return "No Permission Info ";

            for (String name : pinfo) {
                PermissionInfo info = pm.getPermissionInfo(name, PackageManager.GET_META_DATA);

                if (info == null) continue;

                if (info.protectionLevel == PermissionInfo.PROTECTION_DANGEROUS)
                    builder.append(name.substring(name.lastIndexOf('.'))).append(" : ").append(ctx.checkSelfPermission(name) == PackageManager.PERMISSION_GRANTED).append("\n");

            }

        } catch (Exception e) {

            return "Permission ==>\n" + e.toString(); // changed on 21-08-2018
        }

        builder.append("== Permission Detail ===").append("\n");

        return builder.toString();
    }


    private boolean hasPermission(Context ctx, String permission) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            int result = ContextCompat.checkSelfPermission(ctx, permission);

            return result == PackageManager.PERMISSION_GRANTED;
        }

        return true;

    }

    private String getImei(Context ctx) {
        if (ctx == null) return null;

        if (hasPermission(ctx, Manifest.permission.READ_PHONE_STATE)) {
            TelephonyManager tm = (TelephonyManager) ctx.getSystemService(Context.TELEPHONY_SERVICE);

            try {
                return tm == null ? null : tm.getDeviceId();
            } catch (Exception pe) {
                return null;
            }
        }

        return null;
    }

    public static String getLog(Context ctx) {

        if (ctx == null) return null;

        File crashFile = getFile(ctx);

        String crashInfo = null;

        if (crashFile.exists() && crashFile.isFile()) {
            BufferedReader br = null;

            try {
                br = new BufferedReader(new InputStreamReader(new FileInputStream(crashFile.getAbsolutePath())));

                StringBuilder crashBuilder = new StringBuilder();
                while ((crashInfo = br.readLine()) != null)
                    crashBuilder.append(crashInfo).append("\n");

                crashInfo = crashBuilder.toString();

                br.close();
            } catch (Exception e) {

                if (br != null)
                    try {
                        br.close();
                    } catch (Exception ce) {
                    }
                crashInfo = null;
            }
        }


        return crashInfo;
    }

    public static void deleteReport(Context ctx) {

        if (ctx == null) return;

        File crashLog = getFile(ctx);

        if (crashLog.exists() && crashLog.isFile())
            crashLog.delete();
    }


    public static AlertDialog onCrash(@NonNull Activity activity) {
        return onCrash(activity, null, null);
    }


    public static AlertDialog onCrash(@NonNull Activity activity, String title, String msg) {

        if (activity == null || activity.getIntent() == null || !activity.getIntent().hasExtra(MyCrashHandler.CRASH_FLAG))
            return null;


        if (TextUtils.isEmpty(title))
            title = "Info";

        if (TextUtils.isEmpty(msg))
            msg = "Some issue occurred while using application. Issue will be monitored and reported.....";

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(false);
        builder.setTitle(title).
                setMessage(msg)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });


        return builder.create();

    }

    public static void add(Exception e, Context ctx) {

        if (ctx == null) return;

        StringBuffer buf = new StringBuffer();


        buf.append("Message\n").append(e.getMessage())
                .append("Cause\n")
                .append(e.toString()).append("\n");

        buf.append("\n\n == ERROR ").append( new Date()).append("===\n\n");
        for (StackTraceElement ste : e.getStackTrace())
            buf.append("\t").append(ste.toString()).append("=LINE=").append(ste.getLineNumber()).append("=LINE=").append("\n");

        buf.append("\n\n == ERROR ===\n\n");


        FileWriter writer = null;

        try {
            writer = new FileWriter(getFile(ctx), true);

            writer.write(buf.toString());

            writer.close();
        } catch (Exception e1) {

            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e2) {
//                    e2.printStackTrace();
                }
            }
        }


    }
}
