package android.form.avss.prepaidcard.ihmf_form_fill.pojo.controls;


import com.google.gson.annotations.SerializedName;

public class ImageControl extends BaseControl {

    private static final int DEFAULT_ROTATION = 0;

    @SerializedName("defaultImage")
    private transient String placeholder;

    @SerializedName("imageData")
    private String imageData;
    @SerializedName("rotation")
    private Number rotation = DEFAULT_ROTATION;

    @SerializedName("accessLock")
    private transient String accessLock;

    @SerializedName("masking")
    private transient String masking;

    @SerializedName("imgpath")
    private String imgpath;

    @SerializedName("controlIdsInSingature")
    private transient String controlIdsInSingature;

    @SerializedName("signName")
    private transient String signName;

    @SerializedName("photoUploadType")
    private transient String photoUploadType;

    @SerializedName("imageExtension")
    private transient String imageExtension;


    public String getPlaceholder() {
        return placeholder;
    }

    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    public String getImageData() {
        return imageData;
    }

    private void setImageData(String imageData) {
        this.imageData = imageData;
    }

    public Float getRotation() {
        return rotation.floatValue();
    }

    public void setRotation(Float rotation) {
        this.rotation = rotation;
    }

    public String getImagePath() {
        return imgpath;
    }

    public void setImgpath(String imgpath) {
        this.imgpath = imgpath;
    }

    /**
     * Data send in field imageData not actual text
     *
     * @param data - Store image bitmap
     */
    @Override
    public void setData(String data) {
        setImageData(data);
    }

    @Override
    public String getActualText() {
        return getImageData();
    }


    @Override
    public String getData() {
        return getImageData();
    }




}
