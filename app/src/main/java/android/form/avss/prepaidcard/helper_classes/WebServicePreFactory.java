package android.form.avss.prepaidcard.helper_classes;

import android.content.Context;
import android.form.avss.prepaidcard.R;
import android.form.avss.prepaidcard.utils.UtilityClass;
import android.text.TextUtils;

import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public final class WebServicePreFactory {
    private IvParameterSpec ivspec;
    private SecretKeySpec keyspec;
    private Cipher cipher;
    //    private Context ctx;
    private byte[] randByte;

    // used to validateUser whether instance is properly initialized or not.
    private boolean isValid = true;

    public WebServicePreFactory(byte[] randBytes, Context ctx) {
        ivspec = new IvParameterSpec(randBytes);

        keyspec = new SecretKeySpec(getK(ctx).getBytes(), ctx.getString(R.string.algo));

        try {
            cipher = Cipher.getInstance(ctx.getString(R.string.algo));
        } catch (Exception e) {
            isValid = false;
        }
    }

    public byte[] enc(String text) throws Exception {
        if (TextUtils.isEmpty(text))
            throw new Exception("Empty string");

        byte[] enc = null;

        try {
            cipher.init(Cipher.ENCRYPT_MODE, keyspec, ivspec);

            enc = cipher.doFinal(padString(text).getBytes(StandardCharsets.UTF_8));
        } catch (Exception e) {
            throw new Exception("[encrypt] failed");
        }
        return enc;
    }


    /**
     * This method is used internally to generate key to encrypt/decrypt to communicate with server back and forth.
     *
     * @param ctx - Context is used to access key
     * @return null if {@param ctx} is null , otherwise, string of key.
     */
    private String getK(Context ctx) {

        if (ctx == null) return null;


        int[] key = ctx.getResources().getIntArray(R.array.key);

        StringBuilder keyBuilder = new StringBuilder();

        for (int _x : key)
            keyBuilder.append(_x % 9);

        return keyBuilder.toString();
    }

    public byte[] dec(String code) throws Exception {
        if (code == null || code.length() == 0)
            throw new Exception("Empty string");

        byte[] dec = null;

        try {
            cipher.init(Cipher.DECRYPT_MODE, keyspec, ivspec);

            byte[] _decrypted = cipher.doFinal(hexToBytes(code));
            dec = UtilityClass.removeTrailingNulls(_decrypted);
        } catch (Exception e) {
            throw new Exception("[decrypt] failed");
        }
        return dec;
    }
    public String decToString(String code) throws Exception {
        if (code == null || code.length() == 0)
            throw new Exception("Empty string");

        byte[] dec = null;

        try {
            cipher.init(Cipher.DECRYPT_MODE, keyspec, ivspec);

            byte[] _decrypted = cipher.doFinal(hexToBytes(code));
            dec = UtilityClass.removeTrailingNulls(_decrypted);
        } catch (Exception e) {
            throw new Exception("[decrypt] failed");
        }
        return dec == null ? null : new String(dec);
    }


    /**
     * This method is used to perform two operation which oftenly used in application.
     * 1) Encrypt input string  2) convert ecrypt string to hex string.
     *
     * @param input - on which we perform encryption and convert to hex form.
     * @return - null if request is empty or output which hex form string.
     * @throws Exception - if some error occur in encryption process.
     */
    public String encryptToHexString(String input) throws Exception {
        return TextUtils.isEmpty(input) ? null : bytesToHex(enc(input));
    }

    /**
     * This method is used to convert byte array to hex string
     *
     * @param data - array on which we perfrom hex conversion.
     * @return - null if {@param data} is null, otherwise, hex form string .
     */
    public String bytesToHex(byte[] data) {
        if (data == null)
            return null;

//        int len = data.length;
        StringBuffer sb = new StringBuffer();

        for (int val : data) {
            if ((val & 0xFF) < 16)
                sb.append("0").append(Integer.toHexString(val & 0xFF));
            else
                sb.append(Integer.toHexString(val & 0xFF));
        }

        return sb.toString();
    }

    public byte[] hexToBytes(String str) {
        if (str == null || str.length() < 2) {
            return null;
        }
//        else if (str.length() < 2) {
//            return null;
//        }
        else {
            int len = str.length() / 2;
            byte[] buffer = new byte[len];
            for (int i = 0; i < len; i++) {
                buffer[i] = (byte) Integer.parseInt(str.substring(i * 2, i * 2 + 2), 16);
            }
            return buffer;
        }
    }

    private String padString(String source) {
        char paddingChar = ' ';
        int size = 16;
        int x = source.length() % size;
        int padLength = size - x;


        StringBuilder builder = new StringBuilder(source);


        for (int i = 0; i < padLength; i++)
            builder.append(paddingChar);


        return builder.toString();
    }

    public byte[] randomKey() {
        return randByte;
    }
}
