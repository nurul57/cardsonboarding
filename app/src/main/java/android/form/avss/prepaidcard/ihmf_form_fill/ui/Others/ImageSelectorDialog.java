package android.form.avss.prepaidcard.ihmf_form_fill.ui.Others;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.form.avss.prepaidcard.R;
import android.form.avss.prepaidcard.ihmf_form_fill.ui.Activities.CustomCameraActivity;
import android.form.avss.prepaidcard.ihmf_form_fill.ui.Activities.MySignaturePadActivity;
import android.form.avss.prepaidcard.ihmf_form_fill.ui.ControlsView.IHMFImageView;
import android.form.avss.prepaidcard.ihmf_form_fill.util.ImageUtil;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import java.io.File;

import static android.form.avss.prepaidcard.interfaces.Keys.CONTROL_ID;
import static android.form.avss.prepaidcard.interfaces.Keys.CONTROL_TYPE;
import static android.form.avss.prepaidcard.interfaces.Keys.RATIO;

public class ImageSelectorDialog extends RecyclerView.ViewHolder {

    public static final int IMG_CAMERA = 1;
    public static final int SIGNATURE = 2;
    public static final int RESULT_LOAD = 3;
    private BottomSheetDialog mDialog;
    private ViewGroup camera;
    private ViewGroup gallery;
    private ViewGroup signpad;
    private View rl_image_alter, rotate_left, rotate_right, delete;



    public ImageSelectorDialog(Context ctx) {
        this(ctx, true);
    }

    @SuppressLint("InflateParams")
    private ImageSelectorDialog(Context ctx, boolean isCancelable) {
        super(LayoutInflater.from(ctx).inflate(R.layout.image_option_dialog, null, false));

        mDialog = new BottomSheetDialog(ctx);
        mDialog.setContentView(itemView);
        mDialog.setCancelable(isCancelable);
        mDialog.create();

        camera = (ViewGroup) itemView.findViewById(R.id.img_option_camera);
        gallery = (ViewGroup) itemView.findViewById(R.id.img_option_gallery);
        signpad = (ViewGroup) itemView.findViewById(R.id.img_option_signature);

        rl_image_alter = itemView.findViewById(R.id.rl_image_alter);
        rotate_left = itemView.findViewById(R.id.rotate_left);
        rotate_right = itemView.findViewById(R.id.rotate_right);
        delete = itemView.findViewById(R.id.remove);

    }


    public ViewGroup getCamera() {
        return camera;
    }

    public ViewGroup getGallery() {
        return gallery;
    }

    public ViewGroup getSignpad() {
        return signpad;
    }

    public BottomSheetDialog getDialog() {
        return mDialog;
    }


    public void openCameraActivity(Fragment fragment,String controlId ,String type, float ratio) {
        Intent intent = new Intent(fragment.getActivity(), CustomCameraActivity.class);
        intent.putExtra(CONTROL_ID, controlId);
        intent.putExtra(CONTROL_TYPE, type);
        intent.putExtra(RATIO, ratio);
        fragment.startActivityForResult(intent,IMG_CAMERA);
    }



    public void openSignaturepad(Fragment fragment, @Nullable Bundle bundle, String _imagePath) {
        Intent _intent_sign = new Intent(itemView.getContext(), MySignaturePadActivity.class);

        if (bundle == null)
            bundle = new Bundle();

        bundle.putString(MySignaturePadActivity.PATH, _imagePath);

        _intent_sign.putExtras(bundle);
        fragment.startActivityForResult(_intent_sign, SIGNATURE);
    }

    public void galleryIntent(Activity activity) {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        activity.startActivityForResult(galleryIntent, RESULT_LOAD);
    }


    public void galleryIntent(Fragment fragment) {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        fragment.startActivityForResult(galleryIntent, RESULT_LOAD);
    }

    /**
     * Used to manage visibility of remove button in dialog
     *
     * @param view
     */
    @SuppressWarnings("all")
    public void show(View view) {

        getDialog().show();

        if (view instanceof IHMFImageView) {
            final IHMFImageView img = (IHMFImageView) view;

            rl_image_alter.setVisibility(img.getDrawable() == null ? View.GONE : View.VISIBLE);


            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    img.setImageBitmap(null);
                    img.setImageDrawable(null);
                    img.getControl().setData("");

                    ImageSelectorDialog.this.getDialog().dismiss();

                    File file = new File(img.getImagePath());
                    if(file.exists()) file.delete();

                    v.setOnClickListener(null);

                }
            });

            rotate_left.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Drawable d = img.getDrawable();
                    if (d == null) return;
                    BitmapDrawable bd = (BitmapDrawable) d;
                    img.setImageBitmap(ImageUtil.fixOrientation(bd.getBitmap(), 90));
                    v.setOnClickListener(null);
                }
            });

            rotate_right.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Drawable d = img.getDrawable();

                    if (d == null) return;

                    BitmapDrawable bd = (BitmapDrawable) d;
                    img.setImageBitmap(ImageUtil.fixOrientation(bd.getBitmap(), -90));
                    v.setOnClickListener(null);
                }
            });



        }

    }



}
