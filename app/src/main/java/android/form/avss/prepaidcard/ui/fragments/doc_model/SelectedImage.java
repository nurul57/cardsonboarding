package android.form.avss.prepaidcard.ui.fragments.doc_model;

public class SelectedImage {

    private String imgPath;
    private String imgName;

    public SelectedImage() {
    }

    public SelectedImage(String imgPath, String imgName) {
        this.imgPath = imgPath;
        this.imgName = imgName;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public String getImgName() {
        return imgName;
    }

    public void setImgName(String imgName) {
        this.imgName = imgName;
    }
}
