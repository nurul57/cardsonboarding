package android.form.avss.prepaidcard.ihmf_form_fill.util;

import android.content.Context;

import android.form.avss.prepaidcard.R;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

public class MyAnimationUtil {



    public static Animation getPopUpAnimation(Context context){
        return AnimationUtils.loadAnimation(context, R.anim.pop_out);
    }

    public static Animation getPopInAnimation(Context context){
        return AnimationUtils.loadAnimation(context, R.anim.pop_in);
    }

//    public static Animation getPopUpAnimation(Context context){
//        return AnimationUtils.loadAnimation(context, R.anim.pop_out);
//    }
//
//    public static Animation getPopUpAnimation(Context context){
//        return AnimationUtils.loadAnimation(context, R.anim.pop_out);
//    }
}
