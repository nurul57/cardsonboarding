package android.form.avss.prepaidcard.ihmf_form_fill.ui.Adapters;

import android.form.avss.prepaidcard.R;
import android.form.avss.prepaidcard.ihmf_form_fill.interfaces.PositionClickListener;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class SectionAdapter extends RecyclerView.Adapter<SectionAdapter.MyHolder> {


    private int size;

    private PositionClickListener listener;

    public SectionAdapter(int size, PositionClickListener listener) {
        this.size = size;
        this.listener = listener;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_section, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, final int position) {
        holder.tv_sectionName.setText(String.format("%s %s", "SECTION", String.valueOf(position+1)));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener == null) return;

                listener.onPosClick(position);

            }
        });

        if(position == size){
            holder.tv_sectionName.setText("VIEW FULL FORM");
        }

    }

    @Override
    public int getItemCount() {
        return size+1;
    }

    class MyHolder extends RecyclerView.ViewHolder{

        private TextView tv_sectionName;
        public MyHolder(View itemView) {
            super(itemView);
            tv_sectionName = (TextView) itemView.findViewById(R.id.tv_section);
        }
    }
}
