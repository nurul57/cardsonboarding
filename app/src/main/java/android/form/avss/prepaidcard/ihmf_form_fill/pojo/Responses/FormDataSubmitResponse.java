package android.form.avss.prepaidcard.ihmf_form_fill.pojo.Responses;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FormDataSubmitResponse {

    @SerializedName("prevFormId")
    private String previewFormId;

    public String getPreviewFormId() {
        return previewFormId;
    }
}
