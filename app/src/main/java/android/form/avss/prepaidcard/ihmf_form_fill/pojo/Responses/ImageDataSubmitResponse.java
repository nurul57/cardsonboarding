package android.form.avss.prepaidcard.ihmf_form_fill.pojo.Responses;

import com.google.gson.annotations.SerializedName;

public class ImageDataSubmitResponse {

    @SerializedName("imgName")
    private String imageName;

    public String getImageName() {
        return imageName;
    }
}
