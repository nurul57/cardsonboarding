package android.form.avss.prepaidcard.ihmf_form_fill.pojo.controls;

import com.google.gson.annotations.SerializedName;

public class BooleanControl extends BaseControl {

    @SerializedName("booleanType")
    private transient String booleanType = "";

    @SerializedName("isProcessed")
    private Boolean isProcessed = false;



    @SerializedName("selectedControlToDrawType")
    private transient String selectedControlToDrawType;


    public void setIsProcessed(Boolean isProcessed) {
        this.isProcessed  = isProcessed;
    }

    public boolean isChecked() {
        return isProcessed;
    }


//    @SerializedName("booleanType")
//    private transient String booleanType = "";
//
//    @SerializedName("isProcessed")
//    private boolean isProcessed = false;
//
//
//    @SerializedName("selectedControlToDrawType")
//    private transient String selectedControlToDrawType;
//
//
//    public void setIsProcessed(boolean isProcessed) {
//        this.isProcessed = isProcessed;
//    }
//
//    public boolean isChecked() {
//        return isProcessed;
//    }

}
