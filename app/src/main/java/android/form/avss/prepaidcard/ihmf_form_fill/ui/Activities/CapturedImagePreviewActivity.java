package android.form.avss.prepaidcard.ihmf_form_fill.ui.Activities;

import android.form.avss.prepaidcard.R;
import android.form.avss.prepaidcard.ihmf_form_fill.ui.Fragments.FormBaseFragment;
import android.form.avss.prepaidcard.ihmf_form_fill.util.ImageUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import java.util.Objects;

import static android.form.avss.prepaidcard.ihmf_form_fill.util.ConstantsUtils.IMAGE_PATH;

public class CapturedImagePreviewActivity extends FormBaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String imagePath = getIntent().getStringExtra(IMAGE_PATH);

        if(TextUtils.isEmpty(imagePath)) {

            finish();
            return;
        }

        setContentView(R.layout.activity_catured_image_preview);

        ImageView iv_imagePreview = (ImageView) findViewById(R.id.iv_imagePreview);

        Bitmap bitmap = BitmapFactory.decodeFile(imagePath);


        switch (ImageUtil.getCameraPhotoOrientation(null, imagePath)) {
            case 90:
                bitmap = ImageUtil.fixOrientation(bitmap, 90);
                break;
            case 180:
                bitmap = ImageUtil.fixOrientation(bitmap, 180);
                break;
            case 270:
                bitmap = ImageUtil.fixOrientation(bitmap, 270);
                break;
        }

        assert iv_imagePreview != null;
        iv_imagePreview.setImageBitmap(bitmap);


        Objects.requireNonNull(findViewById(R.id.btn_captureAgain)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });


        Objects.requireNonNull(findViewById(R.id.btn_done)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_OK);
                finish();
            }
        });


    }
}
