package android.form.avss.prepaidcard.ui.custom_classes;

import android.content.Context;
import android.content.res.ColorStateList;
import android.form.avss.prepaidcard.R;
import android.graphics.Color;
import android.util.AttributeSet;
import android.widget.CheckBox;


public class  MyCheckBox extends CheckBox {
    Context context;
    public MyCheckBox(Context context) {
        super(context);
        this.context = context;
        this.setTextSize(14.0f);
    }

    public MyCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.setTextSize(14.0f);
    }

    public MyCheckBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        this.setTextSize(14.0f);
    }

    public void setChecked() {
        setChecked(true);
    }

    public void setUnChecked() {
        setChecked(false);
    }

    public void setBlackStatus() {
        int states[][] = {{android.R.attr.state_checked},{}};
        int colors[] = {Color.BLACK, Color.BLACK};
        setTextColor(Color.BLACK);
        setButtonTintList(new ColorStateList(states, colors));
    }

    public void setRedStatus() {
        int states[][] = {{android.R.attr.state_checked},{}};
        int colors[] = {Color.BLACK, Color.RED};
        setTextColor(Color.RED);
        setButtonTintList(new ColorStateList(states, colors));
    }

    public void setTextColorGrey() {
        this.setTextColor(getResources().getColor(R.color.dark_grey));
    }

    public void setTextColorRed() {
        this.setTextColor(Color.RED);
    }

    public boolean stringEquals(String value) {
        return this.getString().equalsIgnoreCase(value);
    }

    public void setDisabled() {
        setEnabled(false);
    }

    public void setEnabled() {
        setEnabled(true);
    }

    public String getString() {
        return this.getText().toString().trim();
    }

    public Boolean isYes() {
        return getString().equals("Y");
    }

    public Boolean isNo() {
        return getString().equals("N");
    }
}
