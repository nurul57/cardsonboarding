package android.form.avss.prepaidcard.ihmf_form_fill.pojo.Responses;

import com.google.gson.annotations.SerializedName;

public class SectionData {

    private float zoom;

    @SerializedName("id")
    private transient float id;

    @SerializedName("type")
    private transient float type;


    @SerializedName("pageId")
    private transient float pageId;

    @SerializedName("y")
    private float top;

    @SerializedName("h")
    private float sectionHeight;

    @SerializedName("name")
    private transient float name;



    private float bottom;



    public SectionData(float zoom, float top, float bottom) {
        this.zoom = zoom;
        this.top = top;
        this.bottom = bottom;
    }


    public float getZoom() {
        return zoom;
    }

    public void setZoom(float zoom) {
        this.zoom = zoom;
    }


    public float getTop() {
        return top;
    }

    public void setTop(float top) {
        this.top = top;
    }


    public float getBottom() {
        return top + sectionHeight;
    }

    public void setBottom(float bottom) {
        this.bottom = bottom;
    }
}

