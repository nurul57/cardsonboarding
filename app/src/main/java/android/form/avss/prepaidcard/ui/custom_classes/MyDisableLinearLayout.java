package android.form.avss.prepaidcard.ui.custom_classes;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.LinearLayout;

/**
 * Created by Gautam on 2/2/2018.
 */

public class MyDisableLinearLayout extends LinearLayout {
    public boolean check = false;
    public MyDisableLinearLayout(Context context) {
        super(context);
    }

    public MyDisableLinearLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public MyDisableLinearLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * if "check" is true, then layout is disabled
     * else if "check" is false then layout is enabled
     * @param ev
     * @return
     */
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return check;
    }
}
