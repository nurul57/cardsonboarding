package android.form.avss.prepaidcard.web_services.responses;

import android.form.avss.prepaidcard.web_services.pojo.ImageOption;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DocumentDataResponse {

    @SerializedName("da")
    public String data;

    public static class DocImageData extends BaseResponse<ArrayList<ImageOption>> {

    }
}
