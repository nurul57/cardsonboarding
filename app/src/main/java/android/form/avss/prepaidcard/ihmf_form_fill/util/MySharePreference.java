package android.form.avss.prepaidcard.ihmf_form_fill.util;

import android.content.Context;
import android.content.SharedPreferences;

public class MySharePreference {

    private SharedPreferences mSharePreference;
    private SharedPreferences.Editor mEditor;

    private static final String TOKEN = "token_no";
    private static final String REFERENCE_ID = "reference_id";
    private static final String FORM_ID = "form_id";
    private static final String FILLER_TYPE = "filler_type";
    private static final String BDE_NUMBER = "bde_number";
    private static final String FILLING_FORM_ID = "filling_form_id";
    private static final String SHOULD_CHECK_MANDATORY = "should_check_mandatory";

    
    public MySharePreference(Context context){
        mSharePreference = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        mEditor = mSharePreference.edit();
        mEditor.apply();
    }


    public void setToken(String token){
        mEditor.putString(TOKEN, token);
        mEditor.apply();
    }


    public String getToken(){
        return mSharePreference.getString(TOKEN, null);
    }


    public void setReferenceID(String referenceNo){
        mEditor.putString(REFERENCE_ID, referenceNo);
        mEditor.apply();
    }


    public  String getReferenceID(){
        return mSharePreference.getString(REFERENCE_ID, null);
    }


    public void setFormId(String formId){
        mEditor.putString(FORM_ID, formId);
        mEditor.apply();
    }


    public  String getFormId(){
        return mSharePreference.getString(FORM_ID, null);
    }




    public void setType(String type){
        mEditor.putString(FILLER_TYPE, type);
        mEditor.apply();
    }


    public String getType() {
        return mSharePreference.getString(FILLER_TYPE, "1");
    }

    public void setBdeNumber(String bdeNumber){
        mEditor.putString(BDE_NUMBER, bdeNumber);
        mEditor.apply();
    }


    public String getBdeNumber(){
        return mSharePreference.getString(BDE_NUMBER, "");
    }



    public void setFillingFormId(String formId){
        mEditor.putString(FILLING_FORM_ID, formId);
        mEditor.apply();
    }


    public String getFillingFormId(){
        return mSharePreference.getString(FILLING_FORM_ID, "");
    }


    public void setShouldCheckMandatory(boolean b){
        mEditor.putBoolean(SHOULD_CHECK_MANDATORY, b);
        mEditor.apply();
    }


    public boolean shouldCheckMandatoryFields(){
        return mSharePreference.getBoolean(SHOULD_CHECK_MANDATORY, false);
    }

}
