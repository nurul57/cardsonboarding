package android.form.avss.prepaidcard.ihmf_form_fill.pojo.Responses;

import com.google.gson.annotations.SerializedName;

public class BaseResponse<T> {

    @SerializedName("status")
    private Boolean status;

    @SerializedName("json")
    private T data;

    @SerializedName("msg")
    private String message;



    public Boolean isSuccess() {
        return status;
    }

    public T getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }








}
