package android.form.avss.prepaidcard.ihmf_form_fill.interfaces;

public interface DialogButtonClickListener {
    void onClickedOK();
    void onClickedCancel();

}
