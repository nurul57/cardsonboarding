package android.form.avss.prepaidcard.ui.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.form.avss.prepaidcard.R;
import android.form.avss.prepaidcard.interfaces.Keys;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.Objects;

public class DashboardActivity extends BaseActivity implements View.OnClickListener, Keys {

    private ImageView iv_newApplication;
    private TextView tv_all_app;
    private TextView tv_Processing;
    private TextView tv_rejected;
    private TextView tv_gbm;
    private TextView tv_closed;
    private ViewGroup parentLayout;
    private RadioGroup rg_ekyc;
    private boolean isAadhaar = false;

    private String bdeno;
    private String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        bdeno = getIntent().getStringExtra(BDE_NO);
        token = getIntent().getStringExtra(TOKEN);

        initView();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_newApplication:
                View view = getLayoutInflater().inflate(R.layout.ekyc_option_dialog_box, parentLayout,false);
                rg_ekyc = (RadioGroup) view.findViewById(R.id.rg_ekyc_option);
                rg_ekyc.setOnClickListener(this);

                final AlertDialog.Builder builder = new AlertDialog.Builder(DashboardActivity.this);
                builder.setTitle(R.string.app_name);
                builder.setMessage(R.string.e_kyc_option);
                builder.setView(view);
                builder.setCancelable(false);

                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (rg_ekyc.getCheckedRadioButtonId() == RadioGroup.NO_ID) {
                            toast("Please select ekyc option, first");
                            dialog.dismiss();
                        } else  {
                            dialog.dismiss();
                            //isAadhaar = rg_ekyc.getCheckedRadioButtonId() == R.id.rb_Aadhaar;
                            isAadhaar = false;
                            Intent intent = new Intent(DashboardActivity.this, EkycOptionActivity.class);
                            intent.putExtra(Keys.IS_AADHAAR, isAadhaar);
                            intent.putExtra(BDE_NO, bdeno);
                            intent.putExtra(TOKEN, token);
                            startActivity(intent);
                        }
                    }
                });

                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        rg_ekyc.clearCheck();
                        isAadhaar = false;
                    }
                });
                builder.create().show();
                break;


            case R.id.fl_all_app:

                break;


            case R.id.fl_Processing:

                break;


            case R.id.fl_rejected:

                break;


            case R.id.fl_gbm:

                break;


            case R.id.fl_closed:

                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sync:
                break;

            case R.id.refresh:
                break;

            case R.id.logout:
                Intent _intent = new Intent(DashboardActivity.this, LoginActivity.class);
                startActivity(_intent);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void initView() {
        // toolbar setup
        setSupportActionBar((Toolbar) findViewById(R.id.my_toolbar));
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        parentLayout = (ViewGroup) findViewById(R.id.cl_module_dashboard);
        // hide the soft keyboard
        hideSoftKeyboard(parentLayout);
        Objects.requireNonNull(findViewById(R.id.rl_project_bar)).setVisibility(View.GONE);

        tv_all_app = (TextView) findViewById(R.id.tv_all_app);
        tv_Processing = (TextView) findViewById(R.id.tv_Processing);
        tv_rejected = (TextView) findViewById(R.id.tv_rejected);
        tv_gbm = (TextView) findViewById(R.id.tv_gbm);
        tv_closed = (TextView) findViewById(R.id.tv_closed);

        iv_newApplication = (ImageView) findViewById(R.id.iv_newApplication);
        setClickListener();
    }

    private void setClickListener() {
        iv_newApplication.setOnClickListener(this);
        (Objects.requireNonNull(findViewById(R.id.fl_all_app))).setOnClickListener(this);
        (Objects.requireNonNull(findViewById(R.id.fl_Processing))).setOnClickListener(this);
        (Objects.requireNonNull(findViewById(R.id.fl_rejected))).setOnClickListener(this);
        (Objects.requireNonNull(findViewById(R.id.fl_gbm))).setOnClickListener(this);
        (Objects.requireNonNull(findViewById(R.id.fl_closed))).setOnClickListener(this);

        Objects.requireNonNull(findViewById(R.id.btn_prepaid)).setOnClickListener(this);
        Objects.requireNonNull(findViewById(R.id.btn_transit_Card)).setOnClickListener(this);
        Objects.requireNonNull(findViewById(R.id.btn_smartCity_card)).setOnClickListener(this);
        Objects.requireNonNull(findViewById(R.id.btn_fatTag_card)).setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.dashboard_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
    }

}
