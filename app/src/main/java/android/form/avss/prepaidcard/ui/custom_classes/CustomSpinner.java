package android.form.avss.prepaidcard.ui.custom_classes;

import android.content.Context;
import android.form.avss.prepaidcard.ui.adapters.CustomDropDownAdapter;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;

public class CustomSpinner extends MaterialBetterSpinner {

	private TextWatcher textWatcher;

	public CustomSpinner (Context context) {
		super(context);
		this.setTextSize(14.0f);
		this.setTextColor(Color.BLACK);
//		this.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showDropDown();
//            }
//        });
	}

	public CustomSpinner (Context arg0, AttributeSet arg1) {
		super(arg0, arg1);
		this.setTextSize(14.0f);
		this.setTextColor(Color.BLACK);
	}

	public CustomSpinner (Context arg0, AttributeSet arg1, int arg2) {
		super(arg0, arg1, arg2);
		this.setTextSize(14.0f);
		this.setTextColor(Color.BLACK);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		return isEnabled() && super.onTouchEvent(event);
	}

	public void addTextWatcher(final CustomTextInputLayout inputLayout) {
		if (inputLayout == null) return;
		textWatcher = new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (inputLayout.isErrorEnabled()) {
					inputLayout.setErrorDisabled();
				}
			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		};
		this.addTextChangedListener(textWatcher);
	}

	public void setFocusChangeListener(final CustomTextInputLayout textInputLayout) {
		if(textInputLayout == null) return;
		this.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (textInputLayout.isErrorEnabled()) {
                        addTextWatcher(textInputLayout);
                    } else {
                        removeTextChangedListener(textWatcher);
                    }

                    setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v1) {
                            if (getAdapter().isEmpty()) {
                                setDropDownHeight(0);
                            } else {
                                showDropDown();
                            }
                        }
                    });

                } else {

                    if (TextUtils.isEmpty(getString())) {
                        textInputLayout.setErrorMessage();
                    } else {
                        textInputLayout.setErrorDisabled();
                    }
                    removeTextChangedListener(textWatcher);
                }
            }
        });
	}

	public void setDisabled() {
		this.setEnabled(false);
	}

	public void setEnabled() {
		this.setEnabled(true);
	}

	public String getString() {
		return this.getText().toString().trim();
	}

	public void setFieldToNull() {
		this.setText(null);
	}

	public boolean toLength(int length) {
		return  this.getString().length() == length;
	}

	public boolean isEmpty() {
	    return !TextUtils.isEmpty(this.getString());
    }

	public void setFieldToEmpty() { this.setText("");}

	public void setAdapter(ArrayList<String> arrayList){
		if (arrayList == null) return;
		this.setAdapter(new CustomDropDownAdapter(getContext(), arrayList));
	}

	public boolean isFieldEmpty( CustomTextInputLayout inputLayout ) {
		if (!TextUtils.isEmpty(this.getString())) {
			inputLayout.setErrorDisabled();
			return true;
		} else {
			inputLayout.setErrorMessage();
			return false;
		}
	}

	public boolean isFieldEmpty( CustomTextInputLayout inputLayout, String msg ) {
		if ( !TextUtils.isEmpty(this.getString()) ) {
			inputLayout.setErrorDisabled();
			return true;
		} else {
			inputLayout.setErrorMessage(msg);
			return false;
		}
	}

}
