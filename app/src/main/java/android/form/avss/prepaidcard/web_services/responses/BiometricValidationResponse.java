package android.form.avss.prepaidcard.web_services.responses;

import com.google.gson.annotations.SerializedName;

public class BiometricValidationResponse {
    @SerializedName("da")
    public String data;

    public static class BiometricValidation  extends BaseResponse<BiometricValidationResponse.BiometricValidation>{
        @SerializedName("ud")
        public String uid;
        @SerializedName("dob")
        public String dob;
        @SerializedName("gr")
        public String gender;
        @SerializedName("nm")
        public String name;
        @SerializedName("fn")
        public String fatherName;
        @SerializedName("pt")
        public String pht;

        @SerializedName("al1")
        public String addressLine1;
        @SerializedName("al2")
        public String addressLine2;
        @SerializedName("al3")
        public String addressLine3;
        @SerializedName("pe")
        public String pincode;
        @SerializedName("ct")
        public String city;
        @SerializedName("st")
        public String state;
        @SerializedName("lmk")
        public String landmark;
        @SerializedName("cty")
        public String country;
    }
}
