package android.form.avss.prepaidcard.database.pojo;

public class ServiceSession {

    private String id ;
    private String token;
    private String formId;
    private String lastSync;


    /**
     *
     * @return BDE ID
     */
    public String getId() {
        return id;
    }



    public void setId(String id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getFormId() {
        return formId;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }

    public String getLastSync() {
        return lastSync;
    }

    public void setLastSync(String lastSync) {
        this.lastSync = lastSync;
    }
}
