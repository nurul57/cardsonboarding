package android.form.avss.prepaidcard.ihmf_form_fill.interfaces;


import android.form.avss.prepaidcard.ihmf_form_fill.pojo.controls.BaseControl;
import android.form.avss.prepaidcard.ihmf_form_fill.util.MySharePreference;

import com.google.gson.JsonObject;


/**
 * This class is implemented by every view who used BaseControl
 * @param <T>  - Instance of type BaseControl
 */
public interface iHMFControl<T extends BaseControl> {
    /**
     * Fetch current control instance
     * @return - Instance of Current Control
     */
    T getControl();

    /**
     * This method is used to set new control
     * @param control - new control for configiuration
     */
    void setControl(T control, JsonObject obj, FocusListener focusListener);

    /**
     * This method is called to saved ui data control instance .
     * This method must be called on background thread, if action is resource intensive
     */
    boolean save();

    /**
     * This method is used to check whether current data is valid or not
     * @return - true if valid or false if not valid
     */
    boolean isValid();

    MySharePreference getPreference();


}

