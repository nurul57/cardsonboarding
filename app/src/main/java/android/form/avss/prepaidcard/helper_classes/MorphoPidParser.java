package android.form.avss.prepaidcard.helper_classes;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;


/**
 * Created by Gautam on 9/19/2017.
 */
public class MorphoPidParser {
    public HashMap<String, String> ParseResp(String pid){
        HashMap<String, String> localHashMap = new HashMap<String, String>();
        try {
            DocumentBuilderFactory dbf =DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(pid));
            Document doc = db.parse(is);
            NodeList nodes = getNodeList(doc,"PidData");
            NodeList node1=nodes.item(0).getChildNodes();
            // System.out.println(nodes.toString());
            //NodeList child= nodes.item(0).getChildNodes();
            int v=node1.getLength();
            for(int i=0;i<node1.getLength();i++)
            {
                Node node=node1.item(i);
                String n=node.getNodeName();
                if(node.getNodeName().equalsIgnoreCase("DeviceInfo")){
                    NamedNodeMap node_attr=node.getAttributes();
                    int j=0;
                    String paramString="";
                    while (j < node_attr.getLength())
                    {
                        String attr = node_attr.item(j).getNodeName();
                        paramString = paramString + " " + attr + "=\"" + node_attr.item(j).getNodeValue().toString() + "\"";
                        j += 1;
                    }
                    localHashMap.put("DeviceInfo", paramString);
                }else if(node.getNodeName().equalsIgnoreCase("Skey")){
                    NamedNodeMap node_attr=node.getAttributes();
                    int j=0;
                    String paramString="";
                    while (j < node_attr.getLength())
                    {
                        String attr = node_attr.item(j).getNodeName();
                        paramString = paramString + " " + attr + "=\"" + node_attr.item(j).getNodeValue().toString() + "\"";
                        j += 1;
                    }
                    localHashMap.put("Skey", "<Skey " + paramString + " ki=\"\">" + node.getTextContent() + "</Skey>");
                }else if(node.getNodeName().equalsIgnoreCase("Hmac")){
                    NamedNodeMap node_attr=node.getAttributes();
                    int j=0;
                    String paramString="";
                    while (j < node_attr.getLength())
                    {
                        String attr = node_attr.item(j).getNodeName();
                        paramString = paramString + " " + attr + "=\"" + node_attr.item(j).getNodeValue().toString() + "\"";
                        j += 1;
                    }
                    localHashMap.put("Hmac", "<Hmac " + paramString + ">" + node.getTextContent() + "</Hmac>");
                }else if(node.getNodeName().equalsIgnoreCase("Data")){
                    NamedNodeMap node_attr=node.getAttributes();
                    int j=0;
                    String paramString="";
                    while (j < node_attr.getLength())
                    {
                        String attr = node_attr.item(j).getNodeName();
                        paramString = paramString + " " + attr + "=\"" + node_attr.item(j).getNodeValue().toString() + "\"";
                        j += 1;
                    }
                    localHashMap.put("Data", "<Data " + paramString + ">" + node.getTextContent() + "</Data>");
                }	else if(node.getNodeName().equalsIgnoreCase("Resp")){
                    NamedNodeMap node_attr=node.getAttributes();
                    int j=0;
                    String paramString="";
                    while (j < node_attr.getLength())
                    {
                        String attr = node_attr.item(j).getNodeName();
                        //paramString = paramString + " " + attr + "=\"" + node_attr.item(j).getNodeValue().toString() + "\"";
                        if(attr.equalsIgnoreCase("errCode")){
                            paramString=node_attr.item(j).getNodeValue().toString();
                            localHashMap.put("errCode", paramString);
                        }
                        if(attr.equalsIgnoreCase("errInfo")){
                            paramString=node_attr.item(j).getNodeValue().toString();
                            localHashMap.put("errInfo", paramString);
                        }
                        j += 1;
                    }
                }
            }

        } catch (ParserConfigurationException e) {

        } catch (SAXException e) {

        } catch (IOException e) {

        }
        return  localHashMap;
    }
    public static NodeList getNodeList(Document doc,String tagName)
    {
        NodeList nodes=null;
        try
        {
            nodes = doc.getElementsByTagName(tagName);
        }
        catch(Exception e)
        {

        }
        return nodes;
    }
}
