package android.form.avss.prepaidcard.ihmf_form_fill.pojo.Responses;


import android.form.avss.prepaidcard.ihmf_form_fill.TempPackage.GroupControls;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.Responses.Form;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.Responses.SectionData;

import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Page {


    @SerializedName("pageId")
    private transient String pageId;

    @SerializedName("pngImageUrls")
    private String pngImageUrls;

    @SerializedName("thumImageUrls")
    private String thumImageUrls;

    @SerializedName("imageId")
    private String imageId;

    @SerializedName("isMark")
    private Integer isMark;

    @Expose
    @SerializedName("objectList")
    private List<JsonObject> controls;

    @SerializedName("sectionList")
    private List<SectionData> sectionList;


    public List<SectionData> getSectionList(){
        return this.sectionList;
    }



    private transient List<JsonObject> pageControls;

    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public Integer getIsMark() {
        return isMark;
    }

    public boolean isMark(){return isMark != null && isMark == Form.TRUE;}

    public void setIsMark(Integer isMark) {
        this.isMark = isMark;
    }

    public void setIsMark(boolean isMark) {
        this.isMark = isMark ? Form.TRUE : Form.FALSE;
    }

    public List<JsonObject> getControls() {
        return controls;
    }

    public void setControls(List<JsonObject> controls) {
        this.controls = controls;
    }


    public boolean hasControls(){return controls != null && !controls.isEmpty();}

    public String getPngImageUrls() {
        return pngImageUrls;
    }

    public void setPngImageUrls(String pngImageUrls) {
        this.pngImageUrls = pngImageUrls;
    }

    public String getThumImageUrls() {
        return thumImageUrls;
    }

    public void setThumImageUrls(String thumImageUrls) {
        this.thumImageUrls = thumImageUrls;
    }


    public  List<JsonObject> getPageControls() {
        return pageControls == null ? (pageControls= new ArrayList<>()) : pageControls;
    }









    private transient List<GroupControls> groupControlsList;

    public List<GroupControls> getGroupControlsList() {
        return groupControlsList;
    }

    public void setGroupControlsList(List<GroupControls> groupControlsList) {
        this.groupControlsList = groupControlsList;
    }

    public void addAll(List<JsonObject> obj){

        if(!getPageControls().isEmpty())
            getPageControls().clear();

        getPageControls().addAll(obj);
    }


    public void overrideFormControl(){
        controls  = getPageControls().isEmpty() ? controls : getPageControls();
    }


    private int position = -1;

    public int getPosition() {
        return position;
    }

    public void setPosition(int i) {

        this.position = i;
    }

}
