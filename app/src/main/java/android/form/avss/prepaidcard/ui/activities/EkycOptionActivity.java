package android.form.avss.prepaidcard.ui.activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.form.avss.prepaidcard.R;
import android.form.avss.prepaidcard.database.MyDBHelper;
import android.form.avss.prepaidcard.ihmf_form_fill.TempPackage.MainActivity;
import android.form.avss.prepaidcard.ihmf_form_fill.util.MySharePreference;
import android.form.avss.prepaidcard.ui.adapters.ImagesName;
import android.form.avss.prepaidcard.ui.custom_classes.CustomButton;
import android.form.avss.prepaidcard.ui.custom_classes.CustomTextInputLayout;
import android.form.avss.prepaidcard.ui.custom_classes.CustomTextWatcher;
import android.form.avss.prepaidcard.ui.custom_classes.MyCheckBox;
import android.form.avss.prepaidcard.helper_classes.Constant;
import android.form.avss.prepaidcard.helper_classes.CreateKycRequest;
import android.form.avss.prepaidcard.helper_classes.MessageViewHolder;
import android.form.avss.prepaidcard.helper_classes.MorphoPidParser;
import android.form.avss.prepaidcard.interfaces.Keys;
import android.form.avss.prepaidcard.ui.custom_classes.RegularExpression;
import android.form.avss.prepaidcard.ui.custom_classes.interfaces.RegularExpressions;
import android.form.avss.prepaidcard.ui.model.CardDetailViewModel;
import android.form.avss.prepaidcard.ui.model.EkycOptionViewModel;
import android.form.avss.prepaidcard.ui.model.FinalDataViewModel;
import android.form.avss.prepaidcard.utils.FileUtils;
import android.form.avss.prepaidcard.utils.MorphoUtil;
import android.form.avss.prepaidcard.utils.UtilityClass;
import android.form.avss.prepaidcard.web_services.model.Services;
import android.form.avss.prepaidcard.web_services.requests.BiometricValidationRequest;
import android.form.avss.prepaidcard.web_services.requests.FinalDataSubmitRequest;
import android.form.avss.prepaidcard.web_services.requests.PanVerificationRequest;
import android.form.avss.prepaidcard.web_services.responses.BiometricValidationResponse;
import android.form.avss.prepaidcard.web_services.responses.FinalDataSubmitResponse;
import android.form.avss.prepaidcard.web_services.responses.PanVerificationResponse;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class EkycOptionActivity extends BaseActivity implements Keys,
		View.OnClickListener, CompoundButton.OnCheckedChangeListener,
		Services.ValidateAadhaar, RadioGroup.OnCheckedChangeListener,
		Services.ValidatePan , Services.FinalDataSubmit, RegularExpressions {
	private final int REQUEST_MORPHO = 100;

    private ViewGroup clAadhaar;
    private ViewGroup rlNonAadhaar;
    private ViewGroup llAadhaarData;
    private ViewGroup llAadhaarCheckBox;

    private ImageView iv_aadhaarImage;
	private CustomTextInputLayout tvAadhaarNo, tvAadhaarUidTokenNo, tvAadhaarName, tvAadhaarFatherName,
		    tvAadhaarGender, tvAadhaarDob, tvAadhaarAddressLine1, tvAadhaarAddressLine2, tvAadhaarAddressLine3,
		    tvAadhaarLandmark, tvAadhaarPinCode, tvAadhaarCity, tvAadhaarState, tvAadhaarCountry;
	private CustomTextInputLayout tvApplicantPrefix,tvApplicantFirstName,tvApplicantMiddleName,tvApplicantLastName,
            tvDob, tvGender, tvApplicantFatherPrefix,tvApplicantFatherFirstName,
		    tvApplicantFatherMiddleName, tvApplicantFatherLastName,tvCommAddressLine1,tvCommAddressLine2,
		    tvCommAddressLine3, tvCommLandmark, tvCommPinCode, tvCommCity, tvCommState, tvCommCountry, tvCommResident,
            tvPermAddressLine1,tvPermAddressLine2, tvPermAddressLine3, tvPermLandmark, tvPermPinCode, tvPermCity, tvPermState,
		    tvPermCountry, tvPermResident, tvMobileNo, tvEmailId;
    private CustomTextInputLayout tvIdProofDoc, tvIdProofNumber, tvIdProofIssuingAuthority, tvIdProofIssuingDate,
			tvIdProofIssuingExpireDate, tvCommAddressProofDoc, tvCommAddressProofNumber, tvCommAddressIssuingAuthority, tvCommAddressExpireDate;

	private RelativeLayout rlPanAvailable, rlNoPanAvailable, rlIncomeMoreThan5Lac;
	private RadioGroup rgPanOption;
	private CustomTextInputLayout tvPanNo, tvAgricultureIncome, tvNonAgricultureIncome, tvApplicationDate, tvAcknowledgementNumber;
	private CustomButton btnValidatePan;

    private CustomButton btnBiometric;
    private ImageView btnNext;
    private ImageView btnPrevious;
    private MyCheckBox cb_ekyc_id_proof, cb_ekyc_comm_proof, cbCommAddress,
		    cbPermAddress, cbPermAddressSameAsCommAddress, tv_aadhaarNonAadhaarHeader;

	private boolean isAadhaar;
	private boolean isPanVerified = false;
	private EkycOptionViewModel model;
	private FinalDataViewModel finalDataModel;

	private String latitude = "77.123456";
	private String longitude = "12.546258";
	private String token;
	private String bdeno;

	private ProgressDialog progressDialog;

	private TextView tvAutoPopulateData;

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ekyc_option);

        if (getIntent() == null) {
            toast("Something went wrong");
            finish();
            return;
        }

		token = getIntent().getStringExtra(TOKEN);
		bdeno = getIntent().getStringExtra(BDE_NO);

        initView();

        isAadhaar = getIntent().getBooleanExtra(Keys.IS_AADHAAR, false);
        clAadhaar.setVisibility(isAadhaar ? View.VISIBLE : View.GONE);
        rlNonAadhaar.setVisibility(isAadhaar ? View.GONE : View.VISIBLE);
		model = new EkycOptionViewModel();

    }

	@Override
    public void onClick(View v) {
		Calendar c = Calendar.getInstance();
		final DatePickerDialog dd;
		AlertDialog alertDialog;

        switch (v.getId()) {
			case R.id.btn_next:
				if (validateMandatoryFields()) {
					if (isAadhaar) {
						llAadhaarCheckBox.setVisibility(View.GONE);
						Bitmap bitmap = UtilityClass.getBitmap(llAadhaarData);
						llAadhaarCheckBox.setVisibility(View.VISIBLE);

						if (!saveImageInStorage(bitmap)) {
							toast("Aadhaar Image is not saved");
							return;
						}
					}

					setData();
					Constant constant = Constant.getInstance();
					if (finalDataModel == null) {
						finalDataModel = new FinalDataViewModel();
					}

					finalDataModel.setEkycOptionViewModel(model);
					finalDataModel.setCardDetailViewModel(constant.fromJson(getIntent()
							.getStringExtra(CARD_DATA), CardDetailViewModel.class));


					callFinalDataSubmitService(constant.toJson(finalDataModel));

				} else {
					ScrollView sv = (ScrollView) findViewById(R.id.sv_details);
					automaticallyScrollToField(getMandatoryFieldsList(), sv);
				}
				break;

			case R.id.btn_previous:
				alertDialog = new AlertDialog.Builder(EkycOptionActivity.this)
						.setTitle(R.string.alert)
						.setMessage(R.string.data_loss)
						.setCancelable(true)
						.create();

				alertDialog.setButton(
						DialogInterface.BUTTON_POSITIVE, "CONTINUE",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
								finish();
							}
						});

				alertDialog.setButton(
						DialogInterface.BUTTON_NEGATIVE, "CANCEL",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
							}
						});

				alertDialog.show();
				break;

			case R.id.btnBiometric:
				if (v.getVisibility() == View.VISIBLE) {

					if (btnBiometric.isEquals(R.string.change)) {
						tvAadhaarNo.setFieldToEmpty();
						tvAadhaarNo.setEnabled();
						return;
					}


					alertDialog = new AlertDialog.Builder(EkycOptionActivity.this)
							.setTitle(R.string.customer_consent_form)
							.setMessage(R.string.bio_aadhaar_msg)
							.setCancelable(true)
							.create();

					alertDialog.setButton(
							DialogInterface.BUTTON_POSITIVE, "PROCEED",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int which) {
									Intent morpho = MorphoUtil.requestMorpho(EkycOptionActivity.this);
									if (morpho != null) {
										startActivityForResult(morpho, REQUEST_MORPHO);
									} else {
										toast(R.string.install_morpho_apk);
									}
								}
							});

					alertDialog.show();
				}
	            break;

            case R.id.etDob:
                dd = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        String dateOfString = dayOfMonth + "-" + (month + 1) + "-" + year;
						tvDob.setText(dateOfString);
                    }
                }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE));
                dd.getDatePicker().setMaxDate(System.currentTimeMillis());
                dd.show();
                break;

            case R.id.etIdProofIssuingDate:
                 dd = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        String dateOfString = dayOfMonth + "-" + (month + 1) + "-" + year;
                        tvIdProofIssuingDate.setText(dateOfString);
                    }
                }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE));
                dd.getDatePicker().setMaxDate(System.currentTimeMillis());
                dd.show();
                break;

            case R.id.etIdProofIssuingExpireDate:
                 dd = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        String dateOfString = dayOfMonth + "-" + (month + 1) + "-" + year;
                        tvIdProofIssuingExpireDate.setText(dateOfString);
                    }
                }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE));
                dd.getDatePicker().setMinDate(System.currentTimeMillis());
                dd.show();
                break;

            case R.id.etCommAddressExpireDate:
                 dd = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        String dateOfString = dayOfMonth + "-" + (month + 1) + "-" + year;
                        tvCommAddressExpireDate.setText(dateOfString);
                    }
                }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE));
                dd.getDatePicker().setMinDate(System.currentTimeMillis());
                dd.show();
                break;

			case R.id.btn_pan_validate:
				if (tvApplicantFirstName.isEmpty() || tvApplicantLastName.isEmpty()) {
					toast(R.string.err_applicant);
					return;
				}

				if (!tvPanNo.matchPanPattern()) {
					tvPanNo.setErrorMessage(R.string.err_validate_pan);
					toast(R.string.msg_pan);
					return;
				}

				if (btnValidatePan.isEquals(R.string.change)) {
					tvPanNo.setFieldToEmpty();
					tvPanNo.setEnabled();
					return;
				}

				callValidatePanService();
				break;

			case R.id.et_applicationDate:
				dd = new DatePickerDialog(EkycOptionActivity.this, new DatePickerDialog.OnDateSetListener() {
					@Override
					public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
						String dateOfString = dayOfMonth + "-" + (month + 1) + "-" + year;
						tvApplicationDate.setText(dateOfString);
					}
				}, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE));
				dd.getDatePicker().setMaxDate(System.currentTimeMillis());
				dd.show();
				break;

			case R.id.tvAutoFill:
				autoFillData();
				break;
        }
    }

	private void autoFillData() {
		tvApplicantPrefix.setText("Mr.");
		tvApplicantFirstName.setText("ABC");
		tvApplicantMiddleName.setText("DEF");
		tvApplicantLastName.setText("GHI");
		tvDob.setText("22/05/2019");
		tvGender.setText("Male");
		tvApplicantFatherPrefix.setText("Mr.");
		tvApplicantFatherFirstName.setText("ABC");
		tvApplicantFatherMiddleName.setText("DEF");
		tvApplicantFatherLastName.setText("GHI");
		tvCommAddressLine1.setText("XXX");
		tvCommAddressLine2.setText("YYY");
		tvCommAddressLine3.setText("ZZZ");
		tvCommLandmark.setText("YYY");
		tvCommPinCode.setText("201301");
		tvIdProofDoc.setText("Adhaar");
		tvIdProofNumber.setText("123456123456");
		tvIdProofIssuingDate.setText("22/05/2019");
		tvIdProofIssuingExpireDate.setText("22/05/2019");
		tvCommAddressProofDoc.setText("Adhaar");
		tvCommAddressProofNumber.setText("123456123456");
		tvCommAddressExpireDate.setText("22/05/2019");
		tvMobileNo.setText("9162769525");
		tvEmailId.setText("abc@gmail.com");


		if (cbPermAddressSameAsCommAddress.isChecked()) {
			tvPermAddressLine1.setText("XXX");
			tvPermAddressLine2.setText("YYY");
			tvPermAddressLine3.setText("ZZZ");
			tvPermLandmark.setText("YYY");
			tvPermPinCode.setText("201301");
		}
	}

	private void callValidatePanService() {
		PanVerificationRequest _panRequest = new PanVerificationRequest();
		_panRequest.setPanCardNo(tvPanNo.getString());
		_panRequest.setName(UtilityClass.getName(tvApplicantFirstName.getString(),
				tvApplicantMiddleName.getString(), tvApplicantLastName.getString()));
		_panRequest.setBdeNo(bdeno);

		progressDialog = new ProgressDialog(EkycOptionActivity.this);
		progressDialog.setTitle("Service Call in Progress");
		progressDialog.setMessage("Verifying Pan Number...");
		progressDialog.show();
		new Services().validatePan(_panRequest, EkycOptionActivity.this, token);
	}

	@Override
    public void onCheckedChanged(CompoundButton v, boolean isChecked) {
        switch (v.getId()) {
            case R.id.cbCommAddress:
                v.setChecked(isChecked);
                break;

            case R.id.cbPermAddress:
                v.setChecked(isChecked);
                break;

            case R.id.cbSameAsCommAddress:
            	if (isCommunicationAddressIsEmpty() && isChecked) {
            		v.setChecked(false);
            		toast("Please fill the Communication Address Details Section, first");
            		return;
				}

	            setPermanentAddressSameAsCommunicationAddress(isChecked);
            	break;
        }
    }

	private void callFinalDataSubmitService(String finalRequest) {
		if (isEmpty(finalRequest)){
			toast("final data is empty");
			return;
		}

		if (progressDialog == null) {
			progressDialog = new ProgressDialog(this);
		}
		progressDialog.setTitle(R.string.app_name);
		progressDialog.setMessage("Submitting Data...");
		progressDialog.show();

		FinalDataSubmitRequest submitRequest = new FinalDataSubmitRequest();
		submitRequest.setBdeNo(getIntent().getStringExtra(BDE_NO));
		submitRequest.setFinalSubmit(finalRequest);

		new Services().finalSubmitData(submitRequest, EkycOptionActivity.this, token);
	}

	private void setPermanentAddressSameAsCommunicationAddress(boolean isSame) {
		if (isSame) {
			tvPermAddressLine1.setText(tvCommAddressLine1.getString());
			tvPermAddressLine2.setText(tvCommAddressLine2.getString());
			tvPermAddressLine3.setText(tvCommAddressLine3.getString());
			tvPermLandmark.setText(tvCommLandmark.getString());
			tvPermPinCode.setText(tvCommPinCode.getString());
			tvPermCity.setText(tvCommCity.getString());
			tvPermState.setText(tvCommState.getString());

			tvCommAddressLine1.addTextWatcher(new CustomTextWatcher(tvCommAddressLine1, tvPermAddressLine1));
			tvCommAddressLine2.addTextWatcher(new CustomTextWatcher(tvCommAddressLine2, tvPermAddressLine2));
			tvCommAddressLine3.addTextWatcher(new CustomTextWatcher(tvCommAddressLine3, tvPermAddressLine3));
			tvCommLandmark.addTextWatcher(new CustomTextWatcher(tvCommLandmark, tvPermLandmark));
			tvCommPinCode.addTextWatcher(new CustomTextWatcher(tvCommPinCode, tvPermPinCode));

			tvPermAddressLine1.setDisabled();
			tvPermAddressLine2.setDisabled();
			tvPermAddressLine3.setDisabled();
			tvPermLandmark.setDisabled();
			tvPermPinCode.setDisabled();

		} else {
			tvPermAddressLine1.setFieldToEmpty();
			tvPermAddressLine2.setFieldToEmpty();
			tvPermAddressLine3.setFieldToEmpty();
			tvPermLandmark.setFieldToEmpty();
			tvPermPinCode.setFieldToEmpty();
			tvPermCity.setFieldToEmpty();
			tvPermState.setFieldToEmpty();

			tvPermAddressLine1.setEnabled();
			tvPermAddressLine2.setEnabled();
			tvPermAddressLine3.setEnabled();
			tvPermLandmark.setEnabled();
			tvPermPinCode.setEnabled();
		}
	}

	private boolean isCommunicationAddressIsEmpty() {
		return tvCommAddressLine1.isFieldEmpty() && tvCommAddressLine2.isFieldEmpty()
				&& tvCommAddressLine3.isFieldEmpty() && tvCommLandmark.isFieldEmpty()
				&& tvCommPinCode.isFieldEmpty() && tvCommCity.isFieldEmpty() && tvCommState.isFieldEmpty();
	}

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == REQUEST_MORPHO) {

			final String PID_DATA = "PID_DATA";

			try {
				final String _pidData = data.getStringExtra(PID_DATA);

				final HashMap<String, String> _pid = new MorphoPidParser().ParseResp(_pidData);
				if (_pid.size() != 0) {
					if (_pid.get("errCode").equalsIgnoreCase("DNC")) {

						showMorphoError(_pid.get("errInfo"));

					} else if (_pid.get("errCode").equalsIgnoreCase("0")) {
						CreateKycRequest _obj = new CreateKycRequest(getIntent().getStringExtra("BDE_NO"));
						String _authData;

						try {
							_authData = _obj.createRequest(tvAadhaarNo.getString(), _pid, UtilityClass.getImeiNumber(EkycOptionActivity.this));

							String[] _test = _authData.split("@");
							if (_test.length == 1) {
								BiometricValidationRequest _biometricRequest = new BiometricValidationRequest();
								_biometricRequest.bdeno = bdeno;
								Toast.makeText(EkycOptionActivity.this, "bdeno" + _biometricRequest.bdeno, Toast.LENGTH_LONG).show();
								_biometricRequest.deviceId = _authData.split("#")[1].split("=")[1].split(" ")[0];
								_biometricRequest.pid = _pidData;
								_biometricRequest.uId = tvAadhaarNo.getString();
								_biometricRequest.kycRequest = _authData.split("#")[0];

								if (latitude.equals("0.0") && longitude.equals("0.0")) {
									try {
										/*ocationManager.requestLocationUpdates(LocationManager
												.GPS_PROVIDER, 5000, 10, locationListener);
										locationManager.requestLocationUpdates(LocationManager
												.NETWORK_PROVIDER, 5000, 10, locationListener);
										return "no location";*/
									} catch (SecurityException ex) {

									}
								} else {
									_biometricRequest.latitude = latitude;
									_biometricRequest.longitude = longitude;

									progressDialog = new ProgressDialog(EkycOptionActivity.this);
									progressDialog.setTitle("Service Call in Progress");
									progressDialog.setMessage("Logging in...");
									progressDialog.show();
									new Services().validateAadhaar(_biometricRequest, this, token);
								}
							} else {
								Toast.makeText(EkycOptionActivity.this, _test[0], Toast.LENGTH_LONG).show();
							}
						} catch (Exception e) {
							//String msg = _pid.containsKey("errInfo") ? "Error : " + _pid.get("errInfo") : "Error occured...";
							String msg = e.getMessage();
							showMorphoError(msg);
						}

					} else {
						showMorphoError(_pid.get("errInfo"));
					}
				} else {
					//enableDisableView(llUserData, true);
				}
			} catch (Exception e) {
				showMorphoError("Error in Invoking RDService.");
			}
		}
    }

	private void showMorphoError(String msg) {
		MessageViewHolder viewHolder = new MessageViewHolder(EkycOptionActivity.this);

		final AlertDialog dialog = new AlertDialog.Builder(EkycOptionActivity.this)
				.setCancelable(false)
				.setView(viewHolder.getRoot()).create();

		viewHolder.setIcon(R.drawable.ic_info_red_48dp);
		viewHolder.setTitle(R.string.morpho_err_title);
		viewHolder.setBody(msg);
		viewHolder.setDone("Ok", new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		dialog.show();
	}

	private void setData() {
		if (model == null) return;

		model.setAadhaar(isAadhaar);

		if (isAadhaar) {
			model.setUidTokenNo(tvAadhaarUidTokenNo.getString());
			model.setAadhaarApplicantName(tvAadhaarName.getString());
			model.setAadhaarApplicantFatherName(tvAadhaarFatherName.getString());
			model.setApplicantDob(tvAadhaarDob.getString());
			model.setApplicantGender(tvAadhaarGender.getString());
			model.setAadhaarAddressLine1(tvAadhaarAddressLine1.getString());
			model.setAadhaarAddressLine2(tvAadhaarAddressLine2.getString());
			model.setAadhaarAddressLine3(tvAadhaarAddressLine3.getString());
			model.setAadhaarLandmark(tvAadhaarLandmark.getString());
			model.setAadhaarPinCode(tvAadhaarPinCode.getString());
			model.setAadhaarCity(tvAadhaarCity.getString());
			model.setAadhaarState(tvAadhaarState.getString());
			model.setAadhaarCountry(tvAadhaarCountry.getString());

			model.setCbEkycIdProof("Y");
			model.setCbEkycCommAddressProof("Y");

			if (cbPermAddress.isChecked() && cbCommAddress.isChecked()) {
				model.setCbSameAsCommAddress("Y");
			} else {
				model.setCbSameAsCommAddress("N");
			}

			model.setCbSameAsPermAddress(cbPermAddress.isChecked() ? "Y" : "N");

		} else {

			model.setApplicationPrefix(tvApplicantPrefix.getString());
			model.setApplicantFirstName(tvApplicantFirstName.getString());
			model.setApplicantMiddleName(tvApplicantMiddleName.getString());
			model.setApplicantLastName(tvApplicantLastName.getString());
			model.setApplicantFatherPrefix(tvApplicantFatherPrefix.getString());
			model.setApplicantFatherFirstName(tvApplicantFatherFirstName.getString());
			model.setApplicantFatherMiddleName(tvApplicantFatherMiddleName.getString());
			model.setApplicantFatherLastName(tvApplicantFatherLastName.getString());
			model.setApplicantDob(tvDob.getString());
			model.setApplicantGender(tvGender.getString());

			model.setCommAddressLine1(tvCommAddressLine1.getString());
			model.setCommAddressLine2(tvCommAddressLine2.getString());
			model.setCommAddressLine3(tvCommAddressLine3.getString());
			model.setCommLandmark(tvCommLandmark.getString());
			model.setCommPinCode(tvCommPinCode.getString());
			model.setCommCity(tvCommCity.getString());
			model.setCommState(tvCommState.getString());
			model.setCommCountry(tvCommCountry.getString());

			model.setPermAddressLine1(tvPermAddressLine1.getString());
			model.setPermAddressLine2(tvPermAddressLine2.getString());
			model.setPermAddressLine3(tvPermAddressLine3.getString());
			model.setPermLandmark(tvPermLandmark.getString());
			model.setPermPinCode(tvPermPinCode.getString());
			model.setPermCity(tvPermCity.getString());
			model.setPermState(tvPermState.getString());
			model.setPermCountry(tvPermCountry.getString());

			model.setIdProofDocName(tvIdProofDoc.getString());
			model.setIdProofDocNumber(tvIdProofNumber.getString());
			model.setIdProofDocIssuingAuthority(tvIdProofIssuingAuthority.getString());
			model.setIdProofDocIssuingDate(tvIdProofIssuingDate.getString());
			model.setIdProofIssuingExpireDate(tvIdProofIssuingExpireDate.getString());

			model.setAddressProofDocName(tvCommAddressProofDoc.getString());
			model.setAddressProofDocNumber(tvCommAddressProofNumber.getString());
			model.setAddressProofDocIssuingAuthority(tvCommAddressIssuingAuthority.getString());
			model.setAddressProofDocExpiryDate(tvCommAddressExpireDate.getString());

			model.setCbSameAsCommAddress(cbPermAddressSameAsCommAddress.isChecked() ? "Y" : "N");
		}

		model.setMobileNo(tvMobileNo.getString());
		model.setEmailId(tvEmailId.getString());

		model.setPanAvailable(rgPanOption.getCheckedRadioButtonId() == R.id.rbYes);
		if (rgPanOption.getCheckedRadioButtonId() == R.id.rbYes) {

			model.setPanNumber(tvPanNo.getString());
		} else {

			model.setAgriCultureIncome(tvAgricultureIncome.getString());
			model.setNonAgriCultureIncome(tvNonAgricultureIncome.getString());
			model.setIncomeMoreThan5Lac(isIncomeMoreThan5Lac());
			if (rlIncomeMoreThan5Lac.getVisibility() == View.VISIBLE) {

				model.setApplicationDate(tvApplicationDate.getString());
				model.setAcknowledgementNumber(tvAcknowledgementNumber.getString());
			}
		}

	}

	private boolean validateMandatoryFields() {
		boolean temp = true;

		if (!isAadhaar) {

			if (tvApplicantPrefix.isFieldEmpty()) temp = false;
			if (tvApplicantFirstName.isFieldEmpty()) temp = false;
			if (tvApplicantLastName.isFieldEmpty()) temp = false;
			if (tvApplicantFatherPrefix.isFieldEmpty()) temp = false;
			if (tvApplicantFatherFirstName.isFieldEmpty()) temp = false;
			if (tvApplicantFatherLastName.isFieldEmpty()) temp = false;
			if (tvGender.isFieldEmpty()) temp = false;
			if (tvDob.isFieldEmpty()) temp = false;

			if (tvCommAddressLine1.isFieldEmpty()) temp = false;
			if (tvCommAddressLine2.isFieldEmpty()) temp = false;
			if (tvCommAddressLine3.isFieldEmpty()) temp = false;
			if (tvCommLandmark.isFieldEmpty()) temp = false;
			if (tvCommPinCode.isFieldEmpty()) temp = false;
			if (tvCommCity.isFieldEmpty()) temp = false;
			if (tvCommState.isFieldEmpty()) temp = false;
			if (tvCommCountry.isFieldEmpty()) temp = false;

			if (tvPermAddressLine1.isFieldEmpty()) temp = false;
			if (tvPermAddressLine2.isFieldEmpty()) temp = false;
			if (tvPermAddressLine3.isFieldEmpty()) temp = false;
			if (tvPermLandmark.isFieldEmpty()) temp = false;
			if (tvPermPinCode.isFieldEmpty()) temp = false;
			if (tvPermCity.isFieldEmpty()) temp = false;
			if (tvPermState.isFieldEmpty()) temp = false;
			if (tvPermCountry.isFieldEmpty()) temp = false;

			if (tvIdProofDoc.isFieldEmpty()) temp = false;
			if (tvIdProofNumber.isFieldEmpty()) temp = false;
			if (tvIdProofIssuingAuthority.isFieldEmpty()) temp = false;
			if (tvIdProofIssuingDate.isFieldEmpty()) temp = false;
			if (tvIdProofIssuingExpireDate.isFieldEmpty()) temp = false;

			if (tvCommAddressProofDoc.isFieldEmpty()) temp = false;
			if (tvCommAddressProofNumber.isFieldEmpty()) temp = false;
			if (tvCommAddressIssuingAuthority.isFieldEmpty()) temp = false;
			if (tvCommAddressExpireDate.isFieldEmpty()) temp = false;
		}

		if (tvMobileNo.isFieldEmpty()) {

			temp = false;
		} else if (!tvMobileNo.matchMobilePattern()) {

			toast(R.string.err_mobile_no);
			tvMobileNo.setErrorMessage(R.string.err_mobile_no);
			temp = false;
		}

		if (rgPanOption.getCheckedRadioButtonId() == R.id.rbYes) {
			if (tvPanNo.isFieldEmpty()) {

				temp = false;
			} else if (!tvPanNo.matchPanPattern()) {

				tvPanNo.setErrorMessage(R.string.msg_pan);
				temp = false;
			}

			if (!isPanVerified) {

				toast("Your pan is not verified");
				return false;
			}

		} else {

			if (tvAgricultureIncome.isFieldEmpty()) temp = false;
			if (tvNonAgricultureIncome.isFieldEmpty()) temp = false;

			if (isIncomeMoreThan5Lac()) {

				if (tvApplicationDate.isFieldEmpty()) temp = false;
				if (tvAcknowledgementNumber.isFieldEmpty()) temp = false;
				if (tvAcknowledgementNumber.getLength() < 15) {
					temp = false;
					tvAcknowledgementNumber.setErrorMessage(R.string.err_ac_no);
				}
			}
		}

		if (!temp) {
			toast(R.string.err_msg_mandatory_fields);
		}
		return temp;
	}

	public void aadhaarSuccessful(BiometricValidationResponse.BiometricValidation data) {
		progressDialog.dismiss();
		if (data.isSuccess()) {

			llAadhaarData.setVisibility(View.VISIBLE);

			tvAadhaarUidTokenNo.setText(data.data().uid);
			tvAadhaarName.setText(data.data().name);
			tvAadhaarFatherName.setText(data.data().fatherName);
			tvAadhaarGender.setText(data.data().gender);
			tvAadhaarDob.setText(data.data().dob);
			tvAadhaarAddressLine1.setText(data.data().addressLine1);
			tvAadhaarAddressLine2.setText(data.data().addressLine2);
			tvAadhaarAddressLine3.setText(data.data().addressLine3);
			tvAadhaarLandmark.setText(data.data().landmark);
			tvAadhaarPinCode.setText(data.data().pincode);
			tvAadhaarCity.setText(data.data().city);
			tvAadhaarState.setText(data.data().state);
			tvAadhaarCountry.setText(data.data().country);
			iv_aadhaarImage.setImageBitmap(UtilityClass.convertStringIntoBitmap(data.data().pht));

			btnBiometric.setText(R.string.change);
			tvAadhaarNo.setDisabled();

		} else {

			toast(data.msg());
		}

		token = data.token();
	}

	@Override
	public void aadhaarFailed(boolean status, String error) {
		toast(error);
		progressDialog.dismiss();
	}

	private void setVisibleLayout(CustomTextInputLayout source, final CustomTextInputLayout destination) {
		if (source == null || destination == null || rlIncomeMoreThan5Lac == null) return;

		source.getCustomEditText().addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (s.length() > 0 && destination.getLength() > 0) {
					if (isEmpty(s.toString()) || destination.isEmpty()) return;

					double sum = Double.parseDouble(s.toString()) + Double.parseDouble(destination.getString());
					rlIncomeMoreThan5Lac.setVisibility(sum >= income5Lakh ? View.VISIBLE : View.GONE);
				}
			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		});
	}

	private TextWatcher tw_commPinCode = new TextWatcher() {
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			if (cbPermAddressSameAsCommAddress.isChecked()) {

				if (tvCommPinCode.isEmpty()) {

					tvCommCity.setFieldToEmpty();
					tvCommState.setFieldToEmpty();
					tvPermPinCode.setFieldToEmpty();
					tvPermState.setFieldToEmpty();
					tvPermState.setFieldToEmpty();

					tvCommCity.setDisabled();
					tvCommState.setDisabled();
					tvPermState.setDisabled();
					tvPermState.setDisabled();
				} else {

					if (start == 5) {
						tvPermPinCode.setDisabled();
						tvPermCity.setText(tvCommCity.getString());
						tvPermState.setText(tvCommState.getString());
					}
				}
			}

		}

		@Override
		public void afterTextChanged(Editable s) {

		}
	};

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		int id = group.getCheckedRadioButtonId();
		rlPanAvailable.setVisibility(id == R.id.rbYes ? View.VISIBLE : View.GONE);
		rlNoPanAvailable.setVisibility(id == R.id.rbNo ? View.VISIBLE : View.GONE);
	}

	private boolean isIncomeMoreThan5Lac() {
		if (tvAgricultureIncome.isEmpty() || tvNonAgricultureIncome.isEmpty()) return false;
		double totalIncome = Double.parseDouble(tvAgricultureIncome.getString()) +
				Double.parseDouble(tvNonAgricultureIncome.getString());
		return totalIncome > income5Lakh;
	}

	@Override
	public void panSuccessful(PanVerificationResponse.PanVerification data) {
		progressDialog.dismiss();
		if (data.isSuccess()) {

			isPanVerified = data.isSuccess();
			rlNoPanAvailable.setVisibility(View.GONE);
			btnValidatePan.setTextOnButton(R.string.change);
			tvPanNo.setEnabled();

			toast("Pan Validation Successful");
		} else {

			toast(data.msg());
			showAlert(data.msg());
		}
		token = data.token();
	}

	// showing alert dialog
	public void showAlert(String msg){
		AlertDialog.Builder builder = new AlertDialog.Builder(EkycOptionActivity.this);
		builder.setTitle("Alert!");
		builder.setMessage(msg);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				rlNoPanAvailable.setVisibility(isPanVerified ? View.VISIBLE : View.GONE);
				btnValidatePan.setText(R.string.validate);
				tvPanNo.setFieldToEmpty();
			}
		});
		builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				rlPanAvailable.setVisibility(View.VISIBLE);
				btnValidatePan.setText(R.string.change);
			}
		});

		builder.create().show();
	}

	@Override
	public void panFailed(boolean status, String error) {
		toast(error);
	}

	@Override
	public void finalDataSubmitSuccessful(FinalDataSubmitResponse.FinalSubmit data) {
		progressDialog.dismiss();
		if (data.isSuccess()) {
			toast("data is successfully submitted");


			Intent intent = new Intent(EkycOptionActivity.this, MainActivity.class);

			MySharePreference preference = new MySharePreference(EkycOptionActivity.this);
			preference.setBdeNumber(getIntent().getStringExtra(BDE_NO));
			preference.setFormId("1113");
			preference.setFillingFormId("1113");
			preference.setReferenceID(data.getReferenceId());
			preference.setToken(data.getToken());

			//todo : bypass
//			Intent intent = new Intent(EkycOptionActivity.this, DocumentActivity.class);
//			intent.putExtra(BDE_NO, getIntent().getStringExtra(BDE_NO));
//			intent.putExtra(FORM_ID, "1113");
//			intent.putExtra(TOKEN, data.getToken());
//			intent.putExtra(REFERENCE_ID, data.getReferenceId());

			startActivity(intent);
			finish();
		} else {
			toast(data.getErrorMessage());
		}
	}

	@Override
	public void finalDataSubmitFailed(boolean status, String error) {
		progressDialog.dismiss();
		toast(error);
	}

	private void initView() {
		clAadhaar = (ViewGroup) findViewById(R.id.clAadhaar);
		rlNonAadhaar = (ViewGroup) findViewById(R.id.rlNonAadhaar);

		llAadhaarData = (ViewGroup) findViewById(R.id.llAadhaarData);
		llAadhaarCheckBox = (ViewGroup) findViewById(R.id.llAadhaarCheckBox);
		iv_aadhaarImage = (ImageView) findViewById(R.id.iv_aadhaarImage);

		tvAadhaarNo = (CustomTextInputLayout) findViewById(R.id.tvAadhaarNo);
		tvAadhaarUidTokenNo = (CustomTextInputLayout) findViewById(R.id.tvAadhaarUidTokenNo);
		tvAadhaarName = (CustomTextInputLayout) findViewById(R.id.tvAadhaarName);
		tvAadhaarFatherName = (CustomTextInputLayout) findViewById(R.id.tvAadhaarFatherName);
		tvAadhaarGender = (CustomTextInputLayout) findViewById(R.id.tvAadhaarGender);
		tvAadhaarDob = (CustomTextInputLayout) findViewById(R.id.tvAadhaarDob);
		tvAadhaarAddressLine1 = (CustomTextInputLayout) findViewById(R.id.tvAadhaarAddressLine1);
		tvAadhaarAddressLine2 = (CustomTextInputLayout) findViewById(R.id.tvAadhaarAddressLine2);
		tvAadhaarAddressLine3 = (CustomTextInputLayout) findViewById(R.id.tvAadhaarAddressLine3);
		tvAadhaarLandmark = (CustomTextInputLayout) findViewById(R.id.tvAadhaarLandmark);
		tvAadhaarPinCode = (CustomTextInputLayout) findViewById(R.id.tvAadhaarPinCode);
		tvAadhaarCity = (CustomTextInputLayout) findViewById(R.id.tvAadhaarCity);
		tvAadhaarState = (CustomTextInputLayout) findViewById(R.id.tvAadhaarState);
		tvAadhaarCountry = (CustomTextInputLayout) findViewById(R.id.tvAadhaarCountry);

		tvApplicantPrefix = (CustomTextInputLayout) findViewById(R.id.tvApplicantPrefix);
		tvApplicantFirstName = (CustomTextInputLayout) findViewById(R.id.tvApplicantFirstName);
		tvApplicantMiddleName = (CustomTextInputLayout) findViewById(R.id.tvApplicantMiddleName);
		tvApplicantLastName = (CustomTextInputLayout) findViewById(R.id.tvApplicantLastName);

		tvDob = (CustomTextInputLayout) findViewById(R.id.tvDob);
		tvGender = (CustomTextInputLayout) findViewById(R.id.tvGender);
		tvApplicantFatherPrefix = (CustomTextInputLayout) findViewById(R.id.tvApplicantFatherPrefix);
		tvApplicantFatherFirstName = (CustomTextInputLayout) findViewById(R.id.tvApplicantFatherFirstName);
		tvApplicantFatherMiddleName = (CustomTextInputLayout) findViewById(R.id.tvApplicantFatherMiddleName);
		tvApplicantFatherLastName = (CustomTextInputLayout) findViewById(R.id.tvApplicantFatherLastName);

		tvCommAddressLine1 = (CustomTextInputLayout) findViewById(R.id.tvCommAddressLine1);
		tvCommAddressLine2 = (CustomTextInputLayout) findViewById(R.id.tvCommAddressLine2);
		tvCommAddressLine3 = (CustomTextInputLayout) findViewById(R.id.tvCommAddressLine3);
		tvCommLandmark = (CustomTextInputLayout) findViewById(R.id.tvCommLandmark);
		tvCommPinCode = (CustomTextInputLayout) findViewById(R.id.tvCommPinCode);
		tvCommCity = (CustomTextInputLayout) findViewById(R.id.tvCommCity);
		tvCommState = (CustomTextInputLayout) findViewById(R.id.tvCommState);
		tvCommCountry = (CustomTextInputLayout) findViewById(R.id.tvCommCountry);
		tvCommResident = (CustomTextInputLayout) findViewById(R.id.tvCommResident);

		tvPermAddressLine1 = (CustomTextInputLayout) findViewById(R.id.tvPermAddressLine1);
		tvPermAddressLine2 = (CustomTextInputLayout) findViewById(R.id.tvPermAddressLine2);
		tvPermAddressLine3 = (CustomTextInputLayout) findViewById(R.id.tvPermAddressLine3);
		tvPermLandmark = (CustomTextInputLayout) findViewById(R.id.tvPermLandmark);
		tvPermPinCode = (CustomTextInputLayout) findViewById(R.id.tvPermPinCode);
		tvPermCity = (CustomTextInputLayout) findViewById(R.id.tvPermCity);
		tvPermState = (CustomTextInputLayout) findViewById(R.id.tvPermState);
		tvPermCountry = (CustomTextInputLayout) findViewById(R.id.tvPermCountry);
		tvPermResident = (CustomTextInputLayout) findViewById(R.id.tvPermResident);

		tvIdProofDoc = (CustomTextInputLayout) findViewById(R.id.tvIdProofDoc);
		tvIdProofNumber = (CustomTextInputLayout) findViewById(R.id.tvIdProofNumber);
		tvIdProofIssuingAuthority = (CustomTextInputLayout) findViewById(R.id.tvIdProofIssuingAuthority);
		tvIdProofIssuingDate = (CustomTextInputLayout) findViewById(R.id.tvIdProofIssuingDate);
		tvIdProofIssuingExpireDate = (CustomTextInputLayout) findViewById(R.id.tvIdProofIssuingExpireDate);
		tvCommAddressProofDoc = (CustomTextInputLayout) findViewById(R.id.tvCommAddressProofDoc);
		tvCommAddressProofNumber = (CustomTextInputLayout) findViewById(R.id.tvCommAddressProofNumber);
		tvCommAddressIssuingAuthority = (CustomTextInputLayout) findViewById(R.id.tvCommAddressProofIssuingAuthority);
		tvCommAddressExpireDate = (CustomTextInputLayout) findViewById(R.id.tvCommAddressExpireDate);


		tvMobileNo = (CustomTextInputLayout) findViewById(R.id.tvMobileNo);
		tvEmailId = (CustomTextInputLayout) findViewById(R.id.tvEmailId);

		btnBiometric = (CustomButton) findViewById(R.id.btnBiometric);
		btnNext = (ImageView) findViewById(R.id.btn_next);
		btnPrevious = (ImageView) findViewById(R.id.btn_previous);
		cb_ekyc_id_proof = (MyCheckBox) findViewById(R.id.cb_ekyc_id_proof);
		cb_ekyc_comm_proof = (MyCheckBox) findViewById(R.id.cb_ekyc_comm_proof);
		cbCommAddress = (MyCheckBox) findViewById(R.id.cbCommAddress);
		cbPermAddress = (MyCheckBox) findViewById(R.id.cbPermAddress);
		cbPermAddressSameAsCommAddress = (MyCheckBox) findViewById(R.id.cbSameAsCommAddress);

		rgPanOption = (RadioGroup) findViewById(R.id.rgPanOption);

		tvPanNo = (CustomTextInputLayout) findViewById(R.id.tvPanNo);
		tvAgricultureIncome = (CustomTextInputLayout) findViewById(R.id.tv_agriculture_income);
		tvNonAgricultureIncome = (CustomTextInputLayout) findViewById(R.id.tv_nonAgricultureIncome);
		tvApplicationDate = (CustomTextInputLayout) findViewById(R.id.tv_application_date);
		tvAcknowledgementNumber = (CustomTextInputLayout) findViewById(R.id.tv_acknowledgement_no);

		btnValidatePan = (CustomButton) findViewById(R.id.btn_pan_validate);

		rlPanAvailable = (RelativeLayout) findViewById(R.id.rlPanAvailable);
		rlNoPanAvailable = (RelativeLayout) findViewById(R.id.rlNoPanAvailable);
		rlIncomeMoreThan5Lac = (RelativeLayout) findViewById(R.id.rlMoreThan5Lac);
		tvAutoPopulateData = (TextView) findViewById(R.id.tvAutoFill);

		tvPanNo.setButtonVisibility(btnValidatePan, 10);
		tvAadhaarNo.setAadhaarButtonVisibility(btnBiometric);

		tvCommPinCode.setPinCodeData(tvCommPinCode, tvCommCity, tvCommState);
		tvPermPinCode.setPinCodeData(tvPermPinCode, tvPermCity, tvPermState);

		setVisibleLayout(tvAgricultureIncome, tvNonAgricultureIncome);
		setVisibleLayout(tvNonAgricultureIncome, tvAgricultureIncome);

		setAdapter();
		setInputFilterOnFields();
		setFocusChangeListener();
		setOnClickListener();
		tvCommPinCode.getCustomEditText().addTextChangedListener(tw_commPinCode);


		ViewGroup rlPanLayout = (ViewGroup) findViewById(R.id.rlPanLayout);
		tvApplicantFirstName.addTextWatcher(rlPanLayout, tvApplicantFirstName, tvApplicantLastName);
        tvMobileNo.addTextWatcherForPattern(RegularExpression.MOBILE_NO_EXPRESSION, 10, R.string.err_mobile_no);
        tvPanNo.addTextWatcherForPattern(RegularExpression.PAN_EXPRESSION, 10, R.string.err_validate_pan);
	}

	private void setAdapter() {
		MyDBHelper dbHelper = MyDBHelper.getInstance(this);

		tvApplicantPrefix.setAdapter(dbHelper.getPrefix());
		tvApplicantFatherPrefix.setAdapter(dbHelper.getFatherPrefix());
		tvGender.setAdapter(dbHelper.getGender());
		tvIdProofDoc.setAdapter(dbHelper.getIdDocProof());
		tvCommAddressProofDoc.setAdapter(dbHelper.getAddDocProof());
	}

	private void setFocusChangeListener() {
		tvAadhaarNo.setFocusChangeListenerAadhaar();
		tvApplicantPrefix.setFocusChangeListener();
		tvApplicantFirstName.setFocusChangeListener();
		tvApplicantLastName.setFocusChangeListener();
		tvDob.setFocusChangeListener();
		tvGender.setFocusChangeListener();

		tvApplicantFatherPrefix.setFocusChangeListener();
		tvApplicantFatherFirstName.setFocusChangeListener();
		tvApplicantFatherLastName.setFocusChangeListener();

		tvCommAddressLine1.setFocusChangeListener();
		tvCommAddressLine2.setFocusChangeListener();
		tvCommAddressLine3.setFocusChangeListener();
		tvCommLandmark.setFocusChangeListener();
		tvCommPinCode.setFocusChangeListenerPinCode();
		tvCommCity.setFocusChangeListener();
		tvCommState.setFocusChangeListener();

		tvPermAddressLine1.setFocusChangeListener();
		tvPermAddressLine2.setFocusChangeListener();
		tvPermAddressLine3.setFocusChangeListener();
		tvPermLandmark.setFocusChangeListener();
		tvPermPinCode.setFocusChangeListenerPinCode();
		tvPermCity.setFocusChangeListener();
		tvPermState.setFocusChangeListener();

		tvIdProofDoc.setFocusChangeListener();
		tvIdProofNumber.setFocusChangeListener();
		tvIdProofIssuingAuthority.setFocusChangeListener();

		tvCommAddressProofDoc.setFocusChangeListener();
		tvCommAddressProofNumber.setFocusChangeListener();
		tvCommAddressIssuingAuthority.setFocusChangeListener();
		tvCommAddressExpireDate.setFocusChangeListener();

		tvMobileNo.setFocusChangeListenerMobileNo();

		tvPanNo.setFocusChangeListenerPan();
		tvAgricultureIncome.setFocusChangeListener();
		tvNonAgricultureIncome.setFocusChangeListener();
		tvApplicationDate.setFocusChangeListener();
		tvAcknowledgementNumber.setFocusChangeListener();
	}

	private void setInputFilterOnFields() {
		tvAadhaarNo.setLengthInputFilter(16);
		tvApplicantFirstName.setCharacterInputFilter(12);
		tvApplicantMiddleName.setCharacterInputFilter(12);
		tvApplicantLastName.setCharacterInputFilter(12);
		tvApplicantFatherFirstName.setCharacterInputFilter(12);
		tvApplicantFatherMiddleName.setCharacterInputFilter(12);
		tvApplicantFatherLastName.setCharacterInputFilter(12);

		tvCommAddressLine1.setAlphaNumericSpecialInputFilter(40);
		tvCommAddressLine2.setAlphaNumericSpecialInputFilter(40);
		tvCommAddressLine3.setAlphaNumericSpecialInputFilter(40);
		tvCommLandmark.setAlphaNumericSpecialInputFilter(25);
		tvCommPinCode.setLengthInputFilter(6);

		tvPermAddressLine1.setAlphaNumericSpecialInputFilter(40);
		tvPermAddressLine2.setAlphaNumericSpecialInputFilter(40);
		tvPermAddressLine3.setAlphaNumericSpecialInputFilter(40);
		tvPermLandmark.setAlphaNumericSpecialInputFilter(25);
		tvPermPinCode.setLengthInputFilter(6);

		tvPanNo.setAlphaNumericInputFilter(10);
	}

	private void setOnClickListener() {
		tvDob.getCustomEditText().setOnClickListener(this);
		tvIdProofIssuingDate.getCustomEditText().setOnClickListener(this);
		tvIdProofIssuingExpireDate.getCustomEditText().setOnClickListener(this);
		cbPermAddressSameAsCommAddress.setOnCheckedChangeListener(this);
		tvCommAddressExpireDate.getCustomEditText().setOnClickListener(this);
		btnBiometric.setOnClickListener(this);
		btnPrevious.setOnClickListener(this);
		btnNext.setOnClickListener(this);

		cb_ekyc_id_proof.setOnCheckedChangeListener(this);
		cb_ekyc_comm_proof.setOnCheckedChangeListener(this);

		rgPanOption.setOnCheckedChangeListener(this);
		tvApplicationDate.getCustomEditText().setOnClickListener(this);
		btnValidatePan.setOnClickListener(this);
		tvAutoPopulateData.setOnClickListener(this);
	}

	private boolean saveImageInStorage (Bitmap bitmap) {
		if (bitmap == null) return false;
		try {
			String aadhaarImagePath = FileUtils.getAadhaarImagePath("");
			if (!TextUtils.isEmpty(aadhaarImagePath)) {
				FileOutputStream out = new FileOutputStream(new File(aadhaarImagePath, ImagesName.AADHAAR_IMAGE));
				bitmap.compress(Bitmap.CompressFormat.JPEG, 60, out);
				out.flush();
				out.close();
				return true;
			}
			return false;
		} catch ( IOException ignore ) {}
		return false;
	}

	private List<Object> getMandatoryFieldsList() {
		ArrayList<Object> list = new ArrayList<>();
		if (!isAadhaar) {
			list.add(tvApplicantPrefix);
			list.add(tvApplicantFirstName);
			list.add(tvApplicantMiddleName);
			list.add(tvApplicantLastName);
			list.add(tvDob);
			list.add(tvGender);
			list.add(tvApplicantFatherPrefix);
			list.add(tvApplicantFatherFirstName);
			list.add(tvApplicantFatherMiddleName);
			list.add(tvApplicantFatherLastName);

			list.add(tvCommAddressLine1);
			list.add(tvCommAddressLine2);
			list.add(tvCommAddressLine3);
			list.add(tvCommPinCode);
			list.add(tvCommCity);
			list.add(tvCommState);

			list.add(tvPermAddressLine1);
			list.add(tvPermAddressLine2);
			list.add(tvPermAddressLine3);
			list.add(tvPermPinCode);
			list.add(tvPermCity);
			list.add(tvPermState);

			list.add(tvIdProofDoc);
			list.add(tvIdProofNumber);
			list.add(tvIdProofIssuingAuthority);
			list.add(tvIdProofIssuingDate);
			list.add(tvIdProofIssuingExpireDate);

			list.add(tvCommAddressProofDoc);
			list.add(tvCommAddressProofNumber);
			list.add(tvCommAddressIssuingAuthority);
			list.add(tvCommAddressExpireDate);

		}
		list.add(tvMobileNo);

		if (rlNoPanAvailable.getVisibility() == View.VISIBLE) {
			list.add(tvPanNo);
		} else if (rlNoPanAvailable.getVisibility() == View.VISIBLE) {
			list.add(tvAgricultureIncome);
			list.add(tvNonAgricultureIncome);
			if (rlIncomeMoreThan5Lac.getVisibility() == View.VISIBLE) {
				list.add(tvApplicationDate);
				list.add(tvAcknowledgementNumber);
			}
		}
		return list;
	}
}
