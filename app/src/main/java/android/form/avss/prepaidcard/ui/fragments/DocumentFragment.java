package android.form.avss.prepaidcard.ui.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.form.avss.prepaidcard.helper_classes.Constant;
import android.form.avss.prepaidcard.interfaces.Keys;
import android.form.avss.prepaidcard.ui.activities.CaptureImagesActivity;
import android.form.avss.prepaidcard.ui.activities.DocumentActivity;
import android.form.avss.prepaidcard.ui.adapters.DocumentImageAdapter;
import android.form.avss.prepaidcard.ui.fragments.doc_model.DocsDataList;
import android.form.avss.prepaidcard.ui.fragments.doc_model.SelectedImage;
import android.form.avss.prepaidcard.ui.model.DocImageViewModel;
import android.form.avss.prepaidcard.utils.FileUtils;
import android.form.avss.prepaidcard.web_services.pojo.ImageOption;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.form.avss.prepaidcard.R;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonSyntaxException;

import java.io.File;
import java.util.ArrayList;

public class DocumentFragment extends Fragment implements Keys, View.OnClickListener {

    private RecyclerView rvPhotoViewer;
    private ArrayList<SelectedImage> selectedImagesList;
    public int counter = 0;
    private FloatingActionButton fabCamera, fabDone;
    private ImageOption imageOption;
    private DocumentFragmentInteraction detailsListener;
    private int fragPosition = 0;
    private String referenceId;
    private DocImageViewModel docModel;
    private Constant constant;
    private DocsDataList docsDataList;

    public DocumentFragment() {
        // Required empty public constructor
    }

    public static DocumentFragment newInstance() {
        return new DocumentFragment();
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        constant = Constant.getInstance();

        imageOption = (ImageOption) getArguments().getSerializable(IMAGE_OPTION);
        try {
            docModel = constant.fromJson(getArguments().getString(DOC_SECTION_DATA), DocImageViewModel.class);
        } catch (JsonSyntaxException e) {
            e.getMessage();
        } catch (Exception e) {
            e.getMessage();
        }
        fragPosition = getArguments().getInt(FRAGMENT_POSITION);
        referenceId = getArguments().getString(REFERENCE_ID);
        selectedImagesList = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_document, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        TextView tvDocHeader = (TextView) view.findViewById(R.id.tvDocHeaderName);
        rvPhotoViewer = (RecyclerView) view.findViewById(R.id.rv_photoViewer);
        fabCamera = (FloatingActionButton) view.findViewById(R.id.fabCamera);
        fabDone = (FloatingActionButton) view.findViewById(R.id.fabDone);

        fabCamera.setOnClickListener(this);
        fabDone.setOnClickListener(this);

        if (imageOption != null && !imageOption.isDocNameEmpty()) {
            tvDocHeader.setText(imageOption.getDocName().replaceAll("_", " "));
        }

        fabCamera.setOnClickListener(this);
        fabDone.setOnClickListener(this);
        populateData();
    }

    private void populateData() {
        if (imageOption == null || docModel == null || docModel.isDocListEmpty() || fragPosition > docModel.size()) return;

        docsDataList = docModel.getFinalDocList().get(fragPosition);
        if (docsDataList.isDataListEmpty()) return;

        selectedImagesList.clear();
        selectedImagesList.addAll(docsDataList.getImages());

        if (!selectedImagesList.isEmpty()) {
            counter = selectedImagesList.size();
        }

        updateRecyclerView(selectedImagesList, aadhaarDocDisplay());
        setFabVisibility(selectedImagesList.size() == imageOption.getMaxCount());
        fabDone.setVisibility(selectedImagesList.isEmpty() ? View.GONE : View.VISIBLE);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == DOCUMENT_CONSTANT && resultCode == Activity.RESULT_OK && data != null) {
            Bundle bundle = data.getExtras();
            if (bundle != null) {
                ArrayList<String> pathList = bundle.getStringArrayList(IMAGE_LIST);
                if (!imageOption.isDocNameEmpty() && pathList != null && !pathList.isEmpty()) {
                    // this is for single image capture
                    if (pathList.size() == 1) {
                        for (String _path : pathList) {
                            SelectedImage img = new SelectedImage();
                            img.setImgPath(_path);
                            img.setImgName(imageOption.getDocName() + counter);
                            selectedImagesList.add(img);
                            counter = bundle.getInt(IMAGE_COUNTER);
                        }
                    }
                    // this is for multiple image capture
                    else {
                        for (String _path : pathList) {
                            SelectedImage img = new SelectedImage();
                            img.setImgPath(_path);
                            img.setImgName(imageOption.getDocName() + counter);
                            selectedImagesList.add(img);
                            counter++;
                        }
                    }

                    updateRecyclerView(selectedImagesList, aadhaarDocDisplay());
                    fabDone.setVisibility(selectedImagesList.isEmpty() ? View.GONE : View.VISIBLE);
                    setFabVisibility(selectedImagesList.size() >= imageOption.getMaxCount());
                }
            }
        }
    }

    private void setFabVisibility(boolean isHide) {
        fabCamera.setVisibility(isHide ? View.GONE : View.VISIBLE);
    }

    private void updateRecyclerView(ArrayList<SelectedImage> imageList, boolean isAadhaarDoc) {
        rvPhotoViewer.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(),
                2, GridLayoutManager.VERTICAL, false);
        rvPhotoViewer.setLayoutManager(gridLayoutManager);
        rvPhotoViewer.setAdapter(new DocumentImageAdapter(imageList, isAadhaarDoc, itemDeleteListener));
    }

    ItemDeleteListener itemDeleteListener = new ItemDeleteListener() {
        @Override
        public void setFloatingActionButtonVisibility(ArrayList<SelectedImage> docList) {
            if (docList == null) return;
            fabDone.setVisibility(docList.isEmpty() ? View.GONE : View.VISIBLE);
            setFabVisibility(docList.size() >= imageOption.getMaxCount());
            counter = docList.isEmpty() ? 0 : docList.size();
        }
    };

    private boolean aadhaarDocDisplay() {
        if (imageOption == null || !imageOption.isDocNameEmpty()) return false;

        File file = new File(FileUtils.getAadhaarImagePath(referenceId));

        if (!file.exists() || !file.isDirectory() || file.list().length == 0) return false;

        String[] listOfImages = file.list();
        String _aadhaarImagePath = file.getAbsolutePath() + File.separator + listOfImages[0];

        SelectedImage img = new SelectedImage();
        img.setImgPath(_aadhaarImagePath);
        img.setImgName(listOfImages[0]);

        if (imageOption.isAadhaar()) {
            if (selectedImagesList.isEmpty()) {
                selectedImagesList.add(img);

            } else  {
                selectedImagesList.clear();
                selectedImagesList.add(img);
            }
            fabCamera.setVisibility(View.GONE);
            return true;

        } else {
            fabCamera.setVisibility(View.VISIBLE);
            return false;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fabCamera:
                if (imageOption == null || imageOption.isDocNameEmpty()) {
                    toast("Something went wrong");
                    return;
                }

                Intent cameraIntent = new Intent(getContext(), CaptureImagesActivity.class);
                cameraIntent.putExtra(MIN_LIMIT, imageOption.getMinCount());
                cameraIntent.putExtra(MAX_LIMIT, imageOption.getMaxCount());
                cameraIntent.putExtra(IMAGE_NAME, imageOption.getDocName());
                cameraIntent.putExtra(IMAGE_COUNTER, counter);
                cameraIntent.putExtra(REFERENCE_ID, referenceId);
                startActivityForResult(cameraIntent, DOCUMENT_CONSTANT);

                break;

            case R.id.fabDone:
                if (imageOption == null || docModel == null) return;

                if (selectedImagesList.size() == imageOption.getMinCount() ||
                        selectedImagesList.size() == imageOption.getMaxCount()) {

                    if (fragPosition < docModel.size()) {
                        DocsDataList docsDataList = docModel.getFinalDocList().get(fragPosition);
                        docsDataList.setDocName(imageOption.getDocName());
                        docsDataList.setImages(selectedImagesList);

//                        ((DocumentActivity)getActivity()).docAdapter.documentList.get(fragPosition).setDataCaptured(true);
//                        ((DocumentActivity)getActivity()).docAdapter.notifyDataSetChanged();

                        docModel.setFinalDocList(docsDataList);
                        detailsListener.setDocModelData(docModel, fragPosition);
                        detailsListener.openNextDocFragment(++fragPosition);
                    } else {
                        toast("All Document is captured");
                    }
                } else {
                    toast(R.string.capture_mandatory_images);
                }
                break;
        }
    }

    public interface ItemDeleteListener {
        void setFloatingActionButtonVisibility(ArrayList<SelectedImage> docImagesArrayList);
    }

    public interface DocumentFragmentInteraction {
        void setDocModelData(DocImageViewModel docModelData, int fragPosition);
        void openNextDocFragment(int position);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof DocumentFragmentInteraction) {
            detailsListener = (DocumentFragmentInteraction) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement parent activity");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        detailsListener = null;
    }

    public void toast(String msg) {
        Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
    }

    public void toast(@StringRes int resId) {
        Toast.makeText(getContext(), getString(resId), Toast.LENGTH_LONG).show();
    }
}
