package android.form.avss.prepaidcard.web_services.requests;

import com.google.gson.annotations.SerializedName;

public class BaseRequest<T> {
    @SerializedName("apv")
    protected String app_version;

    @SerializedName("bn")
    protected String bdeno;

    @SerializedName("token")
    protected String token;

    @SerializedName("refid")
    protected String referenceId;

    @SerializedName("isadr")
    protected boolean isAadhaar = true;

    public boolean getAadhaar() {
        return isAadhaar;
    }

    public void setAadhaar(boolean aadhaar) {
        isAadhaar = aadhaar;
    }

    public final void setAppVersion(String version) {
        this.app_version = version;
    }

    public void setBdeNo(String bdeno) {
        this.bdeno = bdeno;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

}
