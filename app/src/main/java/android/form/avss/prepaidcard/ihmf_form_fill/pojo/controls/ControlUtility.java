package android.form.avss.prepaidcard.ihmf_form_fill.pojo.controls;

import com.google.gson.Gson;
import com.google.gson.JsonObject;


public class ControlUtility {

    static Gson gson  = null;

    public static <T extends BaseControl>T generateControl(String control,Class<T> cls){
        T data =  null;

        if(cls == null) return null;

        try {
            data = getGson().fromJson(control, cls);
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }

        return data;

    }

    private static boolean isValidControl(String controlType) { return true; }

    public static  <T extends BaseControl>T get(JsonObject controlJson){
        T control = null;

        if(controlJson == null ) return null;

        if(!controlJson.has("type")) return null;

        control = (T) generateControl(controlJson.toString(),getControl(controlJson.get(Control.TYPE).getAsString()));


        return control;
    }

    private static <T extends BaseControl>Class<T> getControl(final String type) {

        Class<T> cls = null;


        if(Control.TextField.toString().equalsIgnoreCase(type))
            cls = (Class<T>) TextControl.class;
        else if(Control.IMAGE.toString().equalsIgnoreCase(type))
            cls = (Class<T>) ImageControl.class;
        else if(Control.Photo.toString().equalsIgnoreCase(type))
            cls  = (Class<T>) SuperImageControl.class;
        else if (Control.SUPER_TEXT.toString().equalsIgnoreCase(type))
            cls = (Class<T>) SuperTextControl.class;
        else if(Control.Signature.name().equalsIgnoreCase(type))
            cls = (Class<T>) ImageControl.class;
        else if(Control.Boolean.name().equalsIgnoreCase(type))
            cls = (Class<T>) BooleanControl.class;
        else if(Control.TextFieldBox.name().equalsIgnoreCase(type))
            cls = (Class<T>) SuperTextControl.class;



        return cls;
    }


    public static Gson getGson() {
        return gson == null ? (gson = new Gson()) : gson;
    }
}
