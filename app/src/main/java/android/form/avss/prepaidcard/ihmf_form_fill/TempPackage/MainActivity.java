package android.form.avss.prepaidcard.ihmf_form_fill.TempPackage;

import android.content.Intent;
import android.form.avss.prepaidcard.R;
import android.form.avss.prepaidcard.ihmf_form_fill.interfaces.DownloadImageListener;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.Requests.MarkedDataRequest;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.Responses.Form;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.controls.ControlUtility;
import android.form.avss.prepaidcard.ihmf_form_fill.ui.Activities.FormBaseActivity;
import android.form.avss.prepaidcard.ihmf_form_fill.ui.Activities.FormFillActivity;
import android.form.avss.prepaidcard.web_services.model.Services;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;


import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import static android.form.avss.prepaidcard.interfaces.Keys.IS_PREVIEW_MODE;

public class MainActivity extends FormBaseActivity implements Services.GetMarkingDetails {
    private boolean isPreviewMode;

    private CompositeDisposable mDisposable;

    private DownloadImageTask downloadImageTask;
    private int imageIndex;

    private Form formData;
    private List<String> imageUrlList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDisposable = new CompositeDisposable();

        isPreviewMode = getIntent().getBooleanExtra(IS_PREVIEW_MODE, false);

        if (TextUtils.isEmpty(getSPreference().getFormId())) {
            showShortToast("No form id found");
            finish();
            return;
        }

        getDynamicControls();

        setOnViewClickListener();
    }

    private void setOnViewClickListener() {
        Objects.requireNonNull(findViewById(R.id.btn_getData)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(getSPreference().getFormId())) {
                    showShortToast("Enter form id");
                    return;
                }

                MainActivity.this.getDynamicControls(getSPreference().getFormId());

            }
        });

        Objects.requireNonNull(findViewById(R.id.btn_scan_qr)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showShortToast("currently not available");
//            new IntentIntegrator(this).initiateScan();
            }
        });
    }

    private boolean isFormImageFileExist(String fileName){
        File file = new File(getFormImagesFilesDirectory(), fileName);
        return file.isFile() || file.exists();
    }

    private void getDynamicControls(){
        if(isPreviewMode){
            String mssg = "Fetching preview controls";
            getDynamicControls(mssg);
        }
        else {
            String mssg = "Fetching controls";
            getDynamicControls(mssg);
        }
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        if (result != null) {
            if (result.getContents() == null) {
                showShortToast("Scan cancelled");
            } else {
                getSPreference().setFormId(data.getStringExtra("data")); //TODO: CHECK name
                getDynamicControls();
            }

        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


    public void getDynamicControls(String title) {
        MarkedDataRequest request = new MarkedDataRequest();

        request.setReferenceId(getSPreference().getReferenceID());
        request.setFormId(getSPreference().getFormId());
        request.setType(getSPreference().getType());

        showProgressDialog(title);

        new Services().getMarkingDetails(request, MainActivity.this, getSPreference().getToken());

//        Retrofit retrofit = RestClient.getClient(this);
//        if (retrofit == null) return;
//        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
//        Call<ResponseBody> call = apiInterface.requestMarkedData(request);
//        call.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
//                if (response.body() == null) {
//                    onFailure(call, new Throwable("empty response body"));
//                    return;
//                }
//
//                try {
//                    JSONObject baseResponse = new JSONObject(response.body().string());
//
//                    if (!baseResponse.getBoolean("status")) {
//                        dismissProgressDialog();
//                        Toast.makeText(MainActivity.this, baseResponse.getString("msg"), Toast.LENGTH_SHORT).show();
//                        return;
//                    }
//
//                    formData = new Gson().fromJson(baseResponse.getJSONObject("json").toString(), Form.class);
//
//                    List<String> imageUrl = formData.getPngImageUrls();
//
//
//                    for (int i = 0; i < imageUrl.size(); i++) {
//                        formData.getPages().get(i).setPngImageUrls(imageUrl.get(i));
//                    }
//                    saveDynamicDataInFile();
//
//
//                } catch (Exception e) {
//                    dismissProgressDialog();
//                    e.printStackTrace();
//                }
//
//
//            }
//            @Override
//            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
//                dismissProgressDialog();
//                showShortToast(t.getMessage());
//            }
//        });

    }

    @Override
    public void onGetDynamicControlsSuccess(Form response) {
        this.formData = response;

        for(int i=0; i<formData.getPngImageUrls().size() ; i++){
            formData.getPages().get(i).setPngImageUrls(formData.getPngImageUrls().get(i));
        }
        saveDynamicDataInFile();

    }

    @Override
    public void onGetDynamicControlsFailed(boolean status, String errorMsg) {
        dismissProgressDialog();
        showShortToast(errorMsg);
    }


    @SuppressWarnings("all")  // supprressed: file.delete();
    private void saveDynamicDataInFile() {
        showProgressDialog("Saving controls");

        //save data asynchronously
        Disposable disposable = Completable.fromAction(new Action() {
            @Override
            public void run() {

                //creating new "formId.txt" file inside "/formId_FORM_DATA"
                File file = new File(getFormDataFileDir(), getSPreference().getFormId()+".txt");

                if(file.exists()){
                    file.delete();
                }

                try (FileWriter writer = new FileWriter(file) ){
                    String json = ControlUtility.getGson().toJson(formData);
                    writer.write(json);
                } catch (final IOException e) {
                    //on i/o exception
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            dismissProgressDialog();
                            showShortToast(e.getMessage());
                        }
                    });
                    e.printStackTrace();
                }
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action() {
                    @Override
                    public void run() {
                        //on successfully saved
                        dismissProgressDialog();
                        checkImagesStatus();
//                        startFormFillActivity();

                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable e) {
                        //on any exception/error
                        dismissProgressDialog();
                        showShortToast(e.getMessage());
                    }
                });

        mDisposable.add(disposable);
    }

    //check if images have been updated in server
    private void checkImagesStatus() {
        this.imageUrlList = formData.getPngImageUrls();
        imageIndex = 0;
        if(formData.getIsImagesUpdated()){
            if(imageUrlList == null || imageUrlList.isEmpty()){
                showShortToast("Empty image url list");
                return;
            }

            downLoadAndSaveImageInFolder(imageUrlList.get(imageIndex));
        }
        else {
            //making sure all images are saved in internal storage
            //create a tempList of missing images file's url
            List<String> tempList = new ArrayList<>();
            for(String url: imageUrlList){
                String imageFileName = url.substring(url.lastIndexOf('/') + 1);
                if(!isFormImageFileExist(imageFileName)){
                    tempList.add(url);
                }
            }
            if(tempList.isEmpty()){
            //all images already exists in folder, Proceed to FormFillActivity
                startFormFillActivity();
            }
            else {
                imageUrlList = tempList;
                downLoadAndSaveImageInFolder(imageUrlList.get(imageIndex));
            }

            formData.getPngImageUrls().clear();

        }
    }

    private void downLoadAndSaveImageInFolder(String imageUrl){
        downloadImageTask = new DownloadImageTask(MainActivity.this, downloadImageListener);
        downloadImageTask.execute(imageUrl);
        int imageNo  = imageIndex + 1;
        String title;
        if(isPreviewMode){
            title = "Downloading preview image "+ imageNo + " of "+ imageUrlList.size();
        }
        else {
            title = "Downloading image "+ imageNo + " of "+ imageUrlList.size();
        }
        showProgressDialog(title);
    }




    @SuppressWarnings("unused")
    public static class DownloadImageTask extends AsyncTask<String, Void, Bitmap>{

        private WeakReference<MainActivity> activityReference;
        private DownloadImageListener listener;


        DownloadImageTask(MainActivity activity, DownloadImageListener listener){
            this.activityReference = new WeakReference<>(activity);
            this.listener = listener;
        }

        @Override
        protected Bitmap doInBackground(String... url) {
            Bitmap bm = null;
            try {

                CertificateFactory cf = CertificateFactory.getInstance("X.509");

                KeyStore keyStore;
                Certificate ca;
                try (InputStream caInput = activityReference.get().getResources().openRawResource(
                        activityReference.get().getResources().getIdentifier("selfsigned",
                                "raw", activityReference.get().getPackageName()))) {
                    String keyStoreType = KeyStore.getDefaultType();
                    keyStore = KeyStore.getInstance(keyStoreType);
                    ca = cf.generateCertificate(caInput);
                }

                keyStore.load(null, null);
                keyStore.setCertificateEntry("ca", ca);

                String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
                TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
                tmf.init(keyStore);

                SSLContext context = SSLContext.getInstance("TLSv1.2");
                context.init(null, tmf.getTrustManagers(), null);
                javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory   (context.getSocketFactory());


                URL url1 = new URL(url[0]);
                HttpsURLConnection urlConnection = (HttpsURLConnection) url1.openConnection();

                urlConnection.setSSLSocketFactory(context.getSocketFactory());

                urlConnection.connect();
                InputStream is = urlConnection.getInputStream();
                BufferedInputStream bis = new BufferedInputStream(is);
                bm = BitmapFactory.decodeStream(bis);
                bis.close();
                is.close();
//
//                URL aURL = new URL(url[0]);
//                URLConnection conn = aURL.openConnection();
//                conn.connect();
//                InputStream is = conn.getInputStream();
//                BufferedInputStream bis = new BufferedInputStream(is);
//                bm = BitmapFactory.decodeStream(bis);
//                bis.close();
//                is.close();

            } catch (Exception e) {
                listener.onFailure(e.getMessage());
                e.printStackTrace();
            }
            return bm;
        }


        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            if(listener != null){
                if(bitmap!= null)
                    listener.onSuccess(bitmap);
                else
                    listener.onFailure("bitmap null");
            }

        }
    }

    private final DownloadImageListener downloadImageListener = new DownloadImageListener() {
        @Override
        public void onSuccess(final Bitmap bitmap) {
            try {
                downloadImageTask.cancel(true);
                downloadImageTask = null;

                //save image image asynchronously: run()
                Disposable d = Completable.fromAction(new Action() {
                    @Override
                    public void run() throws Exception {

                        String url = imageUrlList.get(imageIndex);
                        if(TextUtils.isEmpty(url)){
                            throw new NullPointerException("Url not found");
                        }

                        String imageName = url.substring(url.lastIndexOf('/') + 1);
                        File imageFile = new File(getFormImagesFilesDirectory(), imageName);
                        FileOutputStream fos = new FileOutputStream(imageFile);
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                        fos.flush();
                        fos.close();
                    }
                }).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Action() {
                            @Override
                            public void run() {
                                //on image saving successful
                                if(imageIndex < imageUrlList.size()-1) {
                                    imageIndex ++;
                                    downLoadAndSaveImageInFolder(imageUrlList.get(imageIndex));
                                }
                                else {
                                    dismissProgressDialog();
                                    startFormFillActivity();
                                }
                            }
                        }, new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable e) {
                                showShortToast(e.getMessage());
                            }
                        });

                mDisposable.add(d);
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        public void onFailure(String msg) {
            dismissProgressDialog();
            showShortToast(msg);
        }
    };


    private void startFormFillActivity(){
        MainActivity.this.startActivity(new Intent(MainActivity.this, FormFillActivity.class)
                .putExtra(IS_PREVIEW_MODE, isPreviewMode));
        MainActivity.this.finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(!mDisposable.isDisposed())
            mDisposable.dispose();
    }


}
