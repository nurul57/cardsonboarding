package android.form.avss.prepaidcard.web_services.responses;

import com.google.gson.annotations.SerializedName;

public class PrefixMasterResponse {
    @SerializedName("da")
    public String data;

    public static class PrefixMaster  extends BaseResponseList<PrefixMaster> {
        @SerializedName("id")
        public String id;
        @SerializedName("title")
        public String title;
        @SerializedName("maleStatus")
        public String isMaleStatus;
        @SerializedName("femaleStatus")
        public String isFemaleStatus;
        @SerializedName("maidenStatus")
        public String isMaidenStatus;
        @SerializedName("fatherStatus")
        public String isFatherStatus;
        @SerializedName("motherStatus")
        public String isMotherStatus;
        @SerializedName("isActive")
        public String isActive;
        @SerializedName("fullName")
        public String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getIsMaleStatus() {
            return isMaleStatus;
        }

        public void setIsMaleStatus(String isMaleStatus) {
            this.isMaleStatus = isMaleStatus;
        }

        public String getIsFemaleStatus() {
            return isFemaleStatus;
        }

        public void setIsFemaleStatus(String isFemaleStatus) {
            this.isFemaleStatus = isFemaleStatus;
        }

        public String getIsMaidenStatus() {
            return isMaidenStatus;
        }

        public void setIsMaidenStatus(String isMaidenStatus) {
            this.isMaidenStatus = isMaidenStatus;
        }

        public String getIsFatherStatus() {
            return isFatherStatus;
        }

        public void setIsFatherStatus(String isFatherStatus) {
            this.isFatherStatus = isFatherStatus;
        }

        public String getIsMotherStatus() {
            return isMotherStatus;
        }

        public void setIsMotherStatus(String isMotherStatus) {
            this.isMotherStatus = isMotherStatus;
        }

        public String getIsActive() {
            return isActive;
        }

        public void setIsActive(String isActive) {
            this.isActive = isActive;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
