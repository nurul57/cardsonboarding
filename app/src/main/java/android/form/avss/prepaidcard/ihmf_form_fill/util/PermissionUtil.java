package android.form.avss.prepaidcard.ihmf_form_fill.util;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Ankit Sharma on 13-11-2017.
 * <p>
 * This class is used to request/handle dangerous permission at runtime.
 * </p>
 */
public abstract class PermissionUtil {

    public static final int REQUEST_STORAGE_PERMISSION = 88;
    public static final int REQUEST_LOCATION_PERMISSION = 90;
    public static final int REQUEST_CALL_PHONE_PERMISSION = 92;
    public static final int REQUEST_CAMERA_PERMISSION = 94;
    public static final int REQUEST_PHONE_STATE_PERMISSION = 96;
    public static final int REQUEST_MULTIPLE_PERMISSION = 12;
    private static final String FILE_NAME = "permissions";
    private static Callback mCallback = null;

    private static int mRequestCode = -1;

    public static void requestPermission(Activity activity, String[] permissions, int requestcode) {
        mRequestCode = requestcode;
        ActivityCompat.requestPermissions(activity, permissions, requestcode);

    }

    public static void requestPermission(Fragment fragment, String[] permissions, int requestcode) {
        mRequestCode = requestcode;
        fragment.requestPermissions(permissions, requestcode);
    }

    /**
     * @param context    - Context of activity
     * @param permission - Permission on which you want to validateUser
     * @return true if permission is granted , otherwise false
     */
    public static boolean hasPermission(Context context, String permission) {
        return greaterEqualToM() && isPermissionGranted(ContextCompat.checkSelfPermission(context, permission));
    }


    private static boolean isPermissionGranted(int permission) {
        return permission == PackageManager.PERMISSION_GRANTED;
    }

    private static void setPermissionFlag(@NonNull SharedPreferences sharedPreferences, String permission, boolean value) {

        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putBoolean(permission, value);
        editor.commit();
    }

    /**
     * @param activity   - instance of activity
     * @param permission - Permission on which you want to validateUser
     * @return true if permission is granted , otherwise false
     */
    public static void hasPermission(@NonNull Activity activity, String permission, int requestCode, @NonNull Callback callback) {
        mCallback = callback;

        if (greaterEqualToM()) {
            int permissioncheck = ContextCompat.checkSelfPermission(activity, permission);


            if (isPermissionGranted(permissioncheck))
                callback.onPermissionGranted();
            else if (activity.shouldShowRequestPermissionRationale(permission) || callback.makeFirstRequest(activity, permission)) {

                if (!callback.permissionRequest(permission, requestCode))
                    requestPermission(activity, new String[]{permission}, requestCode);

            } else
                callback.onPermissionBlocked();

        } else
            callback.onPermissionGranted();

    }




    /**
     * @param activity   - instance of activity
     * @param permissions - Permission on which you want to validateUser
     * @return true if permission is granted , otherwise false
     */
    public static void hasPermission(@NonNull Activity activity, String[] permissions, int requestCode, @NonNull Callback callback) {
        mCallback = callback;


        if (greaterEqualToM()) {


            boolean granted = true;

            for (String permission : permissions) {

                int selfPermission = ContextCompat.checkSelfPermission(activity, permission);

                if (!isPermissionGranted(selfPermission)) {
                    granted = false;
                    break;
                }
            }



            if (granted)
            {
                callback.onPermissionGranted();
                return;
            }


            boolean request = true;
            for(String p : permissions)
            {

                boolean firstCall = true;

                boolean isRationale  = activity.shouldShowRequestPermissionRationale(p);

                if(isRationale) continue;

               firstCall = callback.makeFirstRequest(activity, p);



                if(!firstCall) {
                    request = false;
                    break;
                }

                if(!isRationale && !firstCall){


                }

            }




// for each permission....
// is Asked - > false then , isFirst
//            is Asked -> true -> Proceed
//            is Asked -> false, isFirst -> false -> Blocked
            if (request) {

//                if (!callback.permissionRequest(permission, requestCode))
                    requestPermission(activity, permissions, requestCode);

            } else
                callback.onPermissionBlocked();

        } else
            callback.onPermissionGranted();

    }

    /**
     * @param fragment   - instance of fragment
     * @param permission - Permission on which you want to validateUser
     * @return true if permission is granted , otherwise false
     */

    public static void hasPermission(@NonNull Fragment fragment, String permission, int requestcode, @NonNull Callback callback) {

        if (fragment.getContext() == null) return;

        mCallback = callback;
        if (greaterEqualToM()) {
            int permissionCheck = ContextCompat.checkSelfPermission(fragment.getContext(), permission);

            boolean granted = isPermissionGranted(permissionCheck);

            if (granted)
                callback.onPermissionGranted();
            else if (fragment.shouldShowRequestPermissionRationale(permission) || callback.makeFirstRequest(fragment.getContext(), permission)) {
                if (!callback.permissionRequest(permission, requestcode))
                    requestPermission(fragment, new String[]{permission}, requestcode);
            } else {
                AlertDialog alertDialog = showSettingDialog(fragment.getContext(), "Alert", "Permission");
                alertDialog.show();
            }

        } else
            callback.onPermissionGranted();

    }


    /**
     * This method must be called at last/end  of onRequestPermissionResult() method.
     */
    public static void clean(){

        mCallback = null;
    }

    /**
     * This method is used to request multiple permissions at once.No Setting Dialog will be shown.
     * Generally used to gather require permission at once which later can be validateUser individually which user denied for permission.
     *
     * @param activity    - Instance of activity.
     * @param permissions - List of Permissions to be requested.
     * @param requestCode - Request Code for list of permission.
     * @param callback    - Callback as action to permission on permission granted.
     */
    public static void requestPermissions(@NonNull Activity activity, @NonNull String[] permissions, int requestCode, @NonNull Callback callback) {
        mCallback = callback;

        if (greaterEqualToM()) {

            boolean granted = true;

            for (String permission : permissions) {

                int selfPermission = ContextCompat.checkSelfPermission(activity, permission);

                if (!isPermissionGranted(selfPermission)) {
                    granted = false;
                    break;
                }
            }

            if (granted)
                callback.onPermissionGranted();
            else if(!isAnyoneBlocked(activity,permissions,callback))
                requestPermission(activity, permissions, requestCode);
            else
                callback.onPermissionBlocked();
// TODO: Need to handle permission block case

        } else
            callback.onPermissionGranted();
    }

    private static boolean isAnyoneBlocked(Activity activity, String[] permissions,Callback callback) {


        if(permissions == null ||permissions.length == 0) return false;

        if(!greaterEqualToM())  return true;

        boolean isBlocked = false;
        for(String permission :permissions){

            if(hasPermission(activity,permission)) continue;

            boolean canShow = activity.shouldShowRequestPermissionRationale(permission) ;

            if(canShow) continue;

            boolean isfirst = callback.makeFirstRequest(activity,permission);

            if(!isfirst && canShow == isfirst){
                isBlocked = true;
                break;
            }

        }


        return isBlocked ;
    }


    /**
     * This method is used to request multiple permissions at once.No Setting Dialog will be shown.
     * Generally used to gather require permission at once which later can be validateUser individually which user denied for permission.
     *
     * @param fragment    - Instance of activity.
     * @param permissions - List of Permissions to be requested.
     * @param requestCode - Request Code for list of permission.
     * @param callback    - Callback as action to permission on permission granted.
     */
    public static void requestPermissions(@NonNull Fragment fragment, @NonNull String[] permissions, int requestCode, @NonNull Callback callback) {
        mCallback = callback;
        if (fragment.getContext() == null) return;
        if (greaterEqualToM()) {

            boolean granted = true;

            for (String permission : permissions) {

                int selfPermission = ContextCompat.checkSelfPermission(fragment.getContext(), permission);

                if (!isPermissionGranted(selfPermission)) {
                    granted = false;
                    break;
                }
            }

            if (granted)
                callback.onPermissionGranted();
            else
                requestPermission(fragment, permissions, requestCode);
// TODO: Need to handle permission block case

        } else
            callback.onPermissionGranted();
    }


    public static boolean greaterEqualToM() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }

    public static AlertDialog showSettingDialog(final Context context, String title, String message) {
        return new AlertDialog.Builder(context).setTitle(title)
                .setMessage(message)
                .setCancelable(true)
                .setPositiveButton("Setting", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        setupSettingIntent(context);
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();
    }



    public static AlertDialog alert(Context ctx){

//        View v =LayoutInflater.from(ctx).inflate(R.layout.permission_layout,null,false);
        final AlertDialog.Builder builder = new AlertDialog.Builder(ctx).setCancelable(false);//.setView(v);

        builder
                .setMessage("Please grant permission to  access system resource.")
                .setTitle("Permission Required")
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        final AlertDialog dialog = builder.create();
        /*v.findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dialog.isShowing())
                    dialog.dismiss();
            }
        });*/

        return dialog;
    }

//================ ACTIVITY PERMISSION REQUEST ===================================

    public static void setupSettingIntent(Context context) {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.setData(Uri.fromParts("package", context.getPackageName(), null));
//        mAlertDialog.dismiss();
        context.startActivity(intent);
    }

    /**
     * AlertDialog which consist custom view and not cancelable so that user can handle by it's own.
     * But no
     *
     * @param context
     * @param customView - View to be display on Setting Dialog
     * @return Instance of Alert Dialog
     */
    public static AlertDialog showSettingDialog(@NonNull Context context, @NonNull View customView) {

        return new AlertDialog.Builder(context).setView(customView).setCancelable(false).create();
    }

    public static void requestCallPhonePermission(Activity activity) {
        requestPermission(activity, new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL_PHONE_PERMISSION);
    }

    public static void requestLocationPermission(Activity activity) {
        requestPermission(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_LOCATION_PERMISSION);
    }

    public static void requestCameraPermission(Activity activity) {
        requestPermission(activity, new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
    }
    //================ ACTIVITY PERMISSION REQUEST ===================================


//================ FRAGMENT PERMISSION REQUEST ===================================

    public static void requestStoragePermission(Activity activity) {

        requestPermission(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_STORAGE_PERMISSION);
    }

    public static void requestReadPhoneStatePermission(Activity activity) {
        requestPermission(activity, new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_PHONE_STATE_PERMISSION);
    }

    public static void requestCallPhonePermission(Fragment fragment, int requestcode) {
        requestPermission(fragment, new String[]{Manifest.permission.CALL_PHONE}, requestcode);
    }

    public static void requestCallPhonePermission(Fragment fragment) {
        requestCallPhonePermission(fragment, REQUEST_CALL_PHONE_PERMISSION);
    }

    public static void requestCameraPermission(Fragment fragment, int requestcode) {
        requestPermission(fragment, new String[]{Manifest.permission.CAMERA}, requestcode);
    }

    public static void requestCameraPermission(Fragment fragment) {
        requestCameraPermission(fragment, REQUEST_CAMERA_PERMISSION);
    }

    public static void requestStoragePermission(Fragment fragment, int requestcode) {

        requestPermission(fragment, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, requestcode);
    }

    public static void requestStoragePermission(Fragment fragment) {

        requestStoragePermission(fragment, REQUEST_STORAGE_PERMISSION);
    }

    public static void requestReadPhoneStatePermission(Fragment fragment, int requestcode) {
        requestPermission(fragment, new String[]{Manifest.permission.READ_PHONE_STATE}, requestcode);
    }

    public static void requestReadPhoneStatePermission(Fragment fragment) {
        requestReadPhoneStatePermission(fragment, REQUEST_PHONE_STATE_PERMISSION);
    }

    //================ FRAGMENT PERMISSION REQUEST ===================================

    public static void requestLocationPermission(Fragment fragment, int requestcode) {
        requestPermission(fragment, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, requestcode);
    }

    public static void requestLocationPermission(Fragment fragment) {
        requestLocationPermission(fragment, REQUEST_LOCATION_PERMISSION);
    }

    //=============== FRAGMENT PERMISSION REQUEST ==============================

    private static boolean getPermissionFlag(@NonNull Context context, String permission) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);

        if (!sharedPreferences.contains(permission)) {
            // Write permission for first time and return true for first request
            setPermissionFlag(sharedPreferences, permission, false);
            return true;
        }

        return sharedPreferences.getBoolean(permission, true);

    }

    public static CallbackResult getLastPermissionHandler(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResult) {

//        if (mCallback == null || permissions == null || grantResult == null || mRequestCode != requestCode)
        if (mCallback == null || mRequestCode != requestCode)
            return null;

        return new CallbackResult(permissions, grantResult);

    }



    public static abstract class Callback {


        private boolean makeFirstRequest(@NonNull Context context, @NonNull String permission) {

            return getPermissionFlag(context, permission);

        }

        public abstract void onPermissionGranted();

        public boolean permissionRequest(String permission, int requestcode) {
            return false;
        }

        /**
         * <p>This method return pending request array from last request made by application.</p>
         *
         * @param permissions  - List of requested permissions .
         * @param grantResults - Array passed by system as permission result.
         * @return This can be null or array of String which represent pending permission.
         */
        private String[] pendingPermissions(String[] permissions, int[] grantResults) {


            // =====Checking permission and extract to array form====
            List<String> list = new ArrayList<>(permissions.length);
            String[] neededPermissions = null;

            for (int i = 0; i < permissions.length; i++)
                if (grantResults[i] != PackageManager.PERMISSION_GRANTED)
                    list.add(permissions[i]);

            if (list.size() != 0) {
                neededPermissions = new String[list.size()];
                list.toArray(neededPermissions);
            }

            // ===========================================================

            return neededPermissions;
        }

        public abstract void onPermissionBlocked();

        /*
         Context context,String title,String msg) {
            AlertDialog alertDialog = PermissionUtils.showSettingDialog(context, "Alert", "Permission");

            alertDialog.show();
        }*/
    }

    public final static class CallbackResult {


        private String[] permissions;
        private int[] grantResult;


        CallbackResult(String[] permissions, int[] grantResult) {
            this.permissions = permissions;
            this.grantResult = grantResult;
        }


        public final void onPermissionGranted() {
            if (mCallback != null)
                mCallback.onPermissionGranted();
        }

        public final boolean isPermissionsGranted() {
            return mCallback != null && mCallback.pendingPermissions(permissions, grantResult) == null;

        }

        public final String[] pendingPermissions() {
            
            return mCallback == null ? null : mCallback.pendingPermissions(permissions, grantResult);
        }
    }
}

