package android.form.avss.prepaidcard.web_services;

/*
    Authorization: Bearer 12345


*/

public class AccessToken {
    private String accesToken;
    private String tokenType;

    public String getAccesToken() {
        return accesToken;
    }

    public String getTokenType() {
        // OAuth requires uppercase Authorization HTTP header value for token type
        if (!Character.isUpperCase(tokenType.charAt(0))) {
            tokenType = Character.toString(tokenType.charAt(0))
                    .toUpperCase() + tokenType.substring(1);
        }
        return tokenType;
    }
}
