package android.form.avss.prepaidcard.ui.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.form.avss.prepaidcard.R;
import android.form.avss.prepaidcard.helper_classes.Constant;
import android.form.avss.prepaidcard.ui.custom_classes.CustomTextInputLayout;
import android.form.avss.prepaidcard.ui.model.CardDetailViewModel;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.Arrays;
import java.util.Date;


public class CardDetailsActivity extends BaseActivity implements View.OnClickListener {
    private String bdeNo;
    private String token;
    private boolean isNewOrExistingCustomer;

    ImageView btnNext;
    ImageView btnPrevious;
    CustomTextInputLayout tvDate, tvCardNumber, tvCompanyName, tvCardRefNumber, tvCardType, tvProductCode;
    Button btnValidate;
    RadioGroup rgInstaType, rgCompany;
    String[] sCardType = {"SMART PAY", "MEAL", "REWARDS"};
    String[] sProductCode = {"101","102","103"};
    CardDetailViewModel model;
    Intent intent;

    TextView tvAutoFillData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_details);
        model = new CardDetailViewModel();
        init();

        intent = getIntent();
        isNewOrExistingCustomer = intent.getBooleanExtra(IS_NEW_CUSTOMER,false);
        bdeNo = intent.getStringExtra(BDE_NO);
        token = intent.getStringExtra(TOKEN);
    }

    public void init(){
        btnNext = (ImageView) findViewById(R.id.btn_next);
        btnPrevious = (ImageView) findViewById(R.id.btn_previous);
        btnValidate = (Button) findViewById(R.id.btn_validate);
        tvDate = (CustomTextInputLayout) findViewById(R.id.tvDate);
        tvCardNumber = (CustomTextInputLayout) findViewById(R.id.tvCardNumber);
        tvCompanyName = (CustomTextInputLayout) findViewById(R.id.tvCompanyName);
        tvCardRefNumber = (CustomTextInputLayout) findViewById(R.id.tvCardPackRefNum);
        rgInstaType = (RadioGroup) findViewById(R.id.rgInsta_NonInsta);
        rgCompany = (RadioGroup) findViewById(R.id.rgCompanyYesNo);
        tvCardType = (CustomTextInputLayout) findViewById(R.id.tvCardType);
        tvProductCode = (CustomTextInputLayout) findViewById(R.id.tvProductCode);
        tvAutoFillData = (TextView) findViewById(R.id.tvAutoFill);


        getDefaultSystemDate();
        setListeners();
        setFocusChangeListener();
        setAdapter();
        setOnCheckedChangeListener();
        setInputFilterOnFields();
        tvCardNumber.setVisibility(View.VISIBLE);
        rgCompany.setEnabled(false);
        rgInstaType.setEnabled(false);
    }

    private void setInputFilterOnFields() {
        tvCardNumber.setLengthInputFilter(16);
        tvCompanyName.setCharacterInputFilter();
        tvCardRefNumber.setLengthInputFilter(16);
    }

    private void setFocusChangeListener() {
        tvCardNumber.setFocusChangeListener();
        tvCompanyName.setFocusChangeListener();
        tvCardType.setFocusChangeListener();
        tvProductCode.setFocusChangeListener();
        tvCardRefNumber.setFocusChangeListener();
    }

    private void setOnCheckedChangeListener() {

        rgInstaType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (radioGroup.getCheckedRadioButtonId() == RadioGroup.NO_ID) return;
                if (radioGroup.getCheckedRadioButtonId() == R.id.rbInsta) {
                    model.setCompany(((RadioButton)rgCompany.findViewById(R.id.rbInsta)).getText().toString().replaceAll("-", " "));
                } else {
                    model.setCompany(((RadioButton)rgCompany.findViewById(R.id.rbNonInsta)).getText().toString().replaceAll("-", " "));
                }
            }
        });

        rgCompany.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (radioGroup.getCheckedRadioButtonId() == RadioGroup.NO_ID) return;
                if (radioGroup.getCheckedRadioButtonId() == R.id.rbYes) {
                    model.setCompany(((RadioButton)rgCompany.findViewById(R.id.rbYes)).getText().toString());
                } else {
                    model.setCompany(((RadioButton)rgCompany.findViewById(R.id.rbNo)).getText().toString());
                }
            }
        });
    }

    private void setAdapter() {
        tvCardType.setAdapter(Arrays.asList(sCardType));
        tvProductCode.setAdapter(Arrays.asList(sProductCode));
    }

    public void getDefaultSystemDate(){
        tvDate.setText(format.format(new Date()));
    }

    public void setListeners(){
        btnValidate.setOnClickListener(this);
        btnNext.setOnClickListener(this);
        btnPrevious.setOnClickListener(this);
        tvAutoFillData.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_validate:

                break;
            case R.id.btn_next:
                if (!validateMandatoryFields()) {
                    toast(R.string.err_msg_mandatory_fields);
                    return;
                }

                setData();

                String finalCardDetailsData = Constant.getInstance().toJson(model);
                Intent intent = new Intent(CardDetailsActivity.this, EkycOptionActivity.class);
                intent.putExtra(CARD_DATA, finalCardDetailsData);
                intent.putExtra(TOKEN, token);
                intent.putExtra(BDE_NO, bdeNo);
                startActivity(intent);
                break;

            case R.id.btn_previous:
                final AlertDialog alertDialog = new AlertDialog.Builder(CardDetailsActivity.this)
                        .setTitle(R.string.alert)
                        .setMessage(R.string.data_loss)
                        .setCancelable(true)
                        .create();

                alertDialog.setButton(
                        DialogInterface.BUTTON_POSITIVE, "CONTINUE",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                alertDialog.dismiss();
                                finish();
                            }
                        });

                alertDialog.setButton(
                        DialogInterface.BUTTON_NEGATIVE, "CANCEL",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                alertDialog.dismiss();
                            }
                        });

                alertDialog.show();

                break;

            case R.id.tvAutoFill:
                autoFillData();
                break;
        }
    }

    private void autoFillData() {
        tvCardNumber.setText("12345");
        tvCompanyName.setText("AVSS");
        tvCardType.setText("MEAL");
        tvProductCode.setText("101");
        tvCardRefNumber.setText("123");


    }

    private boolean validateMandatoryFields() {
        boolean temp = true;

        if (tvDate.isFieldEmpty()) temp = false;
        if (tvCardNumber.isFieldEmpty()) temp = false;
        if (tvCompanyName.isFieldEmpty()) temp = false;
        if (tvCardType.isFieldEmpty()) temp = false;
        if (tvProductCode.isFieldEmpty()) temp = false;
        if (tvCardRefNumber.isFieldEmpty()) temp = false;

        return temp;
    }

    public void setData() {
        model.setIsExistingCustomer(isNewOrExistingCustomer ? "Y" : "N");
        model.setAppliedDate(tvDate.getString());
        model.setCardNumber(tvCardNumber.getString());
        model.setCompanyName(tvCompanyName.getString());
        model.setCardType(tvCardType.getString());
        model.setProductCode(tvProductCode.getString());
        model.setCardRefNumber(tvCardRefNumber.getString());
        if (rgInstaType.getCheckedRadioButtonId() == R.id.rbInsta) {
            model.setInstaType(getString(R.string.insta));
        } else {
            model.setInstaType(getString(R.string.non_insta).replaceAll("-", " "));
        }
        model.setCompany(rgCompany.getCheckedRadioButtonId() == R.id.rbYes ? "Y" : "N");
    }
}
