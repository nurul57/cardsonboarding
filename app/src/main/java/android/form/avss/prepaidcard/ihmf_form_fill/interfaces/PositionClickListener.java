package android.form.avss.prepaidcard.ihmf_form_fill.interfaces;

public interface PositionClickListener {
    void onPosClick(int pos);
}
