package android.form.avss.prepaidcard.ui.custom_classes;

import android.content.Context;
import android.form.avss.prepaidcard.R;
import android.support.annotation.StringRes;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;

public class CustomButton extends android.support.v7.widget.AppCompatButton {
    public CustomButton(Context context) {
        super(context);
        this.setTextColor(context.getResources().getColor(R.color.white));
        this.setAllCaps(true);
        this.setBackground(context.getDrawable(R.drawable.my_button_selector));
        this.setGravity(Gravity.CENTER);
        this.setTextAlignment(TEXT_ALIGNMENT_CENTER);

    }

    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTextColor(context.getResources().getColor(R.color.white));
        this.setAllCaps(true);
        this.setBackground(context.getDrawable(R.drawable.my_button_selector));
        this.setGravity(Gravity.CENTER);
        this.setTextAlignment(TEXT_ALIGNMENT_CENTER);
    }

    public CustomButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.setTextColor(context.getResources().getColor(R.color.white));
        this.setAllCaps(true);
        this.setBackground(context.getDrawable(R.drawable.my_button_selector));
        this.setGravity(Gravity.CENTER);
        this.setTextAlignment(TEXT_ALIGNMENT_CENTER);
    }


    public String getString() {
        return this.getText().toString().trim();
    }

    public void setTextOnButton(@StringRes int resId) {
        this.setText(resId);
    }

    public void setText(String text) {
        this.setText(text);
    }

    public boolean isEquals(String s) {
        if (TextUtils.isEmpty(s) || getContext() == null) return false;
        return this.getString().equals(s);
    }

    public boolean isEqualsIgnoreCase(String s) {
        if (TextUtils.isEmpty(s) || getContext() == null) return false;
        return s.equalsIgnoreCase(this.getString());
    }

    public boolean isEquals(@StringRes int resId) {
        if (getContext() == null) return false;
        return this.getString().equals(getContext().getResources().getString(resId));
    }

    public boolean isEqualsIgnoreCase(@StringRes int resId) {
        if (getContext() == null) return false;
        return getContext().getResources().getString(resId).equalsIgnoreCase(this.getString());
    }

}
