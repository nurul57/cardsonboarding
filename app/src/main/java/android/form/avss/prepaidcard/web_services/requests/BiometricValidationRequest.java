package android.form.avss.prepaidcard.web_services.requests;

import com.google.gson.annotations.SerializedName;

public class BiometricValidationRequest {
    @SerializedName("bn")
    public String bdeno;
    @SerializedName("did")
    public String deviceId;
    @SerializedName("ud")
    public String uId;
    @SerializedName("mno")
    public String mobileNo;
    @SerializedName("el")
    public String email;
    @SerializedName("ime")
    public String imeino;
    @SerializedName("lt")
    public String latitude;
    @SerializedName("lg")
    public String longitude;
    @SerializedName("kr")
    public String kycRequest;
    @SerializedName("pid")
    public String pid;
}
