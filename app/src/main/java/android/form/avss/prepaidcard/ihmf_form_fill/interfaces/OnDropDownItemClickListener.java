package android.form.avss.prepaidcard.ihmf_form_fill.interfaces;

public interface OnDropDownItemClickListener {

    void onClick(String data);
}
