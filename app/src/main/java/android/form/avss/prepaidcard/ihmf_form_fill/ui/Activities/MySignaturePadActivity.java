package android.form.avss.prepaidcard.ihmf_form_fill.ui.Activities;

import android.content.Intent;
import android.form.avss.prepaidcard.R;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.github.gcacace.signaturepad.views.SignaturePad;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by sapple on 4/18/2017.
 */

public class MySignaturePadActivity extends AppCompatActivity {
    public static final String PATH = "PATH";
    public static final String IMAGE_PATH = "IMAGE_PATH";
    String path;
    SignaturePad signaturePad;
    Button _cancelButton, _saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signature);

        signaturePad = (SignaturePad) findViewById(R.id.signature_pad);
        _cancelButton = (Button) findViewById(R.id.clear_button);
        _saveButton = (Button) findViewById(R.id.save_button);

        signaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
                signaturePad.setPenColor(Color.parseColor("#0033CC"));
            }

            @Override
            public void onSigned() {
                _cancelButton.setEnabled(true);
                _saveButton.setEnabled(true);
                _cancelButton.setBackgroundColor(Color.parseColor("#999E9E9E"));
                _saveButton.setBackgroundColor(Color.parseColor("#999E9E9E"));
                _cancelButton.setTextColor(Color.parseColor("#FF252222"));
                _saveButton.setTextColor(Color.parseColor("#FF252222"));
            }

            @Override
            public void onClear() {
                _cancelButton.setEnabled(false);
                _saveButton.setEnabled(false);
            }
        });

        _cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signaturePad.clear();
                _cancelButton.setEnabled(false);
                _saveButton.setEnabled(false);
                _cancelButton.setBackgroundColor(Color.WHITE);
                _saveButton.setBackgroundColor(Color.WHITE);
                _cancelButton.setTextColor(Color.parseColor("#FFAAAAAA"));
                _saveButton.setTextColor(Color.parseColor("#FFAAAAAA"));
            }
        });

        _saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = getIntent().getExtras();


                Intent _intent_Result = new Intent();
                    String signPathInFragment = getIntent().getStringExtra(PATH);
                    Bitmap bitmap = signaturePad.getTransparentSignatureBitmap();
                    try {
                        addSignatureToJPG(bitmap, signPathInFragment);
                    } catch (Exception e) {

                        setResult(RESULT_CANCELED, _intent_Result);
                        finish();
                        return;
                    }

                    _intent_Result.putExtra(PATH, signPathInFragment);

                    if(bundle != null)
                        _intent_Result.putExtras(bundle);

                    setResult(RESULT_OK, _intent_Result);
                    finish();
            }
        });
    }

    public void addSignatureToJPG(Bitmap bitmap, String photo) {
        try (
                FileOutputStream out = new FileOutputStream(photo)) {
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
