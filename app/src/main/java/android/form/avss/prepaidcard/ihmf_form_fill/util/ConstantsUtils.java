package android.form.avss.prepaidcard.ihmf_form_fill.util;

public class ConstantsUtils {

    public static final int RESIZE_TYPE_INCREASE = 0;
    public static final int RESIZE_TYPE_DECREASE = 1;


    public static final int IMAGE_PREVIEW = 101;


    public static final String SINGLE_CHOICE = "Single Choice";
    public static final String MULTIPLE_CHOICE = "Multiple Choice";
    public static final String SAME_DATA = "Same Data";
    public static final String IMAGE_PATH = "image_path";

    public static final String INVALID_DATA = "invalid data";







}
