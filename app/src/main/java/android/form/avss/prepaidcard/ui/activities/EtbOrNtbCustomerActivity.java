package android.form.avss.prepaidcard.ui.activities;

import android.content.Intent;
import android.form.avss.prepaidcard.R;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import static android.form.avss.prepaidcard.interfaces.Keys.BDE_NO;
import static android.form.avss.prepaidcard.interfaces.Keys.IS_NEW_CUSTOMER;
import static android.form.avss.prepaidcard.interfaces.Keys.TOKEN;

public class EtbOrNtbCustomerActivity extends AppCompatActivity implements View.OnClickListener {
    Button btnGetStarted, btnProceed;
    public boolean isNewCustomer = true;

    private String bdeno;
    private String token;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_etb_or_ntb_customer);

        bdeno = getIntent().getStringExtra(BDE_NO);
        token = getIntent().getStringExtra(TOKEN);

        btnGetStarted = (Button) findViewById(R.id.btnGetStarted);
        btnProceed = (Button) findViewById(R.id.btnProceed);

        btnProceed.setOnClickListener(this);
        btnGetStarted.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btnGetStarted){
            launchCardDetailsActivity();
        } if (id == R.id.btnProceed){
            launchCardDetailsActivity();
        }
    }

    public void launchCardDetailsActivity(){
        Intent intent = new Intent(EtbOrNtbCustomerActivity.this, CardDetailsActivity.class);
        intent.putExtra(IS_NEW_CUSTOMER, isNewCustomer);
        intent.putExtra(BDE_NO, bdeno);
        intent.putExtra(TOKEN, token);
        startActivity(intent);
    }
}
