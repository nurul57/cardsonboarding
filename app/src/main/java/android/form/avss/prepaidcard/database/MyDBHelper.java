package android.form.avss.prepaidcard.database;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.form.avss.prepaidcard.database.pojo.ServiceSession;
import android.form.avss.prepaidcard.utils.FileUtils;
import android.form.avss.prepaidcard.utils.PermissionUtils;
import android.form.avss.prepaidcard.web_services.responses.OVDDeemedOVDMasterResponse;
import android.form.avss.prepaidcard.web_services.responses.PrefixMasterResponse;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static android.form.avss.prepaidcard.utils.FileUtils.TAMPER_DB;
import static android.form.avss.prepaidcard.utils.FileUtils.getDBFolderPath;


public class MyDBHelper extends SQLiteOpenHelper {
    /*TODO : Database Name*/
    public static final String DATABASE_NAME = "NPSApp.db";

    /*TODO: database version*/
	private static final int DATABASE_VERSION = 1;

    //TODO: below are the tables and columns name identifier
    private static final String SESSION_TABLE_NAME = "session";
    private static final String SESSION_COLUMN_ID = "id";
    private static final String SESSION_COLUMN_KEY = "sessionKey";
    private static final String SESSION_COLUMN_FORM_ID = "referenceId";
    private static final String SESSION_COLUMN_TIMESTAMP = "timeStamp";

    private static final String DATA_TABLE_NAME = "storeData";
    private static final String DATA_COLUMN_FINAL_OBJECT = "finalObject";
    private static final String DATA_COLUMN_HELPER_OBJECT = "helperObject";
    private static final String DATA_COLUMN_MISCELLANEOUS = "miscellaneous";
    private static final String DATA_COLUMN_FORM_STATUS = "applicationStatus";
    private static final String DATA_COLUMN_FORM_ID = "referenceId";
	private static final String DATA_COLUMN_PRAN = "pranNumber";
	private static final String DATA_COLUMN_SECTIONS_STATUS = "sectionStatus";
	private static final String DATA_COLUMN_IMAGE_PATH = "imagePath";

    private static final String PREFIX_TABLE_NAME = "prefixData";
    private static final String PREFIX_ID_COLUMN_NAME = "prefixId";
    private static final String PREFIX_MALE_STATUS_COLUMN = "maleStatus";
    private static final String PREFIX_FEMALE_STATUS_COLUMN = "femaleStatus";
    private static final String PREFIX_MAIDEN_STATUS_COLUMN = "maidenStatus";
    private static final String PREFIX_FATHER_STATUS_COLUMN = "fatherStatus";
    private static final String PREFIX_MOTHER_STATUS_COLUMN = "motherStatus";
    private static final String PREFIX_IS_ACTIVE_COLUMN = "isActive";
    private static final String PREFIX_FULL_NAME_COLUMN = "fullName";


    private static final String GENDER_TABLE_NAME = "genderData";
    private static final String GENDER_COLUMN_NAME = "genderDataColumn";
    private static final String GENDER_COLUMN_CODE = "code";

    private static final String PROOF_OF_DOB_TABLE_NAME = "proofOfDataData";
    private static final String PROOF_OF_DOB_COLUMN_NAME = "proofOfDataColumn";
    private static final String PROOF_OF_DOB_COLUMN_CODE = "code";

	private static final String MARITAL_TABLE_NAME = "maritalData";
    private static final String MARITAL_COLUMN_NAME = "maritalDataColumn";
    private static final String MARITAL_COLUMN_CODE = "code";

    private static final String RESIDENT_TABLE_NAME = "residentData";
    private static final String RESIDENT_COLUMN_NAME = "residentDataColumn";
    private static final String RESIDENT_COLUMN_CODE = "code";

	private static final String RELATIONSHIP_TABLE_NAME = "relationshipData";
    private static final String RELATIONSHIP_COLUMN_NAME = "relationshipDataColumn";
    private static final String RELATIONSHIP_COLUMN_CODE = "code";

    private static final String OCCUPATION_TABLE_NAME = "occupationData";
    private static final String OCCUPATION_COLUMN_NAME = "occupationDataColumn";
    private static final String OCCUPATION_COLUMN_CODE = "code";

    private static final String INCOME_TABLE_NAME = "incomeData";
    private static final String INCOME_COLUMN_NAME = "incomeDataColumn";
    private static final String INCOME_COLUMN_CODE = "code";

	private static final String EDUCATION_TABLE_NAME = "educationData";
    private static final String EDUCATION_COLUMN_NAME = "educationDataColumn";
    private static final String EDUCATION_COLUMN_CODE = "code";

    private static final String CORRESPONDENCE_TABLE_NAME = "correspondenceData";
    private static final String CORRESPONDENCE_COLUMN_NAME = "correspondenceDataColumn";
    private static final String CORRESPONDENCE_COLUMN_CODE = "code";

    private static final String PERMANENT_TABLE_NAME = "permanentData";
    private static final String PERMANENT_COLUMN_NAME = "permanentDataColumn";
    private static final String PERMANENT_COLUMN_CODE = "code";

    private static final String POI_TABLE_NAME = "poiData";
    private static final String POI_COLUMN_NAME = "poiDataColumn";
    private static final String POI_COLUMN_CODE = "code";

    private static final String POA_TABLE_NAME = "poaData";
    private static final String POA_COLUMN_NAME = "poaDataColumn";
    private static final String POA_COLUMN_CODE = "code";

    private static final String PENSION_FUND_TABLE_NAME = "pensionFundData";
    private static final String PENSION_FUND_COLUMN_NAME = "pensionFundColumn";
    private static final String PENSION_FUND_COLUMN_CODE = "pensionFundCode";

	private static final String PIN_CODE_TABLE_NAME = "pinCodeData";
	private static final String PIN_CODE_COLUMN_NAME = "pinCodeColumn";
	private static final String PIN_CODE_COLUMN_CITY = "cityColumn";
	private static final String PIN_CODE_COLUMN_STATE = "stateColumn";
	private static final String PIN_CODE_COLUMN_STATE_CODE = "stateCode";

	private static final String BIRTH_CITY_TABLE_NAME = "foreignCity";
	private static final String BIRTH_CITY_COLUMN_NAME = "city";
	private static final String BIRTH_CITY_COLUMN_CODE = "code";

	private static final String BIRTH_COUNTRY_TABLE_NAME = "foreignCountry";
	private static final String BIRTH_COUNTRY_COLUMN_NAME = "country";
	private static final String BIRTH_COUNTRY_COLUMN_CODE = "countryCode";

	private static final String SCRUTINY_REJECTION_TABLE_NAME = "scrutinyTable";
	private static final String SCRUTINY_FORM_ID = "referenceId";
	private static final String SCRUTINY_DATA = "scrutinyData";


	private static final String DOC_PROOF_TABLE_NAME = "docProofTable";
	private static final String DOC_PROOF_NAME = "docProofName";
	private static final String DOC_PROOF_CODE = "docProofCode";
	private static final String DOC_PROOF_ID_PROOF = "docIdProof";
	private static final String DOC_PROOF_ADD_PROOF = "docAddProof";

	private static final String DROP_DOWN_TABLE_NAME = "dropDown";
	private static final String DD_LABEL_ID = "ddLabelId";
	private static final String DROP_DOWN_DATA = "dropDownData";

    private static final String TEMP_TABLE_NAME = "replicaTable";

	// TODO: 08/06/2018 work in progress in db changes
	private static final String SESSION_COLUMN_LOG = "log";
	private static final String SESSION_COLUMN_LOG1 = "log1";

	private ArrayList<String> sessionColumnList = new ArrayList<>();
	private ArrayList<String> dataColumnList = new ArrayList<>();
	private ArrayList<String> prefixColumnList = new ArrayList<>();
	private ArrayList<String> proofOfDobColumnList = new ArrayList<>();
	private ArrayList<String> genderColumnList = new ArrayList<>();
	private ArrayList<String> maritalColumnList = new ArrayList<>();
	private ArrayList<String> relationshipColumnList = new ArrayList<>();
	private ArrayList<String> residentColumnList = new ArrayList<>();
	private ArrayList<String> occupationColumnList = new ArrayList<>();
	private ArrayList<String> incomeColumnList = new ArrayList<>();
	private ArrayList<String> educationColumnList = new ArrayList<>();
	private ArrayList<String> correspondenceColumnList = new ArrayList<>();
	private ArrayList<String> permanentColumnList = new ArrayList<>();
	private ArrayList<String> poiColumnList = new ArrayList<>();
	private ArrayList<String> poaColumnList = new ArrayList<>();
	private ArrayList<String> pensionFundColumnList = new ArrayList<>();
	private ArrayList<String> pinCodeColumnList = new ArrayList<>();
	private ArrayList<String> birthCityColumnList = new ArrayList<>();
	private ArrayList<String> birthCountryColumnList = new ArrayList<>();
	private ArrayList<String> scrutinyRejectionColumnList = new ArrayList<>();
	private ArrayList<String> docProofColumnList = new ArrayList<>();
	private ArrayList<String> dropDownList = new ArrayList<>();

    private static MyDBHelper instance;
//	private Constant constant;

	private MyDBHelper(Context context) {
		super(new DataBaseContext(context), DATABASE_NAME, null, DATABASE_VERSION);
	}

	public static synchronized MyDBHelper getInstance(Context context) {
		if (instance == null) {
			instance = new MyDBHelper(context);
		} else if (!isDbFileExist(context)) {
            instance = null;
            instance = new MyDBHelper(context);
		}
		return instance;
	}

	public static synchronized boolean isDbFileExist(Context ctx) {
		if (ctx == null) return false;

		boolean isValid;

		SharedPreferences sharedPreferences = ctx.getSharedPreferences(FileUtils.DB_INFO, Context.MODE_PRIVATE);
		if (!sharedPreferences.contains(MyDBHelper.DATABASE_NAME)) {
			sharedPreferences.edit().putBoolean(MyDBHelper.DATABASE_NAME, true).commit();
			return true;
		}
		else if(sharedPreferences.getBoolean(TAMPER_DB,false)) {
			sharedPreferences.edit().putBoolean(TAMPER_DB, false).commit();
			return false;
		} else {
			File dbFile = new File(getDBFolderPath(), MyDBHelper.DATABASE_NAME);
			isValid = sharedPreferences.getBoolean(MyDBHelper.DATABASE_NAME, false) && dbFile.exists();

			if (!isValid && !sharedPreferences.contains(TAMPER_DB)) {
				sharedPreferences.edit().putBoolean(TAMPER_DB, true).commit();
			}
		}
		return isValid;
	}

	public static void removeTamper(Context ctx) {
		if (ctx == null) return;
		ctx.getSharedPreferences(FileUtils.DB_INFO, Context.MODE_PRIVATE).edit()
				.remove(TAMPER_DB)
				.remove(MyDBHelper.DATABASE_NAME)
				.commit();
	}


	public static boolean isPermissionGrantedToDb(Context context) {
		if (context == null) return false;
		getInstance(context);
		return PermissionUtils.isGranted(context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
				Manifest.permission.WRITE_EXTERNAL_STORAGE});
	}

	public static boolean isAppDbCreated(Context context) {
		if (context == null) return false;
		getInstance(context);
		return isPermissionGrantedToDb(context) && new File(FileUtils.getDBFolderPath(), DATABASE_NAME).exists();
	}

	public static boolean isAppDbCreated() {
		return new File(FileUtils.getDBFolderPath(), DATABASE_NAME).exists();
	}

    @Override
    public void onCreate (SQLiteDatabase db) {
		try {
    		listOfAllTables();
			switch (DATABASE_VERSION) {
				case 1:
					db.execSQL(createTableQuery(DROP_DOWN_TABLE_NAME, dropDownList));
					db.execSQL(createTableQuery(SESSION_TABLE_NAME, sessionColumnList));
					db.execSQL(createTableQuery(DATA_TABLE_NAME, dataColumnList));
					db.execSQL(createTableQuery(PREFIX_TABLE_NAME, prefixColumnList));
					db.execSQL(createTableQuery(PROOF_OF_DOB_TABLE_NAME, proofOfDobColumnList));
					db.execSQL(createTableQuery(GENDER_TABLE_NAME, genderColumnList));
					db.execSQL(createTableQuery(MARITAL_TABLE_NAME, maritalColumnList));
					db.execSQL(createTableQuery(RESIDENT_TABLE_NAME, residentColumnList));
					db.execSQL(createTableQuery(RELATIONSHIP_TABLE_NAME, relationshipColumnList));
					db.execSQL(createTableQuery(OCCUPATION_TABLE_NAME, occupationColumnList));
					db.execSQL(createTableQuery(INCOME_TABLE_NAME, incomeColumnList));
					db.execSQL(createTableQuery(EDUCATION_TABLE_NAME, educationColumnList));
					db.execSQL(createTableQuery(CORRESPONDENCE_TABLE_NAME, correspondenceColumnList));
					db.execSQL(createTableQuery(PERMANENT_TABLE_NAME, permanentColumnList));
					db.execSQL(createTableQuery(POI_TABLE_NAME, poiColumnList));
					db.execSQL(createTableQuery(POA_TABLE_NAME, poaColumnList));
					db.execSQL(createTableQuery(PENSION_FUND_TABLE_NAME, pensionFundColumnList));
					db.execSQL(createTableQuery(PIN_CODE_TABLE_NAME, pinCodeColumnList));
					db.execSQL(createTableQuery(BIRTH_CITY_TABLE_NAME, birthCityColumnList));
					db.execSQL(createTableQuery(BIRTH_COUNTRY_TABLE_NAME, birthCountryColumnList));
					db.execSQL(createTableQuery(SCRUTINY_REJECTION_TABLE_NAME, scrutinyRejectionColumnList));
					db.execSQL(createTableQuery(DOC_PROOF_TABLE_NAME, docProofColumnList));
					break;

//				case 2:
//					// TODO: 08/06/2018 work in progress in db changes
//					sessionColumnList.add("log");
//					db.execSQL( createTableQuery( SESSION_TABLE_NAME, sessionColumnList ) );
//					db.execSQL( createTableQuery( DATA_TABLE_NAME, dataColumnList ) );
//					db.execSQL( createTableQuery( PREFIX_TABLE_NAME, prefixColumnList ) );
//					db.execSQL( createTableQuery( GENDER_TABLE_NAME, genderColumnList ) );
//					db.execSQL( createTableQuery( MARITAL_TABLE_NAME, maritalColumnList ) );
//					db.execSQL( createTableQuery( RESIDENT_TABLE_NAME, residentColumnList ) );
//					db.execSQL( createTableQuery( RELATIONSHIP_TABLE_NAME, relationshipColumnList ) );
//					db.execSQL( createTableQuery( OCCUPATION_TABLE_NAME, occupationColumnList ) );
//					db.execSQL( createTableQuery( INCOME_TABLE_NAME, incomeColumnList ) );
//					db.execSQL( createTableQuery( EDUCATION_TABLE_NAME, educationColumnList ) );
//					db.execSQL( createTableQuery( CORRESPONDENCE_TABLE_NAME, correspondenceColumnList ) );
//					db.execSQL( createTableQuery( PERMANENT_TABLE_NAME, permanentColumnList ) );
//					db.execSQL( createTableQuery( POI_TABLE_NAME, poiColumnList ) );
//					db.execSQL( createTableQuery( POA_TABLE_NAME, poaColumnList ) );
//					db.execSQL( createTableQuery( PENSION_FUND_TABLE_NAME, pensionFundColumnList ) );
//					db.execSQL( createTableQuery( PIN_CODE_TABLE_NAME, pinCodeColumnList ) );
//					db.execSQL( createTableQuery( BIRTH_CITY_TABLE_NAME, birthCityColumnList ) );
//					db.execSQL( createTableQuery( BIRTH_COUNTRY_TABLE_NAME, birthCountryColumnList ) );
//					db.execSQL( createTableQuery( SCRUTINY_REJECTION_TABLE_NAME, scrutinyRejectionColumnList ) );
//					break;
			}
	    } catch ( Exception e ) {
//    		e.getMessage();
	    }
    }

	/**
	 * this method contain lists of all the tables required for application
	 */
	private void listOfAllTables () {
//		constant = Constant.getInstance();

		/*************** Todo: Session Table ***********************/
		if (sessionColumnList.isEmpty()) {
			sessionColumnList.add(SESSION_COLUMN_ID);
			sessionColumnList.add(SESSION_COLUMN_KEY);
			sessionColumnList.add(SESSION_COLUMN_FORM_ID);
			sessionColumnList.add(SESSION_COLUMN_TIMESTAMP);
		}

		/**************** Todo: Data Table ***********************/
		if (dataColumnList.isEmpty()) {
			dataColumnList.add(DATA_COLUMN_FINAL_OBJECT);
			dataColumnList.add(DATA_COLUMN_MISCELLANEOUS);
			dataColumnList.add(DATA_COLUMN_HELPER_OBJECT);
			dataColumnList.add(DATA_COLUMN_FORM_STATUS);
			dataColumnList.add(DATA_COLUMN_FORM_ID);
			dataColumnList.add(DATA_COLUMN_PRAN);
			dataColumnList.add(DATA_COLUMN_SECTIONS_STATUS);
			dataColumnList.add(DATA_COLUMN_IMAGE_PATH);
		}

		/**************** Todo: Prefix Table ***********************/
		if (prefixColumnList.isEmpty()) {
			prefixColumnList.add(PREFIX_ID_COLUMN_NAME);
			prefixColumnList.add(PREFIX_MALE_STATUS_COLUMN);
			prefixColumnList.add(PREFIX_FEMALE_STATUS_COLUMN);
			prefixColumnList.add(PREFIX_MAIDEN_STATUS_COLUMN);
			prefixColumnList.add(PREFIX_FATHER_STATUS_COLUMN);
			prefixColumnList.add(PREFIX_MOTHER_STATUS_COLUMN);
			prefixColumnList.add(PREFIX_IS_ACTIVE_COLUMN);
			prefixColumnList.add(PREFIX_FULL_NAME_COLUMN);
		}

		/**************** Todo: Gender Table ***********************/
		if (genderColumnList.isEmpty()) {
			genderColumnList.add(GENDER_COLUMN_NAME);
			genderColumnList.add(GENDER_COLUMN_CODE);
		}

		/**************** Todo: Proof Of Dob Table ***********************/
		if (proofOfDobColumnList.isEmpty()) {
			proofOfDobColumnList.add(PROOF_OF_DOB_COLUMN_NAME);
			proofOfDobColumnList.add(PROOF_OF_DOB_COLUMN_CODE);
		}

		/**************** Todo: Marital Table ***********************/
		if (maritalColumnList.isEmpty()) {
			maritalColumnList.add(MARITAL_COLUMN_NAME);
			maritalColumnList.add(MARITAL_COLUMN_CODE);
		}

		/**************** Todo: Resident Table ***********************/
		if ( residentColumnList.isEmpty() ) {
			residentColumnList.add(RESIDENT_COLUMN_NAME);
			residentColumnList.add(RESIDENT_COLUMN_CODE);
		}

		/**************** Todo: Relationship Table ***********************/
		if (relationshipColumnList.isEmpty()) {
			relationshipColumnList.add(RELATIONSHIP_COLUMN_NAME);
			relationshipColumnList.add(RELATIONSHIP_COLUMN_CODE);
		}

		/**************** Todo: Occupation Table ***********************/
		if (occupationColumnList.isEmpty()) {
			occupationColumnList.add(OCCUPATION_COLUMN_NAME);
			occupationColumnList.add(OCCUPATION_COLUMN_CODE);
		}

		/**************** Todo: Income Table ***********************/
		if (incomeColumnList.isEmpty()) {
			incomeColumnList.add(INCOME_COLUMN_NAME);
			incomeColumnList.add(INCOME_COLUMN_CODE);
		}

		/**************** Todo: Education Table ***********************/
		if (educationColumnList.isEmpty()) {
			educationColumnList.add(EDUCATION_COLUMN_NAME);
			educationColumnList.add(EDUCATION_COLUMN_CODE);
		}

		/**************** Todo: Correspondence Table ***********************/
		if (correspondenceColumnList.isEmpty()) {
			correspondenceColumnList.add(CORRESPONDENCE_COLUMN_NAME);
			correspondenceColumnList.add(CORRESPONDENCE_COLUMN_CODE);
		}

		/**************** Todo: Permanent Table ***********************/
		if (permanentColumnList.isEmpty()) {
			permanentColumnList.add(PERMANENT_COLUMN_NAME);
			permanentColumnList.add(PERMANENT_COLUMN_CODE);
		}

		/**************** Todo: POI Table ***********************/
		if ( poiColumnList.isEmpty() ) {
			poiColumnList.add(POI_COLUMN_NAME);
			poiColumnList.add(POI_COLUMN_CODE);
		}

		/**************** Todo: POA Table ***********************/
		if (poaColumnList.isEmpty()) {
			poaColumnList.add(POA_COLUMN_NAME);
			poaColumnList.add(POA_COLUMN_CODE);
		}

		/**************** Todo: PensionFund Table ***********************/
		if (pensionFundColumnList.isEmpty()) {
			pensionFundColumnList.add(PENSION_FUND_COLUMN_NAME);
			pensionFundColumnList.add(PENSION_FUND_COLUMN_CODE);
		}

		/**************** Todo: PinCode Table ***********************/
		if (pinCodeColumnList.isEmpty()) {
			pinCodeColumnList.add(PIN_CODE_COLUMN_NAME);
			pinCodeColumnList.add(PIN_CODE_COLUMN_CITY);
			pinCodeColumnList.add(PIN_CODE_COLUMN_STATE);
			pinCodeColumnList.add(PIN_CODE_COLUMN_STATE_CODE);
		}

		/**************** Todo: Birth City Table ***********************/
		if (birthCityColumnList.isEmpty()) {
			birthCityColumnList.add(BIRTH_CITY_COLUMN_NAME);
			birthCityColumnList.add(BIRTH_CITY_COLUMN_CODE);
		}

		/**************** Todo: Birth Country Table ***********************/
		if (birthCountryColumnList.isEmpty()) {
			birthCountryColumnList.add(BIRTH_COUNTRY_COLUMN_NAME);
			birthCountryColumnList.add(BIRTH_COUNTRY_COLUMN_CODE);
		}

		/**************** Todo: Scrutiny Rejection Table ***********************/
		if (scrutinyRejectionColumnList.isEmpty()) {
			scrutinyRejectionColumnList.add(SCRUTINY_FORM_ID);
			scrutinyRejectionColumnList.add(SCRUTINY_DATA);
		}

		/**************** Todo: Document Proof Table ***********************/
		if (docProofColumnList.isEmpty()) {
			docProofColumnList.add(DOC_PROOF_NAME);
			docProofColumnList.add(DOC_PROOF_CODE);
			docProofColumnList.add(DOC_PROOF_ID_PROOF);
			docProofColumnList.add(DOC_PROOF_ADD_PROOF);
		}

		/**************** Todo: Drop Down Table ***********************/
		if (dropDownList.isEmpty()) {
			dropDownList.add(DD_LABEL_ID);
			dropDownList.add(DROP_DOWN_DATA);
		}
	}

	/**
	 * this method is used for create table query string for when Application newly install (initial state)
	 *
	 * @param TableName  table name which want to create
	 * @param columnList list of all the column name that we create in table
	 * @return return queryString
	 */
	private String createTableQuery(String TableName, ArrayList<String> columnList) {
		StringBuilder createQueryString = new StringBuilder();
		int index = 0;
		if (!columnList.isEmpty()) {
			createQueryString.append("create table ").append(TableName)
					.append("( primaryId integer primary key autoincrement, ");
			for (String columnName : columnList) {
				index++;
				if (index == columnList.size()) {
					createQueryString.append(columnName).append(" text )");
				} else {
					createQueryString.append(columnName).append(" text, ");
				}
			}
		}
		return createQueryString.toString();
	}

	@Override
    public void onUpgrade (SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO: 5/31/2018 new db change work in progress
		listOfAllTables();
//        switch (oldVersion) {
//            case 1:
            	/**************************************** TODO : SESSION TABLE ****************************************/

//            	//todo: create temp.table to hold data
//                db.execSQL("create table " + TEMP_TABLE_NAME + "(primaryId integer primary key autoincrement, id text, sessionKey text, referenceId text, timeStamp text)");
//                //todo: insert data from old table into temp table
//                db.execSQL("INSERT INTO " + TEMP_TABLE_NAME + " Select *  FROM " + SESSION_TABLE_NAME);
//                //TODO: drop the old table now that your data is safe in the temporary one
//                deleteQuery(db, SESSION_TABLE_NAME);
//                //TODO: recreate the table this time with all 4 columns
//                db.execSQL("CREATE TABLE " + SESSION_TABLE_NAME + "(primaryId integer primary key autoincrement,id text, sessionKey text, referenceId text, timeStamp text , abc text)");
//                //todo: fill it up using null for the column_3
//                db.execSQL("INSERT INTO " + SESSION_TABLE_NAME + " Select id, sessionKey, referenceId, timeStamp, null FROM " + TEMP_TABLE_NAME);
//                //todo: then drop the temporary table
//                deleteQuery(db, TEMP_TABLE_NAME);

//				ArrayList < String > newColumnList = new ArrayList<>();
//				newColumnList.add(SESSION_COLUMN_LOG);
//				newColumnList.add(SESSION_COLUMN_LOG1);
//				updateTables(db, SESSION_TABLE_NAME, sessionColumnList, newColumnList);
//
//                /**************************************** TODO : PREFIX_TABLE_NAME ************************************/

//                if ( newVersion == 2 ) break;
//
//			case 2:
//				break;
//        }

    }

	// TODO: 5/31/2018 new db change work in progress
	private void updateTables(SQLiteDatabase db, String tableName, ArrayList<String> columnList, ArrayList<String> newColumnList) {

		try {
			StringBuilder createQueryString = new StringBuilder();
			StringBuilder selectedColumnString = new StringBuilder();
			int lastIndex = 0;
			if (!columnList.isEmpty()) {
				selectedColumnString.append("primaryId, ");
				for (String columnName : columnList) {
					lastIndex++;
					if (lastIndex == columnList.size()) {
						createQueryString.append(columnName).append(" text ");
					} else {
						createQueryString.append(columnName).append(" text, ");
					}
					selectedColumnString.append(columnName).append(", ");
				}

				//todo: create temp.table to hold data
				db.execSQL("create table " + TEMP_TABLE_NAME + "(primaryId integer primary key autoincrement," + createQueryString + ")");
				//todo: insert data from old table into temp table
				db.execSQL("INSERT INTO " + TEMP_TABLE_NAME + " Select *  FROM " + tableName);
				//TODO: drop the old table now that your data is safe in the temporary one
				dropTable(db, tableName);

				int index = 0;
				if (!newColumnList.isEmpty()) {
					for (String newColumnName : newColumnList) {
						index++;
						if (newColumnList.size() == 1) {
							createQueryString.append(", ").append(newColumnName).append(" text");
							selectedColumnString.append(" '" + "' ");
						} else if (index == newColumnList.size()) {
							createQueryString.append(newColumnName).append(" text");
							selectedColumnString.append(" '" + "' ");
						} else {
							createQueryString.append(", ").append(newColumnName).append(" text, ");
							selectedColumnString.append(" '" + "' ").append(", ");
						}
					}
				}

				//TODO: recreate the table this time with all n columns
				db.execSQL("CREATE TABLE " + tableName + "(primaryId integer primary key autoincrement," + createQueryString + ")");
				//todo: fill it up using null for the column n
				db.execSQL("INSERT INTO " + tableName + " Select " + selectedColumnString + " FROM " + TEMP_TABLE_NAME);
				//todo: then drop the temporary table
				dropTable(db, TEMP_TABLE_NAME);
			}
		} catch (Exception e) {
//    		e.getMessage();
		}
	}

	private void dropTable (SQLiteDatabase db, String tableName) {
		db.execSQL("DROP TABLE " + tableName);
	}


	/**
	 * this method is used for new application form data in local database
	 * @param data is variable contain all data of application form
	 * @return this return id if no exception occur else return -1;
	 */
//	public long insertAllData (FormData data) {
//    	try {
//		    SQLiteDatabase db = this.getWritableDatabase();
//		    ContentValues contentValues = new ContentValues();
//		    contentValues.put(DATA_COLUMN_FINAL_OBJECT, data.getFinalObjectData());
//		    contentValues.put(DATA_COLUMN_MISCELLANEOUS, data.getMiscObjectData());
//		    contentValues.put(DATA_COLUMN_HELPER_OBJECT, data.getHelperObjectData());
//		    contentValues.put(DATA_COLUMN_FORM_STATUS, data.getFormStatus());
//		    contentValues.put(DATA_COLUMN_FORM_ID, data.getReferenceId());
//		    contentValues.put(DATA_COLUMN_PRAN, data.getPranNumber());
//		    contentValues.put(DATA_COLUMN_SECTIONS_STATUS, data.getSectionObjectData());
//		    contentValues.put(DATA_COLUMN_IMAGE_PATH, data.getImagePath());
//		    return db.insertOrThrow(DATA_TABLE_NAME, null, contentValues);
//	    } catch ( Exception e ) {
////    		e.getMessage();
//    		return -1;
//	    }
//    }

	/**
	 * this method is used for update application form data based upon application id
	 * @param //id current application form id
	 * @param //data is contain all data information of form
	 */
//	public void updateAllData (String id, FormData data) {
//    	try {
//		    SQLiteDatabase db = this.getWritableDatabase();
//		    ContentValues contentValues = new ContentValues();
//		    contentValues.put(DATA_COLUMN_FINAL_OBJECT, data.getFinalObjectData());
//		    contentValues.put(DATA_COLUMN_MISCELLANEOUS, data.getMiscObjectData());
//		    contentValues.put(DATA_COLUMN_HELPER_OBJECT, data.getHelperObjectData());
//		    contentValues.put(DATA_COLUMN_FORM_STATUS, data.getFormStatus());
//		    contentValues.put(DATA_COLUMN_FORM_ID, data.getReferenceId());
//		    contentValues.put(DATA_COLUMN_PRAN, data.getPranNumber());
//		    contentValues.put(DATA_COLUMN_SECTIONS_STATUS, data.getSectionObjectData());
//		    contentValues.put(DATA_COLUMN_IMAGE_PATH, data.getImagePath());
//		    db.update(DATA_TABLE_NAME, contentValues, "primaryId = ?", new String[] {String.valueOf(id)});
//	    } catch ( Exception e ) {
////    		e.getMessage();
//	    }
//    }

//    public void updateWorkItem (String referenceId, String workItemNo) {
//    	try {
//			SQLiteDatabase db = this.getWritableDatabase();
//		    String _selection = DATA_COLUMN_FORM_ID + " = ? ";
//		    String[] _selectionToArgs = {referenceId};
//		    String[] _colToReturn = {DATA_COLUMN_MISCELLANEOUS};
//		    Cursor _cursor = readQuery(DATA_TABLE_NAME, _colToReturn, _selection, _selectionToArgs);
//
//		    if ( _cursor.getCount() > 0 ) {
//			    _cursor.moveToFirst();
//			    String _miscData = getString(_cursor, DATA_COLUMN_MISCELLANEOUS);
//			    MiscData miscData = constant.fromJson(_miscData , MiscData.class);
//				miscData.setWorkItemNumber(workItemNo);
//
//			    ContentValues contentValues = new ContentValues();
//			    contentValues.put(DATA_COLUMN_MISCELLANEOUS, constant.toJson(miscData));
//			    db.update(DATA_TABLE_NAME, contentValues, _selection, _selectionToArgs);
//		    }
//			_cursor.close();
//	    } catch ( Exception e ) {
////    		e.getMessage();
//	    }
//    }

//    public boolean updateApplicationID(long id, FormData data) {
//		if (id == -1 || data == null) return false;
//
//		try {
//			SQLiteDatabase db = this.getWritableDatabase();
//			ContentValues contentValues = new ContentValues();
//
//			contentValues.put(DATA_COLUMN_FINAL_OBJECT, data.getFinalObjectData());
//			contentValues.put(DATA_COLUMN_MISCELLANEOUS, data.getMiscObjectData());
//			contentValues.put(DATA_COLUMN_HELPER_OBJECT, data.getHelperObjectData());
//			contentValues.put(DATA_COLUMN_FORM_STATUS, data.getFormStatus());
//			contentValues.put(DATA_COLUMN_FORM_ID, data.getReferenceId());
//			contentValues.put(DATA_COLUMN_PRAN, data.getPranNumber());
//			contentValues.put(DATA_COLUMN_SECTIONS_STATUS, data.getSectionObjectData());
//			contentValues.put(DATA_COLUMN_IMAGE_PATH, data.getImagePath());
//
//			String _selection = "primaryId = ?";
//			String[] _args = {String.valueOf(id)};
//			long n_rows = db.update(DATA_TABLE_NAME, contentValues, _selection, _args);
//			return n_rows == 1;
//		} catch ( Exception e ) {
//			return false;
//		}
//	}

    public class AllData {
        public String finalObjectData;
        public String helperObjectData;
        public String miscData;
        public String sectionData;
        public String formId;
        public String id;
        public String imagePath;
        public String formStatus;
        public String pran;
    }

    public int deleteApplicationForm (String id) {
        try {
            String _selection = "primaryId = ?";
            String[] _args = {id};
            return deleteQueryWithReturn(DATA_TABLE_NAME, _selection, _args);
        } catch ( Exception ignore ) {}
        return 0;
    }

	/*//todo: return all application forms data from the local database
	public ArrayList< AllData > getData (String applicationStatus) {
		Cursor _cursor = null;
		ArrayList < AllData > _arrayList = new ArrayList<>();
    	try {
		    String _selection;
		    String[] _args;
		    String[] _colToReturn;
		    switch ( applicationStatus ) {
			    case "0":
				    _cursor = readQuery(DATA_TABLE_NAME);
				    break;

			    case ConstantValues.incompleteApplicationFormStatus:
				    _selection = DATA_COLUMN_FORM_STATUS + " = ?";
				    _args = new String[] { ConstantValues.incompleteApplicationFormStatus };
				    _colToReturn = new String[] {"*"};
				    _cursor = readQuery(DATA_TABLE_NAME, _colToReturn, _selection, _args);
				    break;

			    case ConstantValues.processingApplicationFormStatus:
				    _selection = DATA_COLUMN_FORM_STATUS + " = ?";
				    _args = new String[] { ConstantValues.processingApplicationFormStatus };
				    _colToReturn = new String[] {"*"};
				    _cursor = readQuery(DATA_TABLE_NAME, _colToReturn, _selection, _args);
				    break;

			    case ConstantValues.rejectedApplicationFormStatus:
				    _selection = DATA_COLUMN_FORM_STATUS + " = ?";
				    _args = new String[] { ConstantValues.rejectedApplicationFormStatus };
				    _colToReturn = new String[] {"*"};
				    _cursor = readQuery(DATA_TABLE_NAME, _colToReturn, _selection, _args);
				    break;

			    case ConstantValues.gbmApplicationFormStatus:
				    _selection = DATA_COLUMN_FORM_STATUS + " = ?";
				    _args = new String[] { ConstantValues.gbmApplicationFormStatus };
				    _colToReturn = new String[] {"*"};
				    _cursor = readQuery(DATA_TABLE_NAME, _colToReturn, _selection, _args);
				    break;

			    case ConstantValues.closedApplicationFormStatus:
				    _selection = DATA_COLUMN_FORM_STATUS + " = ?";
				    _args = new String[] { ConstantValues.closedApplicationFormStatus };
				    _colToReturn = new String[] {"*"};
				    _cursor = readQuery(DATA_TABLE_NAME, _colToReturn, _selection, _args);
				    break;
		    }

		    if ( _cursor != null ) {
		    	if ( _cursor.getCount() > 0 ) {
				    _cursor.moveToFirst();
				    while ( !_cursor.isAfterLast() ) {
					    AllData _data = new AllData();
					    _data.miscData = getString(_cursor, DATA_COLUMN_MISCELLANEOUS);
					    _data.finalObjectData = getString(_cursor, DATA_COLUMN_FINAL_OBJECT);
					    _data.helperObjectData = getString(_cursor, DATA_COLUMN_HELPER_OBJECT);
					    _data.sectionData = getString(_cursor, DATA_COLUMN_SECTIONS_STATUS);
					    _data.referenceId = getString(_cursor, DATA_COLUMN_FORM_ID);
					    _data.formStatus = getString(_cursor, DATA_COLUMN_FORM_STATUS);
					    _data.imagePath = getString(_cursor, DATA_COLUMN_IMAGE_PATH);
					    _data.id = getString(_cursor, "primaryId");
					    _arrayList.add(_data);
					    _cursor.moveToNext();
				    }
				    _cursor.close();
				    return _arrayList;
			    }
			    _cursor.close();
		    }
	    } catch ( Exception e ) {
//    		e.getMessage();
	    } finally {
    		if ( _cursor != null ) {
    			_cursor.close();
		    }
	    }
		return _arrayList;
	}*/

    //todo: All application forms data count
   /* public int getDataCount (String applicationStatus) {
	    int _totalRows = 0;
	    Cursor _cursor = null;
    	try {
		    SQLiteDatabase db = this.getReadableDatabase();
		    String _selection;
		    String[] _args;
		    String[] _colToReturn;
		    switch ( applicationStatus ) {
			    case "0":
				    _colToReturn = new String[] {DATA_COLUMN_FORM_STATUS};
				    _cursor = db.query(DATA_TABLE_NAME, _colToReturn, null, null, null, null, null);
				    break;

			    case ConstantValues.incompleteApplicationFormStatus:
				    _selection = DATA_COLUMN_FORM_STATUS + " = ?";
				    _args = new String[] { ConstantValues.incompleteApplicationFormStatus };
				    _colToReturn = new String[] {DATA_COLUMN_FORM_STATUS};
				    _cursor = readQuery(DATA_TABLE_NAME, _colToReturn, _selection, _args);
				    break;

			    case ConstantValues.processingApplicationFormStatus:
				    _selection = DATA_COLUMN_FORM_STATUS + " = ?";
				    _args = new String[] { ConstantValues.processingApplicationFormStatus };
				    _colToReturn = new String[] {DATA_COLUMN_FORM_STATUS};
				    _cursor = readQuery(DATA_TABLE_NAME, _colToReturn, _selection, _args);
				    break;

			    case ConstantValues.rejectedApplicationFormStatus:
				    _selection = DATA_COLUMN_FORM_STATUS + " = ?";
				    _args = new String[] { ConstantValues.rejectedApplicationFormStatus };
				    _colToReturn = new String[] {DATA_COLUMN_FORM_STATUS};
				    _cursor = readQuery(DATA_TABLE_NAME, _colToReturn, _selection, _args);
				    break;

			    case ConstantValues.gbmApplicationFormStatus:
				    _selection = DATA_COLUMN_FORM_STATUS + " = ?";
				    _args = new String[] { ConstantValues.gbmApplicationFormStatus };
				    _colToReturn = new String[] {DATA_COLUMN_FORM_STATUS};
				    _cursor = readQuery(DATA_TABLE_NAME, _colToReturn, _selection, _args);
				    break;

			    case ConstantValues.closedApplicationFormStatus:
				    _selection = DATA_COLUMN_FORM_STATUS + " = ?";
				    _args = new String[] { ConstantValues.closedApplicationFormStatus };
				    _colToReturn = new String[] {DATA_COLUMN_FORM_STATUS};
				    _cursor = readQuery(DATA_TABLE_NAME, _colToReturn, _selection, _args);
				    break;
		    }

		    if ( _cursor != null ) {
			    if ( _cursor.getCount() > 0 ) {
				    _totalRows = _cursor.getCount();
				    _cursor.close();
				    return _totalRows;
			    }
			    _cursor.close();
		    }
	    } catch ( Exception e ) {
//    		e.getMessage();
	    } finally {
    		if ( _cursor != null ) {
    			_cursor.close();
		    }
	    }
		return _totalRows;
	}
*/
	/**
	 * this method check referenceId coming from the webService is already exist or not in app database
	 */
	public boolean getFormId (String formId) {
		try {
			String _selection = DATA_COLUMN_FORM_ID + " = ?";
			String[] _args = {formId};
			String[] _colToReturn = {DATA_COLUMN_FORM_ID};
			Cursor _cursor = readQuery(DATA_TABLE_NAME, _colToReturn, _selection, _args);
            if (_cursor.getCount() > 0) {
                _cursor.close();
                return true;
            }
			_cursor.close();
		} catch ( Exception e ) {
//			e.getMessage();
		}
		return false;
	}

	//todo: update application form Status corresponding form_id column in the local database
    public void updateAllFormStatus (String formId, String applicationStatus) {
	    try {
		    SQLiteDatabase db = this.getWritableDatabase();
		    db.beginTransaction();
		    ContentValues contentValues = new ContentValues();
		    contentValues.put(DATA_COLUMN_FORM_ID, formId);
		    contentValues.put(DATA_COLUMN_FORM_STATUS, applicationStatus);
		    db.update(DATA_TABLE_NAME, contentValues, DATA_COLUMN_FORM_ID + " = ? ", new String[] {formId});
		    db.setTransactionSuccessful();
		    db.endTransaction();
	    } catch ( Exception ex ) {
		    //Log.e("update application status : ", ex.getMessage());
	    }
    }

    //todo: prefix data function
	public void insertPrefix (PrefixMasterResponse.PrefixMaster prefixMaster) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.beginTransaction();
		ContentValues contentValues = new ContentValues();
		contentValues.put(PREFIX_ID_COLUMN_NAME, prefixMaster.getId());
		contentValues.put(PREFIX_MALE_STATUS_COLUMN, prefixMaster.getIsMaleStatus());
		contentValues.put(PREFIX_FEMALE_STATUS_COLUMN, prefixMaster.getIsFemaleStatus());
		contentValues.put(PREFIX_MAIDEN_STATUS_COLUMN, prefixMaster.getIsMaidenStatus());
		contentValues.put(PREFIX_FATHER_STATUS_COLUMN, prefixMaster.getIsFatherStatus());
		contentValues.put(PREFIX_MOTHER_STATUS_COLUMN, prefixMaster.getIsMotherStatus());
		contentValues.put(PREFIX_IS_ACTIVE_COLUMN, prefixMaster.getIsActive());
		contentValues.put(PREFIX_FULL_NAME_COLUMN, prefixMaster.getName());
		db.insert(PREFIX_TABLE_NAME, null, contentValues);
		db.setTransactionSuccessful();
		db.endTransaction();

	}

	//todo;;
	public void insertDropDownData (String ddLabel, String data) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.beginTransaction();
		ContentValues contentValues = new ContentValues();
		contentValues.put(DD_LABEL_ID, ddLabel);
		contentValues.put(DROP_DOWN_DATA, data);
		db.insert(DROP_DOWN_TABLE_NAME, null, contentValues);
		db.setTransactionSuccessful();
		db.endTransaction();

	}


	public void deleteDropDown() {
		deleteQuery(DROP_DOWN_TABLE_NAME);
	}


	public List<String> getDropDownList(String ddLabel){
		ArrayList<String> dropDownData = new ArrayList<>();
		try {
			Cursor res = readQuery(DROP_DOWN_TABLE_NAME);
			if(res.moveToFirst()){
				do{
					String ddLabelId= getString(res, DD_LABEL_ID);
					if (ddLabel.equalsIgnoreCase(ddLabelId)) {
						String dropDownListString = getString(res, DROP_DOWN_DATA);
						dropDownData = new Gson().fromJson(dropDownListString, new TypeToken<List<String>>(){}.getType());
						res.close();
						return dropDownData;
					}
				} while (res.moveToNext());
			}
			res.close();
		} catch (Exception ignore) {}
		return dropDownData;
	}




	public ArrayList<String> getPrefix() {
		ArrayList<String> array_list = new ArrayList<>();
		try {
			Cursor res = readQuery(PREFIX_TABLE_NAME);
			if (res.getCount() > 0) {
				res.moveToFirst();
				while (!res.isAfterLast()) {
					array_list.add(getString(res, PREFIX_FULL_NAME_COLUMN));
					res.moveToNext();
				}
				res.close();
				return array_list;
			}
			res.close();
		} catch (Exception ignore) {
		}
		return array_list;
	}

	public ArrayList<String> getFatherPrefix() {
		ArrayList<String> array_list = new ArrayList<>();
		try {
            String _selection = PREFIX_MALE_STATUS_COLUMN + " = ? AND " + PREFIX_FATHER_STATUS_COLUMN + " = ? AND " + PREFIX_IS_ACTIVE_COLUMN + " = ? ";
            String[] _args = {"1", "1", "1"};
            String[] _colToReturn = {PREFIX_FULL_NAME_COLUMN};
            Cursor res = readQuery(PREFIX_TABLE_NAME, _colToReturn, _selection, _args);
			if (res.getCount() > 0) {
				res.moveToFirst();
				while (!res.isAfterLast()) {
					array_list.add(getString(res, PREFIX_FULL_NAME_COLUMN));
					res.moveToNext();
				}
				res.close();
				return array_list;
			}
			res.close();
		} catch (Exception ignore) {
//			ignore.getMessage();
		}
		return array_list;
	}

    private boolean getPrefixCount() {
		try {
			Cursor res = readQuery(PREFIX_TABLE_NAME);
			if (res.getCount() > 0) {
				res.close();
				return true;
			}
			res.close();
		} catch (Exception ignore) {}
        return false;
    }

//    public String getPrefixCode (String prefix) {
//	    String _prefixCode = "" ;
//	    try {
//			String _selection = PREFIX_COLUMN_NAME + " = ?";
//			String[] _args = {prefix};
//			String[] _colToReturn = {PREFIX_COLUMN_CODE};
//			Cursor cursor = readQuery(PREFIX_TABLE_NAME, _colToReturn, _selection, _args);
//			if (cursor.getCount() > 0) {
//				cursor.moveToFirst();
//				_prefixCode = getString(cursor, PREFIX_COLUMN_CODE);
//				cursor.close();
//				return _prefixCode;
//			}
//			cursor.close();
//		} catch (Exception ignore) {}
//        return _prefixCode;
//    }
//    public String getPrefixCode (String prefix) {
//	    String _prefixCode = "" ;
//	    try {
//			String _selection = PREFIX_COLUMN_NAME + " = ?";
//			String[] _args = {prefix};
//			String[] _colToReturn = {PREFIX_COLUMN_CODE};
//			Cursor cursor = readQuery(PREFIX_TABLE_NAME, _colToReturn, _selection, _args);
//			if (cursor.getCount() > 0) {
//				cursor.moveToFirst();
//				_prefixCode = getString(cursor, PREFIX_COLUMN_CODE);
//				cursor.close();
//				return _prefixCode;
//			}
//			cursor.close();
//		} catch (Exception ignore) {}
//        return _prefixCode;
//    }

//    public String getPrefixName (String prefixCode) {
//	    String _prefix = "" ;
//		try {
//			String _selection = PREFIX_COLUMN_CODE + " = ?";
//			String[] _args = {prefixCode};
//			String[] _colToReturn = {PREFIX_COLUMN_NAME};
//			Cursor cursor = readQuery(PREFIX_TABLE_NAME, _colToReturn, _selection, _args);
//			if (cursor.getCount() > 0) {
//				cursor.moveToFirst();
//				_prefix = getString(cursor, PREFIX_COLUMN_NAME);
//				cursor.close();
//				return _prefix;
//			}
//			cursor.close();
//		} catch (Exception ignore) {}
//        return _prefix;
//    }

    public void deletePrefix () {
		deleteQuery(PREFIX_TABLE_NAME);
    }



 //todo: prefix data function
	public void insertDocProof (OVDDeemedOVDMasterResponse.OVDDeemedOVDMaster master) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.beginTransaction();
		ContentValues contentValues = new ContentValues();
		contentValues.put(DOC_PROOF_NAME, master.getName());
		contentValues.put(DOC_PROOF_CODE, master.getCode());
		contentValues.put(DOC_PROOF_ID_PROOF, master.getIdProof());
		contentValues.put(DOC_PROOF_ADD_PROOF, master.getAddProof());
		db.insert(DOC_PROOF_TABLE_NAME, null, contentValues);
		db.setTransactionSuccessful();
		db.endTransaction();
	}

	public ArrayList<String> getIdDocProof() {
		ArrayList<String> array_list = new ArrayList<>();
		try {
            String _selection = DOC_PROOF_ID_PROOF + " = ? OR " + DOC_PROOF_ADD_PROOF + " = ? ";
			String[] _args = {"1", "1"};
			String[] _colToReturn = {DOC_PROOF_NAME};
			Cursor res = readQuery(DOC_PROOF_TABLE_NAME, _colToReturn, _selection, _args);


			if (res.getCount() > 0) {
				res.moveToFirst();
				while (!res.isAfterLast()) {
					array_list.add(getString(res, DOC_PROOF_NAME));
					res.moveToNext();
				}
				res.close();
				return array_list;
			}
			res.close();
		} catch (Exception ignore) {
		}
		return array_list;
	}

    public ArrayList<String> getAddDocProof() {
        ArrayList<String> array_list = new ArrayList<>();
        try {
            String _selection = DOC_PROOF_ID_PROOF + " = ? OR " + DOC_PROOF_ADD_PROOF + " = ? ";
            String[] _args = {"0", "0"};
            String[] _colToReturn = {DOC_PROOF_NAME};
            Cursor res = readQuery(DOC_PROOF_TABLE_NAME, _colToReturn, _selection, _args);
            if (res.getCount() > 0) {
                res.moveToFirst();
                while (!res.isAfterLast()) {
                    array_list.add(getString(res, DOC_PROOF_NAME));
                    res.moveToNext();
                }
                res.close();
                return array_list;
            }
            res.close();
        } catch (Exception ignore) {
        }
        return array_list;
    }


    private boolean getDocProofCount() {
		try {
			Cursor res = readQuery(DOC_PROOF_TABLE_NAME);
			if (res.getCount() > 0) {
				res.close();
				return true;
			}
			res.close();
		} catch (Exception ignore) {}
        return false;
    }

    public String getDocProofCode (String prefix) {
	    String _prefixCode = "" ;
	    try {
			String _selection = DOC_PROOF_NAME + " = ?";
			String[] _args = {prefix};
			String[] _colToReturn = {DOC_PROOF_CODE};
			Cursor cursor = readQuery(DOC_PROOF_TABLE_NAME, _colToReturn, _selection, _args);
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				_prefixCode = getString(cursor, DOC_PROOF_CODE);
				cursor.close();
				return _prefixCode;
			}
			cursor.close();
		} catch (Exception ignore) {}
        return _prefixCode;
    }



    public void deleteDocProof () {
		deleteQuery(DOC_PROOF_TABLE_NAME);
    }

    //todo: gender data function
    public void insertGender (String name, String code) {
		ContentValues contentValues = new ContentValues();
		SQLiteDatabase db = this.getWritableDatabase();
		db.beginTransaction();
		contentValues.put(GENDER_COLUMN_NAME, name);
		contentValues.put(GENDER_COLUMN_CODE, code);
		db.insert(GENDER_TABLE_NAME, null, contentValues);
		db.setTransactionSuccessful();
		db.endTransaction();
    }

    public ArrayList< String > getGender () {
        ArrayList< String > array_list = new ArrayList<>();
        try {
			Cursor res = readQuery(GENDER_TABLE_NAME);
			if (res.getCount() > 0) {
				res.moveToFirst();
				while (!res.isAfterLast()) {
					String _code = getString(res, GENDER_COLUMN_CODE);
					if (_code.equals("O") || _code.equals("T")) {
						array_list.add(getString(res, GENDER_COLUMN_NAME));
						break;
					} else {
						array_list.add(getString(res, GENDER_COLUMN_NAME));
					}
					res.moveToNext();
				}
				res.close();
				return array_list;
			}
			res.close();
		} catch (Exception ignore) {}
        return array_list;
    }

    private boolean getGenderCount() {
		try {
			Cursor res = readQuery(GENDER_TABLE_NAME);
			if (res.getCount() > 0) {
				res.close();
				return true;
			}
			res.close();
		} catch (Exception ignore) {}
        return false;
    }

    public String getGenderCode (String value) {
		String _code = "" ;
		try {
			String _selection = GENDER_COLUMN_NAME + " = ?";
			String[] _args = {value};
			String[] _colToReturn = {GENDER_COLUMN_CODE};
			Cursor cursor = readQuery(GENDER_TABLE_NAME, _colToReturn, _selection, _args);
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				_code = getString(cursor, GENDER_COLUMN_CODE);
				cursor.close();
				return _code;
			}
			cursor.close();
		} catch (Exception ignore) {}
        return _code;
    }

    public String getGenderName (String code) {
		String _value = "";
		try {
			String _selection = GENDER_COLUMN_CODE + " = ?";
			String[] _args = {code};
			String[] _colToReturn = {GENDER_COLUMN_NAME};
			Cursor cursor = readQuery(GENDER_TABLE_NAME, _colToReturn, _selection, _args);
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				_value = getString(cursor, GENDER_COLUMN_NAME);
				cursor.close();
				return _value;
			}
			cursor.close();
		} catch (Exception ignore) {}
        return _value;
    }

    public String getGenderType(String code){
	    String _type = "";
	    try {
			String _selection = GENDER_COLUMN_CODE + " = ?";
			String[] _args = {code};
			String[] _colToReturn = {GENDER_COLUMN_NAME};
			Cursor _cursor = readQuery(GENDER_TABLE_NAME, _colToReturn, _selection, _args);
			if (_cursor.getCount() > 0) {
				_cursor.moveToFirst();
				_type = getString(_cursor, GENDER_COLUMN_NAME);
				_cursor.close();
				return _type;
			}
			_cursor.close();
		} catch (Exception ignore) {}
	    return _type;
    }

    public void deleteGender () {
		deleteQuery(GENDER_TABLE_NAME);
    }

	// TODO: Proof Of Dob Data function
	public void insertProofOfDob (String name, String code) {
		ContentValues contentValues = new ContentValues();
		SQLiteDatabase db = this.getWritableDatabase();
		db.beginTransaction();
		contentValues.put(PROOF_OF_DOB_COLUMN_NAME, name);
		contentValues.put(PROOF_OF_DOB_COLUMN_CODE, code);
		db.insert(PROOF_OF_DOB_TABLE_NAME, null, contentValues);
		db.setTransactionSuccessful();
		db.endTransaction();
	}

	public ArrayList<String> getProofOfDob() {
		ArrayList<String> array_list = new ArrayList<>();
		try {
			Cursor res = readQuery(PROOF_OF_DOB_TABLE_NAME);
			if (res.getCount() > 0) {
				res.moveToFirst();
				while (!res.isAfterLast()) {
					array_list.add(getString(res, PROOF_OF_DOB_COLUMN_NAME));
					res.moveToNext();
				}
				res.close();
				return array_list;
			}
			res.close();
		} catch (Exception ignore) {}
		return array_list;
	}

	private boolean getProofOfDobCount() {
		try {
			Cursor res = readQuery(PROOF_OF_DOB_TABLE_NAME);
			if (res.getCount() > 0) {
				res.close();
				return true;
			}
			res.close();
		} catch (Exception ignore) {}
		return false;
	}

	public String getProofOfDobCode (String value) {
		String _code = "" ;
		try {
			String _selection = PROOF_OF_DOB_COLUMN_NAME + " = ?";
			String[] _args = {value};
			String[] _colToReturn = {PROOF_OF_DOB_COLUMN_CODE};
			Cursor cursor = readQuery(PROOF_OF_DOB_TABLE_NAME, _colToReturn, _selection, _args);
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				_code = getString(cursor, PROOF_OF_DOB_COLUMN_CODE);
				cursor.close();
				return _code;
			}
			cursor.close();
		} catch (Exception ignore) {}
		return _code;
	}

	public String getProofOfDobName (String code) {
		String _value = "" ;
		try {
			String _selection = PROOF_OF_DOB_COLUMN_CODE + " = ?";
			String[] _args = {code};
			String[] _colToReturn = {PROOF_OF_DOB_COLUMN_NAME};
			Cursor cursor = readQuery(PROOF_OF_DOB_TABLE_NAME, _colToReturn, _selection, _args);
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				_value = getString(cursor, PROOF_OF_DOB_COLUMN_NAME);
				cursor.close();
				return _value;
			}
			cursor.close();
		} catch (Exception ignore) {}
		return _value;
	}

	public void deleteProofOfDob() {
		deleteQuery(PROOF_OF_DOB_TABLE_NAME);
	}

    //todo: Martial Data Function
    public void insertMarital (String name, String code) {
		ContentValues contentValues = new ContentValues();
		SQLiteDatabase db = this.getWritableDatabase();
		db.beginTransaction();
		contentValues.put(MARITAL_COLUMN_NAME, name);
		contentValues.put(MARITAL_COLUMN_CODE, code);
		db.insert(MARITAL_TABLE_NAME, null, contentValues);
		db.setTransactionSuccessful();
		db.endTransaction();
    }

    public ArrayList< String > getMarital () {
        ArrayList< String > array_list = new ArrayList<>();
        try {
			Cursor res = readQuery(MARITAL_TABLE_NAME);
			if (res.getCount() > 0) {
				res.moveToFirst();
				while (!res.isAfterLast()) {
					array_list.add(getString(res, MARITAL_COLUMN_NAME));
					res.moveToNext();
				}
				res.close();
				return array_list;
			}
			res.close();
		} catch (Exception ignore) {}
        return array_list;
    }

    private boolean getMaritalCount() {
		Cursor res = readQuery(MARITAL_TABLE_NAME);
		try {
			if (res.getCount() > 0) {
				res.close();
				return true;
			}
			res.close();
		} catch (Exception ignore) {}
        return false;
    }

    public String getMaritalCode (String value) {
        String _code = "" ;
        try {
			String _selection = MARITAL_COLUMN_NAME + " = ?";
			String[] _args = {value};
			String[] _colToReturn = {MARITAL_COLUMN_CODE};
			Cursor cursor = readQuery(MARITAL_TABLE_NAME, _colToReturn, _selection, _args);
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				_code = getString(cursor, MARITAL_COLUMN_CODE);
				cursor.close();
				return _code;
			}
			cursor.close();
		} catch (Exception ignore) {}
        return _code;
    }

    public String getMaritalName (String code) {
        String _value = "" ;
        try {
			String _selection = MARITAL_COLUMN_CODE + " = ?";
			String[] _args = {code};
			String[] _colToReturn = {MARITAL_COLUMN_NAME};
			Cursor cursor = readQuery(MARITAL_TABLE_NAME, _colToReturn, _selection, _args);
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				_value = getString(cursor, MARITAL_COLUMN_NAME);
				cursor.close();
				return _value;
			}
			cursor.close();
		} catch (Exception ignore) {}
        return _value;
    }

	public void deleteMarital() {
		deleteQuery(MARITAL_TABLE_NAME);
	}

    //TODO: Resident Data function
    public void insertResident (String name, String code) {
		ContentValues contentValues = new ContentValues();
		SQLiteDatabase db = this.getWritableDatabase();
		db.beginTransaction();
		contentValues.put(RESIDENT_COLUMN_NAME, name);
		contentValues.put(RESIDENT_COLUMN_CODE, code);
		db.insert(RESIDENT_TABLE_NAME, null, contentValues);
		db.setTransactionSuccessful();
		db.endTransaction();
    }

    public ArrayList< String > getResident () {
        ArrayList< String > array_list = new ArrayList<>();
        try {
			Cursor res = readQuery(RESIDENT_TABLE_NAME);
			res.moveToFirst();

			while (!res.isAfterLast()) {
				array_list.add(getString(res, RESIDENT_COLUMN_NAME));
				res.moveToNext();
			}
			res.close();
		} catch (Exception ignore) {}
        return array_list;
    }

    public boolean getResidentCount() {
		try {
			Cursor res = readQuery(RESIDENT_TABLE_NAME);
			if (res.getCount() > 0) {
				res.close();
				return true;
			}
			res.close();
		} catch (Exception ignore) {}
        return false;
    }

    public void deleteResident () {
		deleteQuery(RESIDENT_TABLE_NAME);
    }

    //TODO: Relationship Data Function
    public void insertRelationship (String name, String code) {
		ContentValues contentValues = new ContentValues();
		SQLiteDatabase db = this.getWritableDatabase();
		db.beginTransaction();
		contentValues.put(RELATIONSHIP_COLUMN_NAME, name);
		contentValues.put(RELATIONSHIP_COLUMN_CODE, code);
		db.insert(RELATIONSHIP_TABLE_NAME, null, contentValues);
		db.setTransactionSuccessful();
		db.endTransaction();
    }

    public ArrayList< String > getRelationship () {
        ArrayList< String > array_list = new ArrayList<>();
        try {
			Cursor res = readQuery(RELATIONSHIP_TABLE_NAME);
			if (res.getCount() > 0) {
				res.moveToFirst();
				while (!res.isAfterLast()) {
					array_list.add(getString(res, RELATIONSHIP_COLUMN_NAME));
					res.moveToNext();
				}
				res.close();
				return array_list;
			}
			res.close();
		} catch (Exception ignore) {}
		return array_list;
    }

    private boolean getRelationshipCount() {
		try {
			Cursor res = readQuery(RELATIONSHIP_TABLE_NAME);
			if (res.getCount() > 0) {
				res.close();
				return true;
			}
			res.close();
		} catch (Exception ignore) {}
        return false;
    }

    public String getRelationshipCode (String value) {
        String _code = "" ;
        try {
			String _selection = RELATIONSHIP_COLUMN_NAME + " = ?";
			String[] _args = {value};
			String[] _colToReturn = {RELATIONSHIP_COLUMN_CODE};
			Cursor cursor = readQuery(RELATIONSHIP_TABLE_NAME, _colToReturn, _selection, _args);
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				_code = getString(cursor, RELATIONSHIP_COLUMN_CODE);
				cursor.close();
				return _code;
			}
			cursor.close();
		} catch (Exception ignore) {}
        return _code;
    }

    public String getRelationshipName (String code) {
        String _value = "" ;
        try {
			String _selection = RELATIONSHIP_COLUMN_CODE + " = ?";
			String[] _args = {code};
			String[] _colToReturn = {RELATIONSHIP_COLUMN_NAME};
			Cursor cursor = readQuery(RELATIONSHIP_TABLE_NAME, _colToReturn, _selection, _args);
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				_value = getString(cursor, RELATIONSHIP_COLUMN_NAME);
				cursor.close();
				return _value;
			}
			cursor.close();
		} catch (Exception ignore) {}
        return _value;
    }

    public void deleteRelationship () {
		deleteQuery(RELATIONSHIP_TABLE_NAME);
    }

    //todo: Occupation data function
    public void insertOccupation (String name, String code) {
		ContentValues contentValues = new ContentValues();
		SQLiteDatabase db = this.getWritableDatabase();
		db.beginTransaction();
		contentValues.put(OCCUPATION_COLUMN_NAME, name);
		contentValues.put(OCCUPATION_COLUMN_CODE, code);
		db.insert(OCCUPATION_TABLE_NAME, null, contentValues);
		db.setTransactionSuccessful();
		db.endTransaction();
    }

    public ArrayList< String > getOccupation () {
        ArrayList< String > array_list = new ArrayList<>();
        try {
			Cursor res = readQuery(OCCUPATION_TABLE_NAME);
			if (res.getCount() > 0) {
				res.moveToFirst();
				while (!res.isAfterLast()) {
					array_list.add(getString(res, OCCUPATION_COLUMN_NAME));
					res.moveToNext();
				}
				res.close();
				return array_list;
			}
			res.close();
		} catch (Exception ignore) {}
        return array_list;
    }

    private boolean getOccupationCount() {
		try {
			Cursor res = readQuery(OCCUPATION_TABLE_NAME);
			if (res.getCount() > 0) {
				res.close();
				return true;
			}
			res.close();
		} catch (Exception ignore) {}
        return false;
    }

    public String getOccupationCode (String value) {
        String _code = "" ;
        try {
			String _selection = OCCUPATION_COLUMN_NAME + " = ?";
			String[] _args = {value};
			String[] _colToReturn = {OCCUPATION_COLUMN_CODE};
			Cursor cursor = readQuery(OCCUPATION_TABLE_NAME, _colToReturn, _selection, _args);
			if ( cursor.getCount() > 0 ){
				cursor.moveToFirst();
				_code = getString(cursor, OCCUPATION_COLUMN_CODE);
				cursor.close();
				return _code;
			}
			cursor.close();
		} catch (Exception ignore) {}
        return _code;
    }

    public String getOccupationName (String code) {
        String _value = "" ;
        try {
			String _selection = OCCUPATION_COLUMN_CODE + " = ?";
			String[] _args = {code};
			String[] _colToReturn = {OCCUPATION_COLUMN_NAME};
			Cursor cursor = readQuery(OCCUPATION_TABLE_NAME, _colToReturn, _selection, _args);
			if ( cursor.getCount() > 0 ){
				cursor.moveToFirst();
				_value = getString(cursor, OCCUPATION_COLUMN_NAME);
				cursor.close();
				return _value;
			}
			cursor.close();
		} catch (Exception ignore) {}
        return _value;
    }

    public void deleteOccupation () {
		deleteQuery(OCCUPATION_TABLE_NAME);
	}

    //todo: Income Data function
    public void insertIncome (String name, String code) {
		ContentValues contentValues = new ContentValues();
		SQLiteDatabase db = this.getWritableDatabase();
		db.beginTransaction();
		contentValues.put(INCOME_COLUMN_NAME, name);
		contentValues.put(INCOME_COLUMN_CODE, code);
		db.insert(INCOME_TABLE_NAME, null, contentValues);
		db.setTransactionSuccessful();
		db.endTransaction();
    }

    public ArrayList< String > getIncome () {
        ArrayList< String > array_list = new ArrayList<>();
	    try {
	        Cursor res = readQuery(INCOME_TABLE_NAME);
	        if ( res.getCount() > 0 ) {
		        res.moveToFirst();
		        while ( !res.isAfterLast() ) {
			        array_list.add(getString(res, INCOME_COLUMN_NAME));
			        res.moveToNext();
		        }
		        res.close();
		        return array_list;
	        }
	        res.close();
		} catch (Exception ignore) {}
	    return array_list;
    }

    private boolean getIncomeCount() {
		try {
            Cursor res = readQuery(INCOME_TABLE_NAME);
            if ( res.getCount() > 0 ) {
                res.close();
                return true;
            }
	        res.close();
		} catch (Exception ignore) {}
        return false;
    }

    public String getIncomeCode (String value) {
        String _code = "" ;
        try {
            String _selection = INCOME_COLUMN_NAME + " = ?";
            String[] _args = {value};
            String[] _colToReturn = {INCOME_COLUMN_CODE};
            Cursor cursor = readQuery(INCOME_TABLE_NAME, _colToReturn, _selection, _args);
            if ( cursor.getCount() > 0 ){
                cursor.moveToFirst();
                _code = getString(cursor, INCOME_COLUMN_CODE);
                cursor.close();
                return _code;
            }
	        cursor.close();
		} catch (Exception ignore) {}
        return _code;
    }

    public String getIncomeName (String code) {
        String _value = "" ;
        try {
            String _selection = INCOME_COLUMN_CODE + " = ?";
            String[] _args = {code};
            String[] _colToReturn = {INCOME_COLUMN_NAME};
            Cursor cursor = readQuery(INCOME_TABLE_NAME, _colToReturn, _selection, _args);
            if ( cursor.getCount() > 0 ){
                cursor.moveToFirst();
                _value = getString(cursor, INCOME_COLUMN_NAME);
                cursor.close();
                return _value;
            }
	        cursor.close();
		} catch (Exception ignore) {}
        return _value;
    }

    public void deleteIncome () {
		deleteQuery(INCOME_TABLE_NAME);
    }

    //todo: Education data function
    public void insertEducation (String name, String code) {
            ContentValues contentValues = new ContentValues();
            SQLiteDatabase db = this.getWritableDatabase();
            db.beginTransaction();
            contentValues.put(EDUCATION_COLUMN_NAME, name);
            contentValues.put(EDUCATION_COLUMN_CODE, code);
            db.insert(EDUCATION_TABLE_NAME, null, contentValues);
            db.setTransactionSuccessful();
            db.endTransaction();
    }

    public ArrayList< String > getEducation () {
        ArrayList< String > array_list = new ArrayList<>();
        try {
			Cursor res = readQuery(EDUCATION_TABLE_NAME);
			if (res.getCount() > 0) {
				res.moveToFirst();
				while (!res.isAfterLast()) {
					array_list.add(getString(res, EDUCATION_COLUMN_NAME));
					res.moveToNext();
				}
				res.close();
				return array_list;
			}
			res.close();
		} catch (Exception ignore) {}
	    return array_list;
    }

    private boolean getEducationCount() {
		try {
			Cursor res = readQuery(EDUCATION_TABLE_NAME);
			if (res.getCount() > 0) {
				res.close();
				return true;
			}
			res.close();
		} catch (Exception ignore) {}
        return false;
    }

    public String getEducationCode (String value) {
        String _code = "" ;
        try {
            String _selection = EDUCATION_COLUMN_NAME + " = ?";
            String[] _args = {value};
            String[] _colToReturn = {EDUCATION_COLUMN_CODE};
            Cursor cursor = readQuery(EDUCATION_TABLE_NAME, _colToReturn, _selection, _args);
            if ( cursor.getCount() > 0 ){
                cursor.moveToFirst();
                _code = getString(cursor, EDUCATION_COLUMN_CODE);
                cursor.close();
                return _code;
            }
	        cursor.close();
		} catch (Exception ignore) {}
        return _code;
    }

    public String getEducationName (String code) {
        String _value = "" ;
        try {
			String _selection = EDUCATION_COLUMN_CODE + " = ?";
			String[] _args = {code};
			String[] _colToReturn = {EDUCATION_COLUMN_NAME};
			Cursor cursor = readQuery(EDUCATION_TABLE_NAME, _colToReturn, _selection, _args);
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				_value = getString(cursor, EDUCATION_COLUMN_NAME);
				cursor.close();
				return _value;
			}
			cursor.close();
		} catch (Exception ignore) {}
        return _value;
    }

	public void deleteEducation() {
		deleteQuery(EDUCATION_TABLE_NAME);
	}

    //todo: Correspondence data function
    public void insertCorrespondence (String name, String code) {
		ContentValues contentValues = new ContentValues();
		SQLiteDatabase db = this.getWritableDatabase();
		db.beginTransaction();
		contentValues.put(CORRESPONDENCE_COLUMN_NAME, name);
		contentValues.put(CORRESPONDENCE_COLUMN_CODE, code);
		db.insert(CORRESPONDENCE_TABLE_NAME, null, contentValues);
		db.setTransactionSuccessful();
		db.endTransaction();
    }

	public ArrayList<String> getCorrespondence() {
		ArrayList<String> array_list = new ArrayList<>();
		try {
			Cursor res = readQuery(CORRESPONDENCE_TABLE_NAME);
			if (res.getCount() > 0) {
				res.moveToFirst();
				while (!res.isAfterLast()) {
					array_list.add(getString(res, CORRESPONDENCE_COLUMN_NAME));
					res.moveToNext();
				}
				res.close();
				return array_list;
			}
			res.close();
		} catch (Exception ignore) {
		}
		return array_list;
	}

    private boolean getCorrespondenceCount() {
		try {
			Cursor res = readQuery(CORRESPONDENCE_TABLE_NAME);
			if (res.getCount() > 0) {
				res.close();
				return true;
			}
			res.close();
		} catch (Exception ignore) {}
        return false;
    }

    public String getCorrespondenceCode (String value) {
        String _code = "" ;
        try {
			String _selection = CORRESPONDENCE_COLUMN_NAME + " = ?";
			String[] _args = {value};
			String[] _colToReturn = {CORRESPONDENCE_COLUMN_CODE};
			Cursor cursor = readQuery(CORRESPONDENCE_TABLE_NAME, _colToReturn, _selection, _args);
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				_code = getString(cursor, CORRESPONDENCE_COLUMN_CODE);
				cursor.close();
				return _code;
			}
			cursor.close();
		} catch (Exception ignore) {}
        return _code;
    }

    public String getCorrespondenceName (String code) {
        String _value = "" ;
            String _selection = CORRESPONDENCE_COLUMN_CODE + " = ?";
            String[] _args = {code};
            String[] _colToReturn = {CORRESPONDENCE_COLUMN_NAME};
            Cursor cursor = readQuery(CORRESPONDENCE_TABLE_NAME, _colToReturn, _selection, _args);
            if ( cursor.getCount() > 0 ){
                cursor.moveToFirst();
                _value = getString(cursor, CORRESPONDENCE_COLUMN_NAME);
                cursor.close();
                return _value;
            }
	        cursor.close();
        return _value;
    }

    public void deleteCorrespondence () {
		deleteQuery(CORRESPONDENCE_TABLE_NAME);
    }

    //todo: Permanent data function
    public void insertPermanent (String name, String code) {
        try {
            ContentValues contentValues = new ContentValues();
            SQLiteDatabase db = this.getWritableDatabase();
            db.beginTransaction();
            contentValues.put(PERMANENT_COLUMN_NAME, name);
            contentValues.put(PERMANENT_COLUMN_CODE, code);
            db.insert(PERMANENT_TABLE_NAME, null, contentValues);
            db.setTransactionSuccessful();
            db.endTransaction();
        } catch ( Exception e ) {
//            e.getMessage();
        }
    }

    public ArrayList< String > getPermanent () {
        ArrayList< String > array_list = new ArrayList<>();
        try {
	        Cursor res = readQuery(PERMANENT_TABLE_NAME);
	        if ( res.getCount() > 0 ) {
		        res.moveToFirst();
		        while ( !res.isAfterLast() ) {
			        array_list.add(getString(res, PERMANENT_COLUMN_NAME));
			        res.moveToNext();
		        }
		        res.close();
		        return array_list;
	        }
	        res.close();
        } catch ( Exception e ) {
//        	e.getMessage();
        }
        return array_list;
	}

    private boolean getPermanentCount() {
        try {
            Cursor res = readQuery(PERMANENT_TABLE_NAME);
            if ( res.getCount() > 0 ) {
                res.close();
                return true;
            }
	        res.close();
        } catch ( Exception e ) {
//            e.getMessage();
        }
        return false;
    }

    public String getPermanentCode (String value) {
        String _code = "" ;
        try {
            String _selection = PERMANENT_COLUMN_NAME + " = ?";
            String[] _args = {value};
            String[] _colToReturn = {PERMANENT_COLUMN_CODE};
            Cursor cursor = readQuery(PERMANENT_TABLE_NAME, _colToReturn, _selection, _args);
            if ( cursor.getCount() > 0 ){
                cursor.moveToFirst();
                _code = getString(cursor, PERMANENT_COLUMN_CODE);
                cursor.close();
                return _code;
            }
	        cursor.close();
        } catch (Exception e) {
//            e.getMessage();
        }
        return _code;
    }

    public String getPermanentName (String code) {
        String _value = "" ;
        try {
            String _selection = PERMANENT_COLUMN_CODE + " = ?";
            String[] _args = {code};
            String[] _colToReturn = {PERMANENT_COLUMN_NAME};
            Cursor cursor = readQuery(PERMANENT_TABLE_NAME, _colToReturn, _selection, _args);
            if ( cursor.getCount() > 0 ){
                cursor.moveToFirst();
                _value = getString(cursor, PERMANENT_COLUMN_NAME);
                cursor.close();
                return _value;
            }
	        cursor.close();
        } catch (Exception e) {
//            e.getMessage();
        }
        return _value;
    }

    public void deletePermanent () {
		deleteQuery(PERMANENT_TABLE_NAME);
    }

    //todo: Poi data function
    public void insertPoi (String name, String code) {
        try {
            ContentValues contentValues = new ContentValues();
            SQLiteDatabase db = this.getWritableDatabase();
            db.beginTransaction();
            contentValues.put(POI_COLUMN_NAME, name.trim());
            contentValues.put(POI_COLUMN_CODE, code);
            db.insert(POI_TABLE_NAME, null, contentValues);
            db.setTransactionSuccessful();
            db.endTransaction();
        } catch ( Exception e ) {
//            e.getMessage();
        }
    }

    public ArrayList< String > getPoi() {
        ArrayList< String > array_list = new ArrayList<>();
        try {
	        Cursor res = readQuery(POI_TABLE_NAME);
	        if ( res.getCount() > 0 ) {
		        res.moveToFirst();
		        while ( !res.isAfterLast() ) {
			        array_list.add(getString(res, POI_COLUMN_NAME));
			        res.moveToNext();
		        }
		        res.close();
		        return array_list;
	        }
	        res.close();
        } catch ( Exception e ) {
//        	e.getMessage();
        }
	    return array_list;
    }

    private boolean getPoiCount() {
        try {
            Cursor res = readQuery(POI_TABLE_NAME);
            if ( res.getCount() > 0 ) {
                res.close();
                return true;
            }
	        res.close();
        } catch ( Exception e ) {
//            e.getMessage();
        }
        return false;
    }

    public String getPoiCode (String value) {
        String _code = "" ;
        try {
            String _selection = POI_COLUMN_NAME + " = ?";
            String[] _args = {value};
            String[] _colToReturn = {POI_COLUMN_CODE};
            Cursor cursor = readQuery(POI_TABLE_NAME, _colToReturn, _selection, _args);
            if ( cursor.getCount() > 0 ){
                cursor.moveToFirst();
                _code = getString(cursor, POI_COLUMN_CODE);
                cursor.close();
                return _code;
            }
	        cursor.close();
        } catch (Exception e) {
//            e.getMessage();
        }
        return _code;
    }

    public String getPoiName (String code) {
        String _value = "" ;
        try {
            String _selection = POI_COLUMN_CODE + " = ?";
            String[] _args = {code};
            String[] _colToReturn = {POI_COLUMN_NAME};
            Cursor cursor = readQuery(POI_TABLE_NAME, _colToReturn, _selection, _args);
            if ( cursor.getCount() > 0 ){
                cursor.moveToFirst();
                _value = getString(cursor, POI_COLUMN_NAME);
                cursor.close();
                return _value;
            }
	        cursor.close();
        } catch (Exception e) {
//            e.getMessage();
        }
        return _value;
    }

    public void deletePoi () {
		deleteQuery(POI_TABLE_NAME);
    }

    //todo: Poa data function
    public void insertPoa (String name, String code) {
        try {
            ContentValues contentValues = new ContentValues();
            SQLiteDatabase db = this.getWritableDatabase();
            db.beginTransaction();
            contentValues.put(POA_COLUMN_NAME, name);
            contentValues.put(POA_COLUMN_CODE, code);
            db.insert(POA_TABLE_NAME, null, contentValues);
            db.setTransactionSuccessful();
            db.endTransaction();
        } catch ( Exception e ) {
//            e.getMessage();
        }
    }

    public ArrayList< String > getPoa () {
        ArrayList< String > array_list = new ArrayList<>();
        try {
	        Cursor res = readQuery(POA_TABLE_NAME);
	        if ( res.getCount() > 0 ) {
		        res.moveToFirst();
		        while ( !res.isAfterLast() ) {
			        array_list.add(getString(res, POA_COLUMN_NAME));
			        res.moveToNext();
		        }
		        res.close();
		        return array_list;
	        }
	        res.close();
        } catch ( Exception e ) {
//        	e.getMessage();
        }
	    return array_list;
    }

    private boolean getPoaCount() {
        try {
            Cursor res = readQuery(POA_TABLE_NAME);
            if ( res.getCount() > 0 ) {
                res.close();
                return true;
            }
	        res.close();
        } catch ( Exception e ) {
//            e.getMessage();
        }
        return false;
    }

    public String getPoaCode (String value) {
        String _code = "" ;
        try {
            String _selection = POA_COLUMN_NAME + " = ?";
            String[] _args = {value};
            String[] _colToReturn = {POA_COLUMN_CODE};
            Cursor cursor = readQuery(POA_TABLE_NAME, _colToReturn, _selection, _args);
            if ( cursor.getCount() > 0 ) {
                cursor.moveToFirst();
                _code = getString(cursor, POA_COLUMN_CODE);
                cursor.close();
                return _code;
            }
	        cursor.close();
        } catch (Exception e) {
//            e.getMessage();
        }
        return _code;
    }

    public String getPoaName (String code) {
        String _value = "";
        try {
            String _selection = POA_COLUMN_CODE + " = ?";
            String[] _args = {code};
            String[] _colToReturn = {POA_COLUMN_NAME};
            Cursor cursor = readQuery(POA_TABLE_NAME, _colToReturn, _selection, _args);
            if ( cursor.getCount() > 0 ){
                cursor.moveToFirst();
                _value = getString(cursor, POA_COLUMN_NAME);
                cursor.close();
                return _value;
            }
	        cursor.close();
        } catch (Exception e) {
//            e.getMessage();
        }
        return _value;
    }

    public void deletePoa () {
		deleteQuery(POA_TABLE_NAME);
    }

    //todo: city data function
    public void insertCity (String cityName, String cityCode) {
		try {
			SQLiteDatabase db = this.getWritableDatabase();
			ContentValues contentValues = new ContentValues();
			contentValues.put(BIRTH_CITY_COLUMN_NAME, cityName);
			contentValues.put(BIRTH_CITY_COLUMN_CODE, cityCode);
			db.insert(BIRTH_CITY_TABLE_NAME, null, contentValues);
		} catch ( Exception e ) {
//			e.getMessage();
		}
    }

    public ArrayList< String > getCity () {
        ArrayList< String > array_list = new ArrayList<>();
        try {
	        Cursor res = readQuery(BIRTH_CITY_TABLE_NAME);
	        if ( res.getCount() > 0 ) {
		        res.moveToFirst();
		        while ( !res.isAfterLast() ) {
			        array_list.add(getString(res, BIRTH_CITY_COLUMN_NAME));
			        res.moveToNext();
		        }
		        res.close();
		        return array_list;
	        }
	        res.close();
        } catch ( Exception e ) {
//        	e.getMessage();
        }
	    return array_list;
    }

    private boolean getCityCount() {
        try {
            Cursor res = readQuery(BIRTH_CITY_TABLE_NAME);
            if ( res.getCount() > 0 ) {
                res.close();
                return true;
            }
	        res.close();
        } catch ( Exception e ) {
//            e.getMessage();
        }
        return false;
    }

    public String getBirthCityCode (String value) {
        String _code = "" ;
        try {
            String _selection = BIRTH_CITY_COLUMN_NAME + " = ?";
            String[] _args = {value};
            String[] _colToReturn = {BIRTH_CITY_COLUMN_CODE};
            Cursor cursor = readQuery(BIRTH_CITY_TABLE_NAME, _colToReturn, _selection, _args);
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				_code = getString(cursor, BIRTH_CITY_COLUMN_CODE);
				cursor.close();
				return _code;
			}
	        cursor.close();
			return _code;
        } catch (Exception e) {
//            e.getMessage();
        }
        return _code;
    }

    public String getBirthCityName (String code) {
        String _value = "" ;
        try {
            String _selection = BIRTH_CITY_COLUMN_CODE + " = ?";
            String[] _args = {code};
            String[] _colToReturn = {BIRTH_CITY_COLUMN_NAME};
			Cursor cursor = readQuery(BIRTH_CITY_TABLE_NAME, _colToReturn, _selection, _args);
            if ( cursor.getCount() > 0 ){
                cursor.moveToFirst();
                _value = getString(cursor, BIRTH_CITY_COLUMN_NAME);
                cursor.close();
                return _value;
            }
	        cursor.close();
        } catch (Exception e) {
//            e.getMessage();
        }
        return _value;
    }

    public void deleteCity () {
		deleteQuery(BIRTH_CITY_TABLE_NAME);
    }

    //todo: Country data function
    public void insertCountry (String countryName, String countryCode) {
	    try {
		    SQLiteDatabase db = this.getWritableDatabase();
		    ContentValues contentValues = new ContentValues();
		    contentValues.put(BIRTH_COUNTRY_COLUMN_NAME, countryName);
		    contentValues.put(BIRTH_COUNTRY_COLUMN_CODE, countryCode);
		    db.insert(BIRTH_COUNTRY_TABLE_NAME, null, contentValues);
	    } catch ( Exception e ) {
//	    	e.getMessage();
	    }
    }

    public ArrayList< String > getCountry () {
        ArrayList< String > array_list = new ArrayList<>();
        try {
	        Cursor res = readQuery(BIRTH_COUNTRY_TABLE_NAME);
	        if ( res.getCount() > 0 ) {
		        res.moveToFirst();
		        while ( !res.isAfterLast() ) {
			        array_list.add(getString(res, BIRTH_COUNTRY_COLUMN_NAME));
			        res.moveToNext();
		        }
		        res.close();
		        return array_list;
	        }
	        res.close();
        } catch ( Exception e ) {
//        	e.getMessage();
        }
        return array_list;
    }

    private boolean getCountryCount() {
        try {
            Cursor res = readQuery(BIRTH_COUNTRY_TABLE_NAME);
            if ( res.getCount() > 0 ) {
                res.close();
                return true;
            }
	        res.close();
        } catch ( Exception e ) {
//            e.getMessage();
        }
        return false;
    }

    public String getBirthCountryCode (String value) {
        String _code = "" ;
        try {
            String _selection = BIRTH_COUNTRY_COLUMN_NAME + " = ?";
            String[] _args = {value};
            String[] _colToReturn = {BIRTH_COUNTRY_COLUMN_CODE};
			Cursor cursor = readQuery(BIRTH_COUNTRY_TABLE_NAME, _colToReturn, _selection, _args);
            if ( cursor.getCount() > 0 ){
                cursor.moveToFirst();
                _code = getString(cursor, BIRTH_COUNTRY_COLUMN_CODE);
                cursor.close();
                return _code;
            }
	        cursor.close();
        } catch (Exception e) {
//            e.getMessage();
        }
        return _code;
    }

    public String getBirthCountryName (String code) {
        String _value = "" ;
        try {
            String _selection = BIRTH_COUNTRY_COLUMN_CODE + " = ?";
            String[] _args = {code};
            String[] _colToReturn = {BIRTH_COUNTRY_COLUMN_NAME};
            Cursor cursor = readQuery(BIRTH_COUNTRY_TABLE_NAME, _colToReturn, _selection, _args);
            if ( cursor.getCount() > 0 ){
                cursor.moveToFirst();
                _value = getString(cursor, BIRTH_COUNTRY_COLUMN_NAME);
                cursor.close();
                return _value;
            }
	        cursor.close();
        } catch (Exception e) {
//            e.getMessage();
        }
        return _value;
    }

    public void deleteCountry () {
		deleteQuery(BIRTH_COUNTRY_TABLE_NAME);
    }

    //todo: PinCode data function
    public void insertPinCode (SQLiteDatabase db, String pinCode, String city, String state, String stateCode) {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(PIN_CODE_COLUMN_NAME, pinCode);
            contentValues.put(PIN_CODE_COLUMN_CITY, city);
            contentValues.put(PIN_CODE_COLUMN_STATE, state);
            contentValues.put(PIN_CODE_COLUMN_STATE_CODE, stateCode);
            db.insert(PIN_CODE_TABLE_NAME, null, contentValues);
        } catch ( Exception e ) {
//            e.getMessage();
        }
    }

    public ArrayList< String > getPinCodeData (String pinCode) {
        ArrayList< String > list = new ArrayList<>();
        try {
            String _selection = PIN_CODE_COLUMN_NAME + " = ?";
            String[] _args = {pinCode};
            String[] _colToReturn = {PIN_CODE_COLUMN_CITY, PIN_CODE_COLUMN_STATE};
            Cursor _res = readQuery(PIN_CODE_TABLE_NAME, _colToReturn, _selection, _args);
            if ( _res.getCount() > 0 ) {
	            _res.moveToFirst();
				list.add(getString(_res, PIN_CODE_COLUMN_CITY));
				list.add(getString(_res, PIN_CODE_COLUMN_STATE));
	            _res.close();
	            return list;
            }
	        _res.close();
        } catch ( Exception e ) {
//            e.getMessage();
        }
        return list;
    }

    public void deletePinCode () {
		deleteQuery(PIN_CODE_TABLE_NAME);
    }

    private boolean getPinCodeCount() {
        try {
            Cursor res = readQuery(PIN_CODE_TABLE_NAME);
            if ( res.getCount() > 0 ) {
                res.close();
                return true;
            }
	        res.close();
        } catch ( Exception e ) {
//            e.getMessage();
        }
        return false;
    }

    public String getStateCode(String value) {
        String _code  = "";
        try {
            String _selection = PIN_CODE_COLUMN_STATE + " = ?";
            String[] _args = {value.toUpperCase()};
            String[] _colToReturn = {PIN_CODE_COLUMN_STATE_CODE};
            Cursor _res = readQuery(PIN_CODE_TABLE_NAME, _colToReturn, _selection, _args);
			if (_res.getCount() > 0) {
				_res.moveToFirst();
				while (!_res.isAfterLast()) {
					_code = getString(_res, PIN_CODE_COLUMN_STATE_CODE);
					_res.moveToNext();
				}
				_res.close();
				return _code;
			}
	        _res.close();
        } catch ( Exception e ) {
//            e.getMessage();
        }
	    return _code;
    }

    public String getStateName(String code) {
        String _value  = "";
        try {
            String _selection = PIN_CODE_COLUMN_STATE_CODE + " = ?";
            String[] _args = {code};
            String[] _colToReturn = {PIN_CODE_COLUMN_STATE};
            Cursor _res = readQuery(PIN_CODE_TABLE_NAME, _colToReturn, _selection, _args);
            if ( _res.getCount() > 0 ) {
	            _res.moveToFirst();
	            _value = getString(_res, PIN_CODE_COLUMN_STATE);
	            _res.close();
	            return _value;
            }
	        _res.close();
        } catch ( Exception e ) {
//            e.getMessage();
        }
	    return _value;
    }

    public void insertScrutinyData (String scrutinyFormId, String scrutinyData) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(SCRUTINY_FORM_ID, scrutinyFormId);
            contentValues.put(SCRUTINY_DATA, scrutinyData);

			String[] _colToReturns = {"*"};
			String _selection = SCRUTINY_FORM_ID + " = ?";
			String[] _args = {scrutinyFormId};
			Cursor _cursor = readQuery(SCRUTINY_REJECTION_TABLE_NAME, _colToReturns, _selection, _args);
			if (_cursor == null) {
				return;
			}

            if (_cursor.getCount() > 0 ) {
                db.update(SCRUTINY_REJECTION_TABLE_NAME, contentValues, "referenceId = ?", new String[] {scrutinyFormId});
            } else {
                db.insert(SCRUTINY_REJECTION_TABLE_NAME, null, contentValues);
            }
            _cursor.close();
        } catch ( Exception e ) {
//            e.getMessage();
        }
    }

    public String getScrutinyData (String formId) {
        String _data = "";
        try {
			String _selection = SCRUTINY_FORM_ID + " = ?";
			String[] _args = {formId};
			String[] _colToReturn = {"*"};
			Cursor _cursor = readQuery(SCRUTINY_REJECTION_TABLE_NAME, _colToReturn, _selection, _args);
//        Cursor _cursor = db.rawQuery("select * from " + SCRUTINY_REJECTION_TABLE_NAME + " where " + SCRUTINY_FORM_ID + " = '" + referenceId + "';", null);
			_cursor.moveToFirst();
			_data = getString(_cursor, SCRUTINY_DATA);
			_cursor.close();
		} catch (Exception ignore) {}
        return _data;
    }

    /*todo: Pension Fund Data Function*/
    public void insertPensionFund(String name, String code) {
        try {
            ContentValues contentValues = new ContentValues();
            SQLiteDatabase db = this.getWritableDatabase();
            db.beginTransaction();
            contentValues.put(PENSION_FUND_COLUMN_NAME, name);
            contentValues.put(PENSION_FUND_COLUMN_CODE, code);
            db.insert(PENSION_FUND_TABLE_NAME, null, contentValues);
            db.setTransactionSuccessful();
            db.endTransaction();
        } catch (Exception e) {
//            e.getMessage();
        }
    }

    public void deletePensionFund () {
    	deleteQuery(PENSION_FUND_TABLE_NAME);
    }

    private boolean getPensionFundCount() {
        try {
            Cursor res = readQuery(PENSION_FUND_TABLE_NAME);
            if ( res.getCount() > 0 ) {
                res.close();
                return true;
            }
	        res.close();
        } catch ( Exception e ) {
//            e.getMessage();
        }
        return false;
    }

    public ArrayList<String> getPensionFund() {
	    ArrayList<String> _pfArrayList = new ArrayList<>();
	    try {
		    Cursor _cursor = readQuery(PENSION_FUND_TABLE_NAME);
			if (_cursor.getCount() > 0) {
				_cursor.moveToFirst();
				while (!_cursor.isAfterLast()) {
					_pfArrayList.add(getString(_cursor, PENSION_FUND_COLUMN_NAME));
					_cursor.moveToNext();
				}
				_cursor.close();
				return _pfArrayList;
			}
		    _cursor.close();
	    } catch ( Exception e ) {
//	    	e.getMessage();
	    }
	    return _pfArrayList;
    }

    public String getPensionFundCode(String value) {
        String _code  = "";
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String _selection = PENSION_FUND_COLUMN_NAME + " = ?";
            String[] _args = {value};
            String[] _colToReturn = {PENSION_FUND_COLUMN_CODE};
            Cursor _res = readQuery(PENSION_FUND_TABLE_NAME, _colToReturn, _selection, _args);
            if ( _res.getCount() > 0 ) {
	            _res.moveToFirst();
	            _code = getString(_res, PENSION_FUND_COLUMN_CODE);
	            _res.close();
	            return _code;
            }
	        _res.close();
        } catch ( Exception e ) {
//            e.getMessage();
        }
	    return _code;
    }

    public String getPensionFundName(String code) {
        String _value  = "";
        try {
            String _selection = PENSION_FUND_COLUMN_CODE + " = ?";
            String[] _args = {code};
            String[] _colToReturn = {PENSION_FUND_COLUMN_NAME};
            Cursor _res = readQuery(PENSION_FUND_TABLE_NAME, _colToReturn, _selection, _args);
            if ( _res.getCount() > 0 ) {
	            _res.moveToFirst();
	            _value = getString(_res, PENSION_FUND_COLUMN_NAME);
	            _res.close();
	            return _value;
            }
	        _res.close();
        } catch ( Exception e ) {
//            e.getMessage();
        }
	    return _value;
    }

    /***
     * this method is use to check either all master data store in database or not
     * @return if it return is true then its means all master data have inserted into database
     *         else false then its means all master data have not inserted into database
     */
    public boolean isAllMasterDataInserted() {
        return getPrefixCount() && getGenderCount() && getMaritalCount() && getRelationshipCount()
                && getOccupationCount() && getIncomeCount() && getEducationCount() && getCorrespondenceCount()
                && getPermanentCount() && getPoiCount() && getPoaCount() && getPensionFundCount() && getCityCount()
                && getCountryCount() && getPinCodeCount() && getProofOfDobCount();
    }

    /*todo: DiscrepencyListData function*/
    public class DiscrepancyListData {
        public String formId;
        public String allData;
    }

    public int getScrutinyDataCount () {
        ArrayList<DiscrepancyListData> _arrayList = new ArrayList<>();
        try {
	        Cursor _cursor = readQuery(SCRUTINY_REJECTION_TABLE_NAME);
			if (_cursor.getCount() > 0) {
				_cursor.moveToFirst();
				while (!_cursor.isAfterLast()) {
					DiscrepancyListData _data = new DiscrepancyListData();
					_data.formId = getString(_cursor, SCRUTINY_FORM_ID);
					_data.allData = getString(_cursor, SCRUTINY_DATA);
					_arrayList.add(_data);
					_cursor.moveToNext();
				}
				_cursor.close();
				return _arrayList.size();
			}
	        _cursor.close();
        } catch ( Exception e ) {
//        	e.getMessage();
        }
	    return _arrayList.size();
    }

    public boolean isScrutinyExist(String formId) {
    	try {
			String _selection = SCRUTINY_FORM_ID + " = ? ";
			String[] _args = {formId};
			String[] _colToReturns = {"*"};
			Cursor _res = readQuery(SCRUTINY_REJECTION_TABLE_NAME, _colToReturns, _selection, _args);
		    boolean _checkCount = _res.getCount() > 0;
		    _res.close();
		    return _checkCount;
	    } catch ( Exception ignore ) {}
	    return false;
    }

    /**
     * Created by Gautam Anand
     * insert session row
     *
     * @param id
     * @param key
     * @return
     */
    public boolean insertSession(String id, String key, String timeStamp) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(SESSION_COLUMN_ID, id);
            contentValues.put(SESSION_COLUMN_KEY, key);
            contentValues.put(SESSION_COLUMN_FORM_ID, "");
            contentValues.put(SESSION_COLUMN_TIMESTAMP, timeStamp);
            long rowInsert = db.insertOrThrow(SESSION_TABLE_NAME, null, contentValues);
			return rowInsert != -1;
        } catch (Exception e) {
//            e.getMessage();
			return false;
        }
    }

    /**
     * Created by Gautam Anand
     * delete all data from session table
     */
    public void deleteSession () {
    	deleteQuery(SESSION_TABLE_NAME);
    	try {
			SQLiteDatabase db = this.getWritableDatabase();
			db.execSQL("delete from " + SESSION_TABLE_NAME);
		} catch (Exception e) {
//			e.getMessage();
		}
    }

    /**
     * Created by Gautam Anand
     * update session row
     *
     * @param id
     * @param key
     * @param formId
     * @return
     */
    public void updateSession (String id, String key, String formId) {
    	try {
			if (TextUtils.isEmpty(formId)) {
				Cursor _cursor = getSession();
				if (_cursor.getCount() > 0) {
					_cursor.moveToFirst();
					String _formId = getString(_cursor, MyDBHelper.SESSION_COLUMN_FORM_ID);
					if (!TextUtils.isEmpty(_formId)) {
						formId = _formId;
					}
				}
				_cursor.close();
			}

			SQLiteDatabase db = this.getReadableDatabase();
			ContentValues contentValues = new ContentValues();
			contentValues.put(SESSION_COLUMN_KEY, key);
			contentValues.put(SESSION_COLUMN_FORM_ID, formId);
			try {
				db.update(SESSION_TABLE_NAME, contentValues, "id = ? ", new String[] {id});
			} catch ( Exception ex ) {
				//ex.printStackTrace();
			}
	    } catch ( Exception e ) {
//    		e.getMessage();
	    }
    }

    public void updateSessionTimeStamp (String id, String timeStamp) {
	    try {
		    SQLiteDatabase db = this.getWritableDatabase();
		    ContentValues contentValues = new ContentValues();
		    contentValues.put(SESSION_COLUMN_TIMESTAMP, timeStamp);
		    db.update(SESSION_TABLE_NAME, contentValues, "id = ? ", new String[] {id});
	    } catch ( Exception ex ) {
//		    ex.printStackTrace();
	    }
    }

    public void updateSessionFormId (String id, String formId) {
	    try {
		    SQLiteDatabase db = this.getWritableDatabase();
		    ContentValues contentValues = new ContentValues();
		    contentValues.put(SESSION_COLUMN_FORM_ID, formId);
		    db.update(SESSION_TABLE_NAME, contentValues, "id = ? ", new String[] {id});
	    } catch ( Exception ex ) {
//		    ex.printStackTrace();
	    }
    }

    public String getBdeNo() {
    	String bdeNo = "";
	    try {
		    Cursor _cursor = getSession();
		    if ( _cursor.getCount() > 0 ) {
			    _cursor.moveToFirst();
			    bdeNo = getString(_cursor, MyDBHelper.SESSION_COLUMN_ID);
				_cursor.close();
				return bdeNo;
		    }
			_cursor.close();
	    } catch ( Exception e ) {
//		    e.getMessage();
	    }
	    return bdeNo;
    }

    public String getFormId() {
    	String formId = "";
	    try {
		    Cursor _cursor = getSession();
		    if ( _cursor.getCount() > 0 ) {
			    _cursor.moveToFirst();
			    formId = getString(_cursor, MyDBHelper.SESSION_COLUMN_FORM_ID);
				_cursor.close();
				return formId;
		    }
			_cursor.close();
	    } catch ( Exception e ) {
//		    e.getMessage();
	    }
	    return formId;
    }

	public String getSessionToken() {
    	String token = "";
		try {
			Cursor res = readQuery(SESSION_TABLE_NAME);
			if ( res.getCount() > 0 ) {
				res.moveToFirst();
				token = getString(res, MyDBHelper.SESSION_COLUMN_KEY);
				res.close();
				return token;
			}
			res.close();
		} catch ( Exception ex ) {
//			UtilityClass.appendLog(ex.getMessage());
		}
		return token;
	}

	/**
	 * Created by Gautam Anand
	 * get row data from given table
	 *
	 * @param bdeId
	 * @return
	 */
	public boolean isBdeIdExists(String bdeId) {
		if (TextUtils.isEmpty(bdeId)) return false;
		try {
			String _selection = "id = ?";
			String[] _selectionArgs = {bdeId};
			String[] _colToReturn = {SESSION_COLUMN_ID, SESSION_COLUMN_KEY};
			Cursor cursor = readQuery(SESSION_TABLE_NAME, _colToReturn, _selection, _selectionArgs);
			boolean isBdeExists = cursor.getCount() > 0;
			cursor.close();
			return isBdeExists;
		} catch (Exception ignore) {}
		return false;
	}

    public String getTimeStampFromSession(String bdeNo) {
        String _timeStamp = "0";
		try {
			String _selection = SESSION_COLUMN_ID + " = ? ";
			String[] _selectionArgs = {bdeNo};
			String[] _colToReturn = {SESSION_COLUMN_TIMESTAMP};
			Cursor _res = readQuery(SESSION_TABLE_NAME, _colToReturn, _selection, _selectionArgs);
			if (_res.getCount() > 0) {
				_res.moveToFirst();
				_timeStamp = getString(_res, SESSION_COLUMN_TIMESTAMP);
				_res.close();
				return _timeStamp;
			}
			_res.close();
		} catch (Exception ignore) {}
        return _timeStamp;
    }

    public Cursor getSession () {
    	try {
			return readQuery(SESSION_TABLE_NAME);
		} catch (Exception e ) {
    		return null;
		}
    }

	public String getString (Cursor cursor, String column) {
		return cursor.getString(cursor.getColumnIndex(column));
	}

	/**
	 * Created by Ankit Sharma
	 * Used to fetch current of BDE with all session information
	 *
	 * @return - {@link ServiceSession} with current session
	 */
/**
     * Created by Ankit Sharma
     * get session row
     *
     * @return
     */
    public ServiceSession session() {
        ServiceSession s = null;
        SQLiteDatabase db = getReadableDatabase();
        Cursor res = db.query(SESSION_TABLE_NAME, null, null, null, null, null, null);

        // moved here on 01/10/2018
        s = new ServiceSession();

        if (res.getCount() != 0) {

            res.moveToFirst();

            s.setId(getString(res, SESSION_COLUMN_ID));
            s.setFormId(getString(res, SESSION_COLUMN_FORM_ID));
            s.setToken(getString(res, SESSION_COLUMN_KEY));
            s.setLastSync(getString(res, SESSION_COLUMN_TIMESTAMP));
        }

        res.close();
        return s;
    }

	/**
	 * This method is used to update user session which only update token w.r.t id pinStatus.e. BDE number
	 *
	 * @param id  - BDE number
	 * @param key - fresh token which need to replace with old token
	 * @return
	 */
	public synchronized boolean updateSession(String id, String key) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues contentValues = new ContentValues();
		contentValues.put(SESSION_COLUMN_KEY, key);

		String where = SESSION_COLUMN_ID + " = ? ";
		int affectedRow = db.update(SESSION_TABLE_NAME, contentValues, where, new String[]{id});
		return affectedRow != 0;
	}

	public synchronized boolean updateUserSession(String id, String key, String formID) {
		if (formID == null)
			updateSession(id, key);
		else
			updateSession(id, key, formID);
		return true;
	}

	private Cursor query(SQLiteDatabase db, String tableName) {
		return db.query(tableName, null, null, null, null, null, null);
	}

	private Cursor readQuery(String tableName) {
		return this.getReadableDatabase().query(tableName, null, null, null, null, null, null);
	}

	private Cursor query(SQLiteDatabase db, String tableName, String[] columns, String selection, String[] selectionArgs) {
		return db.query(tableName, columns, selection, selectionArgs, null, null, null);
	}

	private Cursor readQuery(String tableName, String[] columns, String selection, String[] selectionArgs) {
		return this.getReadableDatabase().query(tableName, columns, selection, selectionArgs, null, null, null);
	}

	//**************to delete table in database function*************/
	private void deleteQuery(SQLiteDatabase db, String tableName) {
		if (db == null || TextUtils.isEmpty(tableName)) return;
		try {
			db.delete(tableName, null, null);
		} catch (Exception e) {
//			e.getMessage();
		}
	}

	private void deleteQuery(String tableName) {
		if (TextUtils.isEmpty(tableName)) return;

		try {
			this.getWritableDatabase().delete(tableName, null, null);
		} catch (Exception e) {
			e.getMessage();
		}
	}

	private void deleteQuery(String tableName, String whereClause, String[] _args) {
		if (TextUtils.isEmpty(tableName)) return;
		try {
			this.getWritableDatabase().delete(tableName, whereClause, _args);
		} catch (Exception e) {
			e.getMessage();
		}
	}

	private int deleteQueryWithReturn(String tableName, String whereClause, String[] _args) {
		if (TextUtils.isEmpty(tableName)) return 0;
		try {
			return this.getWritableDatabase().delete(tableName, whereClause, _args);
		} catch (Exception e) {
//			e.getMessage();
		}
		return 0;
	}

	private void deleteQuery(SQLiteDatabase db, String tableName, String whereClause, String[] _args) {
		if (TextUtils.isEmpty(tableName)) return;
		if (db == null) db = this.getWritableDatabase();

		try {
			db.delete(tableName, whereClause, _args);
		} catch (Exception e) {
//			e.getMessage();
		}
	}

}


