package android.form.avss.prepaidcard.ui.custom_classes.interfaces;

public interface MyOnFocusChangeListener {
    void focusChanged();
}
