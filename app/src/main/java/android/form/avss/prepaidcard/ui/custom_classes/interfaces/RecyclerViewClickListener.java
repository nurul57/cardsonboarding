package android.form.avss.prepaidcard.ui.custom_classes.interfaces;

import android.view.View;

public interface RecyclerViewClickListener {
    void onClick(View v, int position);
}
