package android.form.avss.prepaidcard.ihmf_form_fill.TempPackage;


import android.form.avss.prepaidcard.ihmf_form_fill.interfaces.iHMFView;
import android.form.avss.prepaidcard.ihmf_form_fill.ui.ControlsView.IHMFBooleanView;
import android.form.avss.prepaidcard.ihmf_form_fill.util.ConstantsUtils;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class GroupControls {

    private String groupId;

    private Boolean isMandatory;

    private String activity;
    private Integer mSelected;
    private List<View> viewList = new ArrayList<>();

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public Boolean getMandatory() {
        return isMandatory;
    }

    public void setMandatory(Boolean mandatory) {
        isMandatory = mandatory;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public List<View> getViewList() {
        return viewList;
    }

    public void setViewList(List<View> viewList) {
        this.viewList = viewList;
    }

    /**
     * This is used to check whether any control in group control checked or not
     *
     * @return If empty, return true as no item to check. If it has control, then return true if control is found, otherwise, return false
     */
    public boolean isItemChecked() {

        mSelected = null;
        if (viewList.isEmpty()) return false;

        for (View view : viewList) {

            if (view instanceof IHMFBooleanView) {
                IHMFBooleanView bv = (IHMFBooleanView) view;
                if (bv.isChecked()) {
                    mSelected = bv.getControl().getId();
                    return true;
                }
            }
        }


        return false;
    }


    public iHMFView getSelectedItem() {

        if (mSelected == null)
            isItemChecked();


        if (mSelected == null) return null;

        for (View view : getViewList()) {

            if (view instanceof iHMFView<?>) {
                iHMFView hmfView = (iHMFView) view;

                if(hmfView.getControl() == null || hmfView.getControl().getId() == null) continue;

                if (hmfView.getControl().getId().equals(mSelected))
                    return hmfView;

            }
        }
        return null;

    }


    public void setChecked(int viewId) {


        for (View view : viewList) {
            IHMFBooleanView checkBox = (IHMFBooleanView) view;

            if (viewId != checkBox.getControl().getId()) {
                checkBox.setErrorEnable(false);

                if (ConstantsUtils.SINGLE_CHOICE.equalsIgnoreCase(activity))
                    checkBox.setChecked(checkBox.getControl().getId() == viewId);
            }
//            else if(checkBox.getControl().getId() == viewId)
//                mSelected = view;
        }
    }


    public void addItem(View view) {
        getViewList().add(view);
    }

    public void clear() {
        getViewList().clear();
    }
}
