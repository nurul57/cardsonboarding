package android.form.avss.prepaidcard.web_services.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.form.avss.prepaidcard.database.MyDBHelper;
import android.form.avss.prepaidcard.helper_classes.Constant;
import android.form.avss.prepaidcard.helper_classes.WebServicePreFactory;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.Requests.FormImageSubmitRequest;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.Requests.MarkedDataRequest;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.Requests.DataSubmit;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.Responses.DropDownMaster;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.Responses.Form;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.Responses.FormBaseResponse;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.Responses.FormDataSubmitResponse;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.Responses.ImageDataSubmitResponse;
import android.form.avss.prepaidcard.ihmf_form_fill.ui.Activities.FormFillActivity;
import android.form.avss.prepaidcard.web_services.ServiceGenerator;
import android.form.avss.prepaidcard.web_services.interfaces.AppServices;
import android.form.avss.prepaidcard.web_services.interfaces.ServicesFunctionName;
import android.form.avss.prepaidcard.web_services.requests.BiometricValidationRequest;
import android.form.avss.prepaidcard.web_services.requests.DocsUploadRequest;
import android.form.avss.prepaidcard.web_services.requests.DocumentDataRequest;
import android.form.avss.prepaidcard.web_services.requests.FinalDataSubmitRequest;
import android.form.avss.prepaidcard.web_services.requests.GenderMasterRequest;
import android.form.avss.prepaidcard.web_services.requests.LoginRequest;
import android.form.avss.prepaidcard.web_services.requests.OVDDeemedOVDMasterRequest;
import android.form.avss.prepaidcard.web_services.requests.PanVerificationRequest;
import android.form.avss.prepaidcard.web_services.requests.PincodeMasterRequest;
import android.form.avss.prepaidcard.web_services.requests.PrefixMasterRequest;
import android.form.avss.prepaidcard.web_services.requests.ResidenceMasterRequest;
import android.form.avss.prepaidcard.web_services.responses.BiometricValidationResponse;
import android.form.avss.prepaidcard.web_services.responses.DocumentDataResponse;
import android.form.avss.prepaidcard.web_services.responses.DocumentUploadResponse;
import android.form.avss.prepaidcard.web_services.responses.FinalDataSubmitResponse;
import android.form.avss.prepaidcard.web_services.responses.GenderMasterResponse;
import android.form.avss.prepaidcard.web_services.responses.LoginResponse;
import android.form.avss.prepaidcard.web_services.responses.OVDDeemedOVDMasterResponse;
import android.form.avss.prepaidcard.web_services.responses.PanVerificationResponse;
import android.form.avss.prepaidcard.web_services.responses.PincodeMasterResponse;
import android.form.avss.prepaidcard.web_services.responses.PrefixMasterResponse;
import android.form.avss.prepaidcard.web_services.responses.ResidenceMasterResponse;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.google.gson.JsonSyntaxException;

import java.security.SecureRandom;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Services implements ServicesFunctionName {
    private Constant constant;

    public Services() {
        constant = Constant.getInstance();
    }

    private Login loginService;
    private PrefixMaster prefixMasterService;
    private OVDMaster ovdMasterService;
    private GenderMaster genderMasterService;
    private ResidenceMaster residenceMasterService;
    private PinCodeMaster pinCodeMasterService;
    private ValidateAadhaar aadhaarService;
    private ValidatePan panService;
    private FinalDataSubmit finalDataService;
    private GetMarkingDetails getMarkingDetailsService;
    private GetDropDowns getDropDownsService;
    private SaveFormData saveFormDataService;
    private GetDocumentData getDocumentData;
    private UploadDocumentData uploadDocumentData;
    private FormImageSubmit formImageSubmitService;


    public interface UploadDocumentData {
        void uploadDocumentDataSuccessful(DocumentUploadResponse.UploadDocument data);
        void uploadDocumentDataFailed(boolean status, String error);
    }

    public interface GetDocumentData {
        void getDocumentDataSuccessful(DocumentDataResponse.DocImageData data);
        void getDocumentDataFailed(boolean status, String error);
    }

    public interface SaveFormData {
        void onSaveFormDataSuccessful(FormDataSubmitResponse data);
        void onSaveFormDataFailed(boolean status, String error);
    }

    public interface GetMarkingDetails {
        void onGetDynamicControlsSuccess(Form data);
        void onGetDynamicControlsFailed(boolean status, String error);
    }

    public interface GetDropDowns {
        void getDropDownsSuccessful(ResponseBody data);
        void getDropDownsFailed(boolean status, String error);
    }

    public interface Login {
        void loginSuccessful(LoginResponse.LoginData data);
        void loginFailed(boolean status, String error);
    }

    public interface PrefixMaster {
        void prefixMasterSuccessful(PrefixMasterResponse.PrefixMaster data);
        void prefixMasterFailed(boolean status, String error);
    }

    public interface OVDMaster {
        void ovdMasterSuccessful(OVDDeemedOVDMasterResponse.OVDDeemedOVDMaster data);
        void ovdMasterFailed(boolean status, String error);
    }

    public interface GenderMaster {
        void genderMasterSuccessful(GenderMasterResponse.GenderMaster data);
        void genderMasterFailed(boolean status, String error);
    }

    public interface ResidenceMaster {
        void residenceMasterSuccessful(ResidenceMasterResponse.ResidenceMaster data);
        void residenceMasterFailed(boolean status, String error);
    }

    public interface PinCodeMaster {
        void pinCodeMasterSuccessful(PincodeMasterResponse.PincodeMaster data);
        void pinCodeMasterFailed(boolean status, String error);
    }

    public interface ValidateAadhaar {
        void aadhaarSuccessful(BiometricValidationResponse.BiometricValidation data);
        void aadhaarFailed(boolean status, String error);
    }

    public interface ValidatePan {
        void panSuccessful(PanVerificationResponse.PanVerification data);
        void panFailed(boolean status, String error);
    }

    public interface FinalDataSubmit {
        void finalDataSubmitSuccessful(FinalDataSubmitResponse.FinalSubmit data);
        void finalDataSubmitFailed(boolean status, String error);
    }

    //added by nurul for ihmf_imageUpload
    public interface FormImageSubmit {
        void onImageUploadSuccessful(FormImageSubmitRequest request, ImageDataSubmitResponse response);
        void onImageUploadFailed(boolean b, String msg);
    }

    private String getFinalString(String finalRequest, Context context, boolean encOrDec, byte[] iv) throws Exception {
        WebServicePreFactory preFactory = new WebServicePreFactory(iv, context);
        if (encOrDec) {
            return preFactory.bytesToHex(preFactory.enc(finalRequest));
        } else {
            return new String(preFactory.dec(finalRequest));
        }
    }

    public void uploadDocData(DocsUploadRequest docsUploadRequest, final Context activity, String token, String refId) {
        if (activity == null) return;

        final byte[] randomByte = new byte[16];
        SecureRandom random = new SecureRandom();
        random.nextBytes(randomByte);
        String finalRequest = null;
        String finalEncryptedRequest;
        uploadDocumentData = (UploadDocumentData) activity;

        finalRequest = constant.toJson(docsUploadRequest);
        try {
            finalEncryptedRequest = getFinalString(finalRequest, activity, true, randomByte);
            if (TextUtils.isEmpty(finalEncryptedRequest)) {
                uploadDocumentData.uploadDocumentDataFailed(false, "bad request");
                return;
            }
        } catch (Exception e) {
            uploadDocumentData.uploadDocumentDataFailed(false, e.getMessage());
            return;
        }

        if (TextUtils.isEmpty(finalEncryptedRequest)) {
            uploadDocumentData.uploadDocumentDataFailed(false, "data is empty");
            return;
        }

        AppServices services = ServiceGenerator.createService(AppServices.class, activity, UPLOAD_IMAGE_DATA);
        Call<DocumentUploadResponse> call = services.uploadImageData(finalEncryptedRequest, refId,
                Base64.encodeToString(randomByte, Base64.DEFAULT), ServicesFunctionName.UPLOAD_IMAGE_DATA, token);
        call.enqueue(new Callback<DocumentUploadResponse>() {
            @Override
            public void onResponse(Call<DocumentUploadResponse> call, Response<DocumentUploadResponse> response) {
                if (response != null && response.body() != null) {

                    String _decryptedResponseString = "";
                    try {
                        _decryptedResponseString = getFinalString(response.body().data, activity, false, randomByte);
                        DocumentUploadResponse.UploadDocument _responseFromServer = constant.fromJson(_decryptedResponseString, DocumentUploadResponse.UploadDocument.class);
                        if (_responseFromServer != null) {
                            if (_responseFromServer.data() != null) {
                                uploadDocumentData.uploadDocumentDataSuccessful(_responseFromServer);
                            } else {
                                uploadDocumentData.uploadDocumentDataFailed(false, _responseFromServer.msg());
                            }
                        } else {
                            uploadDocumentData.uploadDocumentDataFailed(false, _responseFromServer.msg());
                        }
                    } catch (JsonSyntaxException e) {
                        e.printStackTrace();
                        uploadDocumentData.uploadDocumentDataFailed(false, e.getMessage());
                    } catch (Exception e) {
                        e.getMessage();
                        uploadDocumentData.uploadDocumentDataFailed(false, e.getMessage());
                    }
                } else {
                    uploadDocumentData.uploadDocumentDataFailed(false, response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<DocumentUploadResponse> call, Throwable t) {
                uploadDocumentData.uploadDocumentDataFailed(false, t.getMessage());
            }
        });
    }

    public void getDocumentData(DocumentDataRequest documentImageRequest, final Context activity, String token, String refId) {
        if (activity == null) return;

        final byte[] randomByte = new byte[16];
        SecureRandom random = new SecureRandom();
        random.nextBytes(randomByte);
        String finalRequest = null;
        String finalEncryptedRequest;
        getDocumentData = (GetDocumentData) activity;

        finalRequest = constant.toJson(documentImageRequest);
        try {
            finalEncryptedRequest = getFinalString(finalRequest, activity, true, randomByte);
            if (TextUtils.isEmpty(finalEncryptedRequest)) {
                getDocumentData.getDocumentDataFailed(false, "bad request");
                return;
            }
        } catch (Exception e) {
            getDocumentData.getDocumentDataFailed(false, e.getMessage());
            return;
        }

        if (TextUtils.isEmpty(finalEncryptedRequest)) {
            getDocumentData.getDocumentDataFailed(false, "data is empty");
            return;
        }

        AppServices services = ServiceGenerator.createService(AppServices.class, activity, GET_IMAGE_DATA);
        Call<DocumentDataResponse> call = services.getImageData(finalEncryptedRequest, refId,
                Base64.encodeToString(randomByte, Base64.DEFAULT), GET_IMAGE_DATA, token);
        call.enqueue(new Callback<DocumentDataResponse>() {
            @Override
            public void onResponse(Call<DocumentDataResponse> call, Response<DocumentDataResponse> response) {
                if (response != null && response.body() != null) {

                    String _decryptedResponseString = "";
                    try {
                        _decryptedResponseString = getFinalString(response.body().data, activity, false, randomByte);
                        DocumentDataResponse.DocImageData _responseFromServer = constant.fromJson(_decryptedResponseString, DocumentDataResponse.DocImageData.class);
                        if (_responseFromServer != null && _responseFromServer.data() != null) {
                            getDocumentData.getDocumentDataSuccessful(_responseFromServer);
                        } else {
                            getDocumentData.getDocumentDataFailed(false, _responseFromServer.msg());
                        }
                    } catch (JsonSyntaxException e) {
                        e.printStackTrace();
                        getDocumentData.getDocumentDataFailed(false, e.getMessage());
                    } catch (Exception e) {
                        e.getMessage();
                        getDocumentData.getDocumentDataFailed(false, e.getMessage());
                    }
                } else {
                    getDocumentData.getDocumentDataFailed(false, response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<DocumentDataResponse> call, Throwable t) {
                getDocumentData.getDocumentDataFailed(false, t.getMessage());
            }
        });
    }

    public void saveFormData(final DataSubmit saveFormDataRequest, final Context activity, String token, String refId) {
        if (activity == null) return;

        final byte[] randomByte = new byte[16];
        SecureRandom random = new SecureRandom();
        random.nextBytes(randomByte);
        String finalRequest;

        saveFormDataService = (SaveFormData) activity;

        finalRequest = constant.toJson(saveFormDataRequest);
        try {
            if (TextUtils.isEmpty(finalRequest)) {
                getMarkingDetailsService.onGetDynamicControlsFailed(false, "bad request");
                return;
            }
        } catch (Exception e) {
            getMarkingDetailsService.onGetDynamicControlsFailed(false, e.getMessage());
            return;
        }

        if (TextUtils.isEmpty(finalRequest)) {
            getMarkingDetailsService.onGetDynamicControlsFailed(false, "data is empty");
            return;
        }

        AppServices services = ServiceGenerator.createService(AppServices.class, activity, PDF_DATA_SAVE_VIEW);
        Call<ResponseBody> call = services.saveFormData(refId ,finalRequest,
                Base64.encodeToString(randomByte, Base64.DEFAULT), PDF_DATA_SAVE_VIEW, token);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response == null || response.body() == null) {
                    onFailure(null, new Throwable("Empty response body"));
                    return;
                }

                try {
                    JSONObject baseResponse = new JSONObject(response.body().string());

                    JSONObject data = baseResponse.getJSONObject("da");

                    if(!data.getBoolean("ss")) {
                        onFailure(call, new Throwable(data.getString("msg")));
                        return;
                    }

                    JSONObject responseData = data.getJSONObject("da");

                    FormDataSubmitResponse response1 = new Gson().fromJson(responseData.toString(), new TypeToken<FormDataSubmitResponse>(){}.getType());
                    saveFormDataService.onSaveFormDataSuccessful(response1);


                } catch (Exception e) {
                    onFailure(call, new Throwable(e.getMessage()));
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                saveFormDataService.onSaveFormDataFailed(false, t.getMessage());
            }
        });
    }

    public void getDropDowns( final Context activity, String token) {
        if (activity == null) return;

        final byte[] randomByte = new byte[16];
        SecureRandom random = new SecureRandom();
        random.nextBytes(randomByte);

        getDropDownsService = (GetDropDowns) activity;

        AppServices services = ServiceGenerator.createService(AppServices.class, activity, GET_DROPDOWN);
        Call<ResponseBody> call = services.getDropDowns("",
                Base64.encodeToString(randomByte, Base64.DEFAULT), GET_DROPDOWN, token);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.body() != null) {
                    try {
                        JSONObject baseResponse = new JSONObject(response.body().string());

                        JSONObject da = baseResponse.getJSONObject("da");

                        if (da.getBoolean("status")) {
                            JSONArray dataArray = da.getJSONArray("json");
                            List<DropDownMaster> dataList = new Gson().fromJson(dataArray.toString(), new TypeToken<List<DropDownMaster>>() {
                            }.getType());

                            if (dataList != null && !dataList.isEmpty()) {
                                MyDBHelper myDBHelper = MyDBHelper.getInstance(activity);
                                myDBHelper.deleteDropDown();
                                for (DropDownMaster ddData : dataList) {
                                    myDBHelper.insertDropDownData(ddData.getDdLabelId(), new Gson().toJson(ddData.getDataList()));
                                }
                            }
                            getDropDownsService.getDropDownsSuccessful(response.body());

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        getDropDownsService.getDropDownsFailed(false, e.getMessage());
                    } catch (Exception e) {
                        e.printStackTrace();
                        getDropDownsService.getDropDownsFailed(false, e.getMessage());
                    }
                } else {
                    getDropDownsService.getDropDownsFailed(false, response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                getDropDownsService.getDropDownsFailed(false, t.getMessage());
            }
        });
    }

    public void getMarkingDetails(MarkedDataRequest markingDetailRequest, final Context activity, String token) {
        if (markingDetailRequest == null || activity == null) return;

        final byte[] randomByte = new byte[16];
        SecureRandom random = new SecureRandom();
        random.nextBytes(randomByte);
        String finalRequest = null;

        getMarkingDetailsService = (GetMarkingDetails) activity;
        finalRequest = constant.toJson(markingDetailRequest);
        try {
            if (TextUtils.isEmpty(finalRequest)) {
                getMarkingDetailsService.onGetDynamicControlsFailed(false, "bad request");
                return;
            }
        } catch (Exception e) {
            getMarkingDetailsService.onGetDynamicControlsFailed(false, e.getMessage());
            return;
        }

        if (TextUtils.isEmpty(finalRequest)) {
            getMarkingDetailsService.onGetDynamicControlsFailed(false, "data is empty");
            return;
        }

        AppServices services = ServiceGenerator.createService(AppServices.class, activity,GET_MARKING_DETAILS);
        Call<ResponseBody> call = services.getMarkingDetails(finalRequest,
                Base64.encodeToString(randomByte, Base64.DEFAULT), GET_MARKING_DETAILS, token);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response == null || response.body() == null) {
                    onFailure(null, new Throwable("Empty body"));
                    return;
                }
                try {
                    JSONObject baseResponse =new JSONObject(response.body().string());

                    JSONObject data = baseResponse.getJSONObject("da");

                    if(data.getBoolean("status")){
                        Form formData = new Gson().fromJson(data.getJSONObject("json").toString(), new TypeToken<Form>(){}.getType());
                        getMarkingDetailsService.onGetDynamicControlsSuccess(formData);
                    }
                    else {
                        onFailure(call, new Throwable(data.getString("msg")));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    onFailure(null, new Throwable(e.getMessage()));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                getMarkingDetailsService.onGetDynamicControlsFailed(false, t.getMessage());
            }
        });
    }

    public void validateAadhaar(BiometricValidationRequest validateAadhaarRequest, final Context activity, String token) {
        if (validateAadhaarRequest == null || activity == null) return;

        final byte[] randomByte = new byte[16];
        SecureRandom random = new SecureRandom();
        random.nextBytes(randomByte);
        String finalRequest = null;
        String finalEncryptedRequest = null;

        aadhaarService = (ValidateAadhaar) activity;
        finalRequest = constant.toJson(validateAadhaarRequest);
        try {
            finalEncryptedRequest = getFinalString(finalRequest, activity, true, randomByte);
            if (TextUtils.isEmpty(finalEncryptedRequest)) {
                aadhaarService.aadhaarFailed(false, "bad request");
                return;
            }
        } catch (Exception e) {
            aadhaarService.aadhaarFailed(false, e.getMessage());
            return;
        }

        if (TextUtils.isEmpty(finalRequest)) {
            aadhaarService.aadhaarFailed(false, "data is empty");
            return;
        }
        AppServices services = ServiceGenerator.createService(AppServices.class, activity, AUTHENTICATE_AADHAAR);
        Call<BiometricValidationResponse> call = services.validateAadhaar(finalEncryptedRequest,
                Base64.encodeToString(randomByte, Base64.DEFAULT), AUTHENTICATE_AADHAAR, token);
        call.enqueue(new Callback<BiometricValidationResponse>() {
            @Override
            public void onResponse(Call<BiometricValidationResponse> call, Response<BiometricValidationResponse> response) {
                if (response != null && response.body() != null) {
                    String _decryptedResponseString = "";
                    try {
                        _decryptedResponseString = getFinalString(response.body().data, activity, false, randomByte);

                        BiometricValidationResponse.BiometricValidation _responseFromServer =
                                new Gson().fromJson(_decryptedResponseString, BiometricValidationResponse.BiometricValidation.class);


                        if (_responseFromServer != null && _responseFromServer.data() != null) {
                            aadhaarService.aadhaarSuccessful(_responseFromServer);
                        } else {
                            aadhaarService.aadhaarFailed(false, response.body().toString());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        aadhaarService.aadhaarFailed(false, e.getMessage());
                    }
                } else {
                    aadhaarService.aadhaarFailed(false, response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<BiometricValidationResponse> call, Throwable t) {
                aadhaarService.aadhaarFailed(false, "");
            }
        });

    }

    public void validatePan(final PanVerificationRequest panRequest, final Context activity, String token) {
        if (panRequest == null || activity == null) return;

        final byte[] randomByte = new byte[16];
        SecureRandom random = new SecureRandom();
        random.nextBytes(randomByte);
        String finalRequest = null;
        String finalEncryptedRequest = null;

        panService = (ValidatePan) activity;
        finalRequest = constant.toJson(panRequest);
        try {
            finalEncryptedRequest = getFinalString(finalRequest, activity, true, randomByte);
            if (TextUtils.isEmpty(finalEncryptedRequest)) {
                panService.panFailed(false, "");
                return;
            }
        } catch (Exception e) {
            panService.panFailed(false, e.getMessage());
            return;
        }

        if (TextUtils.isEmpty(finalRequest)) {
            panService.panFailed(false, "data is empty");
            return;
        }
        AppServices services = ServiceGenerator.createService(AppServices.class, activity, PAN_VERIFICATION);
        Call<PanVerificationResponse> call = services.validatePan(finalEncryptedRequest,
                Base64.encodeToString(randomByte, Base64.DEFAULT), PAN_VERIFICATION, token);
        call.enqueue(new Callback<PanVerificationResponse>() {
            @Override
            public void onResponse(Call<PanVerificationResponse> call, Response<PanVerificationResponse> response) {
                if (response != null && response.body() != null) {
                    String _decryptedResponseString = "";
                    try {
                        _decryptedResponseString = getFinalString(response.body().data, activity, false, randomByte);
                        PanVerificationResponse.PanVerification _responseFromServer = constant.fromJson(_decryptedResponseString, PanVerificationResponse.PanVerification.class);
                        if (_responseFromServer != null) {
                            panService.panSuccessful(_responseFromServer);
                        } else {
                            panService.panFailed(false, response.errorBody().toString());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        panService.panFailed(false, e.getMessage());
                    }

                } else {
                    panService.panFailed(false, response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<PanVerificationResponse> call, Throwable t) {
                panService.panFailed(false, t.getMessage());
            }
        });

    }

    public void finalSubmitData(final FinalDataSubmitRequest finalSubmitRequest, final Context activity, String token) {
        if (finalSubmitRequest == null || activity == null) return;

        final byte[] randomByte = new byte[16];
        SecureRandom random = new SecureRandom();
        random.nextBytes(randomByte);
        String finalRequest = null;
        String finalEncryptedRequest = null;

        finalDataService = (FinalDataSubmit) activity;
        finalRequest = constant.toJson(finalSubmitRequest);
        try {
            finalEncryptedRequest = getFinalString(finalRequest, activity, true, randomByte);
            if (TextUtils.isEmpty(finalEncryptedRequest)) {
                finalDataService.finalDataSubmitFailed(false, "");
                return;
            }
        } catch (Exception e) {
            finalDataService.finalDataSubmitFailed(false, e.getMessage());
            return;
        }

        if (TextUtils.isEmpty(finalRequest)) {
            finalDataService.finalDataSubmitFailed(false, "data is empty");
            return;
        }
        AppServices services = ServiceGenerator.createService(AppServices.class, activity, SAVE_DATA);
        Call<FinalDataSubmitResponse> call = services.finalSubmitData(finalEncryptedRequest,
                Base64.encodeToString(randomByte, Base64.DEFAULT), SAVE_DATA, token);
        call.enqueue(new Callback<FinalDataSubmitResponse>() {
            @Override
            public void onResponse(Call<FinalDataSubmitResponse> call, Response<FinalDataSubmitResponse> response) {
                if (response != null && response.body() != null) {
                    String _decryptedResponseString = "";
                    try {
                        _decryptedResponseString = getFinalString(response.body().data, activity, false, randomByte);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    FinalDataSubmitResponse.FinalSubmit _responseFromServer = constant.fromJson(_decryptedResponseString, FinalDataSubmitResponse.FinalSubmit.class);
                    if (_responseFromServer != null) {
                        finalDataService.finalDataSubmitSuccessful(_responseFromServer);
                    } else {
                        finalDataService.finalDataSubmitFailed(false, response.errorBody().toString());
                    }
                } else {
                    finalDataService.finalDataSubmitFailed(false, response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<FinalDataSubmitResponse> call, Throwable t) {
                finalDataService.finalDataSubmitFailed(false, t.getMessage());
            }
        });

    }

    public void login(LoginRequest loginRequest, final Context activity) {
        if (loginRequest == null || activity == null) return;

        final byte[] randomByte = new byte[16];
        SecureRandom random = new SecureRandom();
        random.nextBytes(randomByte);
        String finalRequest = null;
        String finalEncryptedRequest = null;
        finalRequest = constant.toJson(loginRequest);
        try {
            finalEncryptedRequest = getFinalString(finalRequest, activity, true, randomByte);
            if (TextUtils.isEmpty(finalEncryptedRequest)) {
                loginService.loginFailed(false, "");
                return;
            }
        } catch (Exception e) {
            loginService.loginFailed(false, e.getMessage());
            return;
        }

        if (TextUtils.isEmpty(finalRequest)) {
            loginService.loginFailed(false, "data is empty");
            return;
        }

        loginService = (Login) activity;
        AppServices services = ServiceGenerator.createService(AppServices.class, activity, AUTHENTICATION_BD);
        Call<LoginResponse> call = services.getUserLogin(finalEncryptedRequest,
                Base64.encodeToString(randomByte, Base64.DEFAULT), AUTHENTICATION_BD);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response != null && response.body() != null) {
                    String _decryptedResponseString = "";
                    try {
                        _decryptedResponseString = getFinalString(response.body().data, activity, false, randomByte);
                        LoginResponse.LoginData _responseFromServer = new Gson().fromJson(_decryptedResponseString, LoginResponse.LoginData.class);
                        if (_responseFromServer != null) {
                            loginService.loginSuccessful(_responseFromServer);
                        } else {
                            loginService.loginFailed(false, response.errorBody().toString());
                        }
                    } catch (JsonIOException e) {
                        loginService.loginFailed(false, e.getMessage());
                    }
                    catch (Exception e) {
                        loginService.loginFailed(false, e.getMessage());
                    }

                } else {
                    loginService.loginFailed(false, response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                loginService.loginFailed(false, t.getMessage());
            }
        });

    }

    public void fetchPrefixMaster(PrefixMasterRequest prefixMasterRequest, final Context activity) {
        if (prefixMasterRequest == null || activity == null) return;

        final byte[] randomByte = new byte[16];
        SecureRandom random = new SecureRandom();
        random.nextBytes(randomByte);
        String finalRequest = null;
        String finalEncryptedRequest = null;

        prefixMasterService = (PrefixMaster) activity;
        finalRequest = constant.toJson(prefixMasterRequest);
        try {
            finalEncryptedRequest = getFinalString(finalRequest, activity, true, randomByte);
            if (TextUtils.isEmpty(finalEncryptedRequest)) {
                prefixMasterService.prefixMasterFailed(false, "");
                return;
            }
        } catch (Exception e) {
            prefixMasterService.prefixMasterFailed(false, e.getMessage());
            return;
        }

        if (TextUtils.isEmpty(finalRequest)) {
            prefixMasterService.prefixMasterFailed(false, "data is empty");
            return;
        }
        AppServices services = ServiceGenerator.createService(AppServices.class, activity, GET_TITLE_MASTER);
        Call<PrefixMasterResponse> call = services.getPrefixMaster(finalEncryptedRequest,
                Base64.encodeToString(randomByte, Base64.DEFAULT), GET_TITLE_MASTER);
        call.enqueue(new Callback<PrefixMasterResponse>() {
            @Override
            public void onResponse(Call<PrefixMasterResponse> call, Response<PrefixMasterResponse> response) {
                if (response != null && response.body() != null) {
                    String _decryptedResponseString = "";
                    try {
                        _decryptedResponseString = getFinalString(response.body().data, activity, false, randomByte);
                        PrefixMasterResponse.PrefixMaster _responseFromServer = new Gson().fromJson(_decryptedResponseString, PrefixMasterResponse.PrefixMaster.class);

                        MyDBHelper myDBHelper = MyDBHelper.getInstance(activity);
                        myDBHelper.deletePrefix();

                        if (_responseFromServer.data() != null) {
                            for (PrefixMasterResponse.PrefixMaster prefixMaster : _responseFromServer.data()) {
                                myDBHelper.insertPrefix(prefixMaster);
                            }
                            prefixMasterService.prefixMasterSuccessful(_responseFromServer);
                        } else {
                            prefixMasterService.prefixMasterFailed(false, response.errorBody().toString());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        prefixMasterService.prefixMasterFailed(false, e.getMessage());
                    }
                } else {
                    prefixMasterService.prefixMasterFailed(false, response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<PrefixMasterResponse> call, Throwable t) {
                prefixMasterService.prefixMasterFailed(false, t.getMessage());
            }
        });

    }

    public void fetchOVDMaster(OVDDeemedOVDMasterRequest ovdMasterRequest, final Context activity) {
        if (ovdMasterRequest == null || activity == null) return;

        final byte[] randomByte = new byte[16];
        SecureRandom random = new SecureRandom();
        random.nextBytes(randomByte);
        String finalRequest = null;
        String finalEncryptedRequest = null;

        ovdMasterService = (OVDMaster) activity;
        finalRequest = constant.toJson(ovdMasterRequest);
        try {
            finalEncryptedRequest = getFinalString(finalRequest, activity, true, randomByte);
            if (TextUtils.isEmpty(finalEncryptedRequest)) {
                ovdMasterService.ovdMasterFailed(false, "");
                return;
            }
        } catch (Exception e) {
            ovdMasterService.ovdMasterFailed(false, e.getMessage());
            return;
        }

        if (TextUtils.isEmpty(finalRequest)) {
            ovdMasterService.ovdMasterFailed(false, "data is empty");
            return;
        }

        AppServices services = ServiceGenerator.createService(AppServices.class, activity, GET_OVD_MASTER);
        Call<OVDDeemedOVDMasterResponse> call = services.getOVDMaster(finalEncryptedRequest,
                Base64.encodeToString(randomByte, Base64.DEFAULT), GET_OVD_MASTER);
        call.enqueue(new Callback<OVDDeemedOVDMasterResponse>() {
            @Override
            public void onResponse(Call<OVDDeemedOVDMasterResponse> call, Response<OVDDeemedOVDMasterResponse> response) {
                if (response != null && response.body() != null) {
                    String _decryptedResponseString = "";
                    try {
                        _decryptedResponseString = getFinalString(response.body().data, activity, false, randomByte);
                        OVDDeemedOVDMasterResponse.OVDDeemedOVDMaster _responseFromServer = new Gson().fromJson(_decryptedResponseString, OVDDeemedOVDMasterResponse.OVDDeemedOVDMaster.class);

                        MyDBHelper myDBHelper = MyDBHelper.getInstance(activity);
                        myDBHelper.deleteDocProof();
                        if (_responseFromServer.data() != null && _responseFromServer.data() != null) {
                            for (OVDDeemedOVDMasterResponse.OVDDeemedOVDMaster ovdDeemedOVDMaster : _responseFromServer.data()) {
                                myDBHelper.insertDocProof(ovdDeemedOVDMaster);
                            }
                            ovdMasterService.ovdMasterSuccessful(_responseFromServer);
                        } else {
                            ovdMasterService.ovdMasterFailed(false, response.errorBody().toString());
                        }
                    } catch (JsonIOException e) {
                        e.printStackTrace();
                        ovdMasterService.ovdMasterFailed(false, e.getMessage());
                    } catch (Exception e) {
                        e.printStackTrace();
                        ovdMasterService.ovdMasterFailed(false, e.getMessage());
                    }

                } else {
                    ovdMasterService.ovdMasterFailed(false, response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<OVDDeemedOVDMasterResponse> call, Throwable t) {
                ovdMasterService.ovdMasterFailed(false, t.getMessage());
            }
        });

    }

    public void fetchGenderMaster(GenderMasterRequest genderMasterRequest, final Context activity) {
        if (genderMasterRequest == null || activity == null) return;

        final byte[] randomByte = new byte[16];
        SecureRandom random = new SecureRandom();
        random.nextBytes(randomByte);
        String finalRequest = null;
        String finalEncryptedRequest = null;

        genderMasterService = (GenderMaster) activity;
        finalRequest = constant.toJson(genderMasterRequest);
        try {
            finalEncryptedRequest = getFinalString(finalRequest, activity, true, randomByte);
            if (TextUtils.isEmpty(finalEncryptedRequest)) {
                genderMasterService.genderMasterFailed(false, "");
                return;
            }
        } catch (Exception e) {
            genderMasterService.genderMasterFailed(false, e.getMessage());
            return;
        }

        if (TextUtils.isEmpty(finalRequest)) {
            genderMasterService.genderMasterFailed(false, "data is empty");
            return;
        }

        AppServices services = ServiceGenerator.createService(AppServices.class, activity, GET_GENDER_MASTER);
        Call<GenderMasterResponse> call = services.getGenderMaster(finalEncryptedRequest,
                Base64.encodeToString(randomByte, Base64.DEFAULT), GET_GENDER_MASTER);
        call.enqueue(new Callback<GenderMasterResponse>() {
            @Override
            public void onResponse(Call<GenderMasterResponse> call, Response<GenderMasterResponse> response) {
                if (response != null && response.body() != null) {
                    String _decryptedResponseString = "";
                    try {
                        _decryptedResponseString = getFinalString(response.body().data, activity, false, randomByte);
                        GenderMasterResponse.GenderMaster _responseFromServer = new Gson().fromJson(_decryptedResponseString, GenderMasterResponse.GenderMaster.class);

                        MyDBHelper myDBHelper = MyDBHelper.getInstance(activity);
                        myDBHelper.deleteGender();
                        if (_responseFromServer != null && _responseFromServer.data() != null) {
                            for (GenderMasterResponse.GenderMaster genderMaster : _responseFromServer.data()) {
                                myDBHelper.insertGender(genderMaster.name, genderMaster.code);
                            }
                            genderMasterService.genderMasterSuccessful(_responseFromServer);
                        } else {
                            genderMasterService.genderMasterFailed(false, response.errorBody().toString());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        genderMasterService.genderMasterFailed(false, e.getMessage());
                    }

                } else {
                    genderMasterService.genderMasterFailed(false, response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<GenderMasterResponse> call, Throwable t) {
                genderMasterService.genderMasterFailed(false, t.getMessage());
            }
        });

    }

    public void fetchResidenceMaster(ResidenceMasterRequest residenceMasterRequest, final Context activity) {
        if (residenceMasterRequest == null || activity == null) return;

        final byte[] randomByte = new byte[16];
        SecureRandom random = new SecureRandom();
        random.nextBytes(randomByte);
        String finalRequest = null;
        String finalEncryptedRequest = null;

        residenceMasterService = (ResidenceMaster) activity;
        finalRequest = constant.toJson(residenceMasterRequest);
        try {
            finalEncryptedRequest = getFinalString(finalRequest, activity, true, randomByte);
            if (TextUtils.isEmpty(finalEncryptedRequest)) {
                residenceMasterService.residenceMasterFailed(false, "");
                return;
            }
        } catch (Exception e) {
            residenceMasterService.residenceMasterFailed(false, e.getMessage());
            return;
        }

        if (TextUtils.isEmpty(finalRequest)) {
            residenceMasterService.residenceMasterFailed(false, "data is empty");
            return;
        }
        AppServices services = ServiceGenerator.createService(AppServices.class, activity, GET_RESIDENT_MASTER);
        Call<ResidenceMasterResponse> call = services.getResidenceMaster(finalEncryptedRequest,
                Base64.encodeToString(randomByte, Base64.DEFAULT), GET_RESIDENT_MASTER);
        call.enqueue(new Callback<ResidenceMasterResponse>() {
            @Override
            public void onResponse(Call<ResidenceMasterResponse> call, Response<ResidenceMasterResponse> response) {
                if (response != null && response.body() != null) {
                    String _decryptedResponseString = "";
                    try {
                        _decryptedResponseString = getFinalString(response.body().data, activity, false, randomByte);
                        ResidenceMasterResponse.ResidenceMaster _responseFromServer = new Gson().fromJson(_decryptedResponseString, ResidenceMasterResponse.ResidenceMaster.class);

                        MyDBHelper myDBHelper = MyDBHelper.getInstance(activity);
                        myDBHelper.deleteResident();
                        if (_responseFromServer != null) {
                            for (ResidenceMasterResponse.ResidenceMaster residenceMaster : _responseFromServer.data()) {
                                myDBHelper.insertResident(residenceMaster.residence, residenceMaster.code);
                            }
                            residenceMasterService.residenceMasterSuccessful(_responseFromServer);
                        } else {
                            residenceMasterService.residenceMasterFailed(false, response.errorBody().toString());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        residenceMasterService.residenceMasterFailed(false, e.getMessage());
                    }

                } else {
                    residenceMasterService.residenceMasterFailed(false, response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<ResidenceMasterResponse> call, Throwable t) {
                genderMasterService.genderMasterFailed(false, t.getMessage());
            }
        });

    }

    public void fetchPinCodeMaster(PincodeMasterRequest pinCodeMasterResponse, final Context activity) {
        if (pinCodeMasterResponse == null || activity == null) return;

        final byte[] randomByte = new byte[16];
        SecureRandom random = new SecureRandom();
        random.nextBytes(randomByte);
        String finalRequest = null;
        String finalEncryptedRequest = null;

        pinCodeMasterService = (PinCodeMaster) activity;
        finalRequest = constant.toJson(pinCodeMasterResponse);
        try {
            finalEncryptedRequest = getFinalString(finalRequest, activity, true, randomByte);
            if (TextUtils.isEmpty(finalEncryptedRequest)) {
                pinCodeMasterService.pinCodeMasterFailed(false, "");
                return;
            }
        } catch (Exception e) {
            pinCodeMasterService.pinCodeMasterFailed(false, e.getMessage());
            return;
        }

        if (TextUtils.isEmpty(finalRequest)) {
            pinCodeMasterService.pinCodeMasterFailed(false, "data is empty");
            return;
        }
        AppServices services = ServiceGenerator.createService(AppServices.class, activity, GET_PIN_CODE_MASTER);
        Call<PincodeMasterResponse> call = services.getPinCodeMaster(finalEncryptedRequest,
                Base64.encodeToString(randomByte, Base64.DEFAULT), GET_PIN_CODE_MASTER);
        call.enqueue(new Callback<PincodeMasterResponse>() {
            @Override
            public void onResponse(Call<PincodeMasterResponse> call, Response<PincodeMasterResponse> response) {
                if (response != null && response.body() != null) {
                    String _decryptedResponseString = "";
                    try {
                        _decryptedResponseString = getFinalString(response.body().data, activity, false, randomByte);
                        PincodeMasterResponse.PincodeMaster _responseFromServer = new Gson().fromJson(_decryptedResponseString, PincodeMasterResponse.PincodeMaster.class);
                        MyDBHelper myDBHelper = MyDBHelper.getInstance(activity);
                        SQLiteDatabase db = myDBHelper.getWritableDatabase();
                        db.beginTransaction();
                        myDBHelper.deletePinCode();
                        if (_responseFromServer != null) {
                            for (PincodeMasterResponse.PincodeMaster pinCodeMaster : _responseFromServer.data()) {
                                myDBHelper.insertPinCode(db, pinCodeMaster.pincode, pinCodeMaster.district,
                                        pinCodeMaster.state, pinCodeMaster.stateCode);
                            }
                            db.setTransactionSuccessful();
                            db.endTransaction();
                            pinCodeMasterService.pinCodeMasterSuccessful(_responseFromServer);

                        } else {
                            pinCodeMasterService.pinCodeMasterFailed(false, response.errorBody().toString());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        pinCodeMasterService.pinCodeMasterFailed(false, e.getMessage());
                    }

                } else {
                    pinCodeMasterService.pinCodeMasterFailed(false, response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<PincodeMasterResponse> call, Throwable t) {
                pinCodeMasterService.pinCodeMasterFailed(false, t.getMessage());
            }
        });

    }



    public void submitFormImage(final FormImageSubmitRequest imageSubmitRequest, final Context activity, String token) {
        if (imageSubmitRequest == null || activity == null) return;

        final byte[] randomByte = new byte[16];
        SecureRandom random = new SecureRandom();
        random.nextBytes(randomByte);
        String finalRequest = null;

        formImageSubmitService = (FormImageSubmit) activity;
        finalRequest = constant.toJson(imageSubmitRequest);
        try {
            if (TextUtils.isEmpty(finalRequest)) {
                formImageSubmitService.onImageUploadFailed(false, "bad request");
                return;
            }
        } catch (Exception e) {
            formImageSubmitService.onImageUploadFailed(false, e.getMessage());
            return;
        }

        if (TextUtils.isEmpty(finalRequest)) {
            formImageSubmitService.onImageUploadFailed(false, "data is empty");
            return;
        }

        AppServices services = ServiceGenerator.createService(AppServices.class, activity,SUBMIT_PDF_IMAGE);
        if(services == null) return;
        Call<ResponseBody> call = services.submitPdfImage(finalRequest,
                Base64.encodeToString(randomByte, Base64.DEFAULT), SUBMIT_PDF_IMAGE, token);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response == null || response.body() == null) {
                    onFailure(null, new Throwable("Empty body"));
                    return;
                }
                try {
                    JSONObject baseResponse = new JSONObject(response.body().string());

                    String  data = baseResponse.getString("da");

                    String decryptedResponseString = getFinalString(data, activity, false, randomByte);

                    FormBaseResponse<ImageDataSubmitResponse> imageSubmitResponse = new Gson().fromJson(decryptedResponseString, new TypeToken<FormBaseResponse<ImageDataSubmitResponse>>(){}.getType());

                    if(imageSubmitResponse.isSuccess()){
                        formImageSubmitService.onImageUploadSuccessful(imageSubmitRequest, imageSubmitResponse.getData());
                    }else {
                        onFailure(call, new Throwable(imageSubmitResponse.getError_msg()));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    onFailure(null, new Throwable(e.getMessage()));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                formImageSubmitService.onImageUploadFailed(false, t.getMessage());
                ((FormFillActivity)activity).dismissProgressDialog();
            }
        });
    }



}
