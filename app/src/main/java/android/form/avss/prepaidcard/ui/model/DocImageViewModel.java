package android.form.avss.prepaidcard.ui.model;

import android.form.avss.prepaidcard.ui.fragments.doc_model.DocsDataList;


import java.io.Serializable;
import java.util.ArrayList;

public class DocImageViewModel implements Serializable {
    private ArrayList<DocsDataList> finalDocList = new ArrayList<>();

    public ArrayList<DocsDataList> getFinalDocList() {
        if (finalDocList == null) {
            finalDocList = new ArrayList<>();
        }
        return finalDocList;
    }

    public void setFinalDocList(DocsDataList docList) {
        if (!finalDocList.contains(docList)) {
            this.finalDocList.add(docList);
        }
    }

    public void setFinalDocList(int position,DocsDataList docList) {
        if (!finalDocList.contains(docList)) {
            this.finalDocList.add(position, docList);
        }
    }

    public boolean isDocListEmpty() {
        if (finalDocList == null) return false;
        return finalDocList.isEmpty();
    }

    public int size() {
        if (isDocListEmpty()) return 0;
        return finalDocList.size();
    }

    public DocsDataList get(int position) {
        if (position == -1) return null;
        return finalDocList.get(position);
    }


    public boolean isAllDocCaptured() {
        if (isDocListEmpty()) return false;

        boolean isTrue = true;
        for (int i = 0; i < getFinalDocList().size(); i++) {
            DocsDataList dataList = getFinalDocList().get(i);
            if (i != (getFinalDocList().size() - 1) ) {
                if (dataList.isDocumentCaptured()) {
                    isTrue = false;
                    break;
                }
            } else {
                return true;
            }
        }
        return isTrue;
    }
}
