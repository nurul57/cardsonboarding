package android.form.avss.prepaidcard.helper_classes;


import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.IOException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

public class Constant {
    private volatile static Constant constant = null;
    private static Gson gson;
    private List<String> serviceList;

    private Constant() {
        gson = new Gson();
        serviceList = new ArrayList<>();
    }

    public static Constant getInstance() {
        if (constant == null) {
            // To make thread safe
            synchronized (Constant.class) {
                // check again as multiple threads
                // can reach above step
                if (constant == null) {
                    constant = new Constant();
                }
            }
        }
        return constant;
    }

    public Gson getGson() {
        return gson;
    }

    public String toJson(Object o) {
        return getGson().toJson(o);
    }

    public <T> T fromJson(String json, Class<T> tClass) throws JsonSyntaxException {
        return getGson().fromJson(json, tClass);
    }

    public List<String> getServiceList() {
        return serviceList;
    }

    public void setServiceList(List<String> serviceList) {
        if (serviceList == null) {
            this.serviceList = new ArrayList<>();
        } else {
            this.serviceList = serviceList;
        }
    }

    public void clearServiceList() {
        if (serviceList == null) return;
        if (!isServiceListNotEmpty())
            getServiceList().clear();
    }

    public boolean isServiceListNotEmpty() {
        if (getServiceList() == null) return false;
        return !serviceList.isEmpty();
    }

    public int serviceListSize() {
        return serviceList == null || serviceList.isEmpty() ? 0 : serviceList.size();
    }

}
