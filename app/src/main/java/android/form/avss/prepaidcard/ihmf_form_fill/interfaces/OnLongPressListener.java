package android.form.avss.prepaidcard.ihmf_form_fill.interfaces;

import android.view.View;

public interface OnLongPressListener {
    void onLongPressed(View view);
}
