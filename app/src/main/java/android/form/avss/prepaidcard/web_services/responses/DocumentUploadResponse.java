package android.form.avss.prepaidcard.web_services.responses;

import com.google.gson.annotations.SerializedName;

public class DocumentUploadResponse {
    @SerializedName("da")
    public String data;

    public static class UploadDocument extends BaseResponse {

    }
}
