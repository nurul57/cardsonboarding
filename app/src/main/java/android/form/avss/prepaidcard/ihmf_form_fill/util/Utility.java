package android.form.avss.prepaidcard.ihmf_form_fill.util;

import android.content.Context;
import android.net.ConnectivityManager;

public class Utility {


    public static  String getFormat(String format) {

        if (format == null || format.isEmpty()) return "";

        StringBuilder builder = new StringBuilder();
        for (char ch : format.toCharArray()) {

            if (!Character.isLetter(ch)) {
                builder.append(ch);
                continue;
            }
            if (ch == 'D' || ch == 'Y')
                ch = Character.toLowerCase(ch);
            if (ch == 'm')
                ch = Character.toUpperCase(ch);

            builder.append(ch);
        }
        return builder.toString();

    }


    private static final String[] units = {"", "One", "Two", "Three", "Four",
            "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve",
            "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen",
            "Eighteen", "Nineteen"};

    private static final String[] tens = {
            "",
            "",        // 1
            "Twenty",    // 2
            "Thirty",    // 3
            "Forty",    // 4
            "Fifty",    // 5
            "Sixty",    // 6
            "Seventy",    // 7
            "Eighty",    // 8
            "Ninety"    // 9
    };

    public static String convert(final int n) {
        if (n < 0) {
            return "Minus " + convert(-n);
        }

        if (n < 20) {
            return units[n];
        }

        if (n < 100) {
            return tens[n / 10] + ((n % 10 != 0) ? " " : "") + units[n % 10];
        }

        if (n < 1000) {
            return units[n / 100] + " Hundred" + ((n % 100 != 0) ? " " : "") + convert(n % 100);
        }

        if (n < 100000) {
            return convert(n / 1000) + " Thousand" + ((n % 10000 != 0) ? " " : "") + convert(n % 1000);
        }

        if (n < 10000000) {
            return convert(n / 100000) + " Lakh" + ((n % 100000 != 0) ? " " : "") + convert(n % 100000);
        }

        return convert(n / 10000000) + " Crore" + ((n % 10000000 != 0) ? " " : "") + convert(n % 10000000);
    }

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(cm == null) return false;

        return cm.getActiveNetworkInfo() != null;
    }

}
