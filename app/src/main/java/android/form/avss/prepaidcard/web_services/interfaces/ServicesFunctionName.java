package android.form.avss.prepaidcard.web_services.interfaces;

public interface ServicesFunctionName {
    String AUTHENTICATION_BD = "authBD";

    String GET_TITLE_MASTER = "GetTitleMaster";
    String GET_OVD_MASTER = "GetOvdMaster";
    String GET_GENDER_MASTER = "GetGenderMaster";
    String GET_RESIDENT_MASTER = "GetResidenceType";
    String GET_PIN_CODE_MASTER = "GetPinMaster";

    String AUTHENTICATE_AADHAAR = "authAadhaar";
    String PAN_VERIFICATION = "panVarification";
    String SAVE_DATA = "saveData";
    String GET_MARKING_DETAILS = "getMarkingDetails";
    String SUBMIT_PDF_IMAGE = "submitPdfImage";
    String GET_DROPDOWN = "getDropdown";
    String FINAL_SAVE_DATA = "finalSaveData";
    String PDF_DATA_SAVE_VIEW = "pdfDataSaveView";
    String GET_IMAGE_DATA = "getImageData";
    String UPLOAD_IMAGE_DATA = "uploadImageData";



    String VALIDATE_CUSTOMER_ID = "validateCustomerId";
    String VALIDATE_MOBILE_NO = "validateMobileNo";
}
