package android.form.avss.prepaidcard.ui.custom_classes;

import android.form.avss.prepaidcard.ui.custom_classes.interfaces.RecyclerViewClickListener;
import android.support.v7.widget.RecyclerView;
import android.view.View;


public class DocViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private RecyclerViewClickListener recyclerViewClickListener;

    public DocViewHolder(View itemView, RecyclerViewClickListener listener) {
        super(itemView);
        recyclerViewClickListener = listener;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
         recyclerViewClickListener.onClick(view, getAdapterPosition());
    }
}
