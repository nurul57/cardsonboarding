package android.form.avss.prepaidcard.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.form.avss.prepaidcard.R;
import android.support.annotation.NonNull;
import android.util.Base64;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.List;

public abstract class MorphoUtil {
    private static final String PID_OPTIONS = "PID_OPTIONS";
    private static final String ACTION_CAPTURE_MORPHO = "in.gov.uidai.rdservice.fp.CAPTURE";

    private static String generateWadh(@NonNull Context ctx) {
        MessageDigest _md = null;
        try {
            _md = MessageDigest.getInstance(ctx.getString(R.string.wadh_algo), ctx.getString(R.string.wadh_prv));
            String _wadh = ctx.getString(R.string.wadh_2_5);

            _md.update(_wadh.getBytes(StandardCharsets.UTF_8));
            _wadh = Base64.encodeToString(_md.digest(), Base64.DEFAULT).trim();
            return _wadh;

        } catch (Exception e) {
            return null;
        }
    }



    private static String pidOption(Context ctx) {
        return ctx.getString(R.string.pid_option, ctx.getString(R.string.pid_env), generateWadh(ctx));
    }

    public static Intent requestMorpho(Context ctx){
        final String pidOptions = pidOption(ctx);
        final Intent intent = new Intent(ACTION_CAPTURE_MORPHO);
        PackageManager manager = ctx.getPackageManager();
        List<ResolveInfo> _infos = manager.queryIntentActivities(intent, 0);
        return _infos.size() == 0 ? null : intent.putExtra(PID_OPTIONS,pidOptions);
    }
}
