package android.form.avss.prepaidcard.ui.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.form.avss.prepaidcard.R;
import android.form.avss.prepaidcard.utils.PermissionUtils;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.Objects;

public class AllModuleDashboardActivity extends BaseActivity implements View.OnClickListener {
    private ImageView iv_newApplication;

    private TextView tv_all_app;
    private TextView tv_Processing;
    private TextView tv_rejected;
    private TextView tv_gbm;
    private TextView tv_closed;
    private ViewGroup rlNavigationBottomBar;
    private ViewGroup parentLayout;
    private RadioGroup rg_ekyc;
    private boolean isAadhaar = false;

    private String bdeno;
    private String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        bdeno = getIntent().getStringExtra(BDE_NO);
        token = getIntent().getStringExtra(TOKEN);

        initView();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_newApplication:
                rlNavigationBottomBar.setVisibility(rlNavigationBottomBar.getVisibility()
                        == View.GONE ? View.VISIBLE : View.GONE);
                break;

            case R.id.btn_prepaid:
//                if (PermissionUtils.isGranted(AllModuleDashboardActivity.this, permissionList)) {
                    isAadhaar = false;
                    Intent intent = new Intent(AllModuleDashboardActivity.this, EtbOrNtbCustomerActivity.class);
                    intent.putExtra(IS_AADHAAR, isAadhaar);
                    intent.putExtra(BDE_NO, bdeno);
                    intent.putExtra(TOKEN, token);
                    startActivity(intent);


                    /*todo: this dialog box is bypass*/

                    View view = getLayoutInflater().inflate(R.layout.ekyc_option_dialog_box, parentLayout, false);
                    view.setVisibility(View.GONE);
                    rg_ekyc = (RadioGroup) view.findViewById(R.id.rg_ekyc_option);
                    rg_ekyc.setOnClickListener(this);

                    final AlertDialog.Builder builder = new AlertDialog.Builder(AllModuleDashboardActivity.this);
                    builder.setTitle(R.string.app_name);
                    builder.setMessage(R.string.e_kyc_option);
                    builder.setView(view);
                    builder.setCancelable(false);

                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (rg_ekyc.getCheckedRadioButtonId() == RadioGroup.NO_ID) {
                                toast("Please select ekyc option, first");
                                dialog.dismiss();
                            } else {
                                dialog.dismiss();
                                isAadhaar = false;
                                Intent intent = new Intent(AllModuleDashboardActivity.this, EtbOrNtbCustomerActivity.class);
                                intent.putExtra(IS_AADHAAR, isAadhaar);
                                intent.putExtra(BDE_NO, bdeno);
                                intent.putExtra(TOKEN, token);
                                startActivity(intent);
                            }
                        }
                    });

                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            rg_ekyc.clearCheck();
                            isAadhaar = false;
                        }
                    });
//                    builder.create().show();

//                } else {
//                    toast(R.string.err_reinstall_app);
//                }
                break;

            case R.id.btn_transit_Card:
                if (PermissionUtils.isGranted(AllModuleDashboardActivity.this, permissionList)) {

                } else {
                    toast(R.string.err_reinstall_app);
                }
                break;

            case R.id.btn_smartCity_card:
                if (PermissionUtils.isGranted(AllModuleDashboardActivity.this, permissionList)) {

                } else {
                    toast(R.string.err_reinstall_app);
                }
                break;

            case R.id.btn_fatTag_card:
                if (PermissionUtils.isGranted(AllModuleDashboardActivity.this, permissionList)) {

                } else {
                    toast(R.string.err_reinstall_app);
                }
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sync:
                break;

            case R.id.refresh:
                break;

            case R.id.logout:
                Intent _intent = new Intent(AllModuleDashboardActivity.this, LoginActivity.class);
                startActivity(_intent);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void initView() {
        // toolbar setup
        setSupportActionBar((Toolbar) findViewById(R.id.my_toolbar));
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        parentLayout = (ViewGroup) findViewById(R.id.cl_module_dashboard);
        // hide the soft keyboard
        hideSoftKeyboard(parentLayout);
        Objects.requireNonNull(findViewById(R.id.rl_project_bar)).setVisibility(View.GONE);

        rlNavigationBottomBar = (ViewGroup) findViewById(R.id.rl_project_bar);

        tv_all_app = (TextView) findViewById(R.id.tv_all_app);
        tv_Processing = (TextView) findViewById(R.id.tv_Processing);
        tv_rejected = (TextView) findViewById(R.id.tv_rejected);
        tv_gbm = (TextView) findViewById(R.id.tv_gbm);
        tv_closed = (TextView) findViewById(R.id.tv_closed);
        parentLayout = (ViewGroup) findViewById(R.id.cl_module_dashboard);

        iv_newApplication = (ImageView) findViewById(R.id.iv_newApplication);

        setClickListener();
    }

    private void setClickListener() {
        iv_newApplication.setOnClickListener(this);
        (Objects.requireNonNull(findViewById(R.id.fl_all_app))).setOnClickListener(this);
        (Objects.requireNonNull(findViewById(R.id.fl_Processing))).setOnClickListener(this);
        (Objects.requireNonNull(findViewById(R.id.fl_rejected))).setOnClickListener(this);
        (Objects.requireNonNull(findViewById(R.id.fl_gbm))).setOnClickListener(this);
        (Objects.requireNonNull(findViewById(R.id.fl_closed))).setOnClickListener(this);

        Objects.requireNonNull(findViewById(R.id.btn_prepaid)).setOnClickListener(this);
        Objects.requireNonNull(findViewById(R.id.btn_transit_Card)).setOnClickListener(this);
        Objects.requireNonNull(findViewById(R.id.btn_smartCity_card)).setOnClickListener(this);
        Objects.requireNonNull(findViewById(R.id.btn_fatTag_card)).setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu) {
        getMenuInflater().inflate(R.menu.all_module_dashboard_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /*@Override
    public void onBackPressed() {}*/

}
