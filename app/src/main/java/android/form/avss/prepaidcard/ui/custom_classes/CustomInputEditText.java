package android.form.avss.prepaidcard.ui.custom_classes;

import android.content.Context;
import android.graphics.Color;
import android.support.design.widget.TextInputEditText;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;

public class CustomInputEditText extends TextInputEditText {
    public CustomInputEditText (Context context) {
        super(context);
        this.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                return event.getKeyCode() == KeyEvent.KEYCODE_SOFT_RIGHT || event.getKeyCode() == KeyEvent.KEYCODE_DPAD_CENTER ||
                        event.getKeyCode() == KeyEvent.KEYCODE_DPAD_UP || event.getKeyCode() == KeyEvent.KEYCODE_DPAD_RIGHT ||
                        event.getKeyCode() == KeyEvent.KEYCODE_DPAD_DOWN || event.getKeyCode() == KeyEvent.KEYCODE_DPAD_LEFT;
            }
        });
        this.setTextSize(14.0f);
        this.setTextColor(Color.BLACK);
    }

    public CustomInputEditText (Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                return event.getKeyCode() == KeyEvent.KEYCODE_SOFT_RIGHT || event.getKeyCode() == KeyEvent.KEYCODE_DPAD_CENTER ||
                        event.getKeyCode() == KeyEvent.KEYCODE_DPAD_UP || event.getKeyCode() == KeyEvent.KEYCODE_DPAD_RIGHT ||
                        event.getKeyCode() == KeyEvent.KEYCODE_DPAD_DOWN || event.getKeyCode() == KeyEvent.KEYCODE_DPAD_LEFT;
            }
        });
        this.setTextSize(14.0f);
        this.setTextColor(Color.BLACK);
    }

    public CustomInputEditText (Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                return event.getKeyCode() == KeyEvent.KEYCODE_SOFT_RIGHT || event.getKeyCode() == KeyEvent.KEYCODE_DPAD_CENTER ||
                        event.getKeyCode() == KeyEvent.KEYCODE_DPAD_UP || event.getKeyCode() == KeyEvent.KEYCODE_DPAD_RIGHT ||
                        event.getKeyCode() == KeyEvent.KEYCODE_DPAD_DOWN || event.getKeyCode() == KeyEvent.KEYCODE_DPAD_LEFT;
            }
        });
        this.setTextSize(14.0f);
        this.setTextColor(Color.BLACK);
    }

	public String panExpression = "^[A-Z]{5}[0-9]{4}[A-Z]$";
	public String phoneExpression = "^[6-9][0-9]{9}$";
	public String ifscPattern ="^[A-Z]{4}0[0-9]{6}$";
    //[a-zA-Z0-9\-]
    public String emailPattern = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
            "\\@" +
            "[a-zA-Z0-9]{0,64}" +
            "(" +
            "\\." +
            "[a-zA-Z0-9]{0,25}" +
            ")+";

	public void removeFocusChangeListener(){
	    this.setOnFocusChangeListener(null);
    }

    public void setFocusChangeListener(final CustomTextInputLayout textInputLayout) {
        this.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (getString().equals("")) {
                        textInputLayout.setErrorMessage();
                    } else {
                        textInputLayout.setErrorDisabled();
                    }
                }
            }
        });
    }

	public void setFocusChangeListener(final CustomTextInputLayout textInputLayout, final String msg, final int length)  {
        this.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (getString().length() != length) {
                        textInputLayout.setErrorMessage(msg);
                    } else {
                        textInputLayout.setErrorDisabled();
                    }
                }
            }
        });
    }

    public void setFocusChangeListenerDepositAmount(final CustomTextInputLayout textInputLayout, final String msg, final int depositAmount)  {
        this.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (getString().equals("")) {
                        textInputLayout.setErrorMessage();
                    } else if (Long.parseLong(getString()) < depositAmount) {
                        textInputLayout.setErrorMessage(msg);
                    } else {
                        textInputLayout.setErrorDisabled();
                    }
                }
            }
        });
    }

	public void setFocusChangeListenerAadhaar(final CustomTextInputLayout textInputLayout) {
        this.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (getString().length() != 12) {
                        textInputLayout.setErrorMessage("Please enter 12 digit aadhar no");
                    } else {
                        textInputLayout.setErrorDisabled();
                    }
                }
            }
        });
    }

    public void setFocusChangeListenerPan(final CustomTextInputLayout textInputLayout) {
        this.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (!getString().matches(panExpression)) {
                        textInputLayout.setErrorMessage("Please enter valid Pan Card Number like : ' AERQK1456J ' ");
                    } else {
                        textInputLayout.setErrorDisabled();
                    }
                }
            }
        });
    }

    public void setFocusChangeListenerPinCode(final CustomTextInputLayout textInputLayout) {
        this.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (getString().length() != 6) {
                        textInputLayout.setErrorMessage("Length should be 6");
                    } else {
                        textInputLayout.setErrorDisabled();
                    }
                }
            }
        });
    }

    public void setFocusChangeListenerMobileNo(final CustomTextInputLayout textInputLayout) {
        this.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (!getString().matches(phoneExpression)) {
                        textInputLayout.setErrorMessage("Please Enter Valid Phone No.");
                    } else {
                        textInputLayout.setErrorDisabled();
                    }
                }
            }
        });
    }

    public void setFocusChangeListenerIFSC(final CustomTextInputLayout textInputLayout) {
        this.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (!getString().matches(ifscPattern)) {
                        textInputLayout.setErrorMessage("Please enter valid IFS Code");
                    } else {
                        textInputLayout.setErrorDisabled();
                    }
                }
            }
        });
    }

    public void setFocusChangeListenerEmailId(final CustomTextInputLayout textInputLayout) {
        this.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (!getString().matches(emailPattern)) {
                        textInputLayout.setErrorMessage("Please enter valid email id");
                    } else {
                        textInputLayout.setErrorEnabled(false);
                    }
                }
            }
        });
    }

    public String getString() {
        return this.getText().toString().trim();
    }

    public boolean matches(String value){
	    return this.getString().matches(value);
    }

    public boolean toLength(int length) {
        return  this.getString().length() == length;
    }

    public void setFieldToEmpty() {
        this.setText("");
    }

    public boolean isEmpty() {
        return !TextUtils.isEmpty(this.getString());
    }

    public void setDisabled() {
	    this.setEnabled(false);
    }

    public void setEnabled() {
	    this.setEnabled(true);
    }

    public boolean isFieldEmpty( CustomTextInputLayout inputLayout ) {
        if (!TextUtils.isEmpty(this.getString())) {
            inputLayout.setErrorDisabled();
            return true;
        } else {
            inputLayout.setErrorMessage();
            return false;
        }
    }

    public boolean isFieldEmpty( CustomTextInputLayout inputLayout, String msg ) {
        if (!TextUtils.isEmpty(this.getString())) {
            inputLayout.setErrorDisabled();
            return true;
        } else {
            inputLayout.setErrorMessage(msg);
            return false;
        }
    }

    public void setInputFilterLength(int length) {
        this.setFilters(new InputFilter[] {new InputFilter.LengthFilter(length)});
    }

    public void setCharacterInputFilter() {
        this.setFilters(new InputFilter[] {InputFilters.if_Character});
    }

    public void setCharacterInputFilter(int length) {
        this.setFilters(new InputFilter[] {InputFilters.if_Character, new InputFilter.LengthFilter(length)});
    }

    public void setAlphaNumericInputFilter() {
        this.setFilters(new InputFilter[] {InputFilters.if_AlphaNumeric});
    }

    public void setAlphaNumericInputFilter(int length) {
        this.setFilters(new InputFilter[] {InputFilters.if_AlphaNumeric, new InputFilter.LengthFilter(length)});
    }

    public void setAlphaNumericSpecialInputFilter() {
        this.setFilters(new InputFilter[] {InputFilters.if_AlphaNumericSpecial});
    }

    public void setAlphaNumericSpecialInputFilter(int length) {
        this.setFilters(new InputFilter[] {InputFilters.if_AlphaNumericSpecial, new InputFilter.LengthFilter(length)});
    }
}


