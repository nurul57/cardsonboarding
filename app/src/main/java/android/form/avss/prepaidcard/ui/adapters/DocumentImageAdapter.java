package android.form.avss.prepaidcard.ui.adapters;

import android.content.Context;
import android.form.avss.prepaidcard.R;
import android.form.avss.prepaidcard.ui.fragments.DocumentFragment;
import android.form.avss.prepaidcard.ui.fragments.doc_model.SelectedImage;
import android.form.avss.prepaidcard.utils.UtilityClass;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class DocumentImageAdapter extends RecyclerView.Adapter<DocumentImageAdapter.MyViewHolder> {
    private ArrayList<SelectedImage> imageList;
    private Context context;
    private Handler mHandler;
    private Handler mBackHandler;
    private boolean isAadhaarDoc;
    private DocumentFragment.ItemDeleteListener listener;

    public DocumentImageAdapter(ArrayList<SelectedImage> imageList, boolean isAadhaarDoc,
                                DocumentFragment.ItemDeleteListener itemDeleteListener) {
        this.imageList = imageList;
        this.isAadhaarDoc = isAadhaarDoc;
        listener = itemDeleteListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.doc_image_section, parent, false);
        mHandler = new Handler();
        HandlerThread mBackLooper = new HandlerThread("back_img_list");
        mBackLooper.start();
        mBackHandler = new Handler(mBackLooper.getLooper());
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        if (context == null) return;

        loadImage(holder.ivImageViewer, imageList.get(position));

        holder.ivDelete.setVisibility(isAadhaarDoc ? View.GONE : View.VISIBLE);
        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imageList.isEmpty()) {
                    holder.ivImageViewer.setImageDrawable(context.getDrawable(R.drawable.alert));
                    return;
                }
                String _imgPath = imageList.get(holder.getAdapterPosition()).getImgPath();
                File file = new File(_imgPath);
                if (file.exists()) {
                    try {
                        file.getCanonicalFile().delete();
                        if (file.exists()) {
                            context.getApplicationContext().deleteFile(file.getName());
                        }
                    } catch (IOException e) {
                        e.getMessage();
                        return;
                    } catch (Exception ignore){
                        ignore.getMessage();
                        return;
                    }
                }

                try {
                    imageList.remove(holder.getAdapterPosition());
                    notifyItemRemoved(holder.getAdapterPosition());
                    notifyDataSetChanged();
                    listener.setFloatingActionButtonVisibility(imageList);
                } catch (IndexOutOfBoundsException e) {
                    e.getMessage();
                } catch (Exception e) {
                    e.getMessage();
                }
            }
        });
    }

    private void loadImage(final ImageView _ivImage, final SelectedImage img) {
        mBackHandler.post(new Runnable() {
            @Override
            public void run() {
                if (context != null) {
                    final Bitmap bmp = UtilityClass.decodeUri(context.getApplicationContext(),
                            Uri.fromFile(new File(img.getImgPath())), 200, 200);
                    if (bmp == null) {
                        return;
                    }

                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            _ivImage.setImageBitmap(bmp);
                        }
                    });
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return imageList == null ? 0 : imageList.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView ivImageViewer;
        ImageView ivDelete;

        MyViewHolder(View itemView) {
            super(itemView);
            ivImageViewer = (ImageView) itemView.findViewById(R.id.ivImage);
            ivDelete = (ImageView) itemView.findViewById(R.id.ivDelete);
        }
    }
}
