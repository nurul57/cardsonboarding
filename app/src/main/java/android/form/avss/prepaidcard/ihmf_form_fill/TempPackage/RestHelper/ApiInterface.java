package android.form.avss.prepaidcard.ihmf_form_fill.TempPackage.RestHelper;


import android.form.avss.prepaidcard.ihmf_form_fill.pojo.Requests.MarkedDataRequest;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.Requests.FormImageSubmitRequest;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.Requests.DataSubmit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ApiInterface {


    @POST("/filler/getAllMarkingDetails")
    Call<ResponseBody> requestMarkedData(@Body MarkedDataRequest param1);

    @POST("/save/singleFillingImage")
    Call<ResponseBody> sendImageData(@Body FormImageSubmitRequest param1);


    @POST("/allinone/axis_api/public/dropdown/allDropdownList")
    Call<ResponseBody> getDropDownData();


    @POST("/save/singleFillingData")
    Call<ResponseBody> sendData(@Body DataSubmit data);




}
