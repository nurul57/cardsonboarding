package android.form.avss.prepaidcard.interfaces;

public interface Keys {
    int AADHAAR_CONSTANT = 101;
    int DOCUMENT_CONSTANT = 200;
    int CAMERA_PERMISSION = 300;
    double income5Lakh = 500000.00D;


    String BDE_NO = "BDE_ID";
    String TOKEN = "TOKEN";
    String FORM_ID = "FORM_ID";

    String IS_AADHAAR = "IS_AADHAAR";
    String IS_NEW_CUSTOMER = "isNewCustomer";
    String IS_PAN_AVAILABLE = "IS_PAN_AVAILABLE";
	String EKYC_OPTION_DATA = "EKYC_OPTION_DATA";
	String PAN_DATA = "PAN_DATA";
	String CARD_DATA = "CARD_DATA";
	String REFERENCE_ID = "REFERENCE_ID";


    /* Document Section Details*/
    String FRAGMENT_POSITION = "FRAGMENT_POSITION";
    String DOC_SECTION_DATA = "DOC_SECTION_DATA";
    String IMAGE_OPTION = "IMAGE_OPTION";
    String IMAGE_NAME = "IMAGE_NAME";
    String IMAGE_LIST = "IMAGE_LIST";
    String IMAGE_COUNTER = "IMAGE_COUNTER";
    String MIN_LIMIT = "MIN_LIMIT";
    String MAX_LIMIT = "MAX_LIMIT";
    String IS_PREVIEW_MODE = "IS_PREVIEW_MODE";


    String CONTROL_ID = "CONTROLD_ID";
    String CONTROL_TYPE = "CONTROL_TYPE";
    String RATIO = "RATIO";

}
