package android.form.avss.prepaidcard.web_services.responses;

import com.google.gson.annotations.SerializedName;

public class ResidenceMasterResponse {
    @SerializedName("da")
    public String data;

    public static class ResidenceMaster  extends BaseResponseList<ResidenceMaster> {
        @SerializedName("id")
        public String id;
        @SerializedName("residence")
        public String residence;
        @SerializedName("code")
        public String code;
        @SerializedName("status")
        public String status;
    }
}
