package android.form.avss.prepaidcard.web_services.requests;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class DocsUploadRequest extends BaseRequest implements Serializable {

    @SerializedName("doc_nm")
    private String imageName;
    @SerializedName("doc_list")
    private ArrayList<String> imagesList = new ArrayList<>();

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public ArrayList<String> getImagesList() {
        return imagesList;
    }

    public void setImagesList(ArrayList<String> imagesList) {
        this.imagesList = imagesList;
    }
}
