package android.form.avss.prepaidcard.ui.custom_classes;

import android.content.Context;
import android.form.avss.prepaidcard.R;
import android.form.avss.prepaidcard.database.MyDBHelper;
import android.form.avss.prepaidcard.ui.adapters.CustomDropDownAdapter;
import android.form.avss.prepaidcard.ui.custom_classes.interfaces.MyOnFocusChangeListener;
import android.form.avss.prepaidcard.utils.UtilityClass;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class CustomTextInputLayout extends TextInputLayout {

    private TextWatcher textWatcher;
    private TextWatcher pinCodeTextWatcher;

    public CustomTextInputLayout(Context context) {
        super(context);
    }

    public CustomTextInputLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomTextInputLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void drawableStateChanged() {
        super.drawableStateChanged();
        EditText editText = getEditText();
        if (editText != null && getContext() != null) {
            if (TextUtils.isEmpty(editText.getText().toString().trim()) && isErrorEnabled()) {
                editText.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.bg_error_edit_text));
            } else {
                editText.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.bg_correct_edit_text));
            }
        }
    }

    @Override
    public void setError(@Nullable CharSequence errorText) {
        super.setError(errorText);
        EditText editText = getEditText();
        if (editText != null && getContext() != null) {
            if (TextUtils.isEmpty(editText.getText().toString().trim()) && isErrorEnabled()) {
                editText.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.bg_error_edit_text));
            } else {
                editText.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.bg_correct_edit_text));
            }
        }
    }


    /*todo: Focus Change Function*/
    public void removeFocusChangeListener() {
        if (this.getEditText() == null) return;
        this.setOnFocusChangeListener(null);
    }

    public void setFocusChangeListener() {
        if (this.getEditText() == null) return;

        this.getEditText().setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (isErrorEnabled()) {
                        addTextWatcher();
                    } else {
                        Objects.requireNonNull(getEditText()).removeTextChangedListener(textWatcher);
                    }
                } else {
                    if (TextUtils.isEmpty(getString())) {
                        setErrorMessage();
                    } else {
                        setErrorDisabled();
                    }
                    Objects.requireNonNull(getEditText()).removeTextChangedListener(textWatcher);
                }
            }
        });
    }

    public void setFocusChangeListenerAddress() {
        if (this.getEditText() == null) return;

        this.getEditText().setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (isErrorEnabled()) {
                        addTextWatcher();
                    } else {
                        Objects.requireNonNull(getEditText()).removeTextChangedListener(textWatcher);
                    }
                } else {
                    if (TextUtils.isEmpty(getString()) && isCharAvailable()) {
                        setErrorMessage();
                    } else {
                        setErrorDisabled();
                    }
                    Objects.requireNonNull(getEditText()).removeTextChangedListener(textWatcher);
                }
            }
        });
    }

    public void setFocusChangeListener(final MyOnFocusChangeListener listener) {
        if (this.getEditText() == null) return;

        this.getEditText().setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (getEditText() instanceof CustomSpinner) {
                        if (getCustomSpinner().getAdapter().isEmpty()) {
                            getCustomSpinner().setDropDownHeight(0);
                        } else {
                            getCustomSpinner().showDropDown();
                        }
                    }

                    if (getEditText() instanceof CustomAutoComplete) {
                        if (isNotEmpty()) {
                            setFieldToEmpty();
                        }
                        if (getCustomAutoComplete().getAdapter().isEmpty()) {
                            getCustomAutoComplete().setDropDownHeight(0);
                        } else {
                            getCustomAutoComplete().showDropDown();
                        }
                    }

                    if (isErrorEnabled()) {
                        addTextWatcher();
                    } else {
                        getEditText().removeTextChangedListener(textWatcher);
                    }

                } else {

                    if (TextUtils.isEmpty(getString())) {
                        setErrorMessage();
                    } else {
                        setErrorDisabled();
                    }

                    Objects.requireNonNull(getEditText()).removeTextChangedListener(textWatcher);

                    if (listener != null) listener.focusChanged();
                }
            }
        });
    }

    public void setFocusChangeListener(final String msg, final int length) {
        if (this.getEditText() == null) return;
        this.getEditText().setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (isErrorEnabled()) {
                        addTextWatcher();
                    } else {
                        Objects.requireNonNull(getEditText()).removeTextChangedListener(textWatcher);
                    }
                } else {
                    if (stringLengthEquals(length)) {
                        setErrorDisabled();
                    } else {
                        setErrorMessage(msg);
                    }
                    Objects.requireNonNull(getEditText()).removeTextChangedListener(textWatcher);
                }
            }
        });
    }

    public void setFocusChangeListenerAadhaar() {
        if (this.getEditText() == null) return;
        this.getEditText().setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (isErrorEnabled()) {
                        addTextWatcher();
                    } else {
                        Objects.requireNonNull(getEditText()).removeTextChangedListener(textWatcher);
                    }
                } else {
                    if (stringLengthEquals(12) || stringLengthEquals(16)) {
                        setErrorDisabled();
                    } else {
                        setErrorMessage(getContext().getString(R.string.msg_aadhaar));
                    }
                    Objects.requireNonNull(getEditText()).removeTextChangedListener(textWatcher);
                }
            }
        });
    }

    public void setFocusChangeListenerPan() {
        if (this.getEditText() == null) return;
        this.getEditText().setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (isErrorEnabled()) {
                        addTextWatcher();
                    } else {
                        Objects.requireNonNull(getEditText()).removeTextChangedListener(textWatcher);
                    }
                } else {
                    if (matchPanPattern()) {
                        setErrorDisabled();
                    } else {
                        setErrorMessage(getContext().getString(R.string.msg_pan));
                    }
                    Objects.requireNonNull(getEditText()).removeTextChangedListener(textWatcher);
                }
            }
        });
    }

    public void setFocusChangeListenerPinCode() {
        if (this.getEditText() == null) return;
        this.getEditText().setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (isErrorEnabled()) {
                        addTextWatcher();
                    } else {
                        Objects.requireNonNull(getEditText()).removeTextChangedListener(textWatcher);
                    }
                } else {
                    if (stringLengthEquals(6)) {
                        setErrorDisabled();
                    } else {
                        setErrorMessage(getContext().getString(R.string.msg_pin));
                    }
                    Objects.requireNonNull(getEditText()).removeTextChangedListener(textWatcher);
                }
            }
        });
    }

    public void setFocusChangeListenerMobileNo() {
        if (this.getEditText() == null) return;
        this.getEditText().setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (isErrorEnabled()) {
                        addTextWatcher();
                    } else {
                        Objects.requireNonNull(getEditText()).removeTextChangedListener(textWatcher);
                    }
                } else {
                    if (matchMobilePattern()) {
                        setErrorDisabled();
                    } else {
                        setFieldToEmpty();
                        setErrorMessage(getContext().getString(R.string.msg_phone));
                    }
                    Objects.requireNonNull(getEditText()).removeTextChangedListener(textWatcher);
                }
            }
        });
    }

    public void setFocusChangeListenerEmailId() {
        if (this.getEditText() == null) return;
        this.getEditText().setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (isErrorEnabled()) {
                        addTextWatcher();
                    } else {
                        Objects.requireNonNull(getEditText()).removeTextChangedListener(textWatcher);
                    }
                } else {
                    if (matchEmailIdPattern()) {
                        setErrorDisabled();
                    } else {
                        setErrorMessage(getContext().getString(R.string.msg_email));
                    }
                    Objects.requireNonNull(getEditText()).removeTextChangedListener(textWatcher);
                }
            }
        });
    }

    public void setFocusChangeListenerDepositAmount(final String msg, final int depositAmount)  {
        if (this.getEditText() == null) return;
        this.getEditText().setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (isErrorEnabled()) {
                        addTextWatcher();
                    } else {
                        Objects.requireNonNull(getEditText()).removeTextChangedListener(textWatcher);
                    }
                } else {
                    if (isNotEmpty()) {
                        setErrorDisabled();
                    } else if (Double.parseDouble(getString()) < depositAmount) {
                        setErrorMessage(msg);
                    } else {
                        setErrorDisabled();
                    }
                    Objects.requireNonNull(getEditText()).removeTextChangedListener(textWatcher);
                }
            }
        });
    }

    public void setFocusChangeListenerIFSC () {
        if (this.getEditText() == null) return;

        this.getEditText().setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (isErrorEnabled()) {
                        addTextWatcher();
                    } else {
                        Objects.requireNonNull(getEditText()).removeTextChangedListener(textWatcher);
                    }

                } else {
                    if (matchIfscPattern()) {
                        setErrorDisabled();
                    } else {
                        setErrorMessage(getContext().getString(R.string.msg_ifsc));
                    }
                    Objects.requireNonNull(getEditText()).removeTextChangedListener(textWatcher);
                }
            }
        });
    }

    public void addTextWatcher() {
        textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (isErrorEnabled()) {
                    setErrorDisabled();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        if (this.getEditText() == null) return;

        this.getEditText().addTextChangedListener(textWatcher);
    }


    //todo: error message function
    public void setErrorMessage(){
        this.setErrorEnabled(true);
        this.setError(null);
        this.setError(getContext().getString(R.string.err_msg_field_required));
    }

    public void setErrorMessage(@StringRes int errorMessage){
        setErrorMessage(getResources().getString(errorMessage));
    }

    public void setErrorMessage(@Nullable CharSequence errorMessage){
        this.setErrorEnabled(true);
        this.setError(null);
        this.setError(errorMessage);
    }

    public void setErrorEnabled() {
        this.setErrorEnabled(true);
        this.setError(null);
    }

    public void setErrorDisabled() {
        this.setErrorEnabled(false);
        this.setError(null);
    }

    public boolean isFieldEmpty() {
        if (TextUtils.isEmpty(getString())) {
            this.setErrorMessage();
            return true;
        }  else if (isMultipleSpaceAvailable()) {
            this.setErrorMessage(R.string.err_multiple_space);
            return true;
        } else {
            this.setErrorDisabled();
            return false;
        }
    }

    public boolean isFieldEmpty(@StringRes int stringId) {
        if (TextUtils.isEmpty(getString())) {
            this.setErrorMessage(getResources().getString(stringId));
            return true;
        } else if (isMultipleSpaceAvailable()) {
            this.setErrorMessage(R.string.err_multiple_space);
            return true;
        } else {
            this.setErrorDisabled();
            return false;
        }
    }

    public boolean isFieldNotEmpty() {
        if (TextUtils.isEmpty(this.getString())) {
            this.setErrorMessage();
            return false;
        } else if (isMultipleSpaceAvailable()) {
            this.setErrorMessage(R.string.err_multiple_space);
            return true;
        } else {
            this.setErrorDisabled();
            return true;
        }
    }


    //todo basic functions
    public void setEnabled() {
        if (this.getEditText() == null) return;
        this.getEditText().setEnabled(true);
    }

    public void setDisabled() {
        if (this.getEditText() == null) return;
        this.getEditText().setEnabled(false);
        if (isErrorEnabled()) this.setErrorDisabled();
    }

    public boolean matchPattern(String value){
        return this.getString().matches(value);
    }

    public boolean isCharAvailable() {
        if (isEmpty()) return true;
        return !getString().matches(RegularExpression.ADDRESS);
    }

    public boolean matchPanPattern() { return matchPattern(RegularExpression.PAN_EXPRESSION); }

    public boolean matchMobilePattern() { return matchPattern(RegularExpression.MOBILE_NO_EXPRESSION); }

    public boolean matchEmailIdPattern() { return matchPattern(RegularExpression.EMAIL_EXPRESSION); }

    public boolean matchIfscPattern() { return matchPattern(RegularExpression.IFSC_EXPRESSION); }

    public boolean isMultipleSpaceAvailable() {
        if (this.getEditText() == null) return true;
        return !this.getEditText().getText().toString().matches(RegularExpression.MULTIPLE_SPACE);
    }

    public void setText(String value) {
        if (this.getEditText() == null) return;
        this.getEditText().setText(value);
        if (isErrorEnabled()) setErrorDisabled();
    }

    public boolean stringEqualsIgnoreCase(String matchString) { return this.getString().equalsIgnoreCase(matchString); }

    public boolean stringEquals(String matchString) {
        return this.getString().equals(matchString);
    }

    public boolean stringLengthEquals(int length) {
        if (length < 0) return false;
        return this.getString().length() == length;
    }

    public int getLength() {
        if (TextUtils.isEmpty(this.getString())) return 0;
        return this.getString().length();
    }

    public void setFieldToEmpty() {
        this.setText("");
        if (isErrorEnabled()) this.setErrorDisabled();
    }

    public void setFieldToNull() {
        this.setText(null);
        if (isErrorEnabled()) setErrorDisabled();
    }

    public boolean isNotEmpty() {
        return !TextUtils.isEmpty(this.getString());
    }

    public boolean isEmpty() {
        return TextUtils.isEmpty(this.getString());
    }

    public String getString() {
        if (this.getEditText() == null) return "";
        return this.getEditText().getText().toString().trim();
    }


    //todo: Adapter functions
    public CustomInputEditText getCustomEditText(){
        return (CustomInputEditText)getEditText();
    }

    public CustomAutoComplete getCustomAutoComplete(){
        return (CustomAutoComplete) getEditText();
    }

    public CustomSpinner getCustomSpinner(){
        return (CustomSpinner) getEditText();
    }

    public void setAdapter(List<String> dataList) {
        if (this.getEditText() == null || dataList == null || dataList.isEmpty()) return;

        Collections.sort(dataList);
        if (this.getEditText() instanceof CustomSpinner) {
            this.getCustomSpinner().setAdapter(new CustomDropDownAdapter(getContext(), dataList));
        } else if (this.getEditText() instanceof CustomAutoComplete) {
            this.getCustomAutoComplete().setAdapter(new CustomDropDownAdapter(getContext(), dataList));
        }
    }


    //todo: basic input filter on fields
    public void setLengthInputFilter(int maxLength) {
        if (this.getEditText() == null) return;
        this.getEditText().setFilters(new InputFilter[] {new InputFilter.LengthFilter(maxLength)});
    }

    public void setCharacterInputFilter() {
        if (this.getEditText() == null) return;
        this.getEditText().setFilters(new InputFilter[] {InputFilters.if_Character});
    }

    public void setCharacterInputFilter(int maxLength) {
        if (this.getEditText() == null) return;
        this.getEditText().setFilters(new InputFilter[] {InputFilters.if_Character, new InputFilter.LengthFilter(maxLength)});
    }

    public void setAlphaNumericInputFilter() {
        if (this.getEditText() == null) return;
        this.getEditText().setFilters(new InputFilter[] {InputFilters.if_AlphaNumeric});
    }

    public void setAlphaNumericInputFilter(int maxLength) {
        if (this.getEditText() == null) return;
        this.getEditText().setFilters(new InputFilter[] {InputFilters.if_AlphaNumeric, new InputFilter.LengthFilter(maxLength)});
    }

    public void setAlphaNumericSpecialInputFilter() {
        if (this.getEditText() == null) return;
        this.getEditText().setFilters(new InputFilter[] {InputFilters.if_AlphaNumericSpecial});
    }

    public void setAlphaNumericSpecialInputFilter(int maxLength) {
        if (this.getEditText() == null) return;
        this.getEditText().setFilters(new InputFilter[] {InputFilters.if_AlphaNumericSpecial, new InputFilter.LengthFilter(maxLength)});
    }

    public void setPinCodeData(final CustomTextInputLayout tvPinCode, final CustomTextInputLayout tvCity, final CustomTextInputLayout tvState) {
        if (tvPinCode == null || tvCity == null || tvState == null) return;

        pinCodeTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }

            @Override
            public void afterTextChanged(Editable s) {
                if (getContext() == null) return;

                tvCity.setDisabled();
                tvState.setDisabled();

                if (s.length() == 0){
                    tvCity.setFieldToEmpty();
                    tvState.setFieldToEmpty();

                    tvPinCode.setErrorDisabled();
                }

                if (s.length() != 6) {
                    tvPinCode.setErrorDisabled();
                }

                if (s.length() == 6) {
                    if (tvPinCode.stringLengthEquals(6)) {

                        ArrayList<String> pinList = MyDBHelper.getInstance(getContext()).getPinCodeData(tvPinCode.getString());

                        if (pinList.isEmpty()) {
                            tvPinCode.setFieldToEmpty();
                            tvCity.setFieldToEmpty();
                            tvState.setFieldToEmpty();

                            tvPinCode.setErrorMessage("Invalid pin code");
                            UtilityClass.shortToast(getContext(), "Enter pin code is invalid");

                        } else {
                            tvCity.setText(pinList.get(0));
                            tvState.setText(pinList.get(1));

                            tvPinCode.setEnabled();
                            tvCity.setErrorDisabled();
                            tvState.setErrorDisabled();
                            tvPinCode.setErrorDisabled();
                        }
                    }
                }
            }
        };

        tvPinCode.getCustomEditText().addTextChangedListener(pinCodeTextWatcher);
    }

    public TextWatcher getPinCodeTextWatcher() {
        return pinCodeTextWatcher;
    }

    public void removeTextWatcher(TextWatcher textWatcher) {
        if (textWatcher == null || this.getCustomEditText() == null) return;
        this.getCustomEditText().removeTextChangedListener(textWatcher);
    }



    public void addTextWatcherForPattern(final String matchPattern, final int length,final @StringRes int errorMessage) {
        if (this.getEditText() == null) return;
        this.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (isEmpty()) return;
                if (!getString().matches(matchPattern) && !isErrorEnabled() && getLength() == length) {
                    setErrorMessage(errorMessage);
                } else {
                    setErrorDisabled();
                }
            }
        });
    }

    public void addTextWatcherForPattern(final String matchPattern, final @StringRes int errorMessage) {
        if (this.getEditText() == null) return;
        this.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (isEmpty()) return;
                if (!getString().matches(matchPattern) && !isErrorEnabled()) {
                    setErrorMessage(errorMessage);
                }
            }
        });
    }

    public void addTextWatcherForPattern(final String matchPattern, final String errorMessage) {
        if (this.getEditText() == null) return;

        this.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (isEmpty()) return;
                if (!getString().matches(matchPattern) && !isErrorEnabled()) {
                    setErrorMessage(errorMessage);
                }
            }
        });
    }

	public void setButtonVisibility(final Button mButton, final int length) {
		if (this.getEditText() == null || mButton == null || length <= 0) return;

		this.getEditText().addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				mButton.setEnabled(s.length() == length);
			}
		});
	}

	public void setAadhaarButtonVisibility(final Button mButton) {
		if (this.getEditText() == null || mButton == null ) return;

		this.getEditText().addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				mButton.setEnabled(s.length() == 12 || s.length() == 16);
			}
		});
	}


    public void addTextWatcher(final ViewGroup layoutToVisible,
                               final CustomTextInputLayout firstName, final CustomTextInputLayout lastName) {
        if (layoutToVisible == null || firstName == null || lastName == null) return;


        TextWatcher tw = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                layoutToVisible.setVisibility(firstName.isEmpty() || lastName.isEmpty() ? GONE : VISIBLE);

            }
        };

        firstName.getCustomEditText().addTextChangedListener(tw);
        lastName.getCustomEditText().addTextChangedListener(tw);
    }



    public void addTextWatcher(CustomTextWatcher textWatcher) {
        if (this.getEditText() == null || textWatcher == null) return;
        this.getCustomEditText().addTextChangedListener(textWatcher);
    }

}
