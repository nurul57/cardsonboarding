package android.form.avss.prepaidcard.ihmf_form_fill.pojo.Requests;

import android.form.avss.prepaidcard.ihmf_form_fill.pojo.Responses.Form;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataSubmit {

    @Expose
    @SerializedName("formGroupId")
    private String formGroupId;

    @Expose
    @SerializedName("refId")
    private String refId;


    @Expose
    @SerializedName("type")
    private String type;


    @Expose
    @SerializedName("pageDataArray")
    private Form pageDataArray;


    public void setFormGroupId(String formGroupId) {
        this.formGroupId = formGroupId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setPageDataArray(Form pageDataArray) {
        this.pageDataArray = pageDataArray;
    }
}
