package android.form.avss.prepaidcard.ui.fragments.doc_model;

import java.util.ArrayList;

public class DocsDataList {
    private boolean isDocumentCaptured = false;
    private String docName;
    private ArrayList<SelectedImage> images = new ArrayList<>();

    public boolean isDocumentCaptured() {
        return isDocumentCaptured;
    }

    public void setDocumentCaptured(boolean documentCaptured) {
        isDocumentCaptured = documentCaptured;
    }

    public String getDocName() {
        return docName;
    }

    public void setDocName(String docName) {
        this.docName = docName;
    }

    public ArrayList<SelectedImage> getImages() {
        return images;
    }

    public void setImages(ArrayList<SelectedImage> images) {
        this.images = images;
    }

    public boolean isDataListEmpty() {
        if (images == null) return false;
        return images.isEmpty();
    }

    public void clear() {
        if (images == null) return;
        images.clear();
    }
}
