package android.form.avss.prepaidcard.ihmf_form_fill.pojo.controls;


public enum Control {

    NONE,
    TextField,
    TextFieldBox,
    CHECKBOX,
    IMAGE,
    Signature,
    SUPER_IMAGE,
    SUPER_TEXT,
    Boolean,
    Photo;
    public static final String TYPE = "type";
}
