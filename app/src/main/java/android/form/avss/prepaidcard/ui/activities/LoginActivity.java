package android.form.avss.prepaidcard.ui.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.form.avss.prepaidcard.BuildConfig;
import android.form.avss.prepaidcard.R;
import android.form.avss.prepaidcard.database.MyDBHelper;
import android.form.avss.prepaidcard.helper_classes.Constant;
import android.form.avss.prepaidcard.receivers.NetworkReceiver;
import android.form.avss.prepaidcard.ui.custom_classes.CustomTextInputLayout;
import android.form.avss.prepaidcard.utils.MyCrashHandler;
import android.form.avss.prepaidcard.utils.PermissionUtils;
import android.form.avss.prepaidcard.utils.UtilityClass;
import android.form.avss.prepaidcard.web_services.interfaces.ServicesFunctionName;
import android.form.avss.prepaidcard.web_services.model.Services;
import android.form.avss.prepaidcard.web_services.requests.GenderMasterRequest;
import android.form.avss.prepaidcard.web_services.requests.LoginRequest;
import android.form.avss.prepaidcard.web_services.requests.OVDDeemedOVDMasterRequest;
import android.form.avss.prepaidcard.web_services.requests.PincodeMasterRequest;
import android.form.avss.prepaidcard.web_services.requests.PrefixMasterRequest;
import android.form.avss.prepaidcard.web_services.requests.ResidenceMasterRequest;
import android.form.avss.prepaidcard.web_services.responses.GenderMasterResponse;
import android.form.avss.prepaidcard.web_services.responses.LoginResponse;
import android.form.avss.prepaidcard.web_services.responses.OVDDeemedOVDMasterResponse;
import android.form.avss.prepaidcard.web_services.responses.PincodeMasterResponse;
import android.form.avss.prepaidcard.web_services.responses.PrefixMasterResponse;
import android.form.avss.prepaidcard.web_services.responses.ResidenceMasterResponse;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import java.util.Objects;

import okhttp3.ResponseBody;

public class LoginActivity extends BaseActivity implements View.OnClickListener, Services.Login, Services.PrefixMaster,
    Services.OVDMaster, Services.GenderMaster, Services.ResidenceMaster, Services.PinCodeMaster, Services.GetDropDowns, ServicesFunctionName {
    private CustomTextInputLayout tvEmail;
    private CustomTextInputLayout tvPassword;
    private ProgressDialog progressDialog;
    private String token;
    private int serviceCounter = -1;

    private static final int ALL_PERMISSION_CONSTANT = 101;
    private TextView tvImeiNumber;
    private TextView tvAppVersion;
    private boolean dismissProgressDialog = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initViews();

        if (PermissionUtils.isOSVersionMorHigher()) {
            PermissionUtils.permissionGranted(LoginActivity.this, permissionList, ALL_PERMISSION_CONSTANT);
            if (PermissionUtils.isGranted(LoginActivity.this, permissionList)) {
                setImeiNumber();
            }
        } else {
            setImeiNumber();
        }

        if (BuildConfig.DEBUG) {
            tvEmail.setText("103072");
            tvPassword.setText("123456");
        }
    }

    private void setImeiNumber() {
        String imeiNumber = getString(R.string.tab_imei) + " " + UtilityClass.getImeiNumber(LoginActivity.this);
        tvImeiNumber.setText(imeiNumber);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                attemptLogin();
                break;
        }
    }

    private void attemptLogin() {
        LoginRequest request = new LoginRequest();

        tvEmail.setErrorDisabled();
        tvPassword.setErrorDisabled();

        if (tvEmail.isEmpty() || tvPassword.isEmpty()) {
            tvEmail.setErrorMessage();
            tvPassword.setErrorMessage();
            toast("User id and password is required of login");
            return;
        }

        if (tvEmail.isEmpty()) {
            tvEmail.setErrorMessage("Bde Id is not valid");
            return;
        } else {
            request.setUserName(tvEmail.getString());
        }

        if (tvPassword.isFieldEmpty()) {
            return;
        } else if (tvPassword.getLength() < 4) {
            tvPassword.setErrorMessage("It's length should be greater than 4");
            return;
        } else {
            request.setUserPassword(tvPassword.getString());
        }

        if (PermissionUtils.isOSVersionMorHigher()) {
            PermissionUtils.permissionGranted(LoginActivity.this, permissionList, ALL_PERMISSION_CONSTANT);
            if (PermissionUtils.isGranted(LoginActivity.this, permissionList)) {
                String imeiNumber = getString(R.string.tab_imei) + " " + UtilityClass.getImeiNumber(LoginActivity.this);
                tvImeiNumber.setText(imeiNumber);
                if (NetworkReceiver.isConnected(this)) {
                    callLoginService(request);
                } else {
                    toast(R.string.err_no_internet);
                }
            } else {
                PermissionUtils.proceedAfterPermission(LoginActivity.this, true);
            }
        } else {
            String imeiNumber = getString(R.string.tab_imei) + " " + UtilityClass.getImeiNumber(LoginActivity.this);
            tvImeiNumber.setText(imeiNumber);
            if (NetworkReceiver.isConnected(this)) {
                callLoginService(request);
            } else {
                toast(R.string.err_no_internet);
            }
        }
    }

    private void callLoginService(LoginRequest request) {
//        if (!MyDBHelper.isPermissionGrantedToDb(this)) {
//            toast("App local database storage permission is not granted");
//            return;
//        }
//
//        if (!MyDBHelper.isAppDbCreated(this)) {
//            toast("Application local database is not created");
//            return;
//        }

        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
        }

        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setTitle("Service Call in Progress");
        progressDialog.setMessage("Logging in...");
        progressDialog.show();





//        String _ts = MyDBHelper.getInstance(this).getTimeStampFromSession(tvEmail.getString());
        request.setAppVersion(getString(R.string.version_no));
        request.setAppHash(UtilityClass.generateAppHash(LoginActivity.this));
        request.setCrashListReport(MyCrashHandler.getLog(LoginActivity.this));
//        request.setTimeStamp(TextUtils.isEmpty(_ts) ? "0" : _ts);

        new Services().login(request, this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == ALL_PERMISSION_CONSTANT) {

            boolean allGranted = false;
            if (permissions.length > 0 && grantResults.length > 0) {

                for (int result : grantResults) {
                    allGranted = result == PackageManager.PERMISSION_GRANTED;
                    if (!allGranted) break;
                }

                boolean isShouldShowRational = PermissionUtils.
                        isShouldShowRequestPermissionRationale(LoginActivity.this, permissionList);

                if (allGranted) {

                    PermissionUtils.proceedAfterPermission(LoginActivity.this, true);
                    setImeiNumber();
                } else if (isShouldShowRational) {

                    ActivityCompat.requestPermissions(LoginActivity.this, permissionList, ALL_PERMISSION_CONSTANT);
                } else {

                    toast(getString(R.string.err_permissions));
                    PermissionUtils.openSettings(LoginActivity.this, permissionList, ALL_PERMISSION_CONSTANT);
                }
            }
        }
    }

    private void initViews() {
        tvAppVersion = (TextView) findViewById(R.id.tvAppVersion);
        tvImeiNumber = (TextView) findViewById(R.id.tvImeiNumber);
        tvEmail = (CustomTextInputLayout) findViewById(R.id.tvEmail);
        tvPassword = (CustomTextInputLayout) findViewById(R.id.tvPassword);
        (Objects.requireNonNull(findViewById(R.id.btnLogin))).setOnClickListener(this);

        tvEmail.setLengthInputFilter(8);
        tvPassword.setLengthInputFilter(20);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void loginSuccessful(LoginResponse.LoginData data) {
        token = data.token();

        if (data.isSuccess()) {

            progressDialog.setMessage("Fetching Prefix Master");
            PrefixMasterRequest prefixMasterRequest = new PrefixMasterRequest();
            prefixMasterRequest.setBdeNo(tvEmail.getString());
            prefixMasterRequest.setToken(data.token());
            new Services().fetchPrefixMaster(prefixMasterRequest, this);


//            MyDBHelper dbHelper = MyDBHelper.getInstance(this);
//            String _timeStamp = dbHelper.getTimeStampFromSession(tvEmail.getString());
//            Constant constant = Constant.getInstance();
//
//            if (!constant.isServiceListNotEmpty())
//                constant.clearServiceList();
//
//            if (!data.isServiceListEmpty()) {
//                constant.setServiceList(data.getServiceStatusList());
//            }
//
//            if (TextUtils.isEmpty(_timeStamp)) {
//
//                if (dbHelper.insertSession(tvEmail.getString(), data.token(), String.valueOf(System.currentTimeMillis()))) {
//
//                    progressDialog.setMessage("Fetching Prefix Master");
//                    PrefixMasterRequest prefixMasterRequest = new PrefixMasterRequest();
//                    prefixMasterRequest.setBdeNo(tvEmail.getString());
//                    prefixMasterRequest.setToken(data.token());
//                    new Services().fetchPrefixMaster(prefixMasterRequest, this);
//
////                    callMasterService();
//                } else {
//
//                    progressDialog.dismiss();
//                    toast("please re-login again");
//                }
//
//            } else if (dbHelper.isBdeIdExists(tvEmail.getString()) && !TextUtils.isEmpty(_timeStamp)) {
//                dbHelper.updateSession(tvEmail.getString(), data.token());
//
//
//                progressDialog.setMessage("Fetching Prefix Master");
//                PrefixMasterRequest prefixMasterRequest = new PrefixMasterRequest();
//                prefixMasterRequest.setBdeNo(tvEmail.getString());
//                prefixMasterRequest.setToken(data.token());
//                new Services().fetchPrefixMaster(prefixMasterRequest, this);
//            } else {
//
//                toast("Invalid Bde Number");
//                progressDialog.dismiss();
//                tvEmail.setFieldToEmpty();
//                tvPassword.setFieldToEmpty();
//            }

        } else {

            toast(data.msg());
            progressDialog.dismiss();
        }
    }

    private void callMasterService() {
        if (dismissProgressDialog) {
            progressDialog.dismiss();
            toast("Master Syncing was successful");
            Intent _intent = new Intent(LoginActivity.this, AllModuleDashboardActivity.class);
            _intent.putExtra(BDE_NO, tvEmail.getString());
            startActivity(_intent);
            finish();
            return;
        }

        Constant constant = Constant.getInstance();
        if (constant.isServiceListNotEmpty()) {
            int size = constant.serviceListSize() - 1;
            String token = MyDBHelper.getInstance(this).getSessionToken();

            if (size == -1 || TextUtils.isEmpty(token)) return;

            if (serviceCounter < constant.serviceListSize()) {
                if (progressDialog == null) progressDialog = new ProgressDialog(this);
                progressDialog.setCancelable(false);

                String serviceName = constant.getServiceList().get(++serviceCounter);

                switch (serviceName) {
                    case GET_TITLE_MASTER:
                        progressDialog.setMessage("Fetching Prefix Master");
                        PrefixMasterRequest prefixMasterRequest = new PrefixMasterRequest();
                        prefixMasterRequest.setBdeNo(tvEmail.getString());
                        prefixMasterRequest.setToken(token);
                        new Services().fetchPrefixMaster(prefixMasterRequest, this);
                        break;

                    case GET_OVD_MASTER:
                        progressDialog.setMessage("Fetching OVD / Deemed OVD Master");
                        OVDDeemedOVDMasterRequest ovdDeemedOVDMasterRequest = new OVDDeemedOVDMasterRequest();
                        ovdDeemedOVDMasterRequest.setBdeNo(tvEmail.getString());
                        ovdDeemedOVDMasterRequest.setToken(token);
                        new Services().fetchOVDMaster(ovdDeemedOVDMasterRequest, this);
                        break;

                    case GET_GENDER_MASTER:
                        progressDialog.setMessage("Fetching Gender Master...");
                        GenderMasterRequest genderMasterRequest = new GenderMasterRequest();
                        genderMasterRequest.setBdeNo(tvEmail.getString());
                        genderMasterRequest.setToken(token);
                        new Services().fetchGenderMaster(genderMasterRequest, LoginActivity.this);
                        break;

                    case GET_RESIDENT_MASTER:
                        progressDialog.setMessage("Fetching Residence Type Master...");
                        ResidenceMasterRequest residenceMasterRequest = new ResidenceMasterRequest();
                        residenceMasterRequest.setBdeNo(tvEmail.getString());
                        residenceMasterRequest.setToken(token);
                        new Services().fetchResidenceMaster(residenceMasterRequest, this);
                        break;

                    case GET_PIN_CODE_MASTER:
                        progressDialog.setMessage("Fetching PinCode Master...");
                        PincodeMasterRequest pincodeMasterRequest = new PincodeMasterRequest();
                        pincodeMasterRequest.setBdeNo(tvEmail.getString());
                        pincodeMasterRequest.setToken(token);
                        new Services().fetchPinCodeMaster(pincodeMasterRequest, this);
                        break;

                    default:
                        progressDialog.dismiss();
                        toast("Something went wrong");
                        break;
                }

                if (serviceCounter == constant.serviceListSize()) {
                    String _timeStamp = String.valueOf(System.currentTimeMillis());
                    MyDBHelper.getInstance(this).updateSessionTimeStamp(tvEmail.getString(), _timeStamp);
                    dismissProgressDialog = true;
                    progressDialog.dismiss();
                }
            }

        }
    }

    @Override
    public void loginFailed(boolean status, String error) {
        toast(error);
        progressDialog.dismiss();
    }

    @Override
    public void prefixMasterSuccessful(PrefixMasterResponse.PrefixMaster data) {
        token = data.token();
        MyDBHelper.getInstance(this).updateSession(tvEmail.getString(), data.token());

        if (data.isSuccess()) {

            progressDialog.setMessage("Fetching OVD / Deemed OVD Master");
            progressDialog.setCancelable(false);
            OVDDeemedOVDMasterRequest ovdDeemedOVDMasterRequest = new OVDDeemedOVDMasterRequest();
            ovdDeemedOVDMasterRequest.setBdeNo(tvEmail.getString());
            ovdDeemedOVDMasterRequest.setToken(data.token());
            new Services().fetchOVDMaster(ovdDeemedOVDMasterRequest, this);

//            callMasterService();
        } else {

            toast(data.msg());
            progressDialog.dismiss();
        }
    }

    @Override
    public void prefixMasterFailed(boolean status, String error) {
        toast(error);
        progressDialog.dismiss();
    }

    @Override
    public void ovdMasterSuccessful(OVDDeemedOVDMasterResponse.OVDDeemedOVDMaster data) {
        token = data.token();
        MyDBHelper.getInstance(this).updateSession(tvEmail.getString(), data.token());

        if (data.isSuccess()) {

            progressDialog.setMessage("Fetching Gender Master...");
            progressDialog.setCancelable(false);
            GenderMasterRequest genderMasterRequest = new GenderMasterRequest();
            genderMasterRequest.setBdeNo(tvEmail.getString());
            genderMasterRequest.setToken(data.token());
            new Services().fetchGenderMaster(genderMasterRequest, LoginActivity.this);
//            callMasterService();
        } else {
            toast(data.msg());
            progressDialog.dismiss();
        }
    }

    @Override
    public void ovdMasterFailed(boolean status, String error) {
        toast(error);
        progressDialog.dismiss();
    }

    @Override
    public void genderMasterSuccessful(GenderMasterResponse.GenderMaster data) {
        token = data.token();
        MyDBHelper.getInstance(this).updateSession(tvEmail.getString(), data.token());
        if (data.isSuccess()) {
            progressDialog.setMessage("Fetching Residence Type Master...");
            progressDialog.setCancelable(false);
            ResidenceMasterRequest residenceMasterRequest = new ResidenceMasterRequest();
            residenceMasterRequest.setBdeNo(tvEmail.getString());
            residenceMasterRequest.setToken(data.token());
            new Services().fetchResidenceMaster(residenceMasterRequest, this);
//            callMasterService();
        } else {

            toast(data.msg());
            progressDialog.dismiss();
        }
    }

    @Override
    public void genderMasterFailed(boolean status, String error) {
        toast(error);
        progressDialog.dismiss();
    }

    @Override
    public void residenceMasterSuccessful(ResidenceMasterResponse.ResidenceMaster data) {
        token = data.token();
        MyDBHelper.getInstance(this).updateSession(tvEmail.getString(), data.token());

        if (data.isSuccess()) {

            progressDialog.setMessage("Fetching Pincode Master...");
            progressDialog.setCancelable(false);
            PincodeMasterRequest pincodeMasterRequest = new PincodeMasterRequest();
            pincodeMasterRequest.setBdeNo(tvEmail.getString());
            pincodeMasterRequest.setToken(data.token());
            new Services().fetchPinCodeMaster(pincodeMasterRequest, this);

//            callMasterService();
        } else {

            toast(data.msg());
            progressDialog.dismiss();
        }
    }

    @Override
    public void residenceMasterFailed(boolean status, String error) {
        toast(error);
        progressDialog.dismiss();
    }

    @Override
    public void pinCodeMasterSuccessful(final PincodeMasterResponse.PincodeMaster data) {
        token = data.token();
        MyDBHelper.getInstance(this).updateSession(tvEmail.getString(), data.token());
        if (data.isSuccess()) {
            new Thread() {
                public void run() {
                    try {
                        MyDBHelper myDBHelper = MyDBHelper.getInstance(LoginActivity.this);
                        SQLiteDatabase db = myDBHelper.getWritableDatabase();
                        db.beginTransaction();
                        myDBHelper.deletePinCode();
                        for (PincodeMasterResponse.PincodeMaster pincodeMaster : data.data()) {
                            myDBHelper.insertPinCode(db, pincodeMaster.pincode, pincodeMaster.district,
                                    pincodeMaster.state, pincodeMaster.stateCode);
                        }
                        db.setTransactionSuccessful();
                        db.endTransaction();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.setMessage("Fetching Drop Down Master...");
                                new Services().getDropDowns(LoginActivity.this, token);
//                                callMasterService();
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        } else {
            toast(data.msg());
            progressDialog.dismiss();
        }
    }

    @Override
    public void pinCodeMasterFailed(boolean status, String error) {
        toast(error);
        progressDialog.dismiss();
    }

    @Override
    public void getDropDownsSuccessful(ResponseBody data) {
//        MyDBHelper.getInstance(this).updateSession(tvEmail.getString(), "");
        progressDialog.dismiss();
        toast("Master Syncing was successful");
        Intent _intent = new Intent(LoginActivity.this, AllModuleDashboardActivity.class);
        _intent.putExtra(BDE_NO, tvEmail.getString());
        startActivity(_intent);
        finish();
    }

    @Override
    public void getDropDownsFailed(boolean status, String error) {
        toast(error);
        progressDialog.dismiss();
    }
}
