package android.form.avss.prepaidcard.ihmf_form_fill.ui.ControlsView;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.form.avss.prepaidcard.R;
import android.form.avss.prepaidcard.database.MyDBHelper;
import android.form.avss.prepaidcard.ihmf_form_fill.util.CustomInputFilter;
import android.form.avss.prepaidcard.ihmf_form_fill.util.DragTouchListener;
import android.form.avss.prepaidcard.ihmf_form_fill.interfaces.FocusListener;
import android.form.avss.prepaidcard.ihmf_form_fill.interfaces.OnDropDownItemClickListener;
import android.form.avss.prepaidcard.ihmf_form_fill.interfaces.OnLongPressListener;
import android.form.avss.prepaidcard.ihmf_form_fill.interfaces.iHMFView;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.controls.BaseControl;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.controls.TextControl;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.controls.iHMFGroupListener;
import android.form.avss.prepaidcard.ihmf_form_fill.ui.Adapters.DropDownListAdapter;
import android.form.avss.prepaidcard.ihmf_form_fill.util.MySharePreference;
import android.form.avss.prepaidcard.ihmf_form_fill.util.Utility;
import android.form.avss.prepaidcard.ihmf_form_fill.util.ValidationUtils;
import android.form.avss.prepaidcard.ihmf_form_fill.util.ConstantsUtils;
import android.form.avss.prepaidcard.ihmf_form_fill.util.iHMFUtility;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.JsonObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Simple Edittext for input
 */
public class IHMFEditText extends AppCompatEditText implements iHMFView<TextControl>, View.OnLongClickListener, View.OnFocusChangeListener {

    private TextControl mControl;
    private OnTouchListener mTouchListener;
    private final OnTouchListener DEFAULT_TOUCH_LISTENER = new DragTouchListener();
    private Drawable mErrorDrawable;
    private Drawable mDefaultDrawable;
    private OnLongPressListener mLongPressedListener;
    private DatePickerDialog datePickerDialog;
    private iHMFGroupListener<String> mGroupListener;
//    private GroupControls mGroup;
    private AlertDialog alertDialog;
    private DropDownListAdapter adapter;
    private MySharePreference mSharePreference;

    private boolean errorEnabled;

    public IHMFEditText(Context context) {
        super(context);
        init();
    }

    public IHMFEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();

    }

    public IHMFEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {

        if (getContext() instanceof OnLongPressListener)
            mLongPressedListener = (OnLongPressListener) getContext();

        setOnFocusChangeListener(this);

        setTextColor(getResources().getColor(R.color.iHMF_textColor));

        setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);   //TODO: remove (its conditional)

        setOnLongClickListener(new OnLongClickListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onLongClick(View v) {
                if (!IHMFEditText.this.getControl().isMarkedControl()) {
                    if (mTouchListener == null)
                        mTouchListener = DEFAULT_TOUCH_LISTENER;
                    IHMFEditText.this.setOnTouchListener(mTouchListener);
                    IHMFEditText.this.clearFocus();
                    if (mLongPressedListener != null) {
                        mLongPressedListener.onLongPressed(IHMFEditText.this);
                    }
                }
                return false;
            }
        });


        setErrorEnabled(false);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        if (mControl.isMarkedControl())
            setMeasuredDimension(getControl().getScaleWidth().intValue(), getControl().getScaleHeight().intValue());
        else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }

    public void removeOnTouchListener() {
        setOnTouchListener(null);
        mTouchListener = null;
    }

    @Override
    public View getView() {
        return this;
    }

    public void resizeView(int resizeType) {
        if (resizeType == ConstantsUtils.RESIZE_TYPE_INCREASE) {
            this.setTextSize(TypedValue.COMPLEX_UNIT_PX, this.getTextSize() + 1.0f);
        } else {
            if (this.getTextSize() > 10)
                this.setTextSize(TypedValue.COMPLEX_UNIT_PX, this.getTextSize() - 1.0f);
        }
    }


    @Override
    public TextControl getControl() {
        if (mControl == null)
            mControl = new TextControl();
        return mControl;
    }

    private JsonObject jsonObject;

    private FocusListener focusListener;

    @Override
    public void setControl(TextControl control, JsonObject jsonObject, FocusListener focusListener) {

        mControl = control;
        this.jsonObject = jsonObject;
        this.focusListener = focusListener;

        getControl();

        setX(getControl().getScaleX());
        setY(getControl().getScaleY());

        setHint(getControl().getHelpText());
        setText(getControl().getActualText());

        setVisibility(getControl().getControlVisible() == BaseControl.TRUE ? VISIBLE : GONE);


        if (getControl().isMarkedControl()) {
            if (getControl().hasGroup())
                addTextWatcher();

            if (getControl().getDataType().equals("date")) {
                setFocusable(false);
                setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        IHMFEditText.this.showDatePickerDialog();
                    }
                });
            }

            if(!TextUtils.isEmpty(getControl().getDdLabelId()) && !getControl().getDdLabelId().equals("0")){
                setFocusable(false);
                setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        IHMFEditText.this.showDropDownDialog();
                    }
                });

            }

            if (getControl().getDataType().equals(ValidationUtils.DataType.DATE)) {
                setFocusable(false);
                setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        IHMFEditText.this.showDatePickerDialog();
                    }
                });

            }
            setDataTypeFilter();
            setEnabled(!getControl().isLock());
        }
        else {
            setEms(5);
        }
    }


    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        return false;
    }

    private void setDataTypeFilter() {

        switch (getControl().getDataType()) {
            case ValidationUtils.DataType.NUM_CONTINOUS:
            case ValidationUtils.DataType.MOBILE_NUM_WITH_OTP:
            case ValidationUtils.DataType.MOBILE_NUM:
            case ValidationUtils.DataType.AADHAAR_NUM:
            case ValidationUtils.DataType.PINCODE:
            case ValidationUtils.DataType.CURRENCY:
                setInputType(InputType.TYPE_CLASS_NUMBER);
                break;
//            case Validation.Type.EMAIL:
//            case Validation.Type.EMAIL_WITH_OTP:
//                setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
//                break;

            default:
                if (getControl().isCapital()) {
                    setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
                }

                String regex = getControl().getInputFilterRegex();
                if(!TextUtils.isEmpty(regex)){
                    setFilters(new InputFilter[]{new CustomInputFilter(regex)});
                }
        }

    }


    private void showDatePickerDialog() {
        if (datePickerDialog != null) {
            datePickerDialog.show();
            return;
        }

        Calendar calendar = Calendar.getInstance();
        int y = calendar.get(Calendar.YEAR);
        int m = calendar.get(Calendar.MONTH);
        int d = calendar.get(Calendar.DAY_OF_MONTH);
        datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year1, int month1, int dayOfMonth1) {

                SimpleDateFormat format = new SimpleDateFormat(Utility.getFormat(IHMFEditText.this.getControl().getDateFormat()), Locale.getDefault());

                Date date;
                String dateString = null;
                try {
                    Calendar cal = Calendar.getInstance();
                    cal.set(year1, month1, dayOfMonth1);
                    date = cal.getTime();
                    dateString = format.format(date);
                } catch (Exception e) {
                    Toast.makeText(IHMFEditText.this.getContext(), "Some error occurred, Please fill manually", Toast.LENGTH_SHORT).show();
                    IHMFEditText.this.setFocusable(true);
                    IHMFEditText.this.setOnClickListener(null);
                    e.printStackTrace();
                }
                IHMFEditText.this.setText(dateString);

            }
        }, y, m, d);
        datePickerDialog.show();
    }


    @Override
    public boolean save() {
        if (getPreference().shouldCheckMandatoryFields() && getControl().isMarkedControl()
                && getControl().isMandatory() && !isDataValid()) {
            post(new Runnable() {
                @Override
                public void run() {
                    IHMFEditText.this.setErrorDrawable();
                    IHMFEditText.this.setErrorEnabled(true);
                }
            });
            return false;
        }
        getControl().setData(getText().toString());

        if (jsonObject != null) {
            jsonObject.addProperty("actualText", getText().toString());
        }
        return true;
    }

    @Override
    public boolean isValid() {
        return !getText().toString().isEmpty();
    }

    @Override
    public MySharePreference getPreference() {
        if(mSharePreference == null){
            mSharePreference = new MySharePreference(getContext());
        }
        return mSharePreference;
    }


    private void setErrorDrawable() {
        if (mErrorDrawable == null)
            mErrorDrawable = getResources().getDrawable(R.drawable.et_background_error);

        setBackground(mErrorDrawable);
    }

    private void setDefaultDrawable() {
        if (mDefaultDrawable == null)
            mDefaultDrawable = getResources().getDrawable(R.drawable.et_background_default);

        setBackground(mDefaultDrawable);
    }

    @Override
    public boolean onLongClick(View v) {

        return true;
    }


    private TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (getControl().hasGroup() && mGroupListener != null)
                mGroupListener.handleGroup(getControl().getId(), getControl().getGroupId(), s.toString());
        }
    };

    public void removeTextWatcher() {
        removeTextChangedListener(mTextWatcher);
    }

    public void addTextWatcher() {
        addTextChangedListener(mTextWatcher);
    }

    @Override
    public void onDraw(Canvas canvas) {

//        Set font size as per available input control height
        int txtSize = (int) (getHeight() * 0.6 - getPaddingBottom() - getPaddingTop());

//        int txtSize = getControl().getScaledFontSize();

        if(txtSize > 40) txtSize = 34;// todo: find a logic

        getPaint().setTextSize(txtSize);

        super.onDraw(canvas);

        // If mandate, mark it
        addStarMarkForMandatoryField(canvas);
    }


    private void addStarMarkForMandatoryField(Canvas canvas) {
        if (getControl().isMandatory()) {
            float w = getPaint().measureText("*");
            int color = getPaint().getColor();
            getPaint().setColor(Color.RED);
            canvas.drawText("*", getWidth() - w - getPaddingEnd(), w * 1.5f, getPaint());
            getPaint().setColor(color);
        }

    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (getControl().isMarkedControl() && !hasFocus) {

            if (getControl().isMandatory() && TextUtils.isEmpty(getText().toString())) {
                setErrorDrawable();
                setErrorEnabled(true);
                return;
            }

            if (!TextUtils.isEmpty(getText().toString()) && !isDataValid()) {
                setErrorDrawable();
                setErrorEnabled(true);
                return;
            }

        }

        if (isErrorEnabled()) {
            setDefaultDrawable();
            setErrorEnabled(false);
        }


        //listener to move view up when keyboard pops up by addOnGlobalLayoutListener
        //check implementation in FormFillFragment
        if(focusListener != null) {
            if(hasFocus)
                focusListener.addGlobalLayoutListener
                        (getControl().getScaleY() + getControl().getScaleHeight() + 20); // 20 pixels = gap between view's bottom and keyboard top
            else
                focusListener.removeGlobalLayoutListener();
        }
    }


    public void setTextFieldText(String text) {
        if (!TextUtils.isEmpty(text) && getControl().getDataType().equalsIgnoreCase(ValidationUtils.DataType.CURRENCY)
                && getControl().getAmountDisplayType().equalsIgnoreCase("Words")) {
            try {
                text = Utility.convert(Integer.parseInt(text));
            } catch (Exception e) {
                text = "";
            }
        }
        setText(text);
    }

    public void setOnGroupListener(iHMFGroupListener<String> mListener) {
        this.mGroupListener = mListener;
    }

//    public GroupControls getGroup() {
//
//        if (mGroup == null)
//            mGroup = mGroupListener == null ? null : mGroupListener.getGroupControl(getControl().getGroupId());
//
//        return mGroup;
//    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    private boolean isDataValid() {
        return ValidationUtils.isDataValid(getText().toString(), getControl().getRegex());
    }

    private boolean isErrorEnabled() {
        return this.errorEnabled;
    }

    private void setErrorEnabled(boolean b) {
        this.errorEnabled = b;
    }


    private void showDropDownDialog(){
        if(alertDialog != null){
            alertDialog.show();
            return;
        }

        MyDBHelper myDBHelper = MyDBHelper.getInstance(getContext());
        List<String> dataList = myDBHelper.getDropDownList(getControl().getDdLabelId());

        if(dataList == null || dataList.isEmpty()) {
            Toast.makeText(getContext(), "Drop down data not found", Toast.LENGTH_SHORT).show();
            return;
        }


        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        @SuppressLint("InflateParams")
        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_drop_down_layout, null);

        RecyclerView rv_item = (RecyclerView) view.findViewById(R.id.rv_item);
//        TextView tv_title = (TextView) view.findViewById(R.id.tv_title);

        EditText et_searchView = (EditText) view.findViewById(R.id.et_search);

        rv_item.setLayoutManager(new LinearLayoutManager(getContext()));

        adapter = new DropDownListAdapter(dataList, new OnDropDownItemClickListener() {
            @Override
            public void onClick(String data) {
                setText(data);
                if(isErrorEnabled()){
                    setDefaultDrawable();
                    setErrorEnabled(false);
                }
                alertDialog.dismiss();
            }
        });

        rv_item.setAdapter(adapter);

        et_searchView.addTextChangedListener(textWatcher);
        builder.setView(view);

        alertDialog = builder.create();

        alertDialog.show();
        iHMFUtility.hideSoftKeyboard(this);

    }


    private final TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if(adapter != null){
                adapter.getFilter().filter(s.toString());
            }
        }
    };

}
