package android.form.avss.prepaidcard.ihmf_form_fill.interfaces;

import android.form.avss.prepaidcard.ihmf_form_fill.pojo.controls.BaseControl;
import android.view.View;

public interface iHMFView<T extends BaseControl> extends iHMFControl<T> {

    void resizeView(int resizeType);

    void removeOnTouchListener();

    View getView();
}
