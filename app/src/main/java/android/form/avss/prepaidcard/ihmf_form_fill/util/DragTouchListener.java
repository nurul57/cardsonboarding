package android.form.avss.prepaidcard.ihmf_form_fill.util;

import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.EditText;
import android.widget.TextView;

/**
 * This is used to drag view inside it's parent.
 * In order to drag view, use setOnTouchListener(new DragTouchListener())
 */
public class DragTouchListener implements View.OnTouchListener {

    private float initX;
    private float initY;

    private float deltaX, deltaY;
    private String TAG = getClass().getSimpleName();
    private int inputType = InputType.TYPE_NULL;

    private ViewConfiguration viewConfiguration;

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        if(!v.isAttachedToWindow()) return false;

        if (viewConfiguration == null)
            viewConfiguration = ViewConfiguration.get(v.getContext());

        boolean handled = false;

        switch (event.getActionMasked()) {

            case MotionEvent.ACTION_CANCEL:

                v.setX(initX);
                v.setY(initY);

                break;
            case MotionEvent.ACTION_DOWN:

                if (v.getParent() != null)
                    v.getParent().requestDisallowInterceptTouchEvent(true);

                if (v instanceof EditText) {
                    inputType = ((TextView) v).getInputType() == InputType.TYPE_NULL ? InputType.TYPE_CLASS_TEXT : ((EditText) v).getInputType();
                    ((EditText) v).setInputType(InputType.TYPE_NULL);
                }

                initX = v.getX();
                initY = v.getY();

                deltaX =  (initX - event.getRawX());
                deltaY =  (initY - event.getRawY());

                handled = true;
                break;

            case MotionEvent.ACTION_MOVE:


                float currentX =  event.getRawX() + deltaX;
                float currentY =  event.getRawY() + deltaY;

                int touchSlop = (int) Math.max(Math.abs(currentX - initX), Math.abs(currentY - initY));

                if (touchSlop < viewConfiguration.getScaledTouchSlop()) {
                    handled = true;
                    break;
                }

                View viewParent = (View) v.getParent();

                if (viewParent != null) {
                    int width = viewParent.getLayoutParams().width;
                    int height = viewParent.getLayoutParams().height;

                    if (currentX < 0 || currentX + v.getWidth() > width)
                        currentX = (int) v.getX();


                    if (currentY < 0 || currentY + v.getHeight() > height)
                        currentY = (int) v.getY();

                }

                v.animate().x(currentX).y(currentY).setDuration(0).start();

                handled = true;


                break;

            case MotionEvent.ACTION_UP:

                if (v instanceof EditText) {
                    ((EditText) v).setInputType(inputType);
                }

                if (v.getParent() != null)
                    v.getParent().requestDisallowInterceptTouchEvent(false);
                handled = true;


                break;
            default:
                handled = true;

        }


        return handled;
    }
}
