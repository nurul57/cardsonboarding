package android.form.avss.prepaidcard.ui.activities;

import android.content.Intent;
import android.form.avss.prepaidcard.R;
import android.form.avss.prepaidcard.ihmf_form_fill.TempPackage.MainActivity;
import android.form.avss.prepaidcard.ui.custom_classes.CustomButton;
import android.form.avss.prepaidcard.ui.custom_classes.CustomTextInputLayout;
import android.form.avss.prepaidcard.helper_classes.Constant;
import android.form.avss.prepaidcard.ui.model.EkycOptionViewModel;
import android.form.avss.prepaidcard.ui.model.FinalDataViewModel;
import android.form.avss.prepaidcard.ui.model.PanDataViewModel;
import android.form.avss.prepaidcard.web_services.model.Services;
import android.form.avss.prepaidcard.web_services.requests.FinalDataSubmitRequest;
import android.form.avss.prepaidcard.web_services.responses.FinalDataSubmitResponse;
import android.os.Bundle;
import android.view.View;


public class WalletAndCustomerIdActivity extends BaseActivity implements View.OnClickListener, Services.FinalDataSubmit {
	private CustomTextInputLayout tvCustomerMobileNo;
	private CustomTextInputLayout tvCustomerId;
	private CustomButton btnCustomerId;
	private CustomButton btnMobileNo;
	private CustomButton btnFinalSubmit;
	private String token;
	private FinalDataViewModel model;
	private Constant constant;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wallet_and_customer_id);

		if (getIntent() == null) {
			toast("Something went wrong");
			finish();
			return;
		}

		token = getIntent().getStringExtra(TOKEN);

		model = new FinalDataViewModel();
		constant = Constant.getInstance();
		initView();
	}

	public void initView(){
		tvCustomerId = (CustomTextInputLayout) findViewById(R.id.tvCustomerId);
		tvCustomerMobileNo = (CustomTextInputLayout) findViewById(R.id.tvCustomerMobileNo);

		btnCustomerId = (CustomButton) findViewById(R.id.btn_customerId_validate);
		btnMobileNo = (CustomButton) findViewById(R.id.btn_mobileno_validate);
		btnFinalSubmit = (CustomButton) findViewById(R.id.btnFinalSubmit);

		btnCustomerId.setOnClickListener(this);
		btnMobileNo.setOnClickListener(this);
		btnFinalSubmit.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.btn_customerId_validate:
				if (tvCustomerId.isFieldEmpty()) return;
				// TODO: 5/13/2019 Customer id service call

				break;

			case R.id.btn_mobileno_validate:
				if (!tvCustomerMobileNo.matchMobilePattern()) {
					tvCustomerMobileNo.setErrorMessage(R.string.msg_phone);
					return;
				}

				// TODO: 5/13/2019 mobile service call

				break;

			case R.id.btnFinalSubmit:
				// TODO: 5/13/2019 final data submit service call

				Intent intent = getIntent();
				try {
					model.setEkycOptionViewModel(constant.fromJson(intent.getStringExtra(EKYC_OPTION_DATA), EkycOptionViewModel.class));
					model.setPanDataViewModel(constant.fromJson(intent.getStringExtra(PAN_DATA), PanDataViewModel.class));
					token = intent.getStringExtra(TOKEN);
					callFinalDataSubmitService(constant.toJson(model));

				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
		}
	}

	private void callFinalDataSubmitService(String finalRequest) {
		if (isEmpty(finalRequest)){
			toast("final data is empty");
			return;
		}

		FinalDataSubmitRequest submitRequest = new FinalDataSubmitRequest();
		submitRequest.setBdeNo(getIntent().getStringExtra(BDE_NO));
		submitRequest.setFinalSubmit(finalRequest);

		new Services().finalSubmitData(submitRequest, WalletAndCustomerIdActivity.this, token);
	}

	@Override
	public void finalDataSubmitSuccessful(FinalDataSubmitResponse.FinalSubmit data) {
		if (data.isSuccess()) {
			toast("data is successfully submitted");

			//todo : bypass
            Intent intent = new Intent(WalletAndCustomerIdActivity.this, DocumentActivity.class);
            intent.putExtra(BDE_NO, getIntent().getStringExtra(BDE_NO));
            intent.putExtra(FORM_ID, "1113");
            intent.putExtra(TOKEN, data.getToken());
			intent.putExtra(REFERENCE_ID, data.getReferenceId());
            startActivity(intent);
            finish();
		}
	}

	@Override
	public void finalDataSubmitFailed(boolean status, String error) {
		toast(error);
	}
}
