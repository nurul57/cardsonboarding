package android.form.avss.prepaidcard.web_services.responses;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;


public class FinalDataSubmitResponse {

    @SerializedName("da")
    public String data;

    public static class FinalSubmit {

        @SerializedName("ss")
        private String status;

        @SerializedName("em")
        private String errorMessage;


        @SerializedName("tn")
        private String token;

        @SerializedName("refid")
        private String referenceId;


        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getErrorMessage() {
            return errorMessage;
        }

        public void setErrorMessage(String errorMessage) {
            this.errorMessage = errorMessage;
        }

        private final static String TRUE = "true";
        private final static String FALSE = "false";

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public boolean isSuccess() {
            return !TextUtils.isEmpty(getStatus()) && !FALSE.equalsIgnoreCase(getStatus());
        }

        public boolean hasToken() {
            return TextUtils.isEmpty(getToken());
        }

        public String getReferenceId() {
            return referenceId;
        }

        public void setReferenceId(String referenceId) {
            this.referenceId = referenceId;
        }
    }

}
