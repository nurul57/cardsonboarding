package android.form.avss.prepaidcard.ihmf_form_fill.ui.Adapters;

import android.form.avss.prepaidcard.R;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PageListAdapter extends RecyclerView.Adapter<PageListAdapter.CustomViewHolder> {

    private List<String> imageUrlList;

    private OnPageClicked listener;

    private int rawPos = 0;
    private Drawable mSelector;

    public PageListAdapter(List<String> list, OnPageClicked listener) {
        imageUrlList = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CustomViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final CustomViewHolder holder, int position) {

        if (mSelector == null)
            mSelector = holder.itemView.getContext().getResources().getDrawable(R.drawable.shape_rectangle_2dp_red_stroke);

        holder.imageView.setBackground(position == rawPos ? mSelector : null);

        Picasso.with(holder.itemView.getContext()).load(imageUrlList.get(position)).fit().into(holder.imageView, new Callback() {
            @Override
            public void onSuccess() {
                holder.progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError() {
//                holder.imageView.setImageDrawable(holder.itemView.getContext().getResources().getDrawable(R.drawable.ic_image_downlaod_error));
                holder.progressBar.setVisibility(View.GONE);
            }
        });

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null && holder.getAdapterPosition() != RecyclerView.NO_POSITION)
                    listener.clicked(holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return imageUrlList == null ? 0 : imageUrlList.size();
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageView;
        private ProgressBar progressBar;

        CustomViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.iv_page);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);
        }
    }

    public interface OnPageClicked {
        void clicked(int pos);
    }


    public void currentSelectedPosition(int pos) {
        if (pos >= 0 && pos < getItemCount()) {
            notifyDataSetChanged();
            rawPos = pos;
        }
    }
}
