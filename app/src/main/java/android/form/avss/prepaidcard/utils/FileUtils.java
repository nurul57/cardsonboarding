package android.form.avss.prepaidcard.utils;

import android.os.Environment;
import android.text.TextUtils;

import java.io.File;

public class FileUtils {
    public static final String ROOT_FOLDER = "AllInOne";
    public static final String DB_FOLDER = "databases";
    public static final String DB_INFO = "info";
    public static final String AADHAAR_IMAGE_FOLDER = "AadhaarImages/form_";
    public static final String TAMPER_DB = "tamper";

    //todo: path for root folder in external storage
    public static String getRoot() {
        File f = new File(Environment.getExternalStorageDirectory(), ROOT_FOLDER);
        if (!f.exists())
            f.mkdirs();
        return f.getAbsolutePath();
    }

    public static File getDBFolderPath() {
        File dbFolder = new File(getRoot(), DB_FOLDER);
        if (!dbFolder.exists())
            dbFolder.mkdirs();
        return dbFolder;
    }

    public static String getAadhaarImagePath(String formId) {
        File aadhaarImagefile = new File(getRoot(), AADHAAR_IMAGE_FOLDER + formId);
        if (!aadhaarImagefile.exists()) {
            aadhaarImagefile.mkdirs();
        }
        return aadhaarImagefile.getAbsolutePath();
    }

    public static boolean isFile(String absolutePath) {
        if (TextUtils.isEmpty(absolutePath)) return false;
        File f = new File(absolutePath);
        return f.exists() && f.isFile();
    }
}
