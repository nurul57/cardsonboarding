package android.form.avss.prepaidcard.ihmf_form_fill.ui.Activities;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.form.avss.prepaidcard.R;
import android.form.avss.prepaidcard.ihmf_form_fill.TempPackage.ImageData;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.Requests.FormImageSubmitRequest;
import android.form.avss.prepaidcard.ihmf_form_fill.TempPackage.MainActivity;
import android.form.avss.prepaidcard.ihmf_form_fill.interfaces.DialogButtonClickListener;
import android.form.avss.prepaidcard.ihmf_form_fill.interfaces.OnLongPressListener;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.Requests.DataSubmit;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.Responses.Form;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.Responses.FormDataSubmitResponse;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.Responses.ImageDataSubmitResponse;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.Responses.Page;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.controls.BaseControl;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.controls.Control;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.controls.ControlUtility;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.controls.ImageControl;
import android.form.avss.prepaidcard.ihmf_form_fill.ui.Adapters.FormFillFragmentPagerAdapter;
import android.form.avss.prepaidcard.ihmf_form_fill.ui.Adapters.PageListAdapter;
import android.form.avss.prepaidcard.ihmf_form_fill.ui.Fragments.FormFillFragmentForm;
import android.form.avss.prepaidcard.ihmf_form_fill.ui.Others.CustomViewPager;
import android.form.avss.prepaidcard.ihmf_form_fill.util.MyAnimationUtil;
import android.form.avss.prepaidcard.ihmf_form_fill.util.iHMFUtility;
import android.form.avss.prepaidcard.ihmf_form_fill.util.ConstantsUtils;
import android.form.avss.prepaidcard.ui.activities.DocumentActivity;
import android.form.avss.prepaidcard.web_services.model.Services;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.JsonObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

import static android.form.avss.prepaidcard.ihmf_form_fill.util.ConstantsUtils.INVALID_DATA;
import static android.form.avss.prepaidcard.interfaces.Keys.BDE_NO;
import static android.form.avss.prepaidcard.interfaces.Keys.IS_PREVIEW_MODE;
import static android.form.avss.prepaidcard.interfaces.Keys.REFERENCE_ID;
import static android.form.avss.prepaidcard.interfaces.Keys.TOKEN;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class FormFillActivity extends FormBaseActivity implements View.OnClickListener, DrawerLayout.DrawerListener
        , OnLongPressListener, Services.SaveFormData, ViewPager.OnPageChangeListener, Services.FormImageSubmit {

    private static String TAG = FormFillActivity.class.getName();

    private Form formData;
    private CustomViewPager vp_mViewPager;
    private FormFillFragmentPagerAdapter adapter;

    private RelativeLayout default_toolbar;
    private RelativeLayout controls_toolbar;


    private View mUnmarkcontrolView;
    private ImageButton btn_next, btn_prev;
    private Button btn_submit, btn_edit;

    private TextView tv_formName, tv_formNumber, tv_mandtoryControls, tv_totalControls, txt_pageIndex;
    private ProgressBar pb_progressBar;
    public DrawerLayout drawer;
    private ListView lv_mandatoryFieldsList;
    private BottomSheetDialog mUnmarkControlSheetDialog;
    private FormFillFragmentForm mCurrentFragment;
    @SuppressWarnings("unused") //TODO
    private PageListAdapter pageListAdapter;


    private CompositeDisposable mDisposable;

    private boolean isDrawerUpdated = false;
    private boolean isPreviewMode;

    @SuppressWarnings("unused") //TODO optional
    private Runnable mLeftArrowRunnable = new Runnable() {
        @Override
        public void run() {
            Objects.requireNonNull(FormFillActivity.this.findViewById(R.id.ic_up)).animate().alpha(0.5f).scaleX(1f).scaleY(1f).start();
        }
    };
    private List<FormImageSubmitRequest> imageDataList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_fill);

        mDisposable = new CompositeDisposable();
        mUnmarkControlSheetDialog = new BottomSheetDialog(this);

        isPreviewMode = getIntent().getBooleanExtra(IS_PREVIEW_MODE, false);

        initializeViews();

        setViewListeners();

        toggleToolbars(VISIBLE);

        mUnmarkControlSheetDialog.setCancelable(false);
        mUnmarkControlSheetDialog.setCanceledOnTouchOutside(true);
        mUnmarkControlSheetDialog.setContentView(mUnmarkcontrolView);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, null, R.string.app_name,R.string.mandate_list);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        toggle.setDrawerIndicatorEnabled(false);
        drawer.addDrawerListener(this);


        if(isPreviewMode){
            Objects.requireNonNull(findViewById(R.id.text_previewMode)).setVisibility(VISIBLE);
            btn_edit.setVisibility(VISIBLE);
            btn_submit.setText(R.string.done);
        }

        getDataFromFile();
    }

    @SuppressLint("InflateParams")
    private void initializeViews(){
        default_toolbar = (RelativeLayout) findViewById(R.id.base_toolbar);
        controls_toolbar = (RelativeLayout) findViewById(R.id.controls_toolbar);

        tv_formName = (TextView) findViewById(R.id.form_name);
        tv_formNumber = (TextView) findViewById(R.id.form_number);
        tv_totalControls = (TextView) findViewById(R.id.tv_totalControl);
        tv_mandtoryControls = (TextView) findViewById(R.id.tv_mandatoryControl);

        pb_progressBar = (ProgressBar) findViewById(R.id.pb_progressBar);

        btn_next = (ImageButton) findViewById(R.id.btnArrowRight);
        btn_prev = (ImageButton) findViewById(R.id.btnArrowLeft);
        btn_submit = (Button) findViewById(R.id.action_submit);
        btn_edit = (Button) findViewById(R.id.btn_edit);

        txt_pageIndex = (TextView) findViewById(R.id.txtPageIndex);

        vp_mViewPager = (CustomViewPager) findViewById(R.id.viewPager);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        lv_mandatoryFieldsList = (ListView) findViewById(R.id.right_nav_list);

        mUnmarkcontrolView = LayoutInflater.from(this).inflate(R.layout.fab_toolbar,null,false);
    }

    private void setViewListeners() {
        Objects.requireNonNull(findViewById(R.id.scaleBig)).setOnClickListener(this);
        Objects.requireNonNull(findViewById(R.id.scaleSmall)).setOnClickListener(this);
        Objects.requireNonNull(findViewById(R.id.btn_ok)).setOnClickListener(this);
        Objects.requireNonNull(findViewById(R.id.btn_delete)).setOnClickListener(this);
        Objects.requireNonNull(findViewById(R.id.action_save_draft)).setOnClickListener(this);
        Objects.requireNonNull(findViewById(R.id.action_auto_fill)).setOnClickListener(this);


        mUnmarkcontrolView.findViewById(R.id.btn_addText).setOnClickListener(this);
        mUnmarkcontrolView.findViewById(R.id.btn_addCheckbox).setOnClickListener(this);
        mUnmarkcontrolView.findViewById(R.id.btn_addSign).setOnClickListener(this);
        mUnmarkcontrolView.findViewById(R.id.btn_addPhoto).setOnClickListener(this);


        btn_next.setOnClickListener(this);
        btn_prev.setOnClickListener(this);
        btn_submit.setOnClickListener(this);
        btn_edit.setOnClickListener(this);

        vp_mViewPager.addOnPageChangeListener(this);
    }

    private void getDataFromFile() {
        pb_progressBar.setVisibility(VISIBLE);
        File file = new File(getFormDataFileDir(), getSPreference().getFormId()+".txt");

        // If file not found, close form screen
        if (!file.isFile() || !file.exists()) {
            pb_progressBar.setVisibility(GONE);
            new AlertDialog.Builder(this)
                    .setCancelable(false)
                    .setTitle("Unable to proceed")
                    .setMessage("Form file not found...")
                    .setPositiveButton("CLOSE", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                            dialog.dismiss();
                        }
                    }).show();
            return;
        }

//        try (BufferedReader data = new BufferedReader(new InputStreamReader(getResources().openRawResource(R.raw.dump)))) {
        Disposable d = Single.just(file)
                .subscribeOn(Schedulers.io())
                .map(new Function<File, String>() {
                    @Override
                    public String apply(File f) {

                        StringBuilder readJsonBuilder = new StringBuilder();
//                        try (BufferedReader data = new BufferedReader(new InputStreamReader(FormFillActivity.this.getResources().openRawResource(R.raw.sample)))) {
                        try (BufferedReader data = new BufferedReader(new FileReader(f))) {
                            String str;
                            while ((str = data.readLine()) != null)
                                readJsonBuilder.append(str);
                        } catch (Exception e) {
                            throw new IllegalStateException("Failed to load", e);
                        }

                        return readJsonBuilder.toString();

                    }
                })
                .map(new Function<String, Form>() {
                    @Override
                    public Form apply(String o) {
                        return ControlUtility.getGson().fromJson(o, Form.class);
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Form>() {
                    @Override
                    public void accept(Form form) {
                        formData = form;
                        adapter = new FormFillFragmentPagerAdapter(FormFillActivity.this.getSupportFragmentManager());
                        for (Page data : formData.getPages()) {
                            adapter.addFragment(FormFillFragmentForm.newInstance(data, isPreviewMode));
                        }
                        vp_mViewPager.setAdapter(adapter);
                        vp_mViewPager.setPagingEnabled(false);

                        tv_formName.setText(formData.getTitle());
                        tv_formNumber.setText(getSPreference().getReferenceID());

                        pb_progressBar.setVisibility(GONE);

                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable e) {
                        pb_progressBar.setVisibility(GONE);
                        showShortToast( e.getMessage());
                    }
                });


        mDisposable.add(d);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnArrowLeft:
                iHMFUtility.hideSoftKeyboard(v);
                validateAndSaveData(vp_mViewPager.getCurrentItem() - 1);
                break;

            case R.id.btnArrowRight:
                iHMFUtility.hideSoftKeyboard(v);
                validateAndSaveData(vp_mViewPager.getCurrentItem() + 1);
                break;

            case R.id.btn_addText:
                mCurrentFragment.setControl(Control.TextField);
                break;

            case R.id.btn_addCheckbox:
                mCurrentFragment.setControl(Control.Boolean);

                break;
            case R.id.btn_addSign:
                showShortToast("Currently not available");
//                mCurrentFragment.setControl(Control.Signature);
//                mBottomToolbar.hide();
                break;

            case R.id.btn_addPhoto:
                mCurrentFragment.setControl(Control.Photo);
//                mBottomToolbar.hide();
                break;

            case R.id.scaleBig:
                mCurrentFragment.resizeViewSize(ConstantsUtils.RESIZE_TYPE_INCREASE);
                break;

            case R.id.scaleSmall:
                mCurrentFragment.resizeViewSize(ConstantsUtils.RESIZE_TYPE_DECREASE);
                break;

            case R.id.btn_ok:
                mCurrentFragment.setControl(Control.NONE);
                mCurrentFragment.removeTouchListener();
                toggleToolbars(VISIBLE);
                break;

            case R.id.btn_delete:
                mCurrentFragment.deleteUnmarkedView();
                mCurrentFragment.setControl(Control.NONE);
                toggleToolbars(VISIBLE);
                break;

            case R.id.action_save_draft:
                break;
            case R.id.action_submit:
                if(isPreviewMode) {
                    Intent intent = new Intent(FormFillActivity.this, DocumentActivity.class);
                    intent.putExtra(BDE_NO, getSPreference().getBdeNumber());
                    intent.putExtra(REFERENCE_ID, getSPreference().getReferenceID());
                    intent.putExtra(TOKEN, getSPreference().getToken());
                    startActivity(intent);
                    finish();
                }
                else {
                    validateAndSaveData(-1);
                }

                break;

            case R.id.action_auto_fill:
                if (!mUnmarkControlSheetDialog.isShowing())
                    mUnmarkControlSheetDialog.show();
                else
                    mUnmarkControlSheetDialog.dismiss();

                break;

                case R.id.btn_edit:
                    Intent intent = new Intent(FormFillActivity.this, MainActivity.class);
                    intent.putExtra(IS_PREVIEW_MODE, false);
                    startActivity(intent);
                    finish();
                    break;

        }
    }

    // if param == -1 -> submit data
    private void validateAndSaveData(final int scrollPosition) {
        showProgressDialog("Saving data");
        Disposable d = Completable.fromRunnable(new Runnable() {
            @Override
            public void run() {
                if (mCurrentFragment != null)
                    mCurrentFragment.validateAndSaveData();
            }
        }).subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action() {
                               @Override
                               public void run() {
                                   //on validation and save successful
                                   dismissProgressDialog();
                                   if(scrollPosition == -1){
                                       confirmAndSubmitData();
                                   }else {
                                       vp_mViewPager.setCurrentItem(scrollPosition);
                                   }
                               }
                           },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable e) {
                                dismissProgressDialog();
                                e.printStackTrace();
                                if (e.getMessage().equalsIgnoreCase(INVALID_DATA))
                                    FormFillActivity.this.showShortToast("PLease fill/validate all mandatory fields");
                            }
                        });

        mDisposable.add(d);
    }


    private void confirmAndSubmitData(){
        showAlertDialog(getString(R.string.mssg_submit_data), new DialogButtonClickListener() {
            @Override
            public void onClickedOK() {
//                           saveDataInFile();

                imageDataList = getImageDataRequest();
                if (imageDataList.size() > 0) {
                    FormFillActivity.this.sendImageData(imageDataList.get(imageReqCount));
                } else {
                    FormFillActivity.this.sendFormData();
                }

            }

            @Override
            public void onClickedCancel() {
                hideAlertDialog();
            }
        });
    }


    public void toggleToolbars(int baseToolbarVisibility) {
        default_toolbar.setVisibility(baseToolbarVisibility);
        controls_toolbar.setVisibility(baseToolbarVisibility == VISIBLE ? GONE : VISIBLE);
    }

    public void setCurrentUnmarkedFocusedView(View view) {
        mCurrentFragment.setCurrentView(view);
    }


    @Override
    public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {
        float previous = 0.0f;
        boolean isOpening = Math.signum(slideOffset - previous) > 0;
        if(isOpening) iHMFUtility.hideSoftKeyboard(drawerView);

        if(drawerView.getId() == R.id.right_nav_view) {
            if (isOpening && !isDrawerUpdated) {

                if (mCurrentFragment != null && mCurrentFragment.getMandateAdapter() != lv_mandatoryFieldsList.getAdapter()) {
                    lv_mandatoryFieldsList.setAdapter(mCurrentFragment.getMandateAdapter());

                }

                if (mCurrentFragment != null && mCurrentFragment.isVisible()) {
                    if (mCurrentFragment.getMandateAdapter().getCount() == 0)
                        Objects.requireNonNull(findViewById(R.id.no_item)).setVisibility(View.VISIBLE);
                    else
                        Objects.requireNonNull(findViewById(R.id.no_item)).setVisibility(View.GONE);
                }


                isDrawerUpdated = true;
                ((TextView) Objects.requireNonNull(findViewById(R.id.text_star))).setText(Html.fromHtml(getString(R.string.text_mandatory)));
                tv_totalControls.setText(String.valueOf(mCurrentFragment.getTotalMarkedcontrols()));
                tv_mandtoryControls.setText(String.valueOf(mCurrentFragment.getMandateAdapter().getCount()));

            }
        }
//        else if (drawerView.getId() == R.id.left_nav_view) {
//            if (pageListAdapter != null) {
//                return;
//            }
//
//        }

//        if (slideOffset == 0 && !isOpening) {
//            // action when slider is closed
//        }

    }

    @Override
    public void onDrawerOpened(@NonNull View drawerView) {
        isDrawerUpdated = false;
    }

    @Override
    public void onDrawerClosed(@NonNull View drawerView) {
        isDrawerUpdated = false;
    }

    @Override
    public void onLongPressed(View view) {
        showShortToast("Move/Resize the view");
        toggleToolbars(GONE);
        setCurrentUnmarkedFocusedView(view);
        iHMFUtility.clearAndHideSoftKeyboard(view);

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if(position != vp_mViewPager.getCurrentItem()) return;

        btn_next.setEnabled(position + 1 != adapter.getCount());
        btn_prev.setEnabled(position != 0);
        assert txt_pageIndex != null;
        txt_pageIndex.setText(String.format("%s%s%s", position + 1, "/", adapter.getCount()));

        mCurrentFragment = adapter.getRegisteredFragment(position);

        if (mCurrentFragment != null) {
            lv_mandatoryFieldsList.setAdapter(mCurrentFragment.getMandateAdapter());
        }
        if (pageListAdapter != null) {
            pageListAdapter.currentSelectedPosition(position);
        }

        if(position + 1 == adapter.getCount()){
            btn_submit.startAnimation(MyAnimationUtil.getPopUpAnimation(FormFillActivity.this));
            btn_submit.setVisibility(VISIBLE);
        }
        else if(btn_submit.getVisibility() == VISIBLE){
            Animation anim = MyAnimationUtil.getPopInAnimation(FormFillActivity.this);
            btn_submit.startAnimation(anim);
            anim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    Log.d(TAG, "onAnimationStart: ");
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    btn_submit.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                    Log.d(TAG, "onAnimationRepeat: ");
                }
            });
        }
    }



    /**
     * create imageDataRequest
     **/
    private List<FormImageSubmitRequest> getImageDataRequest() {
        List<FormImageSubmitRequest> requestList = new ArrayList<>();
        for (Page page : formData.getPages()) {
            for (JsonObject jsonObject : page.getControls()) {
                BaseControl baseControl = ControlUtility.get(jsonObject);

                if (baseControl.getType().equalsIgnoreCase(Control.Photo.toString())
                        || baseControl.getType().equalsIgnoreCase(Control.Signature.toString())) {
                    ImageControl imageControl = (ImageControl) baseControl;
                    if(TextUtils.isEmpty(imageControl.getImageData())){
                        continue;
                    }
                    boolean temp = false;
                    for (FormImageSubmitRequest request : requestList) {
                        if (request.getGroupId().equals(imageControl.getGroupId())) {
                            temp = true;
                        }
                    }
                    if (!temp) {
                        FormImageSubmitRequest formImageSubmitRequest = new FormImageSubmitRequest();
                        formImageSubmitRequest.setType(getSPreference().getType());
                        formImageSubmitRequest.setFormId(getSPreference().getFormId());
                        formImageSubmitRequest.setReferenceId(getSPreference().getReferenceID());

                        if (imageControl.hasGroup()) {
                            formImageSubmitRequest.setGroupId(imageControl.getGroupId());
                        } else {
                            formImageSubmitRequest.setGroupId("0");
                        }

                        ImageData imageData = new ImageData();
                        imageData.setName(imageControl.getName());
                        imageData.setControlId(String.valueOf(imageControl.getId()));
                        imageData.setImageData(imageControl.getImageData());
                        imageData.setImageId(page.getImageId());
                        formImageSubmitRequest.setImageData(imageData);
                        requestList.add(formImageSubmitRequest);
                    }
                }

            }

        }

        return requestList;
    }

    private int imageReqCount = 0;
    public void sendImageData(FormImageSubmitRequest request){
        showProgressDialog("Uploading image "+ request.getImageData().getName());
        new Services().submitFormImage(request, this, getSPreference().getToken());
//        Retrofit retrofit = RestClient.getClient(this);
//        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
//
//        Call<ResponseBody> call = apiInterface.sendImageData(request);
//        call.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
//                if(response.body() == null) {
//                    dismissProgressDialog();
//                    showShortToast("Empty response body");
//                    return;
//                }
//                try {
//                    JSONObject baseResponse = new JSONObject(response.body().string());
//                    if(baseResponse.getBoolean("status")){
//                        for (Page page : formData.getPages()) {
//                            for (JsonObject jsonObject : page.getControls()) {
//                                if(jsonObject.get("id").getAsString().equalsIgnoreCase(request.getImageData().getControlId())) {
//                                    if (Control.Photo.name().equalsIgnoreCase(jsonObject.get("type").getAsString())
//                                            || Control.Signature.name().equalsIgnoreCase(jsonObject.get("type").getAsString())) {
//                                        jsonObject.addProperty("imgpath", baseResponse.getJSONObject("json").getString("imgName"));
//                                    }
//                                }
//                            }
//                        }
//                        dismissProgressDialog();
//                        imageReqCount ++;
//                        if(imageReqCount < requestList.size()){
//                            sendImageData(requestList);
//                        }
//                        else {
//                            showShortToast("Image submitted successfully");
//                            sendFormData();
//                        }
//
//                    }
//                    else {
//                        showShortToast(baseResponse.getString("msg"));
//                        dismissProgressDialog();
//                    }
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    dismissProgressDialog();
//                }
//            }
//
//            @Override
//            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
//                dismissProgressDialog();
//                showShortToast(t.getMessage());
//            }
//        });
    }


    @Override
    public void onImageUploadSuccessful(FormImageSubmitRequest request, ImageDataSubmitResponse response) {
        for (Page page : formData.getPages()) {
            for (JsonObject jsonObject : page.getControls()) {
                if(jsonObject.get("id").getAsString().equalsIgnoreCase(request.getImageData().getControlId())) {
                    if (Control.Photo.name().equalsIgnoreCase(jsonObject.get("type").getAsString())
                            || Control.Signature.name().equalsIgnoreCase(jsonObject.get("type").getAsString())) {
                        jsonObject.addProperty("imageData", "");
                        jsonObject.addProperty("imgpath",response.getImageName());
                    }
                }
            }
        }
        imageReqCount ++;
        if(imageReqCount < imageDataList.size()){
            sendImageData(imageDataList.get(imageReqCount));
        }
        else {
            dismissProgressDialog();
            showShortToast("Image submitted successfully");
            sendFormData();
        }
    }

    @Override
    public void onImageUploadFailed(boolean b, String msg) {
        imageReqCount = 0;
        dismissProgressDialog();
        showShortToast(msg);
    }

    public void sendFormData(){
        showProgressDialog("Sending data");
        for (Page page : formData.getPages()) {
            for (JsonObject jsonObject : page.getControls()) {
                if (Control.Photo.name().equalsIgnoreCase(jsonObject.get("type").getAsString())
                        || Control.Signature.name().equalsIgnoreCase(jsonObject.get("type").getAsString())) {
                    jsonObject.addProperty("imageData", "");
                }
            }
        }

//        String json = ControlUtility.getGson().newBuilder().serializeNulls().create().toJson(formData);

        DataSubmit dataSubmit = new DataSubmit();
        dataSubmit.setFormGroupId(getSPreference().getFormId());
        dataSubmit.setRefId(getSPreference().getReferenceID());
        dataSubmit.setType(getSPreference().getType());
        dataSubmit.setPageDataArray(formData);

        new Services().saveFormData(dataSubmit, this, getSPreference().getToken(),  getSPreference().getReferenceID());

//        ApiInterface apiInterface = RestClient.getClient(this).create(ApiInterface.class);

//        Gson gson = new Gson().newBuilder().excludeFieldsWithoutExposeAnnotation().create();
//
//        String finalDataAsString = gson.toJson(dataSubmit);
//
//        Call<ResponseBody> call = apiInterface.sendData(dataSubmit);
//
//        call.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
//
//                if (response.body() == null) {
//                    onFailure(call, new Throwable("Empty response body"));
//                    return;
//                }
//
//                try {
//                    JSONObject data = new JSONObject(response.body().string());
//
//                    // JSONObject data = baseResponse.getJSONObject("da");
//
//                    if(!data.getBoolean("status")) {
//                        onFailure(call, new Throwable(data.getString("msg")));
//                        return;
//                    }
//
//                    //JSONObject responseData = data.getJSONObject("da");
//
//                    //FormDataSubmitResponse response1 = new Gson().fromJson(responseData.toString(), new TypeToken<FormDataSubmitResponse>(){}.getType());
//
//                    FormDataSubmitResponse response1 = new FormDataSubmitResponse();
//                    onSaveFormDataSuccessful(response1);
//
//                } catch (Exception e) {
//                    onFailure(call, new Throwable(e.getMessage()));
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
//                showShortToast(t.getMessage());
//                dismissProgressDialog();
//            }
//        });
    }

    @Override
    public void onSaveFormDataSuccessful(FormDataSubmitResponse data) {
        dismissProgressDialog();
        showShortToast("Data submission successful");


        getSPreference().setFillingFormId(getSPreference().getFormId()); // saving current form id before preview.
        getSPreference().setFormId(data.getPreviewFormId()); //  overwriting current forId id with preview Form id

        Intent intent = new Intent(FormFillActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(IS_PREVIEW_MODE, true);
        startActivity(intent);
        finish();
    }

    @Override
    public void onSaveFormDataFailed(boolean status, String errorMsg) {
        dismissProgressDialog();
        showShortToast(errorMsg);
    }




    @Override
    public void onDrawerStateChanged(int newState) { }
    @Override
    public void onPageSelected(int position) {}
    @Override
    public void onPageScrollStateChanged(int state) {}

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0)
            super.onBackPressed();
        else {
            new AlertDialog.Builder(this)
                    .setMessage("Do you want to exit form ?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                            dialog.dismiss();
                        }
                    })
                    .setNeutralButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();

        }
    }

    @Override
    protected void onDestroy() {
        if (!mDisposable.isDisposed())
            mDisposable.dispose();
        super.onDestroy();
    }

}

