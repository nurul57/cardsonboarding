package android.form.avss.prepaidcard.ui.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CardDetailViewModel implements Serializable {
    @SerializedName("iec")
    private String isExistingCustomer;
    @SerializedName("apd")
    private String appliedDate;
    @SerializedName("cno")
    private String cardNumber;
    @SerializedName("ityp")
    private String instaType;
    @SerializedName("comp")
    private String company;
    @SerializedName("comp_nm")
    private String companyName;
    @SerializedName("ca_te")
    private String cardType;
    @SerializedName("pt_ce")
    private String productCode;
    @SerializedName("cd_rf")
    private String cardRefNumber;


    public String getIsExistingCustomer() {
        return isExistingCustomer;
    }

    public void setIsExistingCustomer(String isExistingCustomer) {
        this.isExistingCustomer = isExistingCustomer;
    }

    public String getAppliedDate() {
        return appliedDate;
    }

    public void setAppliedDate(String appliedDate) {
        this.appliedDate = appliedDate;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getInstaType() {
        return instaType;
    }

    public void setInstaType(String instaType) {
        this.instaType = instaType;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getCardRefNumber() {
        return cardRefNumber;
    }

    public void setCardRefNumber(String cardRefNumber) {
        this.cardRefNumber = cardRefNumber;
    }
}
