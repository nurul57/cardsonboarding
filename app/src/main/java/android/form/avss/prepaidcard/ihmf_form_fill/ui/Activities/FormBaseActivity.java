package android.form.avss.prepaidcard.ihmf_form_fill.ui.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.form.avss.prepaidcard.ihmf_form_fill.interfaces.DialogButtonClickListener;
import android.form.avss.prepaidcard.ihmf_form_fill.util.MySharePreference;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import java.io.File;


@SuppressLint("Registered")
public class FormBaseActivity extends AppCompatActivity {
   
//    private AlertDialog mDialog;
    private ProgressDialog mProgressDialog;
    private MySharePreference mSPreference;
    private File formDatafile;
    private File imageDatafile;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    /**
     * @return current formId data directory in internal storage
     */
    @SuppressWarnings("all")
    public File getFormDataFileDir(){
        if(formDatafile == null) {
            formDatafile = getDir(getSPreference().getFormId() + "_FORM_DATA", MODE_PRIVATE);
            if(!formDatafile.exists()) formDatafile.mkdirs();
        }
        return formDatafile;
    }



    /**
     * @return CONTROL_IMAGES  directory inside current formId data directory
     */
    @SuppressWarnings("all")
    public File getControlImagesFilesDirectory(){
        if(imageDatafile == null) {
            imageDatafile = new File(getFormDataFileDir(), "CONTROL_IMAGES");
            if(!imageDatafile.exists()) imageDatafile.mkdirs();
        }
        return imageDatafile;
    }


    /**
     * @return FORM_IMAGES  directory inside current formId data directory
     */
    @SuppressWarnings("all")
    public File getFormImagesFilesDirectory(){
        if(imageDatafile == null) {
            imageDatafile = new File(getFormDataFileDir(), "FORM_IMAGES");
            if(!imageDatafile.exists()) imageDatafile.mkdirs();
        }
        return imageDatafile;
    }



    public String getImagePath(String controlId){
        File file = new File(getControlImagesFilesDirectory(),getSPreference().getFillingFormId() + "_" + controlId + ".png");
        if(!file.exists()) return  null;
        return file.getAbsolutePath();
    }


    public void showShortToast(String mssg) {
        Toast.makeText(this, mssg, Toast.LENGTH_SHORT).show();
    }


    public void showProgressDialog(String title){
        if(mProgressDialog == null){
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Please wait....");
            mProgressDialog.setCancelable(false);
        }
        mProgressDialog.setTitle(title);
        if(!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }

    }


    public void dismissProgressDialog(){
        if(mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }

    public MySharePreference getSPreference(){
        if(mSPreference == null){
            mSPreference = new MySharePreference(FormBaseActivity.this);
        }
        return mSPreference;
    }


    private AlertDialog alertDialog;

    protected void showAlertDialog(String msg, final DialogButtonClickListener listener) {
        if(alertDialog != null){
            alertDialog.setTitle("Alert");
            alertDialog.setMessage(msg);
            alertDialog.show();
            return;
        }

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Alert");
        alertDialogBuilder.setMessage(msg);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setIcon(android.R.drawable.ic_dialog_alert);

        alertDialogBuilder.setPositiveButton("PROCEED", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if(listener !=null) listener.onClickedOK();
            }
        });

        alertDialogBuilder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if(listener != null) listener.onClickedCancel();
            }
        });

        alertDialog = alertDialogBuilder.show();
    }

    protected void hideAlertDialog() {
        if (alertDialog != null && alertDialog.isShowing())
            alertDialog.dismiss();
    }








}
