package android.form.avss.prepaidcard.web_services.responses;

import com.google.gson.annotations.SerializedName;

public class OVDDeemedOVDMasterResponse {
    @SerializedName("da")
    public String data;

    public static class OVDDeemedOVDMaster extends BaseResponseList<OVDDeemedOVDMasterResponse.OVDDeemedOVDMaster> {
        @SerializedName("name")
        private String name;
        @SerializedName("code")
        private String code;
        @SerializedName("idProof")
        private String idProof;
        @SerializedName("addProof")
        private String addProof;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getIdProof() {
            return idProof;
        }

        public void setIdProof(String idProof) {
            this.idProof = idProof;
        }

        public String getAddProof() {
            return addProof;
        }

        public void setAddProof(String addProof) {
            this.addProof = addProof;
        }
    }
}
