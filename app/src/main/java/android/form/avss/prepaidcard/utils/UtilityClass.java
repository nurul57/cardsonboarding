package android.form.avss.prepaidcard.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.form.avss.prepaidcard.R;
import android.form.avss.prepaidcard.ui.activities.LoginActivity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.TypedValue;
import android.view.ViewGroup;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class UtilityClass {

    public static final String PACKAGE_NAME = "com.avsstech.nps";

    /**
     * Created by Gautam Anand
     * remove the invalid chars after aes decryption
     * @param source
     * @return
     */
    public static byte[] removeTrailingNulls(byte[] source) {
        int i = source.length;
        while (source[i - 1] == 0x00) {
            i--;
        }
        byte[] result = new byte[i];
        System.arraycopy(source, 0, result, 0, i);
        return result;
    }

    /**
     * Cretaed by Gautam Anand
     * get the byte array from the input stream
     * @param inputStream
     * @return
     * @throws IOException
     */
    public static byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream _byteBuffer = new ByteArrayOutputStream();
        int _bufferSize = 1024;
        byte[] _buffer = new byte[_bufferSize];

        int _len = 0;
        while((_len = inputStream.read(_buffer)) != -1) {
            _byteBuffer.write(_buffer, 0, _len);
        }
        return _byteBuffer.toByteArray();
    }

    public static void appendLog(String text)
    {
        File logFile = new File(FileUtils.getRoot() + "/logNps.txt");
        if (!logFile.exists())
        {
            try
            {
                logFile.createNewFile();
            }
            catch (IOException e)
            {
                // TODO Auto-generated catch block
//                e.printStackTrace();
            }
        }
        try
        {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();
            buf.close();
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
//            e.printStackTrace();
        }
    }

    public static Bitmap decodeUri(Context c, Uri uri,  int requiredWidth,  int requiredHeight)  {
        try {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o);

            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;

            requiredWidth = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,requiredWidth,c.getResources().getDisplayMetrics());
            requiredHeight = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,requiredHeight,c.getResources().getDisplayMetrics());
            while (true) {
                if (width_tmp / 2 < requiredWidth || height_tmp / 2 < requiredHeight)
                    break;
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            o.inSampleSize = scale;
            o.inJustDecodeBounds = false;
            return BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o);
        } catch (IOException e){
            return null;
        }
    }

    public static String getByteArrayOfImage(Context ctx, String path) throws IOException {
        InputStream _iStream = null;
        byte[] _byteArray = new byte[0];
        try {

            File _file = new File(path);
            _iStream = ctx.getContentResolver().openInputStream(Uri.fromFile(_file));

            if (_iStream != null) {
                _byteArray = UtilityClass.getBytes(_iStream);
            }

        } catch (FileNotFoundException e) {
            e.getMessage();
        }
        return Base64.encodeToString(_byteArray, Base64.DEFAULT);
    }

    public static boolean verifyInstaller(Context context, String myPackageName) {
        //Renamed?
        if (context.getPackageName().compareTo(myPackageName) != 0) {
            return true; // BOOM!
        }
        //Relocated?
        String installer = context.getPackageManager().getInstallerPackageName(myPackageName);

        if (installer == null){
            return true; // BOOM!
        }

//        if (installer.compareTo(google) != 0 && installer.compareTo(amazon) != 0){
//            return true; // BOOM!
//        }
        return false;
    }

    public static String getName(String firstName, String middleName, String lastName) {
        if (TextUtils.isEmpty(firstName) && TextUtils.isEmpty(lastName)) {
            return "";
        } else if (TextUtils.isEmpty(middleName)) {
            return firstName + " " + lastName;
        } else {
            return firstName + " " + middleName + " " + lastName;
        }
    }


    public static Bitmap getBitmap(ViewGroup layout) {
        if (layout == null) return null;
        Bitmap bmp;
        layout.setDrawingCacheEnabled(true);
        layout.buildDrawingCache();
        bmp = Bitmap.createBitmap(layout.getDrawingCache());
        layout.setDrawingCacheEnabled(false);
        return bmp;
    }


    //todo: create calender object for one birth day
    public static int calculateAge(Date date) {
        Calendar birthday = Calendar.getInstance();
        birthday.setTimeInMillis(date.getTime());
        Calendar now = Calendar.getInstance();

        if (birthday.after(now)) {
            throw new IllegalArgumentException("Can't be born in the future");
        }

        int year1 = now.get(Calendar.YEAR);
        int year2 = birthday.get(Calendar.YEAR);
        int age = year1 - year2;
        int month1 = now.get(Calendar.MONTH);
        int month2 = birthday.get(Calendar.MONTH);
        if (year1 != year2) {
            if (month2 > month1) {
                age--;
            } else if (month1 == month2) {
                int day1 = now.get(Calendar.DAY_OF_MONTH);
                int day2 = birthday.get(Calendar.DAY_OF_MONTH);
                if (day2 < day1) {
                    age++;
                }
            }
        }
        return age;
    }

    public static int calculateAge(String dob) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
            Calendar birthday = Calendar.getInstance();
            birthday.setTimeInMillis(dateFormat.parse(dob).getTime());

            Calendar now = Calendar.getInstance();

            if (birthday.after(now)) {
                throw new IllegalArgumentException("Can't be born in the future");
            }

            int year1 = now.get(Calendar.YEAR);
            int year2 = birthday.get(Calendar.YEAR);
            int age = year1 - year2;
            int month1 = now.get(Calendar.MONTH);
            int month2 = birthday.get(Calendar.MONTH);
            if (year1 != year2) {
                if (month2 > month1) {
                    age--;
                } else if (month1 == month2) {
                    int day1 = now.get(Calendar.DAY_OF_MONTH);
                    int day2 = birthday.get(Calendar.DAY_OF_MONTH);
                    if (day2 < day1) {
                        age++;
                    }
                }
            }
            return age;
        } catch (ParseException e) {
//            e.printStackTrace();
        }
        return -1;
    }

    @SuppressLint({"MissingPermission", "HardwareIds"})
    public static String  getImeiNumber(Context context) {
        String imeiNumber = "is not found";
        if ( context == null) return imeiNumber;

        try {
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (tm != null) {
                if (Build.VERSION.SDK_INT > 27) {
                    return tm.getDeviceId();
                } else {
                    return tm.getDeviceId();
                }
            }
        } catch (Exception ignore) {}
        return imeiNumber;
    }


    public static String generateAppHash(Context ctx){
        String _hash = null;
        try {
            PackageInfo info = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), PackageManager.GET_SIGNATURES);

            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA1");
                md.update(signature.toByteArray());
                _hash = Base64.encodeToString(md.digest(), Base64.DEFAULT);
            }

        } catch (Throwable e){_hash = "";}

        return _hash;
    }


    public static boolean checkDebuggable(Context context){
        return (context.getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0;

    }

    public static String getImageName(String imgName, int imgCounter) {
        return File.separator + imgName + String.valueOf(imgCounter) + ".jpg";
    }

    public static String docImageCaptureInfo(int min, int max) {
        return "Min : " + min + "\n" + "Max : " + max;
    }

    public static String docImageCaptureInfo(String min, String max) {
        return "Min : " + min + "\n" + "Max : " + max;
    }

    public static String displayImageCount(int captureCount, int max) {
        return String.valueOf(captureCount) + "/" + max;
    }


    public static AlertDialog.Builder dialog(Context ctx, String msg){
        return new AlertDialog.Builder(ctx)
                .setCancelable(false)
                .setMessage(msg);
    }

    public static String convertBitmapIntoString (Bitmap bm) {
        if ( bm != null ) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            return Base64.encodeToString(byteArray, Base64.DEFAULT);
        }
        return "no bitmap found";
    }

    public static Bitmap convertStringIntoBitmap (String imageBase64String) {
        if (TextUtils.isEmpty(imageBase64String)) return null;

        Bitmap decodedByte;
        byte[] decodedString = Base64.decode(imageBase64String, Base64.DEFAULT);
        decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }

    public static void sessionOut(final Context context) {
        if (context == null) return;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.app_name);
        builder.setMessage(R.string.session_expire);
        builder.setCancelable(false);
        builder.setPositiveButton(context.getString(R.string.login), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent _intent = new Intent(context, LoginActivity.class);
                context.startActivity(_intent);
                ((Activity)context).finish();
            }
        });
        builder.create().show();
    }


    public static void longToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    public static void longToast(Context context, @StringRes int id) {
        Toast.makeText(context, context.getResources().getString(id), Toast.LENGTH_LONG).show();
    }

    public static void shortToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static void shortToast(Context context, @StringRes int id) {
        Toast.makeText(context, context.getResources().getString(id), Toast.LENGTH_SHORT).show();
    }

}

