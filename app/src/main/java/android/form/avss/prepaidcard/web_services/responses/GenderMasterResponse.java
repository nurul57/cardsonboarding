package android.form.avss.prepaidcard.web_services.responses;

import com.google.gson.annotations.SerializedName;

public class GenderMasterResponse {
    @SerializedName("da")
    public String data;

    public static class GenderMaster  extends BaseResponseList<GenderMaster> {
        @SerializedName("id")
        public String id;
        @SerializedName("code")
        public String code;
        @SerializedName("name")
        public String name;
        @SerializedName("is_enable")
        public String is_enable;
    }
}
