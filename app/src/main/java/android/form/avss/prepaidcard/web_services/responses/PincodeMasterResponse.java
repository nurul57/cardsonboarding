package android.form.avss.prepaidcard.web_services.responses;

import com.google.gson.annotations.SerializedName;

public class PincodeMasterResponse {
    @SerializedName("da")
    public String data;

    public static class PincodeMaster  extends BaseResponseList<PincodeMaster> {
        @SerializedName("id")
        public String id;
        @SerializedName("pincode")
        public String pincode;
        @SerializedName("dist")
        public String district;
        @SerializedName("statename")
        public String state;
        @SerializedName("state")
        public String stateCode;
    }
}
