package android.form.avss.prepaidcard.ui.custom_classes;

import android.content.Context;
import android.form.avss.prepaidcard.R;
import android.util.AttributeSet;

public class CustomCheckBox extends android.support.v7.widget.AppCompatCheckBox {
    public CustomCheckBox(Context context) {
        super(context);
        this.setTextColor(getResources().getColor(R.color.dark_grey));
        this.setTextAppearance(context, R.style.MyCheckBox);
    }

    public CustomCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTextColor(getResources().getColor(R.color.dark_grey));
        this.setTextAppearance(context, R.style.MyCheckBox);

    }

    public CustomCheckBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.setTextColor(getResources().getColor(R.color.dark_grey));
        this.setTextAppearance(context, R.style.MyCheckBox);
    }

    public void Checked(){
        this.setChecked(true);
    }

    public void UnChecked() {
        this.setChecked(false);
    }

    @Override
    public String toString() {
        return this.getText().toString().trim();
    }
}
