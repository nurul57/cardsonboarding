package android.form.avss.prepaidcard.ihmf_form_fill.interfaces;

public interface FocusListener {
    void addGlobalLayoutListener(float viewBottom);
    void removeGlobalLayoutListener();

}
