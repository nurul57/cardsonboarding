package android.form.avss.prepaidcard.ihmf_form_fill.pojo.Requests;

import android.form.avss.prepaidcard.ihmf_form_fill.TempPackage.ImageData;

import com.google.gson.annotations.SerializedName;

public class FormImageSubmitRequest {

    @SerializedName("refId")
    private String referenceId;

    @SerializedName("formGroupId")
    private String formId;

    @SerializedName("type")
    private String type;

    @SerializedName("groupId")
    private String groupId;

    @SerializedName("imageData")
    private ImageData imageData;


    @SerializedName("fillingId")
    private String fillingId;

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupId(){
        return this.groupId;
    }

    public void setImageData(ImageData imageData) {
        this.imageData = imageData;
    }

    public void setFillingId(String fillingId) {
        this.fillingId = fillingId;
    }

    public ImageData getImageData() {
        return imageData;
    }
}
