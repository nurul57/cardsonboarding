package android.form.avss.prepaidcard.ui.adapters;

import android.content.Context;
import android.form.avss.prepaidcard.R;
import android.form.avss.prepaidcard.ui.activities.DocumentActivity;
import android.form.avss.prepaidcard.ui.custom_classes.DocViewHolder;
import android.form.avss.prepaidcard.ui.custom_classes.interfaces.RecyclerViewClickListener;
import android.form.avss.prepaidcard.web_services.pojo.ImageOption;
import android.form.avss.prepaidcard.web_services.responses.DocumentDataResponse;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.List;

public class DocumentDrawerAdapter extends RecyclerView.Adapter<DocumentDrawerAdapter.MyViewHolder> {

    private RecyclerViewClickListener mListener;
    public List<ImageOption> documentList;
    public ArrayList<ConstraintLayout> clItemList = new ArrayList<>();
    public ArrayList<MyViewHolder> holderList = new ArrayList<>();
    private Context context;

    public DocumentDrawerAdapter(List<ImageOption> listOfImages, RecyclerViewClickListener mListener) {
        this.mListener = mListener;
        documentList = listOfImages;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.document_list, parent, false);
        return new MyViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        if (!documentList.get(position).isDocNameEmpty()) {
            holder.tvDocName.setText(documentList.get(position).getDocName().replaceAll("_", " "));
        }

        if (position == 0) {
            holder.item.setBackgroundColor(context.getResources().getColor(R.color.dark_red));
        }

        if (!clItemList.contains(holder.item)) {
            clItemList.add(position, holder.item);
        }

        if (!holderList.contains(holder)) {
            holderList.add(position, holder);
        }

        if (documentList.get(position).isSelectedItem()) {
            holder.item.setBackgroundColor(context.getResources().getColor(R.color.dark_red));
        } else {
            holder.item.setBackgroundColor(context.getResources().getColor(R.color.drawerColor));
        }

        if (documentList.get(position).isDataCaptured()) {
            holder.ivDocStatus.setBackground(null);
            holder.ivDocStatus.setBackground(context.getDrawable(R.drawable.done));
        } else {
            holder.ivDocStatus.setBackground(null);
            holder.ivDocStatus.setBackground(context.getDrawable(R.drawable.alert));
        }
    }

    @Override
    public int getItemCount() {
        return documentList == null || documentList.isEmpty() ? 0 : documentList.size();
    }

    public static class MyViewHolder extends DocViewHolder {
        private ConstraintLayout item;
        private TextView tvDocName;
        private ImageView ivDocStatus;

        MyViewHolder(View v, RecyclerViewClickListener mListener) {
            super(v, mListener);
            tvDocName = (TextView) v.findViewById(R.id.tvDocName);
            ivDocStatus = (ImageView) v.findViewById(R.id.ivDocStatus);
            item = (ConstraintLayout) v.findViewById(R.id.clItem);
        }

        public ImageView getIvDocStatus() {
            return ivDocStatus;
        }
    }
}

