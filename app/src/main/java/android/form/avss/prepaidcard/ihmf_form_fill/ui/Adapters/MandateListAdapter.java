package android.form.avss.prepaidcard.ihmf_form_fill.ui.Adapters;

import android.content.Context;
import android.form.avss.prepaidcard.R;
import android.form.avss.prepaidcard.ihmf_form_fill.interfaces.iHMFView;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Used to hold {@link iHMFView} reference which is most case is {@link View } type.
 * <p>
 * Must be clear list at {@link Fragment#onDestroyView()} to avoid memory leak if we are removing views in @{link Fragment#onDestroyView()}
 */
public class MandateListAdapter extends BaseAdapter {

    private List<iHMFView> controlList;
    private OnListItemClick itemClick;

    public MandateListAdapter(OnListItemClick itemClick) {
        controlList = new ArrayList<>();
        this.itemClick = itemClick;
    }

    public void add(iHMFView control) {
        controlList.add(control);
    }

    /**
     * This is used to remove @{link iHMFView} reference.
     * <p>It is usefult to avoid memory leak or update with new data.</p>
     */
    public void clear() {
        int size = controlList.size();
        controlList.clear();
        if(size > 0)
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return controlList.size();
    }

    @Override
    public Object getItem(int i) {
        return controlList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        ViewHolder mViewHolder = null;

        if (view == null) {

            mViewHolder = new ViewHolder();

            LayoutInflater vi = (LayoutInflater) viewGroup.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            view = vi.inflate(R.layout.layout_mandate_row, viewGroup, false);
            mViewHolder.cTv_name = (CheckedTextView) view.findViewById(R.id.text1); // title

            view.setTag(mViewHolder);

            mViewHolder.cTv_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!controlList.get(i).getView().isAttachedToWindow()) return;
                    if (itemClick != null)
                        itemClick.onMandateItemClick(view, controlList.get(i));
                }
            });
        } else {
            mViewHolder = (ViewHolder) view.getTag();
        }


        mViewHolder.cTv_name.setText(controlList.get(i).getControl().getType());
        mViewHolder.cTv_name.setChecked(controlList.get(i).isValid());

        return view;
    }


    public interface OnListItemClick {
        void onMandateItemClick(View view, iHMFView control);
    }

    private static class ViewHolder {
        private CheckedTextView cTv_name;

    }

}
