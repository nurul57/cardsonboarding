package android.form.avss.prepaidcard.ihmf_form_fill.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;

import java.io.ByteArrayOutputStream;
import java.io.File;

public class ImageUtil {

    private static final String EMPTY = "";

    public static Bitmap fixOrientation(Bitmap bm, int degree) {
//        if (bm.getWidth() > bm.getHeight()) {
        Matrix matrix = new Matrix();

        matrix.postRotate(degree);

        Bitmap oldBitmap = bm;

        bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
//        }

        oldBitmap.recycle();
        return bm;
    }


    public static int getCameraPhotoOrientation(Uri imageUri, String imagePath) {
        int rotate = 0;
        try {
//            context.getContentResolver().notifyChange(imageUri, null);
            File imageFile = new File(imagePath);

            ExifInterface exif = new ExifInterface(imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (Exception e) {
//            e.printStackTrace();
        }
        return rotate;
    }

    public static Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);

        bm.recycle();
        return resizedBitmap;
    }

    public static String encodeImage(Bitmap bitmap, int quality) {

        if (bitmap == null) return "";

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, quality, stream);
        byte[] image = stream.toByteArray();

        String encodedImage = android.util.Base64.encodeToString(image, android.util.Base64.DEFAULT);

        return encodedImage == null ? EMPTY : encodedImage;
    }


    public static Bitmap decodeImage(String image) {

        byte[] decodeImage = android.util.Base64.decode(image, android.util.Base64.DEFAULT);


        if (decodeImage == null || decodeImage.length == 0) return null;

        return BitmapFactory.decodeByteArray(decodeImage, 0, decodeImage.length);
    }

    public static int inSampleSize(BitmapFactory.Options options, int newWidth, int newHeight) {

        int width = options.outWidth;
        int height = options.outHeight;

        int sample = 2;

        while (true) {

            if (width / sample > newWidth && height / sample > newHeight)
                sample += 2;
            else
                break;
        }

        return sample;
    }
}
