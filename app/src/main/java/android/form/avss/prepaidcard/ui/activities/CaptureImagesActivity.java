package android.form.avss.prepaidcard.ui.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.form.avss.prepaidcard.R;
import android.form.avss.prepaidcard.utils.FileUtils;
import android.form.avss.prepaidcard.utils.PermissionUtils;
import android.form.avss.prepaidcard.utils.UtilityClass;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CaptureImagesActivity extends BaseActivity {
    String _imageName, fName;
    private boolean mSurfaceTextureAvailable = false;
    private boolean mPermissionsGranted = false;


    private String[] cameraPermission = {
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private ArrayList<String> listOfImagesPath = new ArrayList<>();

    // camera id to use
    private String cameraId;

    // button to click the picture
    private Button takePictureButton;
    private TextView tvDocName, tvCountImage;

    private Button btnDone;
    private int imageCounter, imageMaxLimit;

    // texture view for custom camera view
    private TextureView textureView;

    // camera device to use
    protected CameraDevice cameraDevice;

    // camera capture session variable
    protected CameraCaptureSession cameraCaptureSessions;

    // camera request builder
    protected CaptureRequest.Builder captureRequestBuilder;

    // the size for captured image
    private Size imageDimension;

    // image reader to reade image bytes
    private ImageReader imageReader;

    // background handler for background tasks
    private Handler mBackgroundHandler;

    // background thread
    private HandlerThread mBackgroundThread;

    // file for image
    File file;

    // file path to store image
    String imageFilePath;

    private String imageName;

    // sparse array for captured image orientation
    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();

    private ProgressDialog progressDialog;

    // static block to initialize the sparse array
    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 90);
        ORIENTATIONS.append(Surface.ROTATION_90, 0);
        ORIENTATIONS.append(Surface.ROTATION_180, 270);
        ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    ImageReader reader;
    private int imageMinLimit = 0;
    private String referenceId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capture_images);

        if (getIntent() == null) {
            finish();
        }

        _imageName = getIntent().getStringExtra(IMAGE_NAME);
        imageName = getIntent().getStringExtra(IMAGE_NAME);
        referenceId = getIntent().getStringExtra(REFERENCE_ID);
        imageCounter = getIntent().getIntExtra(IMAGE_COUNTER, 0);
        imageMinLimit = getIntent().getIntExtra(MIN_LIMIT, 0);
        imageMaxLimit = getIntent().getIntExtra(MAX_LIMIT, 0);
        initializeCamera();
    }

    private void initializeCamera() {
        tvDocName = (TextView) findViewById(R.id.tvDocName);
        tvCountImage = (TextView) findViewById(R.id.tvCountImage);
        textureView = (TextureView) findViewById(R.id.texture);
        assert textureView != null;
        textureView.setSurfaceTextureListener(textureListener);
        takePictureButton = (Button) findViewById(R.id.btnTakePicture);
        btnDone = (Button) findViewById(R.id.btn_Done);

        assert takePictureButton != null;
        takePictureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvCountImage.setText(displayImageCount());
                v.setVisibility(imageCounter == imageMaxLimit ? View.GONE : View.VISIBLE);
                btnDone.setVisibility(imageCounter < imageMinLimit ? View.GONE : View.VISIBLE);
                takePicture();
            }
        });

        takePictureButton.setVisibility(imageCounter >= imageMaxLimit ? View.GONE : View.VISIBLE);
        btnDone.setVisibility(imageCounter < imageMinLimit ? View.GONE : View.VISIBLE);

        progressDialog = new ProgressDialog(CaptureImagesActivity.this);
        progressDialog.setTitle("Uploading...");
        progressDialog.setMessage("Please Wait...");
        progressDialog.setIndeterminate(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setCancelable(false);

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvCountImage.setText(displayImageCount());
                Intent _intent = new Intent();
                _intent.putStringArrayListExtra(IMAGE_LIST, listOfImagesPath);
                _intent.putExtra(IMAGE_COUNTER, imageCounter);
                setResult(RESULT_OK, _intent);
                finish();
            }
        });

        tvDocName.setText(imageName);
        tvCountImage.setText(displayImageCount());

    }

    private String displayImageCount() {
        return String.valueOf(imageCounter) + "/" +imageMaxLimit;
    }

    /**
     * Created by Gautam Anand
     * texture listener to listen to texture changes
     */
    TextureView.SurfaceTextureListener textureListener = new TextureView.SurfaceTextureListener() {
        /**
         * @param surface
         * @param width
         * @param height
         */
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            mSurfaceTextureAvailable = true;
            if (PermissionUtils.isOSVersionMorHigher()) {
                PermissionUtils.permissionGranted(CaptureImagesActivity.this, cameraPermission, CAMERA_PERMISSION);
                if (PermissionUtils.isGranted(CaptureImagesActivity.this, cameraPermission)) {
                    setUpCamera();
                } else {
                    mPermissionsGranted = false;
                    toast("camera permission is not granted");
                }
            } else {
                setUpCamera();
            }
        }

        /**
         *
         * @param surface
         * @param width
         * @param height
         */
        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
            // Transform you image captured size according to the surface width and height
        }

        /**
         * @param surface
         * @return
         */
        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            return false;
        }

        /**
         *
         * @param surface
         */
        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) { }
    };

    private void setUpCamera() {
        mPermissionsGranted = true;
        //Execute code after 500 millisecond
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mSurfaceTextureAvailable && mPermissionsGranted) {
                    openCamera();
                }
            }
        }, 500);
    }

    /**
     * Created by Gautam Anand
     * state callback method for the camera callbacks
     */
    private final CameraDevice.StateCallback stateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(CameraDevice camera) {
            cameraDevice = camera;

            ImageReader.OnImageAvailableListener readerListener = new ImageReader.OnImageAvailableListener() {
                @Override
                public void onImageAvailable(ImageReader reader) {
                    try (Image image = reader.acquireLatestImage()) {
                        ByteBuffer buffer = image.getPlanes()[0].getBuffer();
                        byte[] bytes = new byte[buffer.capacity()];
                        buffer.get(bytes);
                        save(bytes);
                    } catch (IOException e) {
//                        e.printStackTrace();
                    } catch (Exception ignore) {}
                }

                private void save(byte[] bytes) throws IOException {
                    try (OutputStream output = new FileOutputStream(file)) {
                        output.write(bytes);
                        performCrop();
                    }
                }
            };

            CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
            CameraCharacteristics characteristics = null;
            try {
                assert manager != null;
                characteristics = manager.getCameraCharacteristics(cameraDevice.getId());
            } catch (CameraAccessException ignored ) {}
            Size[] jpegSizes = null;
            if (characteristics != null) {
                jpegSizes = Objects.requireNonNull(characteristics.get(CameraCharacteristics
                        .SCALER_STREAM_CONFIGURATION_MAP)).getOutputSizes(ImageFormat.JPEG);
            }
            int width = 640;
            int height = 480;
            if (jpegSizes != null && 0 < jpegSizes.length) {
                width = jpegSizes[0].getWidth();
                height = jpegSizes[0].getHeight();
            }

            reader = ImageReader.newInstance(width, height, ImageFormat.JPEG, 1);
            List<Surface> outputSurfaces = new ArrayList<>(2);
            outputSurfaces.add(reader.getSurface());
            if (textureView == null ) return;
            SurfaceTexture sv = textureView.getSurfaceTexture();
            if (sv == null ) return;
            outputSurfaces.add(new Surface(sv));
            reader.setOnImageAvailableListener(readerListener, mBackgroundHandler);

            createCameraPreview();
        }
        @Override
        public void onDisconnected(@NonNull CameraDevice camera) {
            cameraDevice.close();
        }

        @Override
        public void onError(@NonNull CameraDevice camera, int error) {
            cameraDevice.close();
            cameraDevice = null;
        }
    };

    /**
     * Created by Gautam Anand
     * starts the background thread for camera background
     */
    protected void startBackgroundThread() {
        mBackgroundThread = new HandlerThread("Camera Background");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
    }

    /**
     * Created by Gautam Anand
     * stops the background thread for camera safely
     */
    protected void stopBackgroundThread() {
        mBackgroundThread.quitSafely();
        try {
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            //e.printStackTrace();
        }
    }

    /**
     * Created by Gautam Anand
     * method used to take picture in the custom class
     */
    protected void  takePicture() {
        if ( null == cameraDevice ) {
            return;
        }

        try {
            File myDir = new File(FileUtils.getRoot() + File.separator + referenceId);
            if (!myDir.exists()) {
                myDir.mkdirs();
            }
            String subfolder = myDir.getAbsolutePath();

            if (!TextUtils.isEmpty(imageName)) {
                fName = UtilityClass.getImageName(imageName, imageCounter);
                file = new File(myDir, fName);
                if (file.exists()) file.delete();
                imageFilePath = subfolder + fName;
            }
            file = new File(imageFilePath);

            captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
            captureRequestBuilder.addTarget(reader.getSurface());

            //Orientation
            int rotation = getWindowManager().getDefaultDisplay().getRotation();
            captureRequestBuilder.set(CaptureRequest.JPEG_ORIENTATION, ORIENTATIONS.get(rotation));

            final CameraCaptureSession.CaptureCallback captureListener = new CameraCaptureSession.CaptureCallback() {
                @Override
                public void onCaptureCompleted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull TotalCaptureResult result) {
                    super.onCaptureCompleted(session, request, result);
                    toast("Saved:" + file);

                    createCameraPreview();
                }
            };

            cameraCaptureSessions.capture(captureRequestBuilder.build(), captureListener, mBackgroundHandler);
        } catch (Exception e) {
            toast("Please take picture again");
        }
    }

    /**
     * Created by Gautam Anand
     * perform crop on the captured image
     */
    private void performCrop(){
        try {
            Uri _croppedImage = Uri.fromFile(file);
            CropImage.activity(_croppedImage).setGuidelines(CropImageView.Guidelines.ON).start(this);
        } catch (ActivityNotFoundException anfe) {
            toast("Whoops - your device doesn't support the crop action!");
        }
    }

    /**
     * Created by Gautam Anand
     * method to process the data returned from an activity started with startActivityForResult
     * @param requestCode
     * @param resultCode
     * @param data
     */
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        if ( resultCode == RESULT_OK ) {
            if ( requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE ){

                @SuppressLint( "HandlerLeak" )
                final Handler handler = new Handler() {
                    @Override
                    public void handleMessage(Message msg) {

                        progressDialog.dismiss();
                        if (imageMaxLimit > -1 && imageCounter >= imageMaxLimit) {
                            takePictureButton.setVisibility(View.GONE);
                            setResult();
                        }

                        if (imageCounter > 0) {
                            btnDone.setVisibility(imageCounter < imageMinLimit ? View.GONE : View.VISIBLE);
                            takePictureButton.setText(R.string.take_another_picture);
                        }
                    }
                };

                progressDialog = new ProgressDialog(CaptureImagesActivity.this);
                progressDialog.setTitle("Saving cropped image");
                progressDialog.setMessage("Please Wait...");
                progressDialog.setIndeterminate(false);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                progressDialog.setCancelable(false);
                progressDialog.show();
                new Thread()
                {
                    public void run()
                    {
                        CropImage.ActivityResult _result = CropImage.getActivityResult(data);
                        Uri _resultUri = _result.getUri();
                        Bitmap _thePic = null;
                        try {
                            _thePic = decodeUri(CaptureImagesActivity.this, _resultUri, 480, 640);
                        } catch (IOException e) {
                            //e.printStackTrace();
                        }

                        File myDir = new File(FileUtils.getRoot() + File.separator + referenceId);
                        if (!myDir.exists()) {
                            myDir.mkdirs();
                        }

                        if (!TextUtils.isEmpty(imageName)) {
                            fName = UtilityClass.getImageName(imageName, imageCounter);
                        }

                        File _file = new File(myDir, fName);
                        if (_file.exists()) _file.delete();
                        try {
                            FileOutputStream out = new FileOutputStream(_file);
                            assert _thePic != null;
                            _thePic.compress(Bitmap.CompressFormat.JPEG, 40, out);
                            out.flush();
                            out.close();
                        } catch (Exception e) {
                            //e.printStackTrace();
                        }
                        listOfImagesPath.add(imageFilePath);

                        String _path = _resultUri.getPath();
                        File _fileToDelete = null;
                        if (_path != null) {
                            _fileToDelete = new File(_path);
                            _fileToDelete.delete();
                        }

                        imageCounter++;
                        handler.sendEmptyMessage(0);
                    }

                }.start();
            }
        }
    }

    private Bitmap decodeUri(Context c, Uri uri, final int requiredWidth,
                             final int requiredHeight) throws  FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o);

        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;

        while (width_tmp / 2 >= requiredWidth && height_tmp / 2 >= requiredHeight) {
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o2);
    }

    /**
     * Created by Gautam Anand
     * creates the custom camera preview for image capture
     */
    protected void createCameraPreview() {
        try {
            SurfaceTexture texture = textureView.getSurfaceTexture();
            assert texture != null;
            texture.setDefaultBufferSize(imageDimension.getWidth(), imageDimension.getHeight());
            Surface surface = new Surface(texture);
            captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            captureRequestBuilder.addTarget(surface);
            List<Surface> outputSurfaces = new ArrayList<>(2);
            outputSurfaces.add(reader.getSurface());
            outputSurfaces.add(surface);
            cameraDevice.createCaptureSession(outputSurfaces, new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                    //The camera is already closed
                    if (null == cameraDevice) {
                        return;
                    }
                    // When the session is ready, we start displaying the preview.
                    cameraCaptureSessions = cameraCaptureSession;
                    updatePreview();
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
                    toast("Configuration change");
                }
            }, null);
        } catch (CameraAccessException e) {
//            e.printStackTrace();
        }
    }

    /**
     * Created by Gautam Anand
     * open camera for capturing image
     */
    private void openCamera() {
        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            assert manager != null;
            cameraId = manager.getCameraIdList()[0];
            CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);
            StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            assert map != null;
            imageDimension = map.getOutputSizes(SurfaceTexture.class)[0];
            // Add permission for camera and let user grant the permission
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(CaptureImagesActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, CAMERA_PERMISSION);
                return;
            }
            manager.openCamera(cameraId, stateCallback, null);
        } catch (CameraAccessException e) {
            //e.printStackTrace();
        }
    }

    /**
     * Created by Gautam Anand
     * update the preview once a image is captured
     */
    protected void updatePreview () {
        try {
            if ( null == cameraDevice) { return;}
            captureRequestBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
            captureRequestBuilder.set(CaptureRequest.CONTROL_AE_LOCK, false);
            captureRequestBuilder.set(CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER, CameraMetadata.CONTROL_AE_PRECAPTURE_TRIGGER_START);
            captureRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE, CameraMetadata.CONTROL_AE_MODE_ON);
            captureRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER, CameraMetadata.CONTROL_AF_TRIGGER_START);
            captureRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CameraMetadata.CONTROL_AF_MODE_AUTO);
            captureRequestBuilder.set(CaptureRequest.CONTROL_AE_EXPOSURE_COMPENSATION, 6);
            cameraCaptureSessions.setRepeatingRequest(captureRequestBuilder.build(), null, mBackgroundHandler);
        } catch (CameraAccessException e) {
//                e.printStackTrace();
        }
    }

    /**
     * Created by Gautam Anand
     * close the camera after using it
     */
    private void closeCamera() {
        if (null != cameraDevice) {
            cameraDevice.close();
            cameraDevice = null;
        }

        if (null != imageReader) {
            imageReader.close();
            imageReader = null;
        }
    }

    /**
     * Created by Gautam Anand
     * onResume method of activity lifecycle, used to initialize background thread and texture view
     */
    @Override
    protected void onResume() {
        super.onResume();
        startBackgroundThread();
        if (textureView.isAvailable()) {
            openCamera();
        } else {
            textureView.setSurfaceTextureListener(textureListener);
        }
        tvCountImage.setText(displayImageCount());
    }

    /**
     * Created by Gautam Anand
     * onPause method of activity lifecycle, used to stop background thread and closing the camera
     */
    @Override
    protected void onPause() {
        super.onPause();
        stopBackgroundThread();
        closeCamera();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult();
    }

    private void setResult() {
        Intent intent = new Intent();
        intent.putStringArrayListExtra(IMAGE_LIST, listOfImagesPath);
        intent.putExtra(IMAGE_COUNTER, imageCounter);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CAMERA_PERMISSION) {
            boolean allGranted = false;
            if (permissions.length > 0 && grantResults.length > 0 ) {
                for (int result : grantResults) {
                    allGranted = result == PackageManager.PERMISSION_GRANTED;
                    if (!allGranted) break;
                }

                boolean isShouldShowRational = PermissionUtils.
                        isShouldShowRequestPermissionRationale(CaptureImagesActivity.this, permissionList);

                if (allGranted) {
                    PermissionUtils.proceedAfterPermission(CaptureImagesActivity.this, true);
                } else if (isShouldShowRational) {
                    ActivityCompat.requestPermissions(CaptureImagesActivity.this, permissionList, CAMERA_PERMISSION);
                } else {
                    toast(getString(R.string.err_permissions));
                    PermissionUtils.openSettings(CaptureImagesActivity.this, permissionList, CAMERA_PERMISSION);
                }
            }
        }
    }
}
