package android.form.avss.prepaidcard.web_services.interfaces;

import android.form.avss.prepaidcard.web_services.responses.BiometricValidationResponse;
import android.form.avss.prepaidcard.web_services.responses.CustomerIdResponse;
import android.form.avss.prepaidcard.web_services.responses.DocumentDataResponse;
import android.form.avss.prepaidcard.web_services.responses.DocumentUploadResponse;
import android.form.avss.prepaidcard.web_services.responses.FinalDataSubmitResponse;
import android.form.avss.prepaidcard.web_services.responses.GenderMasterResponse;
import android.form.avss.prepaidcard.web_services.responses.LoginResponse;
import android.form.avss.prepaidcard.web_services.responses.MobileNoResponse;
import android.form.avss.prepaidcard.web_services.responses.OVDDeemedOVDMasterResponse;
import android.form.avss.prepaidcard.web_services.responses.PanVerificationResponse;
import android.form.avss.prepaidcard.web_services.responses.PincodeMasterResponse;
import android.form.avss.prepaidcard.web_services.responses.PrefixMasterResponse;
import android.form.avss.prepaidcard.web_services.responses.ResidenceMasterResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

import static android.form.avss.prepaidcard.web_services.interfaces.AppServices.ServiceKeys.FUNCTION_NAME;
import static android.form.avss.prepaidcard.web_services.interfaces.AppServices.ServiceKeys.IV;
import static android.form.avss.prepaidcard.web_services.interfaces.AppServices.ServiceKeys.REF_ID;
import static android.form.avss.prepaidcard.web_services.interfaces.AppServices.ServiceKeys.REQUEST;
import static android.form.avss.prepaidcard.web_services.interfaces.AppServices.ServiceKeys.TOKEN;
import static android.form.avss.prepaidcard.web_services.interfaces.ServicesFunctionName.AUTHENTICATE_AADHAAR;
import static android.form.avss.prepaidcard.web_services.interfaces.ServicesFunctionName.AUTHENTICATION_BD;
import static android.form.avss.prepaidcard.web_services.interfaces.ServicesFunctionName.FINAL_SAVE_DATA;
import static android.form.avss.prepaidcard.web_services.interfaces.ServicesFunctionName.GET_GENDER_MASTER;
import static android.form.avss.prepaidcard.web_services.interfaces.ServicesFunctionName.GET_DROPDOWN;
import static android.form.avss.prepaidcard.web_services.interfaces.ServicesFunctionName.GET_IMAGE_DATA;
import static android.form.avss.prepaidcard.web_services.interfaces.ServicesFunctionName.GET_MARKING_DETAILS;
import static android.form.avss.prepaidcard.web_services.interfaces.ServicesFunctionName.GET_OVD_MASTER;
import static android.form.avss.prepaidcard.web_services.interfaces.ServicesFunctionName.GET_RESIDENT_MASTER;
import static android.form.avss.prepaidcard.web_services.interfaces.ServicesFunctionName.PAN_VERIFICATION;
import static android.form.avss.prepaidcard.web_services.interfaces.ServicesFunctionName.GET_PIN_CODE_MASTER;
import static android.form.avss.prepaidcard.web_services.interfaces.ServicesFunctionName.GET_TITLE_MASTER;
import static android.form.avss.prepaidcard.web_services.interfaces.ServicesFunctionName.PDF_DATA_SAVE_VIEW;
import static android.form.avss.prepaidcard.web_services.interfaces.ServicesFunctionName.SAVE_DATA;
import static android.form.avss.prepaidcard.web_services.interfaces.ServicesFunctionName.SUBMIT_PDF_IMAGE;
import static android.form.avss.prepaidcard.web_services.interfaces.ServicesFunctionName.UPLOAD_IMAGE_DATA;
import static android.form.avss.prepaidcard.web_services.interfaces.ServicesFunctionName.VALIDATE_CUSTOMER_ID;
import static android.form.avss.prepaidcard.web_services.interfaces.ServicesFunctionName.VALIDATE_MOBILE_NO;

public interface AppServices {
    @FormUrlEncoded
    @POST(AUTHENTICATION_BD)
    Call<LoginResponse> getUserLogin(@Field(REQUEST) String request, @Field(IV) String iv, @Field(FUNCTION_NAME) String fn );

    @FormUrlEncoded
    @POST(GET_TITLE_MASTER)
    Call<PrefixMasterResponse> getPrefixMaster(@Field(REQUEST) String request, @Field(IV) String iv, @Field(FUNCTION_NAME) String fn );

    @FormUrlEncoded
//    @POST("ovd/getGenderMaster")
    @POST(GET_GENDER_MASTER)
    Call<GenderMasterResponse> getGenderMaster(@Field(REQUEST) String request, @Field(IV) String iv, @Field(FUNCTION_NAME) String fn );

    @FormUrlEncoded
//    @POST("ovd/deemedOvdMaster")
    @POST(GET_OVD_MASTER)
    Call<OVDDeemedOVDMasterResponse> getOVDMaster(@Field(REQUEST) String request, @Field(IV) String iv, @Field(FUNCTION_NAME) String fn );

    @FormUrlEncoded
//    @POST("ovd/residenceMaster")
    @POST(GET_RESIDENT_MASTER)
    Call<ResidenceMasterResponse> getResidenceMaster(@Field(REQUEST) String request, @Field(IV) String iv, @Field(FUNCTION_NAME) String fn );

    @FormUrlEncoded
//    @POST("ovd/pincodeMaster")
    @POST(GET_PIN_CODE_MASTER)
    Call<PincodeMasterResponse> getPinCodeMaster(@Field(REQUEST) String request, @Field(IV) String iv, @Field(FUNCTION_NAME) String fn );

    @FormUrlEncoded
    @POST(AUTHENTICATE_AADHAAR)
    Call<BiometricValidationResponse> validateAadhaar(@Field(REQUEST) String request, @Field(IV) String iv, @Field(FUNCTION_NAME) String fn, @Field(TOKEN) String token );

    @FormUrlEncoded
    @POST(PAN_VERIFICATION)
    Call<PanVerificationResponse> validatePan(@Field(REQUEST) String request, @Field(IV) String iv, @Field(FUNCTION_NAME) String fn, @Field(TOKEN) String token );

    @FormUrlEncoded
    @POST(SAVE_DATA)
    Call<FinalDataSubmitResponse> finalSubmitData(@Field(REQUEST) String request, @Field(IV) String iv, @Field(FUNCTION_NAME) String fn, @Field(TOKEN) String token );

    @FormUrlEncoded
    @POST(GET_MARKING_DETAILS)
    Call<ResponseBody> getMarkingDetails(@Field(REQUEST) String request, @Field(IV) String iv, @Field(FUNCTION_NAME) String fn, @Field(TOKEN) String token );

    @FormUrlEncoded
    @POST(GET_DROPDOWN)
    Call<ResponseBody> getDropDowns(@Field(REQUEST) String request, @Field(IV) String iv, @Field(FUNCTION_NAME) String fn, @Field(TOKEN) String token );

    @FormUrlEncoded
    @POST(PDF_DATA_SAVE_VIEW)
    Call<ResponseBody> saveFormData(@Field(REF_ID) String refId, @Field(REQUEST) String request, @Field(IV) String iv, @Field(FUNCTION_NAME) String fn, @Field(TOKEN) String token );

//    @FormUrlEncoded
//    @POST(FINAL_SAVE_DATA)
//    Call<ResponseBody> finalSaveData(@Field(REF_ID) String refId, @Field(REQUEST) String request, @Field(IV) String iv, @Field(FUNCTION_NAME) String fn, @Field(TOKEN) String token );

    @FormUrlEncoded
    @POST(GET_IMAGE_DATA)
    Call<DocumentDataResponse> getImageData(@Field(REQUEST) String request, @Field(REF_ID) String refId, @Field(IV) String iv, @Field(FUNCTION_NAME) String fn, @Field(TOKEN) String token);

    @FormUrlEncoded
    @POST(UPLOAD_IMAGE_DATA)
    Call<DocumentUploadResponse> uploadImageData(@Field(REQUEST) String request, @Field(REF_ID) String refId, @Field(IV) String iv, @Field(FUNCTION_NAME) String fn, @Field(TOKEN) String token);


    // TODO: 5/22/2019 still these function is not used but they will use in future
    @FormUrlEncoded
    @POST(VALIDATE_CUSTOMER_ID)
    Call<CustomerIdResponse> validateCustomerId(@Field(REQUEST) String request, @Field(IV) String iv, @Field(FUNCTION_NAME) String fn, @Field(TOKEN) String token );

    @FormUrlEncoded
    @POST(VALIDATE_MOBILE_NO)
    Call<MobileNoResponse> validateMobileNo(@Field(REQUEST) String request, @Field(IV) String iv, @Field(FUNCTION_NAME) String fn, @Field(TOKEN) String token );


    @FormUrlEncoded
    @POST(SUBMIT_PDF_IMAGE)
    Call<ResponseBody> submitPdfImage(@Field(REQUEST) String request, @Field(IV) String iv, @Field(FUNCTION_NAME) String fn, @Field(TOKEN) String token );



    interface ServiceKeys {
        String REQUEST = "request";
        String REF_ID = "refid";
        String IV = "iv";
        String FUNCTION_NAME = "fn";
        String TOKEN = "token";
    }
}
