package android.form.avss.prepaidcard.web_services.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sapple on 16/01/2017.
 */
public class PanVerificationResponse{
    @SerializedName("da")
    public String data;

    public static class PanVerification  extends BaseResponse<PanVerificationResponse.PanVerification> {
        @SerializedName("pus")
        public String pus;
        @SerializedName("ns")
        public String nsdl_status;
        @SerializedName("ps")
        public String pan_status;
        @SerializedName("napn")
        public String name;
        @SerializedName("ap_nmp")
        public String aadhaarPanNamingPercentage;
    }
}