package android.form.avss.prepaidcard.ihmf_form_fill.ui.Fragments;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.form.avss.prepaidcard.ihmf_form_fill.TempPackage.GroupControls;
import android.form.avss.prepaidcard.ihmf_form_fill.interfaces.FocusListener;
import android.form.avss.prepaidcard.ihmf_form_fill.interfaces.iHMFView;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.Responses.Page;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.controls.BaseControl;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.controls.BooleanControl;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.controls.Control;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.controls.ControlUtility;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.controls.ImageControl;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.controls.TextControl;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.controls.iHMFGroupListener;
import android.form.avss.prepaidcard.ihmf_form_fill.ui.Adapters.MandateListAdapter;
import android.form.avss.prepaidcard.ihmf_form_fill.ui.ControlsView.IHMFEditText;
import android.form.avss.prepaidcard.ihmf_form_fill.ui.Others.ImageSelectorDialog;
import android.form.avss.prepaidcard.ihmf_form_fill.ui.zoomlayout.ZoomLayout;
import android.form.avss.prepaidcard.ihmf_form_fill.util.ConstantsUtils;
import android.form.avss.prepaidcard.ihmf_form_fill.util.FormUtil;
import android.form.avss.prepaidcard.ihmf_form_fill.util.ImageUtil;
import android.form.avss.prepaidcard.ihmf_form_fill.util.PermissionUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PointF;
import android.graphics.drawable.BitmapDrawable;
import android.form.avss.prepaidcard.R;
import android.form.avss.prepaidcard.ihmf_form_fill.ui.Activities.MySignaturePadActivity;
import android.form.avss.prepaidcard.ihmf_form_fill.ui.ControlsView.PinEntryEditText;
import android.form.avss.prepaidcard.ihmf_form_fill.ui.ControlsView.IHMFBooleanView;
import android.form.avss.prepaidcard.ihmf_form_fill.ui.ControlsView.IHMFImageView;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;


public class FormFillFragmentForm extends FormBaseFragment implements FormUtil.onViewCreated, MandateListAdapter.OnListItemClick {

//    private FragmentFormFillingBinding mBinder;

//    private String TAG = getClass().getSimpleName();
//    private List<SectionData> sectionSectionData;
//    private static final String PAGE_DATA = "PAGE_DATA";
    private Page pageData;
    private CompositeDisposable mDisposable;
    private ImageSelectorDialog mDialog;
    private MandateListAdapter adapter;
    private PointF point;
    private IHMFImageView imageSelectorView;
    private View currentFocusedUnmarkedView;

    private Bitmap bitmap;

    private ZoomLayout zoomLayout;

    private boolean isPreviewMode;


    private boolean mAlreadyInflated;

//    private RelativeLayout rl_rvSection;
//    private int lastKnownPos = 0;
//    private SectionAdapter sectionAdapter;
//    private float backgroundImageHeight;
//    private RecyclerView rvSection;


    private ViewGroup imageContainer;
    private ProgressBar progressBar;

    private float viewBottomY;


    private iHMFGroupListener<Boolean> mBooleanGroupListener = new iHMFGroupListener<Boolean>() {
        @Override
        public GroupControls getGroupControl(String groupId) {
            GroupControls groupControls = null;

            if (!pageData.getGroupControlsList().isEmpty()) {
                for (GroupControls gControls : pageData.getGroupControlsList()) {
                    if (gControls.getGroupId().equalsIgnoreCase(groupId)) {
                        groupControls = gControls;
                        break;
                    }
                }
            }
            return groupControls;
        }

        @Override
        public void handleGroup(int viewId, String groupId, Boolean isChecked) {

        }
    };

    private iHMFGroupListener<String> mTextFieldGroupListener = new iHMFGroupListener<String>() {
        @Override
        public GroupControls getGroupControl(String groupId) {
            GroupControls groupControls = null;
            if (!pageData.getGroupControlsList().isEmpty()) {
                for (GroupControls gControls : pageData.getGroupControlsList()) {
                    if (gControls.getGroupId().equalsIgnoreCase(groupId)) {
                        groupControls = gControls;
                        break;
                    }
                }
            }
            return groupControls;
        }

        @Override
        public void handleGroup(int viewId, String groupId, String text) {
            if (pageData.getGroupControlsList() == null) return;

            GroupControls controls = getGroupControl(groupId);

            for (View view : controls.getViewList())
                if (viewId != view.getId()) {
                    if (controls.getActivity().equalsIgnoreCase(ConstantsUtils.SAME_DATA)) {
                        if (view instanceof IHMFEditText) {
                            IHMFEditText editText = (IHMFEditText) view;
                            editText.removeTextWatcher();
                            editText.setTextFieldText(text);
                            editText.addTextWatcher();
                        } else if (view instanceof PinEntryEditText) {
                            PinEntryEditText editText = (PinEntryEditText) view;
                            editText.removeTextWatcher();
                            editText.setBoxText(text);
                            editText.addTextWatcher();
                        }
                    }
                }
        }
    };

    private iHMFGroupListener<Bitmap> mImageGroupListener = new iHMFGroupListener<Bitmap>() {
        @Override
        public GroupControls getGroupControl(String groupId) {
            GroupControls groupControls = null;
            if (!pageData.getGroupControlsList().isEmpty()) {
                for (GroupControls gControls : pageData.getGroupControlsList()) {
                    if (gControls.getGroupId().equalsIgnoreCase(groupId)) {
                        groupControls = gControls;
                        break;
                    }
                }
            }
            return groupControls;
        }

        @Override
        public void handleGroup(int viewId, String groupId, Bitmap bitmap) {

            GroupControls groupControlData = getGroupControl(groupId);
            if (groupControlData == null) return;

            if (groupControlData.getGroupId().equals(groupId)) {
                for (View view : groupControlData.getViewList()) {
                    if (viewId != view.getId()) {
                        IHMFImageView imageView = (IHMFImageView) view;
                        imageView.setImageBitmap(bitmap);
                    }
                }

            }
        }
    };


//    private View.OnFocusChangeListener mViewGroupFocusListener = new View.OnFocusChangeListener() {
//        @Override
//        public void onFocusChange(View v, boolean hasFocus) {
//            if (!hasFocus) {
//                if (((ViewGroup) v).getFocusedChild() != null)
//                    ((ViewGroup) v).getFocusedChild().clearFocus();
//            }
//        }
//    };

    private View.OnTouchListener mViewGroupTouchListener = new View.OnTouchListener() {
        @SuppressLint("ClickableViewAccessibility")
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            FormFillFragmentForm.this.onTap(event);
            return false;
        }
    };


    public FormFillFragmentForm() {
        // Required empty public constructor
    }


    private final FocusListener focusListener = new FocusListener() {
        @Override
        public void addGlobalLayoutListener(float viewB) {
            addGListener();
            viewBottomY = viewB;
        }

        @Override
        public void removeGlobalLayoutListener() {
            removeGListener();
        }
    };


    public void addGListener(){
        getActivity().getWindow().getDecorView().getViewTreeObserver()
                .addOnGlobalLayoutListener(listener);
    }

    public void removeGListener(){
        getActivity().getWindow().getDecorView().getViewTreeObserver()
                .removeOnGlobalLayoutListener(listener);
    }


    private float panRatio;
    private final ViewTreeObserver.OnGlobalLayoutListener listener = new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override
        public void onGlobalLayout() {

            // @temp (image's y coordinate at zoomLayout bottom when keyboard appears)
            float temp = panRatio * zoomLayout.getHeight() - zoomLayout.getPanY();

            //move the view up only if keyboard overlaps the view
            if(viewBottomY > temp){
                float diff = viewBottomY - temp + Math.abs(zoomLayout.getPanY());
                zoomLayout.moveTo(zoomLayout.getZoom() , 0, -diff, false);
            }
        }
    };





    public static FormFillFragmentForm newInstance(Page pageData, boolean isPreviewMode) {
        FormFillFragmentForm fragment = new FormFillFragmentForm();
        Bundle args = new Bundle();
        fragment.pageData = pageData;
//        fragment.sectionSectionData = pageData.getSectionList();
        fragment.isPreviewMode = isPreviewMode;
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new MandateListAdapter(this);
        mDialog = new ImageSelectorDialog(getContext());
    }


    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
    mDisposable = new CompositeDisposable();

    if(getView() != null){
        mAlreadyInflated = true;
        return getView();
    }
    View view = inflater.inflate(R.layout.fragment_form_filling, container, false);


    imageContainer = (ViewGroup) view.findViewById(R.id.iv);
    imageContainer.setOnTouchListener(mViewGroupTouchListener);

    progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
    zoomLayout = (ZoomLayout) view.findViewById(R.id.viewGroup);

//        rvSection = (RecyclerView) view.findViewById(R.id.rv_sections);
//        rl_rvSection = (RelativeLayout) view.findViewById(R.id.rl_rvSection);

    return view;
    }




    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (mAlreadyInflated)
            mDisposable.add(FormUtil.drawControl(pageData.getControls(), imageContainer, point, this, focusListener));
        else {
            getBackgroundBitmap();
        }


    }

    private double scale = 1;
    private void getBackgroundBitmap() {
        final String imageUrl = pageData.getPngImageUrls();
        if (TextUtils.isEmpty(imageUrl)) {
            showShortToast("Empty image url");
            return;
        }

        progressBar.setVisibility(View.VISIBLE);

        Disposable disposable = Completable.fromAction(new Action() {
            @Override
            public void run() {
                final File file = new File(getFormImagesFileDir(), imageUrl.substring(imageUrl.lastIndexOf('/') + 1));
                if (!file.exists()) {
                    progressBar.post(new Runnable() {
                        @Override
                        public void run() {
                            showShortToast("Form image not found");
                        }
                    });
                    bitmap = null;
                    return;
                }
                try {
                    FileInputStream fileInputStream = new FileInputStream(file.getPath());
                    bitmap = BitmapFactory.decodeStream(fileInputStream);
                    fileInputStream.close();

                    //restricting image height to 3800, width also scaled down by same ratio
                    if(!isPreviewMode & bitmap.getHeight() > 3800){
                        scale = 3800 / (float)bitmap.getHeight();
                        double width = scale * bitmap.getWidth();
                        bitmap = Bitmap.createScaledBitmap(bitmap, (int)width, 3800, false);
                    }

                } catch (Exception e) {
                    progressBar.setVisibility(View.GONE);
                    showShortToast(e.getMessage());
                }
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action() {
                    @Override
                    public void run() {
                        //On Success
                        progressBar.setVisibility(View.GONE);
                        setUpBackgroundImage();  //enable ZoomLayout here
//                        loadBackgroundImageInScrollView();  //enable Scrollview here
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable e) {
                        progressBar.setVisibility(View.GONE);
                        showShortToast(e.getMessage());
                    }
                });

        mDisposable.add(disposable);

    }


    private void setUpBackgroundImage(){
        if(bitmap == null){
            return;
        }
        progressBar.setVisibility(View.VISIBLE);

        int pageHeight = bitmap.getHeight();
        int pageWidth = bitmap.getWidth();

        imageContainer.setBackground(new BitmapDrawable(getResources(), bitmap));

        imageContainer.getLayoutParams().width = pageWidth;
        imageContainer.getLayoutParams().height = pageHeight;

        zoomLayout.setSize(pageWidth, pageHeight);
        point = new PointF((float) scale, (float) scale);

        drawControls();

        double bitmapRatio  = ((double)bitmap.getWidth())/bitmap.getHeight();
        double imageContainerRatio  = ((double)imageContainer.getWidth())/imageContainer.getHeight();

        float zoomFactor;

        if(bitmapRatio >= imageContainerRatio){
            zoomFactor = 1f;
        }else {
            double drawWidth = (bitmapRatio/imageContainerRatio) * imageContainer.getWidth();
            double imageX = (imageContainer.getWidth() - drawWidth)/2;
            double totalSpaceInWidth = imageX * 2;
            double widthPixelsToZoomIn = zoomLayout.getWidth() - totalSpaceInWidth;
            double heightPixelsToZoomIn = widthPixelsToZoomIn / (((float)zoomLayout.getWidth()/zoomLayout.getHeight()));

            zoomFactor = zoomLayout.getHeight() / (float)heightPixelsToZoomIn;
        }



        if(!isPreviewMode) {
            //when y=0, panRatio is undefined, so consider passing a small value for y
            zoomLayout.moveTo(zoomFactor, 0, -1f, false);
            panRatio = zoomLayout.getPanY()/zoomLayout.getEngine().getScaledPanY();
            zoomLayout.setZoomEnabled(false);
        }

        progressBar.setVisibility(View.GONE);

    }


    private void drawControls(){
        //start drawing controls
        mDisposable.add(FormUtil.drawControl(pageData.getControls(), imageContainer, point, FormFillFragmentForm.this, focusListener));
    }

    
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (getContext() == null) return;

        final IHMFImageView img = imageSelectorView;
        imageSelectorView = null;

        Bitmap originalBitmap = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        options.inJustDecodeBounds = true;

        if (img == null) return;

        String path;

        switch (requestCode) {

            case ImageSelectorDialog.SIGNATURE:

                if (resultCode != Activity.RESULT_OK) return;
                path = data.getStringExtra(MySignaturePadActivity.PATH);

                BitmapFactory.decodeFile(path, options);
                options.inSampleSize = 1; //ImageUtil.inSampleSize(options, img.getControl().getW().intValue(), img.getControl().getH().intValue());
                options.inJustDecodeBounds = false;
                originalBitmap = BitmapFactory.decodeFile(path, options);
                break;

            case ImageSelectorDialog.IMG_CAMERA:

                if (resultCode != Activity.RESULT_OK) return;

                path = data.getStringExtra(MySignaturePadActivity.PATH);

                BitmapFactory.decodeFile(path, options);
                options.inSampleSize = ImageUtil.inSampleSize(options, img.getControl().getW().intValue(), img.getControl().getH().intValue());
                options.inJustDecodeBounds = false;
                originalBitmap = BitmapFactory.decodeFile(path, options);
                switch (ImageUtil.getCameraPhotoOrientation(null, path)) {
                    case 90:
                        originalBitmap = ImageUtil.fixOrientation(originalBitmap, 90);
                        break;
                    case 180:
                        originalBitmap = ImageUtil.fixOrientation(originalBitmap, 180);
                        break;
                    case 270:
                        originalBitmap = ImageUtil.fixOrientation(originalBitmap, 270);
                        break;
                }
                break;


            case ImageSelectorDialog.RESULT_LOAD:
//                    if (resultCode != Activity.RESULT_OK || data == null) return;
//
//                    Uri uri = data.getData();
//                    if (getActivity() == null || uri == null) return;
//
//                    try (InputStream is = getContext().getContentResolver().openInputStream(uri)) {
//
//                        options.inSampleSize = ImageUtil.inSampleSize(options, img.getControl().getW().intValue(), img.getControl().getW().intValue());
//
//                        if(is != null) is.reset();
//                        originalBitmap = BitmapFactory.decodeStream(is, null, options);
//
//                    } catch (IOException e) {
//                        originalBitmap = null;
//                    }

                if (resultCode != Activity.RESULT_OK || data == null) return;
                if (getActivity() == null) return;

                Uri uri = data.getData();

                try {
                    originalBitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                } catch (IOException e) {
                    originalBitmap = null;
                    e.printStackTrace();
                }
                break;
        }

        if (originalBitmap == null) return;


        Bitmap b = Bitmap.createScaledBitmap(originalBitmap, img.getControl().getW().intValue(), img.getControl().getH().intValue(), true);
        originalBitmap.recycle();

        img.setImageBitmap(b);

    }


    @Override
    public void onDestroyView() {

        if (!mDisposable.isDisposed())
            mDisposable.dispose();

        removeViewsFromControlGroup();
        removeControl();
        imageContainer.removeAllViews();

        bitmap.recycle();
        bitmap = null;

        super.onDestroyView();
    }

    /**
     * Remove view reference hold in fragment
     */
    private void removeControl() {
        imageSelectorView = null;
        currentFocusedUnmarkedView = null;
        adapter.clear();

    }


    private void removeViewsFromControlGroup() {
        if (pageData.getGroupControlsList() != null)
            for (GroupControls controls : pageData.getGroupControlsList())
                controls.clear();
    }


    public void setCurrentView(View view) {
        this.currentFocusedUnmarkedView = view;
    }

    public void deleteUnmarkedView() {
        if (currentFocusedUnmarkedView != null) {
            imageContainer.removeView(currentFocusedUnmarkedView);
            currentFocusedUnmarkedView = null;
        }
    }

    public void removeTouchListener() {
        if (currentFocusedUnmarkedView instanceof iHMFView)
            ((iHMFView) currentFocusedUnmarkedView).removeOnTouchListener();

    }


    public void resizeViewSize(int resizeType) {
        if (currentFocusedUnmarkedView instanceof iHMFView)
            ((iHMFView) currentFocusedUnmarkedView).resizeView(resizeType);

    }

    public void setControl(Control control) {
        mControl = control;
    }

    @Override
    public void onViewDrawCompleted(final View view, final iHMFView control) {
        if (pageData.getGroupControlsList() == null) {
            List<GroupControls> list = new ArrayList<>();
            pageData.setGroupControlsList(list);
        }


        if (control.getControl().hasGroup()) {
            boolean temp = false;
            String groupId = control.getControl().getGroupId();
            for (GroupControls data : pageData.getGroupControlsList()) {
                if (data.getGroupId().equals(groupId)) {
                    data.addItem(view);
                    temp = true;
                    break;
                }
            }

            if (!temp) {
                GroupControls groupControls = new GroupControls();
                groupControls.setGroupId(groupId);
                groupControls.setActivity(control.getControl().getActivity());
                groupControls.setMandatory(Boolean.parseBoolean(control.getControl().getGroupId()));
                groupControls.addItem(view);
                pageData.getGroupControlsList().add(groupControls);
            }


            if (view instanceof IHMFBooleanView) {


                IHMFBooleanView ibv = (IHMFBooleanView) view;

                boolean addItem = true;
                if (control.getControl().hasGroup()) {
                    ibv.setOnGroupListener(mBooleanGroupListener);
                    if (control.getControl().isMandatory()) {
                        addItem = !ibv.contains(control.getControl().getGroupId());
                    } else
                        addItem = false;
                }

                if (addItem)
                    adapter.add(control);
            }

            if (view instanceof IHMFEditText)
                ((IHMFEditText) view).setOnGroupListener(mTextFieldGroupListener);

            if (view instanceof PinEntryEditText)
                ((PinEntryEditText) view).setOnGroupListener(mTextFieldGroupListener);


            if (view instanceof IHMFImageView)
                ((IHMFImageView) view).setOnGroupListener(mImageGroupListener);
        }


        if (!(view instanceof IHMFBooleanView) && control.getControl().isMandatory())
            adapter.add(control);


        if (view instanceof IHMFImageView) {

            IHMFImageView ihmf = (IHMFImageView) view;

            final BaseControl imageControl = ihmf.getControl();

            ihmf.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {

                    imageSelectorView = (IHMFImageView) v;

                    mDialog.getSignpad().setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v1) {
                            showShortToast("Not available");
//                            mDialog.getDialog().dismiss();
                        }
                    });


                    mDialog.getGallery().setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v1) {
//                            mDialog.galleryIntent(FormFillFragmentForm.this);
                            showShortToast("Not available");
                            mDialog.getDialog().dismiss();
                        }
                    });


                    mDialog.getCamera().setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v1) {
                            PermissionUtil.requestPermissions(FormFillFragmentForm.this, new String[]{Manifest.permission.CAMERA}, 12, new PermissionUtil.Callback() {
                                @Override
                                public void onPermissionGranted() {
                                    float ratio = imageControl.getH()/imageControl.getW();
                                    mDialog.openCameraActivity(FormFillFragmentForm.this, String.valueOf(imageControl.getId()) ,imageControl.getType(), ratio);
                                    mDialog.getDialog().dismiss();
                                }

                                @Override
                                public void onPermissionBlocked() {
                                    Toast.makeText(v.getContext(), "Camera Permission Required", Toast.LENGTH_SHORT).show();
                                    mDialog.getDialog().dismiss();
                                }
                            });
                        }
                    });

                    mDialog.show(view);

                }
            });
        }

    }

    private void initOtherProperties(MotionEvent event, BaseControl baseControl) {
        List<JsonObject> list = new ArrayList<>();
        baseControl.setType(mControl.name());
        baseControl.setX(event.getX() / point.x);
        baseControl.setY(event.getY() / point.y);
        baseControl.setMarkedObject(false);
        String jsonString = ControlUtility.getGson().toJson(baseControl);
        JsonElement jsonElement = ControlUtility.getGson().fromJson(jsonString, JsonElement.class);
        JsonObject jsonObject = jsonElement.getAsJsonObject();
        list.add(jsonObject);
        mDisposable.add(FormUtil.drawControl(list, imageContainer, point, FormFillFragmentForm.this, focusListener));
        mControl = Control.NONE;
    }


    public void validateAndSaveData() {
        FormUtil.validateAndSaveData(imageContainer);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


        PermissionUtil.CallbackResult result = PermissionUtil.getLastPermissionHandler(requestCode, permissions, grantResults);

        if (result == null) return;

        if (requestCode == 12) {
            if (result.isPermissionsGranted())
                result.onPermissionGranted();
        }


        PermissionUtil.clean();

    }


    @Override
    public void onMandateItemClick(View view, iHMFView control) {

        if (getContext() == null) return;

        getDrawer().closeDrawer(GravityCompat.END);

        if (control.getView() instanceof PinEntryEditText)
            ((PinEntryEditText) control.getView()).focus();
        else
            control.getView().requestFocus();

//        zoomLayout.moveTo(zoomLayout.getZoom(), control.getView().getX(), control.getView().getY(), true);
    }


    public BaseAdapter getMandateAdapter() {
        return adapter;
    }


    public int getTotalMarkedcontrols() {
        return pageData.getControls().size();
    }


    //TODO:: no need to implement for bank, need proper implementation
    private void onTap(MotionEvent event) {

        if (mControl == null || mControl == Control.NONE) {
            mControl = Control.NONE;
            return;
        }

        BaseControl baseControl;
        if (mControl == Control.TextField) {
            baseControl = new TextControl();
            baseControl.setHelpText("Type Here...");
            baseControl.setW(200f);
            baseControl.setH(50f);
            initOtherProperties(event, baseControl);
        } else if (mControl == Control.Boolean) {
            baseControl = new BooleanControl();
            baseControl.setH(40f);
            baseControl.setW(40f);
            initOtherProperties(event, baseControl);
        } else if (mControl == Control.Photo) {
            baseControl = new ImageControl();
            baseControl.setH(100f);
            baseControl.setW(100f);
            initOtherProperties(event, baseControl);
        } else if (mControl == Control.Signature) {
            baseControl = new ImageControl();
            baseControl.setH(100f);
            baseControl.setW(200f);
            initOtherProperties(event, baseControl);
        }

    }


    //    public void setToLastSection(){
//        setSection(lastKnownPos);
//    }
//
//    public void setSection(int pos){
//
//        if(sectionSectionData == null || sectionSectionData.isEmpty()){
//            return;
//        }
//
//        lastKnownPos = pos;
//
//        SectionData sectionData = sectionSectionData.get(pos);
//
//        float sectionHeight = sectionData.getBottom() - sectionData.getTop();
//
//        float zoomLayoutHeight = zoomLayout.getMeasuredHeight();
//
//        float heightDiffRatio = zoomLayoutHeight / backgroundImageHeight;
//
//        float zoomFactor = zoomLayoutHeight / (sectionHeight * heightDiffRatio);
//
//        zoomLayout.getEngine().moveTo(zoomFactor, 300f, -sectionData.getTop(), true);
//
//        zoomLayout.getEngine().setVerticalPanEnabled(false);
//    }
//
//    public void initSetctionRecyclerView(){
//        if(sectionAdapter != null){
//            sectionAdapter = null;
//        }
//
//        sectionAdapter = new SectionAdapter(sectionSectionData.size(), new PositionClickListener() {
//            @Override
//            public void onPosClick(int pos) {
//                if(pos == sectionSectionData.size()){
//                    zoomLayout.getEngine().zoomTo(1, true);
//                    zoomLayout.getEngine().setVerticalPanEnabled(true);
//                    zoomLayout.getEngine().setZoomEnabled(true);
//                    return;
//                }
//                setSection(pos);
//            }
//        });
//                rvSection.setLayoutManager(new GridLayoutManager(getContext(), 4));
//        rvSection.setAdapter(sectionAdapter);
//        setSection(lastKnownPos);
//    }



//    private void loadBackgroundImageInScrollView(){
//        if(bitmap == null){
//            return;
//        }
//        progressBar.setVisibility(View.VISIBLE);
//        float scale = (float) getResources().getDisplayMetrics().widthPixels / (float) bitmap.getWidth();
//
//        float scaledWidth = bitmap.getWidth() * scale;
//        float scaledHeight = bitmap.getHeight() * scale;
//
//        Bitmap scaledBm = Bitmap.createScaledBitmap(bitmap, (int) scaledWidth, (int) scaledHeight, false);
//
//        imageContainer.setBackground(new BitmapDrawable(getResources(), scaledBm));
//
//
//        imageContainer.getLayoutParams().width = scaledBm.getWidth();
//        imageContainer.getLayoutParams().height = scaledBm.getHeight();
//
//        point = new PointF(scale, scale);
//        drawControls();
//        progressBar.setVisibility(View.GONE);
//    }
}