package android.form.avss.prepaidcard.ui.custom_classes;

public class CustomTag {

    private String _id ;
    private String _type;
    private String _validationType;
    private String _isRequired;

    public CustomTag() {
        this._id = null;
        this._type = null;
        this._validationType = null;
        this._isRequired = null;
    }

    public void setTag(String id, String type, String validationType, String isRequired){
        this._id = id;
        this._type = type;
        this._validationType = validationType;
        this._isRequired = isRequired;
    }

    public String getTagId(){
        return  this._id;
    }

    public String getTagValidationTpe(){
        return  this._validationType;
    }

    public String getTagType(){
        return  this._type;
    }

    public String getTagRequired(){
        return  this._isRequired;
    }
}
