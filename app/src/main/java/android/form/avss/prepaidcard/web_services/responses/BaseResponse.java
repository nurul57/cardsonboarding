package android.form.avss.prepaidcard.web_services.responses;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BaseResponse<Data> implements Serializable {
    @SerializedName("da")
    protected Data data;
    @SerializedName("em")
    protected String error_msg;
    @SerializedName("ss")
    protected String status;
    @SerializedName("tn")
    protected String token;

    public boolean isSuccess(){return !TextUtils.isEmpty(status) && status.equalsIgnoreCase("true");}

    public String msg() {
        return error_msg;
    }

    public String token() {
        return token;
    }

    public Data data() {return  data;}

    public void setData (Data data){ this.data = data;}
}
