package android.form.avss.prepaidcard.ihmf_form_fill.TempPackage;

import com.google.gson.annotations.SerializedName;

public class ImageData{
    @SerializedName("pageId")
    private String imageId;

    @SerializedName("controlId")
    private String controlId;

    @SerializedName("name")
    private String name;

    @SerializedName("imageData")
    private String imageData;


    public String getImageIdId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getControlId() {
        return controlId;
    }

    public void setControlId(String controlId) {
        this.controlId = controlId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageData() {
        return imageData;
    }

    public void setImageData(String imageData) {
        this.imageData = imageData;
    }
}

