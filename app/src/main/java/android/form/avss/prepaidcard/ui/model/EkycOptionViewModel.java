package android.form.avss.prepaidcard.ui.model;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class EkycOptionViewModel implements Serializable {
	@SerializedName("isadr")
	private String isAadhaar;

	@SerializedName("uidtn")
	private String uidTokenNo;

	@SerializedName("apx")
	private String applicationPrefix;
	@SerializedName("afn")
	private String applicantFirstName;
	@SerializedName("amn")
	private String applicantMiddleName;
	@SerializedName("aln")
	private String applicantLastName;
	@SerializedName("adob")
	private String applicantDob;
	@SerializedName("agn")
	private String applicantGender;
	@SerializedName("fp")
	private String applicantFatherPrefix;
	@SerializedName("ffn")
	private String applicantFatherFirstName;
	@SerializedName("fmn")
	private String applicantFatherMiddleName;
	@SerializedName("fln")
	private String applicantFatherLastName;

	@SerializedName("comm_ale1")
	private String commAddressLine1;
	@SerializedName("comm_ale2")
	private String commAddressLine2;
	@SerializedName("comm_ale3")
	private String commAddressLine3;
	@SerializedName("comm_alk")
	private String commLandmark;
	@SerializedName("comm_ape")
	private String commPinCode;
	@SerializedName("comm_acy")
	private String commCity;
	@SerializedName("comm_ase")
	private String commState;
	@SerializedName("comm_cty")
	private String commCountry;

	@SerializedName("perm_ale1")
	private String permAddressLine1;
	@SerializedName("perm_ale2")
	private String permAddressLine3;
	@SerializedName("perm_ale3")
	private String permAddressLine2;
	@SerializedName("perm_ape")
	private String permPinCode;
	@SerializedName("perm_alk")
	private String permLandmark;
	@SerializedName("perm_acy")
	private String permCity;
	@SerializedName("perm_ase")
	private String permState;
	@SerializedName("perm_cty")
	private String permCountry;

	 /******************Id Proof Document Section*************************/
	@SerializedName("idpn")
	private String idProofDocName;

	@SerializedName("idpnum")
	private String idProofDocNumber;

	@SerializedName("idpia")
	private String idProofDocIssuingAuthority;

	@SerializedName("idpide")
	private String idProofDocIssuingDate;

	@SerializedName("idpede")
	private String idProofIssuingExpireDate;

	/****************Address Proof Document Section*************************/

	@SerializedName("adpn")
	private String addressProofDocName;

	@SerializedName("adpnum")
	private String addressProofDocNumber;

	@SerializedName("adpia")
	private String addressProofDocIssuingAuthority;

	@SerializedName("adexp")
	private String addressProofDocExpiryDate;


	@SerializedName("mn")
	private String mobileNo;
	@SerializedName("el")
	private String emailId;

	@SerializedName("cb_idpf")
	private String cbEkycIdProof;
	@SerializedName("cb_aspf")
	private String cbEkycCommAddressProof;
	@SerializedName("cb_saca")
	private String cbSameAsCommAddress;
	@SerializedName("cb_sapa")
	private String cbSameAsPerAddress;


	private String aadhaarApplicantName;

	private String aadhaarApplicantFatherName;

	/*public boolean isAadhaar() {
		return isAadhaar;
	}*/

	public void setAadhaar(boolean aadhaar) {
		if(aadhaar) {
			isAadhaar = "Y";
		} else {
			isAadhaar = "N";
		}
	}

	public String getUidTokenNo() {
		return uidTokenNo;
	}

	public void setUidTokenNo(String uidTokenNo) {
		this.uidTokenNo = uidTokenNo;
	}

	public String getApplicationPrefix() {
		return applicationPrefix;
	}

	public void setApplicationPrefix(String applicationPrefix) {
		this.applicationPrefix = applicationPrefix;
	}

	public String getApplicantFirstName() {
		return applicantFirstName;
	}

	public void setApplicantFirstName(String applicantFirstName) {
		this.applicantFirstName = applicantFirstName;
	}

	public String getApplicantMiddleName() {
		return applicantMiddleName;
	}

	public void setApplicantMiddleName(String applicantMiddleName) {
		this.applicantMiddleName = applicantMiddleName;
	}

	public String getApplicantLastName() {
		return applicantLastName;
	}

	public void setApplicantLastName(String applicantLastName) {
		this.applicantLastName = applicantLastName;
	}

	public String getApplicantDob() {
		return applicantDob;
	}

	public void setApplicantDob(String applicantDob) {
		this.applicantDob = applicantDob;
	}

	public String getApplicantGender() {
		return applicantGender;
	}

	public void setApplicantGender(String applicantGender) {
		this.applicantGender = applicantGender;
	}

	public String getApplicantFatherPrefix() {
		return applicantFatherPrefix;
	}

	public void setApplicantFatherPrefix(String applicantFatherPrefix) {
		this.applicantFatherPrefix = applicantFatherPrefix;
	}

	public String getApplicantFatherFirstName() {
		return applicantFatherFirstName;
	}

	public void setApplicantFatherFirstName(String applicantFatherFirstName) {
		this.applicantFatherFirstName = applicantFatherFirstName;
	}

	public String getApplicantFatherMiddleName() {
		return applicantFatherMiddleName;
	}

	public void setApplicantFatherMiddleName(String applicantFatherMiddleName) {
		this.applicantFatherMiddleName = applicantFatherMiddleName;
	}

	public String getApplicantFatherLastName() {
		return applicantFatherLastName;
	}

	public void setApplicantFatherLastName(String applicantFatherLastName) {
		this.applicantFatherLastName = applicantFatherLastName;
	}

	public String getCommAddressLine1() {
		return commAddressLine1;
	}

	public void setCommAddressLine1(String commAddressLine1) {
		this.commAddressLine1 = commAddressLine1;
	}

	public String getCommAddressLine2() {
		return commAddressLine2;
	}

	public void setCommAddressLine2(String commAddressLine2) {
		this.commAddressLine2 = commAddressLine2;
	}

	public String getCommAddressLine3() {
		return commAddressLine3;
	}

	public void setCommAddressLine3(String commAddressLine3) {
		this.commAddressLine3 = commAddressLine3;
	}

	public String getCommLandmark() {
		return commLandmark;
	}

	public void setCommLandmark(String commLandmark) {
		this.commLandmark = commLandmark;
	}

	public String getCommPinCode() {
		return commPinCode;
	}

	public void setCommPinCode(String commPinCode) {
		this.commPinCode = commPinCode;
	}

	public String getCommCity() {
		return commCity;
	}

	public void setCommCity(String commCity) {
		this.commCity = commCity;
	}

	public String getCommState() {
		return commState;
	}

	public void setCommState(String commState) {
		this.commState = commState;
	}

	public String getCommCountry() {
		return commCountry;
	}

	public void setCommCountry(String commCountry) {
		this.commCountry = commCountry;
	}

	public String getPermAddressLine1() {
		return permAddressLine1;
	}

	public void setPermAddressLine1(String permAddressLine1) {
		this.permAddressLine1 = permAddressLine1;
	}

	public String getPermAddressLine2() {
		return permAddressLine2;
	}

	public void setPermAddressLine2(String permAddressLine2) {
		this.permAddressLine2 = permAddressLine2;
	}

	public String getPermAddressLine3() {
		return permAddressLine3;
	}

	public void setPermAddressLine3(String permAddressLine3) {
		this.permAddressLine3 = permAddressLine3;
	}

	public String getPermLandmark() {
		return permLandmark;
	}

	public void setPermLandmark(String permLandmark) {
		this.permLandmark = permLandmark;
	}

	public String getPermPinCode() {
		return permPinCode;
	}

	public void setPermPinCode(String permPinCode) {
		this.permPinCode = permPinCode;
	}

	public String getPermCity() {
		return permCity;
	}

	public void setPermCity(String permCity) {
		this.permCity = permCity;
	}

	public String getPermState() {
		return permState;
	}

	public void setPermState(String permState) {
		this.permState = permState;
	}

	public String getPermCountry() {
		return permCountry;
	}

	public void setPermCountry(String permCountry) {
		this.permCountry = permCountry;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getCbEkycIdProof() {
		return cbEkycIdProof;
	}

	public void setCbEkycIdProof(String cbEkycIdProof) {
		this.cbEkycIdProof = cbEkycIdProof;
	}

	public String getCbEkycCommAddressProof() {
		return cbEkycCommAddressProof;
	}

	public void setCbEkycCommAddressProof(String cbEkycCommAddressProof) {
		this.cbEkycCommAddressProof = cbEkycCommAddressProof;
	}

	public String getCbSameAsCommAddress() {
		return cbSameAsCommAddress;
	}

	public void setCbSameAsCommAddress(String cbSameAsCommAddress) {
		this.cbSameAsCommAddress = cbSameAsCommAddress;
	}

	public String getCbSameAsPerAddress() {
		return cbSameAsPerAddress;
	}

	public void setCbSameAsPermAddress(String cbSameAsPerAddress) {
		this.cbSameAsPerAddress = cbSameAsPerAddress;
	}


	public String getAadhaarApplicantName() {
		return aadhaarApplicantName;
	}

	public void setAadhaarApplicantName(String aadhaarApplicantName) {
		this.aadhaarApplicantName = aadhaarApplicantName;
	}

	public String getAadhaarApplicantFatherName() {
		return aadhaarApplicantFatherName;
	}

	public void setAadhaarApplicantFatherName(String aadhaarApplicantFatherName) {
		this.aadhaarApplicantFatherName = aadhaarApplicantFatherName;
	}


	public String getIdProofDocName() {
		return idProofDocName;
	}

	public void setIdProofDocName(String idProofDocName) {
		this.idProofDocName = idProofDocName;
	}

	public String getAddressProofDocName() {
		return addressProofDocName;
	}

	public void setAddressProofDocName(String addressProofDocName) {
		this.addressProofDocName = addressProofDocName;
	}


	//	@SerializedName("")
	private ProofData proofData;

	public ProofData getProofData() {
		return proofData;
	}

	public static class ProofData {
		//		@SerializedName("op")
		private String option;

		//		@SerializedName("field")
		private JsonObject wrapper = new JsonObject();

		public String getOption() {
			return option;
		}

		public void setOption(String option) {
			this.option = option;
		}

		public JsonObject getWrapper() {
			return wrapper;
		}

		public void setWrapper(JsonObject wrapper) {
			this.wrapper = wrapper;
		}
	}

	@SerializedName("ale1")
	private String aadhaarAddressLine1;
	@SerializedName("ale2")
	private String aadhaarAddressLine2;
	@SerializedName("ale3")
	private String aadhaarAddressLine3;
	@SerializedName("alk")
	private String aadhaarLandmark;
	@SerializedName("ape")
	private String aadhaarPinCode;
	@SerializedName("acy")
	private String aadhaarCity;
	@SerializedName("ase")
	private String aadhaarState;
	@SerializedName("acty")
	private String aadhaarCountry;

	public String getAadhaarAddressLine1() {
		return aadhaarAddressLine1;
	}

	public void setAadhaarAddressLine1(String aadhaarAddressLine1) {
		this.aadhaarAddressLine1 = aadhaarAddressLine1;
	}

	public String getAadhaarAddressLine2() {
		return aadhaarAddressLine2;
	}

	public void setAadhaarAddressLine2(String aadhaarAddressLine2) {
		this.aadhaarAddressLine2 = aadhaarAddressLine2;
	}

	public String getAadhaarAddressLine3() {
		return aadhaarAddressLine3;
	}

	public void setAadhaarAddressLine3(String aadhaarAddressLine3) {
		this.aadhaarAddressLine3 = aadhaarAddressLine3;
	}

	public String getAadhaarLandmark() {
		return aadhaarLandmark;
	}

	public void setAadhaarLandmark(String aadhaarLandmark) {
		this.aadhaarLandmark = aadhaarLandmark;
	}

	public String getAadhaarPinCode() {
		return aadhaarPinCode;
	}

	public void setAadhaarPinCode(String aadhaarPinCode) {
		this.aadhaarPinCode = aadhaarPinCode;
	}

	public String getAadhaarCity() {
		return aadhaarCity;
	}

	public void setAadhaarCity(String aadhaarCity) {
		this.aadhaarCity = aadhaarCity;
	}

	public String getAadhaarState() {
		return aadhaarState;
	}

	public void setAadhaarState(String aadhaarState) {
		this.aadhaarState = aadhaarState;
	}

	public String getAadhaarCountry() {
		return aadhaarCountry;
	}

	public void setAadhaarCountry(String aadhaarCountry) {
		this.aadhaarCountry = aadhaarCountry;
	}


	public String getIdProofDocNumber() {
		return idProofDocNumber;
	}

	public void setIdProofDocNumber(String idProofDocNumber) {
		this.idProofDocNumber = idProofDocNumber;
	}

	public String getIdProofDocIssuingAuthority() {
		return idProofDocIssuingAuthority;
	}

	public void setIdProofDocIssuingAuthority(String idProofDocIssuingAuthority) {
		this.idProofDocIssuingAuthority = idProofDocIssuingAuthority;
	}

	public String getIdProofDocIssuingDate() {
		return idProofDocIssuingDate;
	}

	public void setIdProofDocIssuingDate(String idProofDocIssuingDate) {
		this.idProofDocIssuingDate = idProofDocIssuingDate;
	}


	public String getIdProofIssuingExpireDate() {
		return idProofIssuingExpireDate;
	}

	public void setIdProofIssuingExpireDate(String idProofIssuingExpireDate) {
		this.idProofIssuingExpireDate = idProofIssuingExpireDate;
	}

	public String getAddressProofDocNumber() {
		return addressProofDocNumber;
	}

	public void setAddressProofDocNumber(String addressProofDocNumber) {
		this.addressProofDocNumber = addressProofDocNumber;
	}

	public String getAddressProofDocIssuingAuthority() {
		return addressProofDocIssuingAuthority;
	}

	public void setAddressProofDocIssuingAuthority(String addressProofDocIssuingAuthority) {
		this.addressProofDocIssuingAuthority = addressProofDocIssuingAuthority;
	}

	public String getAddressProofDocExpiryDate() {
		return addressProofDocExpiryDate;
	}

	public void setAddressProofDocExpiryDate(String addressProofDocExpiryDate) {
		this.addressProofDocExpiryDate = addressProofDocExpiryDate;
	}



	/********************************Pan Details*************************************/
	@SerializedName("pn")
	private String panNumber;
	@SerializedName("aci")
	private String agriCultureIncome;
	@SerializedName("naci")
	private String nonAgriCultureIncome;
	@SerializedName("ar")
	private String acknowledgementNumber;
	@SerializedName("ad")
	private String applicationDate;
	@SerializedName("ic")
	private String isIncomeMoreThan5Lac;
	@SerializedName("ispn")
	private String isPanAvailable;

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public String getAgriCultureIncome() {
		return agriCultureIncome;
	}

	public void setAgriCultureIncome(String agriCultureIncome) {
		this.agriCultureIncome = agriCultureIncome;
	}

	public String getNonAgriCultureIncome() {
		return nonAgriCultureIncome;
	}

	public void setNonAgriCultureIncome(String nonAgriCultureIncome) {
		this.nonAgriCultureIncome = nonAgriCultureIncome;
	}

	public String getAcknowledgementNumber() {
		return acknowledgementNumber;
	}

	public void setAcknowledgementNumber(String acknowledgementNumber) {
		this.acknowledgementNumber = acknowledgementNumber;
	}

	public String getApplicationDate() {
		return applicationDate;
	}

	public void setApplicationDate(String applicationDate) {
		this.applicationDate = applicationDate;
	}

	public void setIncomeMoreThan5Lac(boolean incomeMoreThan5Lac) {
		if(incomeMoreThan5Lac) {
			isIncomeMoreThan5Lac = "Y";
		} else {
			isIncomeMoreThan5Lac = "N";
		}
	}


	public void setPanAvailable(boolean panAvailable) {
		if(panAvailable) {
			isPanAvailable = "Y";
		} else {
			isPanAvailable = "N";
		}
	}
}



