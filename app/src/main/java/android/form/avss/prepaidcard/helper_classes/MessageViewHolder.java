package android.form.avss.prepaidcard.helper_classes;

import android.content.Context;
import android.form.avss.prepaidcard.R;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MessageViewHolder {
    private View row;
    private TextView title, body;
    private Button cancel, done;
    private ImageView image;

    public MessageViewHolder(Context ctx) {
        this.row = LayoutInflater.from(ctx).inflate(R.layout.errror_dialog_box, null, false);
        title = (TextView) row.findViewById(R.id.err_title);
        image = (ImageView) row.findViewById(R.id.err_img);
        body = (TextView) row.findViewById(R.id.err_body);
        done = (Button) row.findViewById(R.id.err_ok_btn);
        cancel = (Button) row.findViewById(R.id.err_cancel);
        row.setTag(this);
    }

    public MessageViewHolder setTitle(String msg) {
        title.setText(msg);
        return this;
    }

    public MessageViewHolder setTitle(@StringRes int title) {
        this.title.setText(title);
        return this;
    }

    public MessageViewHolder setBody(String msg) {
        body.setText(msg);
        return this;
    }

    public MessageViewHolder setIcon(@DrawableRes int drawableId) {
        image.setImageResource(drawableId);
        return this;
    }

    public MessageViewHolder setIcon(BitmapDrawable drawable) {
        image.setImageDrawable(drawable);
        return this;
    }

    public MessageViewHolder setDone(String msg, View.OnClickListener l) {
        done.setText(msg);
        done.setOnClickListener(l);
        return this;
    }

    public MessageViewHolder setCancel(String msg, View.OnClickListener l) {
        cancel.setText(msg);
        cancel.setOnClickListener(l);
        cancel.setVisibility(View.VISIBLE);
        return this;
    }

    public View getRoot() {
        return row;
    }

    public MessageViewHolder setBody(int body) {
        this.body.setText(body);
        return this;
    }
}
