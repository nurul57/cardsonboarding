package android.form.avss.prepaidcard.ihmf_form_fill.ui.ControlsView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.form.avss.prepaidcard.R;
import android.form.avss.prepaidcard.ihmf_form_fill.TempPackage.GroupControls;
import android.form.avss.prepaidcard.ihmf_form_fill.util.DragTouchListener;
import android.form.avss.prepaidcard.ihmf_form_fill.interfaces.FocusListener;
import android.form.avss.prepaidcard.ihmf_form_fill.interfaces.OnLongPressListener;
import android.form.avss.prepaidcard.ihmf_form_fill.interfaces.iHMFView;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.controls.BooleanControl;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.controls.iHMFGroupListener;
import android.form.avss.prepaidcard.ihmf_form_fill.util.ConstantsUtils;
import android.form.avss.prepaidcard.ihmf_form_fill.util.MySharePreference;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Checkable;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.JsonObject;



/**
 * Simple Checkbox with can be act as GroupControl
 */
public class IHMFBooleanView extends ImageView implements View.OnClickListener, Checkable, iHMFView<BooleanControl> {


    private OnTouchListener mTouchListener;
    private final DragTouchListener DRAG_LISTENER = new DragTouchListener();
    private JsonObject jsonObject;
    private BooleanControl mControl;
    private boolean mChecked;
    private OnLongPressListener mLongPressListener;
    private iHMFGroupListener<Boolean> mGroupListener;
    private GroupControls mGroup;
    private boolean mError = false;
    private MySharePreference mSharePreference;

    public IHMFBooleanView(Context context) {
        this(context, null);
    }

    public IHMFBooleanView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public IHMFBooleanView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public IHMFBooleanView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
//        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
//        color = ContextCompat.getColor(getContext(), R.color.colorPrimary);
        init();
    }

    private void init() {
        if (getContext() instanceof OnLongPressListener)
            mLongPressListener = (OnLongPressListener) getContext();
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {


        if (mControl.isMarkedControl()) {
            setMeasuredDimension(getControl().getScaleWidth().intValue(), getControl().getScaleHeight().intValue());
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }

    }


    @Override
    public BooleanControl getControl() {
        if (mControl == null)
            mControl = new BooleanControl();
        return mControl;
    }


//    public boolean isErrorEnabled() {
//        return mError;
//    }

    public void setErrorEnable(boolean mError) {
        this.mError = mError;
//        postInvalidate();

        setBackground(mError ? getResources().getDrawable(R.drawable.shape_rectangle_2dp_red_stroke)
                : getResources().getDrawable(R.drawable.shape_rectangle_2dp_balck_stroke));

    }




    @Override
    public void setControl(BooleanControl control, JsonObject jsonObject, FocusListener focusListener) {
        this.mControl = control;
        this.jsonObject = jsonObject;
        getControl();

        setX((control.getX() * mControl.getRatio().x));
        setY((control.getY() * mControl.getRatio().y));

        setOnClickListener(this);
        setChecked(control.isChecked());

        if(getControl().isMarkedControl()){
            setEnabled(!getControl().isLock());
        }


        if (!mControl.isMarkedControl()) {
            setOnLongClickListener(new OnLongClickListener() {
                @SuppressLint("ClickableViewAccessibility")
                @Override
                public boolean onLongClick(View v) {
                    if (mTouchListener == null)
                        mTouchListener = DRAG_LISTENER;

                    IHMFBooleanView.this.setOnTouchListener(mTouchListener);

                    if (mLongPressListener != null)
                        mLongPressListener.onLongPressed(IHMFBooleanView.this);
                    return false;
                }
            });
        }
    }

    @Override
    public boolean save() {
        if (getPreference().shouldCheckMandatoryFields() && getControl().isMandatory() && getControl().hasGroup() && !isChecked() ) {
            if (mGroup != null && !mGroup.isItemChecked()) {
                setErrorEnable(true);
                return false;
            }
        }

        getControl().setIsProcessed(isChecked());
        if (jsonObject != null) {
            jsonObject.addProperty("isProcessed", isChecked());
        }
        return true;
    }


    public void setOnGroupListener(iHMFGroupListener<Boolean> mListener) {
        this.mGroupListener = mListener;
    }

    @Override
    public boolean isValid() {

        GroupControls groupControls = null;
        if (getContext() != null && getControl().hasGroup())
            groupControls = getGroup();


        return groupControls != null && groupControls.isItemChecked();
    }

    @Override
    public MySharePreference getPreference() {
        if(mSharePreference == null){
            mSharePreference = new MySharePreference(getContext());
        }
        return mSharePreference;
    }


    public GroupControls getGroup() {

        if (mGroup == null)
            mGroup = mGroupListener == null ? null : mGroupListener.getGroupControl(getControl().getGroupId());

        return mGroup;
    }

    @Override
    public void onClick(View v) {
        toggle();
    }

    @Override
    public void setChecked(boolean checked) {
        if (checked == isChecked()) return;

        mChecked = checked;
        mError = false;
//        postInvalidate();

        setImageDrawable(isChecked() ? getResources().getDrawable(R.drawable.ic_tick_icon) : null);

    }

    @Override
    public boolean isChecked() {
        return mChecked;
    }

    @Override
    public void toggle() {
        setChecked(!isChecked());

        if (getControl().isMarkedControl() && getControl().hasGroup()) {

            if (getGroup() != null)
                getGroup().setChecked(getControl().getId());

        }
    }


    public void removeOnTouchListener() {
        setOnTouchListener(null);
        mTouchListener = null;
    }

    @Override
    public View getView() {
        return this;
    }

    @Override
    public void resizeView(int resizeType) {
        if (resizeType == ConstantsUtils.RESIZE_TYPE_INCREASE) {
            this.setScaleX((this.getScaleX() + 0.05f));
            this.setScaleY((this.getScaleY() + 0.05f));
        } else {
            if (this.getScaleX() > 0.3f) {
                this.setScaleX((this.getScaleX() - 0.05f));
                this.setScaleY((this.getScaleY() - 0.05f));
            } else
                Toast.makeText(getContext(), R.string.size_cant_reduce_further, Toast.LENGTH_SHORT).show();
        }
    }


    public boolean contains(String groupId) {
        GroupControls controls = getGroup();

        if (TextUtils.isEmpty(groupId) || controls == null ||controls.getViewList().isEmpty()) return false;

        for (View v : controls.getViewList()) {

            if (!(v instanceof iHMFView)) continue;

            if (((iHMFView) v).getControl().hasGroup() && ((iHMFView) v).getControl().getGroupId().equals(groupId))
                return true;
        }

        return false;
    }







    //    @Override
//    protected void onDraw(Canvas canvas) {
//
//
//        if (isChecked()) {
//            paint.setColor(color);
//            paint.setStyle(Paint.Style.FILL);
//            canvas.drawColor(paint.getColor());
//        } else {
//            paint.setColor(isErrorEnabled() ? getResources().getColor(R.color.error_solid) : Color.BLACK);
//            paint.setStyle(isErrorEnabled() ? Paint.Style.FILL : Paint.Style.STROKE);
//            if (isErrorEnabled()) {
//                canvas.drawColor(paint.getColor());
//            } else {
//                paint.setStrokeWidth(2f);
//                canvas.drawRect(0, 0, getWidth(), getHeight(), paint);
//            }
//        }
//    }
}
