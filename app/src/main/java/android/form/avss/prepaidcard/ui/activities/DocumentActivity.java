package android.form.avss.prepaidcard.ui.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.form.avss.prepaidcard.R;
import android.form.avss.prepaidcard.helper_classes.Constant;
import android.form.avss.prepaidcard.ui.adapters.DocumentDrawerAdapter;
import android.form.avss.prepaidcard.ui.custom_classes.interfaces.RecyclerViewClickListener;
import android.form.avss.prepaidcard.ui.fragments.DocumentFragment;
import android.form.avss.prepaidcard.ui.fragments.doc_model.DocsDataList;
import android.form.avss.prepaidcard.ui.fragments.doc_model.SelectedImage;
import android.form.avss.prepaidcard.ui.model.DocImageViewModel;
import android.form.avss.prepaidcard.utils.PermissionUtils;
import android.form.avss.prepaidcard.utils.UtilityClass;
import android.form.avss.prepaidcard.web_services.model.Services;
import android.form.avss.prepaidcard.web_services.pojo.ImageOption;
import android.form.avss.prepaidcard.web_services.requests.DocsUploadRequest;
import android.form.avss.prepaidcard.web_services.requests.DocumentDataRequest;
import android.form.avss.prepaidcard.web_services.responses.DocumentDataResponse;
import android.form.avss.prepaidcard.web_services.responses.DocumentUploadResponse;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class DocumentActivity extends BaseActivity implements DocumentFragment.DocumentFragmentInteraction,
        Services.GetDocumentData, Services.UploadDocumentData {

    public RecyclerView rvDrawer;
    public FrameLayout fl_images;
    public TextView tvDisplayDocName, tvDisplayImageCount;
    public DocumentFragment currentFragment;
    private FragmentManager fm;
    private Constant constant;
    public DocumentDrawerAdapter docAdapter;
    private List<ImageOption> optionList;
    private List<DocumentFragment> docFragList;
    public boolean isAadhaar = false;
    public boolean isPanAvailable = false;
    private String token;
    private String referenceId;
    private String bdeNo;
    private ProgressDialog progressDialog;
    private DocImageViewModel docModel;
    private boolean isAllDocCaptured = false;
    private int pos = 0;
    private boolean isGetFetchDocData = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_document);

        if (getIntent() == null) {
            toast("Something went wrong");
            finish();
            return;
        }

        Intent intent = getIntent();
        isAadhaar = intent.getBooleanExtra(IS_AADHAAR, false);
        isPanAvailable = intent.getBooleanExtra(IS_PAN_AVAILABLE, false);

        constant = Constant.getInstance();
        optionList = new ArrayList<>();
        docFragList = new ArrayList<>();

        if (PermissionUtils.isOSVersionMorHigher()) {
            PermissionUtils.permissionGranted(DocumentActivity.this, cameraPermission, CAMERA_PERMISSION);
            if (!PermissionUtils.isGranted(DocumentActivity.this, cameraPermission)) {
                toast("Permission is granted");
            } else {
                callFetchDocumentDataService();
            }
        } else {
            callFetchDocumentDataService();
        }

        initViews();
    }

    private void callFetchDocumentDataService() {
        if (isGetFetchDocData) {
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(this);
            }
            progressDialog.setTitle(R.string.app_name);
            progressDialog.setMessage("Fetching Document Data...");
            progressDialog.show();

            bdeNo = getIntent().getStringExtra(BDE_NO);
            token = getIntent().getStringExtra(TOKEN);
            referenceId = getIntent().getStringExtra(REFERENCE_ID);

            DocumentDataRequest request = new DocumentDataRequest();
            request.setBdeNo(bdeNo);
            request.setToken(token);
            request.setReferenceId(referenceId);
            new Services().getDocumentData(request, DocumentActivity.this, token, referenceId);
        }
    }

    private void initViews() {
        tvDisplayDocName = (TextView) findViewById(R.id.tvDisplayDocName);
        tvDisplayImageCount = (TextView) findViewById(R.id.tvDisplayImageCount);
        fl_images = (FrameLayout) findViewById(R.id.fl_images);
        rvDrawer = (RecyclerView) findViewById(R.id.rvDocumentDrawer);
    }

    public void openFragment(final int position) {
        if (position < 0) return;

        if (!optionList.isEmpty()) {
            ImageOption option = optionList.get(position);
            tvDisplayImageCount.setText(UtilityClass.docImageCaptureInfo(option.getMin(), option.getMax()));

            if (docModel == null) {
                docModel = new DocImageViewModel();
            }

            Bundle bundle = new Bundle();
            bundle.putSerializable(IMAGE_OPTION, option);
            bundle.putInt(FRAGMENT_POSITION, position);
            bundle.putString(REFERENCE_ID, referenceId);
            bundle.putString(DOC_SECTION_DATA, constant.toJson(docModel));

            currentFragment = DocumentFragment.newInstance();
            currentFragment.setArguments(bundle);

            String tag = optionList.get(position).getDocName();
            fm.findFragmentByTag(tag);
            fm.beginTransaction().replace(R.id.fl_images, currentFragment, tag)
                    .addToBackStack(tag).commit();

            if (!docFragList.contains(currentFragment)) {
                for (ImageOption doc : docAdapter.documentList) {
                    doc.setSelectedItem(false);
                }
                docAdapter.documentList.get(position).setSelectedItem(true);
                docAdapter.notifyDataSetChanged();
                docFragList.add(position, currentFragment);
            }
        }
    }

    @Override
    public void setDocModelData(DocImageViewModel docModelData, final int fragPosition) {
        docModel = docModelData;
        docAdapter.documentList.get(fragPosition).setDataCaptured(true);
        docAdapter.notifyDataSetChanged();
    }

    @Override
    public void openNextDocFragment(int position) {
        if (optionList == null || position < 0) return;

        if (position >= optionList.size()) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this)
                    .setTitle(R.string.app_name)
                    .setMessage("You have captured all the images. Do you want to review it ?")
                    .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                isAllDocCaptured = false;
                                callDocUploadService(pos);
                            } catch (IOException e) {
                                e.printStackTrace();
                            } catch (Exception e) {
                                e.getMessage();
                            }
                        }
                    })
                    .setNegativeButton("Review", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            isAllDocCaptured = true;
                        }
                    });

            builder.create().show();
        } else {

            openFragment(position);
        }
    }

    private void callDocUploadService(int position) throws IOException {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(DocumentActivity.this);
        }
        progressDialog.setCancelable(false);
        progressDialog.setTitle(R.string.app_name);
        progressDialog.setMessage("Please wait submitting the document");
        progressDialog.show();

        DocsUploadRequest request = new DocsUploadRequest();
        request.setBdeNo(getIntent().getStringExtra(BDE_NO));
        request.setToken(getIntent().getStringExtra(TOKEN));
        request.setReferenceId(getIntent().getStringExtra(REFERENCE_ID));

        DocsDataList dataList = docModel.get(position);
        request.setImageName(dataList.getDocName());
        ArrayList<String> imageData = new ArrayList<>();
        for (SelectedImage imagePath : dataList.getImages()) {
            imageData.add(UtilityClass.getByteArrayOfImage(this, imagePath.getImgPath()));
        }
        request.setImagesList(imageData);

        new Services().uploadDocData(request, this, token, referenceId);
    }

    private void recyclerViewSetUp() {
        fm = getSupportFragmentManager();

        //recycler view setup
        rvDrawer.setHasFixedSize(true);
        rvDrawer.setLayoutManager(new LinearLayoutManager(this));

        RecyclerViewClickListener listener = new RecyclerViewClickListener() {
            @Override
            public void onClick(View v, int position) {
                if (isAllDocCaptured) {
                    openFragment(position);
                }
            }
        };

        docAdapter = new DocumentDrawerAdapter(optionList, listener);
        rvDrawer.setAdapter(docAdapter);
        openFragment(0);
    }

    @Override
    public void getDocumentDataSuccessful(DocumentDataResponse.DocImageData data) {
        progressDialog.dismiss();
        docModel = new DocImageViewModel();
        if (data.isSuccess()) {
            isGetFetchDocData = false;
            optionList = data.data();

            if (optionList != null) {
                for (int i = 0; i < optionList.size(); i++) {
                    DocsDataList docsDataList = new DocsDataList();
                    docModel.setFinalDocList(i, docsDataList);
                }
                recyclerViewSetUp();
            } else {
                //callFetchDocumentDataService();
                showDialogOfGetData(data.msg());
            }
        } else {
             showDialogOfGetData(data.msg());
        }
    }

    @Override
    public void getDocumentDataFailed(boolean status, String error) {
        progressDialog.dismiss();
        if (!status) {
            showDialogOfGetData(error);
        }
    }

    @Override
    public void uploadDocumentDataSuccessful(DocumentUploadResponse.UploadDocument data) {
        progressDialog.dismiss();
        int size = docModel.size();
        if (data.isSuccess()) {
            if (pos < size - 1) {
                pos++;
                try {
                    callDocUploadService(pos);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.getMessage();
                }

            } else {
                toast("Data is successfully submitted");
                Intent intent = new Intent(DocumentActivity.this, AllModuleDashboardActivity.class);
                intent.putExtra(BDE_NO, bdeNo);
                startActivity(intent);
                finish();
            }

        } else {
            showDialogOfUploadData(pos, data.msg());
        }
    }

    private void showDialogOfUploadData(final int position, String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle(R.string.app_name)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            callDocUploadService(position);
                        } catch (IOException e) {
                            e.getMessage();
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }
                })

                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        pos = 0;
                    }
                });
        builder.create().show();
    }

    @Override
    public void uploadDocumentDataFailed(boolean status, String error) {
        progressDialog.dismiss();
        if (!status) {
            showDialogOfGetData(error);
        }
    }


    public void showDialogOfGetData(String errorMessage) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
            .setTitle(R.string.app_name)
            .setMessage(errorMessage)
            .setCancelable(false)
            .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    callFetchDocumentDataService();
                }
            })
            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
        builder.create().show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CAMERA_PERMISSION) {
            boolean allGranted = false;
            if (permissions.length > 0 && grantResults.length > 0 ) {
                for (int result : grantResults) {
                    allGranted = result == PackageManager.PERMISSION_GRANTED;
                    if (!allGranted) break;
                }

                boolean isShouldShowRational = PermissionUtils.
                        isShouldShowRequestPermissionRationale(DocumentActivity.this, permissionList);

                if (allGranted) {
                    PermissionUtils.proceedAfterPermission(DocumentActivity.this, true);
                } else if (isShouldShowRational) {
                    ActivityCompat.requestPermissions(DocumentActivity.this, permissionList, CAMERA_PERMISSION);
                } else {
                    toast(getString(R.string.err_permissions));
                    PermissionUtils.openSettings(DocumentActivity.this, permissionList, CAMERA_PERMISSION);
                }
            }
        }
    }

    @Override
    public void onBackPressed() {}
}


