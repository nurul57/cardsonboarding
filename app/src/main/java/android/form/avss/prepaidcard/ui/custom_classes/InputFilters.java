package android.form.avss.prepaidcard.ui.custom_classes;

import android.text.InputFilter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;

import java.util.regex.Pattern;

public class InputFilters {
    public static final String BASIC_ADDRESS_REX = "^([a-zA-Z0-9\\(\\)\\[\\]\\.\\,\\#\\-\\/]*\\s)*[a-zA-Z0-9\\(\\)\\[\\]\\.\\,\\#\\-\\/]*$";
    private static final String BASIC_ALPHABET = "^[a-zA-Z ]+$";
    private static final String BASIC_ALPHA_NUMERIC = "^[A-Za-z0-9 ]+$";

    public static InputFilter if_Character = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dStart, int dEnd) {

            boolean keepOriginal = true;
            StringBuilder sb = new StringBuilder(end - start);

            for (int i = start; i < end; i++) {
                char c = source.charAt(i);

                if (Character.isWhitespace(source.charAt(i))) {
                    if (dStart == 0 && !dest.toString().equals("")) {
                        return "";

                    } else if (dStart > 0) {
                        if (Character.isWhitespace(dest.charAt(dStart - 1))) {
                            return "";
                        }
                    }

                } else if (isCharAllowed(c)) {
                    sb.append(c);

                } else {
                    keepOriginal = false;
                }
            }

            if (keepOriginal) {
                return null;

            } else if (source instanceof Spanned) {
                SpannableString sp = new SpannableString(sb);
                TextUtils.copySpansFrom((Spanned) source, start, sb.length(), null, sp, 0);
                return sp;

            } else {
                return sb;
            }
        }

        private boolean isCharAllowed(char c) {
            return Pattern.matches(BASIC_ALPHABET, String.valueOf(c));
        }
    };

    public static InputFilter if_AlphaNumeric = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dStart, int dEnd) {
            boolean keepOriginal = true;
            StringBuilder sb = new StringBuilder(end - start);

            for (int i = start; i < end; i++) {
                char c = source.charAt(i);

                if (Character.isWhitespace(source.charAt(i))) {
                    if (dStart == 0 && !dest.toString().equals("")) {
                        return "";

                    }else if (dStart > 0) {
                        if (Character.isWhitespace(dest.charAt(dStart - 1))) {
                            return "";
                        }
                    }

                } else if (isCharAllowed(c)) {
                    sb.append(c);

                } else {
                    keepOriginal = false;
                }
            }

            if (keepOriginal) {
                return null;

            } else if (source instanceof Spanned) {
                SpannableString sp = new SpannableString(sb);
                TextUtils.copySpansFrom((Spanned) source, start, sb.length(), null, sp, 0);
                return sp;

            } else {
                return sb;
            }
        }

        private boolean isCharAllowed(char c) {
            return Pattern.matches(BASIC_ALPHA_NUMERIC, String.valueOf(c));
        }
    };

    public static InputFilter if_AlphaNumericSpecial = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dStart, int dEnd) {
            boolean keepOriginal = true;
            StringBuilder sb = new StringBuilder(end - start);

            for (int i = start; i < end; i++) {
                char c = source.charAt(i);

                if (Character.isWhitespace(source.charAt(i))) {
                    if (dStart == 0 && !dest.toString().equals("")) {
                        return "";

                    } else if (dStart > 0) {
                        if (Character.isWhitespace(dest.charAt(dStart - 1))) {
                            return "";
                        }
                    }

                } else if (isCharAllowed(c)) {
                    sb.append(c);

                } else {
                    keepOriginal = false;
                }
            }

            if (keepOriginal) {
                return null;

            } else if (source instanceof Spanned) {
                SpannableString sp = new SpannableString(sb);
                TextUtils.copySpansFrom((Spanned) source, start, sb.length(), null, sp, 0);
                return sp;

            } else {
                return sb;
            }
        }

        private boolean isCharAllowed(char c) {
            return Pattern.matches(BASIC_ADDRESS_REX, String.valueOf(c));
        }
    };
}
