package android.form.avss.prepaidcard.ihmf_form_fill.util;

import android.form.avss.prepaidcard.ihmf_form_fill.interfaces.FocusListener;
import android.form.avss.prepaidcard.ihmf_form_fill.interfaces.iHMFControl;
import android.form.avss.prepaidcard.ihmf_form_fill.interfaces.iHMFView;
import android.graphics.PointF;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.google.gson.JsonObject;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;

import static android.form.avss.prepaidcard.ihmf_form_fill.util.ConstantsUtils.INVALID_DATA;

public class FormUtil {


    public static Disposable drawControl(List<JsonObject> controls, final ViewGroup viewGroup, final PointF point
            , final onViewCreated viewCallback, final FocusListener focusListener) {


        final View NO_VIEW = new View(viewGroup.getContext());

        return Observable.just(controls)
                .subscribeOn(Schedulers.computation())
                .flatMapIterable(new Function<List<JsonObject>, Iterable<? extends JsonObject>>() {
                    @Override
                    public Iterable<? extends JsonObject> apply(List<JsonObject> list) {
                        return list;
                    }
                })
                .map(new Function<JsonObject, View>() {
                    @Override
                    public View apply(JsonObject obj) {
                        View v = iHMFUtility.getView(viewGroup, obj, point, focusListener);

                        return v == null ? NO_VIEW : v;
                    }
                })
                .filter(new Predicate<View>() {
                    @Override
                    public boolean test(View view) {
                        return view instanceof iHMFView;
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<View>() {
                    @Override
                    public void accept(View view) {

                        iHMFView control = (iHMFView) view;

                        int width;
                        int height;


                        width = (int) iHMFUtility.getScale(control.getControl().getW(), point.x);
                        height = (int) iHMFUtility.getScale(control.getControl().getH(), point.y);
//
//                    if(!control.getControl().isMarkedControl()){
//                        width = ViewGroup.LayoutParams.WRAP_CONTENT;
//                        height = ViewGroup.LayoutParams.WRAP_CONTENT;
//                    }
//                    else {
//                        width = (int) iHMFUtility.getScale(control.getControl().getW(), point.x);
//                        height = (int) iHMFUtility.getScale(control.getControl().getH(), point.y);
//                    }

                        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(width, height);

                        if (viewCallback != null)
                            viewCallback.onViewDrawCompleted(view, control);


                        viewGroup.addView(view, params);

                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable e) {
                        e.printStackTrace();
                    }
                });

    }


    public interface onViewCreated {

        void onViewDrawCompleted(View view, iHMFView control);
    }


    public static void validateAndSaveData(ViewGroup viewGroup){

        boolean isDataValid = true;
        iHMFControl<?> control;

        for (int i = 0; i < viewGroup.getChildCount(); i++) {

            View view = viewGroup.getChildAt(i);

            if (!(view instanceof iHMFControl<?>)) continue;

            control = (iHMFControl<?>) view;
            if(!control.save())
                isDataValid = false;
        }

        if(!isDataValid)
            throw new IllegalArgumentException(INVALID_DATA);
    }
}
