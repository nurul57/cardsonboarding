package android.form.avss.prepaidcard.ihmf_form_fill.util;


import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class ValidationUtils {

    public static boolean isDataValid(String input, String pattern){
        if(pattern == null || pattern.isEmpty()) return true;
        return makeMatch(input, pattern);
    }


    private static boolean makeMatch(String input, String regex){
        Pattern mPattern = Pattern.compile(regex);
        Matcher matcher = mPattern.matcher(input);
        return matcher.matches();
    }


    public final class DataType {
        public static final String AADHAAR_NUM = "aadhar";
        public static final String PINCODE = "pincode";
        public static final String CURRENCY = "currency";
        public static final String MOBILE_NUM = "mobileNo";
        public static final String NUM_CONTINOUS = "nContinue";
        public static final String MOBILE_NUM_WITH_OTP = "mobileNoWithOTP";
        public static final String DATE = "date";


//        public static final String PANCARD_NUMBER = "panCard";
//        public static final String EMAIL = "emailId";
//        public static final String VOTER_ID = "voterId";
//        public static final String PASSPORT_NUMBER = "passportNo";
//        public static final String STRING = "String";
//        public static final String ALPHA_STR = "aString";
//        public static final String ALPHA_SPECIAL_CONTINOUS = "aSContinue";
//        public static final String ALPHA_SPECIAL_STR = "aSString";
//        public static final String NUM_SPECIAL_CONTINOUS = "nSContinue";
//        public static final String EMAIL_WITH_OTP = "emailIdWithOTP";
//        public static final String ALPHA_NUM_STR = "anString";
//        public static final String ALPHA_NUM_CONTINOUS = "anContinue";
//        public static final String NUM_STR = "nString";
//        public static final String ALPHA_NUM_SPECIAL_CONTINOUS = "anSContinue";
//        public static final String ALPHA_NUM_SPECIAL_STR = "anSString";
//        public static final String ALPHA_CONTINOUS = "aContinue";
//        public static final String NUM_SPECIAL_STR = "nSString";
//        public static final String ALPHA = "a";
//        public static final String CONTINOUS = "Continue";
//        public static final String SPECIAL_CHAR = "S";


    }
}
