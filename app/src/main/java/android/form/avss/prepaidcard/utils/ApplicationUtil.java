package android.form.avss.prepaidcard.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ApplicationInfo;
import android.form.avss.prepaidcard.R;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;


public final class ApplicationUtil {
    public static boolean checkDebuggable(Context context){
        return (context.getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0;
    }

    public static AlertDialog.Builder dialog(final Context ctx, String msg){
        return new AlertDialog.Builder(ctx)
                .setCancelable(false)
		        .setTitle(R.string.app_name)
                .setMessage(msg)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ((Activity) ctx).finish();
                    }
                });
    }

    public static AlertDialog.Builder dialog(final Context ctx, String tittle, String msg, String buttonText){
        return new AlertDialog.Builder(ctx)
                .setCancelable(false)
                .setTitle(tittle)
		        .setMessage(msg)
                .setPositiveButton(buttonText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ((Activity) ctx).finish();
                    }
                });
    }

    public static AlertDialog.Builder dialog(final Context ctx){
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ((Activity) ctx).finish();
                    }
                });
        LayoutInflater inflater = ((Activity) ctx).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_alert_dialog, null, false);
        builder.setView(dialogView);
        builder.create();
        return builder;
    }


    public static AlertDialog.Builder customDialog(final Context ctx, final String msg){
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ((Activity) ctx).finish();
                    }
                });

        LayoutInflater inflater = ((Activity) ctx).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_alert_dialog, null, false);
        ((TextView) dialogView.findViewById(R.id.tvMsg)).setText(msg);
        builder.setView(dialogView);
        builder.create();
        return builder;
    }
}
