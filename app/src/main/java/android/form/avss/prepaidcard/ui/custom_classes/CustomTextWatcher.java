package android.form.avss.prepaidcard.ui.custom_classes;

import android.text.Editable;
import android.text.TextWatcher;

public class CustomTextWatcher implements TextWatcher {
    private CustomTextInputLayout source, destination;

    private CustomTextInputLayout tvCommPinCode;
    private CustomTextInputLayout tvPermPinCode;
    private CustomTextInputLayout tvComCity;
    private CustomTextInputLayout tvPermCity;
    private CustomTextInputLayout tvCommState;
    private CustomTextInputLayout tvPermState;

    public CustomTextWatcher(CustomTextInputLayout source, CustomTextInputLayout destination) {
        this.source = source;
        this.destination = destination;
    }


    public CustomTextWatcher() {
    }

    @Override
    public void beforeTextChanged (CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged (CharSequence s, int start, int before, int count) {
        if ( source == null || destination == null) return;
        if (source.isNotEmpty()){
            destination.setText(source.getString());
            destination.setDisabled();
        }
    }

    @Override
    public void afterTextChanged (Editable s) {

    }

    public CustomTextInputLayout getTvCommPinCode() {
        return tvCommPinCode;
    }

    public void setTvCommPinCode(CustomTextInputLayout tvCommPinCode) {
        this.tvCommPinCode = tvCommPinCode;
    }

    public CustomTextInputLayout getTvPermPinCode() {
        return tvPermPinCode;
    }

    public void setTvPermPinCode(CustomTextInputLayout tvPermPinCode) {
        this.tvPermPinCode = tvPermPinCode;
    }

    public CustomTextInputLayout getTvComCity() {
        return tvComCity;
    }

    public void setTvComCity(CustomTextInputLayout tvComCity) {
        this.tvComCity = tvComCity;
    }

    public CustomTextInputLayout getTvPermCity() {
        return tvPermCity;
    }

    public void setTvPermCity(CustomTextInputLayout tvPermCity) {
        this.tvPermCity = tvPermCity;
    }

    public CustomTextInputLayout getTvCommState() {
        return tvCommState;
    }

    public void setTvCommState(CustomTextInputLayout tvCommState) {
        this.tvCommState = tvCommState;
    }

    public CustomTextInputLayout getTvPermState() {
        return tvPermState;
    }

    public void setTvPermState(CustomTextInputLayout tvPermState) {
        this.tvPermState = tvPermState;
    }
}
