package android.form.avss.prepaidcard.ihmf_form_fill.ui.Activities;

import android.form.avss.prepaidcard.R;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.controls.Control;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import io.reactivex.disposables.CompositeDisposable;

import static android.form.avss.prepaidcard.ihmf_form_fill.ui.Activities.MySignaturePadActivity.PATH;
import static android.form.avss.prepaidcard.ihmf_form_fill.util.ConstantsUtils.IMAGE_PATH;
import static android.form.avss.prepaidcard.ihmf_form_fill.util.ConstantsUtils.IMAGE_PREVIEW;
import static android.form.avss.prepaidcard.interfaces.Keys.CONTROL_ID;
import static android.form.avss.prepaidcard.interfaces.Keys.CONTROL_TYPE;
import static android.form.avss.prepaidcard.interfaces.Keys.RATIO;


public class CustomCameraActivity extends FormBaseActivity {

    private boolean mSurfaceTextureAvailable = false;
    private boolean mPermissionGranted = false;

    // static identifier for camera permission
    private static final int REQUEST_CAMERA_PERMISSION = 200;

    // button to click the picture
    private Button takePictureButton;

    // texture view for custom camera view
    private TextureView textureView;

    // camera device to use
    protected CameraDevice cameraDevice;

    // camera capture session variable
    protected CameraCaptureSession cameraCaptureSessions;

    // camera request builder
    protected CaptureRequest.Builder captureRequestBuilder;

    // the size for captured image
    private Size imageDimension;

    // image reader to reade image bytes
    private ImageReader imageReader;

    // background handler for background tasks
    private Handler mBackgroundHandler;

    // background thread
    private HandlerThread mBackgroundThread;

    // file for image
    private CompositeDisposable mDisposable;


    private String controlId;

    // sparse array for captured image orientation
    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();

    // static block to initialize the sparse array
    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 90);
        ORIENTATIONS.append(Surface.ROTATION_90, 0);
        ORIENTATIONS.append(Surface.ROTATION_180, 270);
        ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    private String controlType;
    private RelativeLayout shapeContainer;

    private ProgressBar pb_progressBar;
    private File file;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_camera);


        controlId = getIntent().getStringExtra(CONTROL_ID);

        textureView = (TextureView) findViewById(R.id.texture);
        assert textureView != null;
        textureView.setSurfaceTextureListener(textureListener);
        takePictureButton = (Button) findViewById(R.id.btn_TakePicture);
        pb_progressBar = (ProgressBar) findViewById(R.id.pb_progressBar);
        assert takePictureButton != null;
        takePictureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture();
            }
        });

        controlType = getIntent().getStringExtra(CONTROL_TYPE);

        shapeContainer = (RelativeLayout) findViewById(R.id.rl_shapeContainer);

        mDisposable = new CompositeDisposable();

        if (controlType.equalsIgnoreCase(Control.Signature.toString())) {

            Objects.requireNonNull(findViewById(R.id.v_faceShape)).setVisibility(View.GONE);

            float ratio = getIntent().getFloatExtra(RATIO, 0.0f);

            DisplayMetrics displayMetrics = getResources().getDisplayMetrics();

            float width = displayMetrics.widthPixels - 100;
            float height = width * ratio;
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams((int) width, (int) height);
            lp.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
            shapeContainer.setLayoutParams(lp);
            Objects.requireNonNull(findViewById(R.id.text_hint)).setVisibility(View.VISIBLE);

        }

    }

    private TextureView.SurfaceTextureListener textureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            //open your camera here
            //openCamera();

            mSurfaceTextureAvailable = true;
            if(ContextCompat.checkSelfPermission(CustomCameraActivity.this, Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(CustomCameraActivity.this, new String[] {Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
            } else {
                mPermissionGranted = true;
                // execute code after 500 millisecond
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        setUpCamera();
                    }
                }, 500);
            }
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
            // Transform you image captured size according to the surface width and height
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {
        }
    };

    private void setUpCamera() {
        if(mSurfaceTextureAvailable && mPermissionGranted) {
            openCamera();
        }
    }

    private final CameraDevice.StateCallback stateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(@NonNull CameraDevice camera) {
            cameraDevice = camera;

            ImageReader.OnImageAvailableListener readerListener = new ImageReader.OnImageAvailableListener() {
                @Override
                public void onImageAvailable(ImageReader reader) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pb_progressBar.setVisibility(View.VISIBLE);
                            takePictureButton.setEnabled(false);
                        }
                    });

                    try (Image image = reader.acquireLatestImage()) {
                        ByteBuffer buffer = image.getPlanes()[0].getBuffer();
                        byte[] bytes = new byte[buffer.capacity()];
                        buffer.get(bytes);
                        save(bytes);
                    } catch (Exception ex) {
                        showShortToast(ex.getMessage());
                        ex.printStackTrace();
                    }
                }

                @SuppressWarnings("all")
                private void save(byte[] bytes) {
                    if(file == null) {
                        String fileName = getSPreference().getFormId() + "_" + controlId + ".png";
                        file = new File(getControlImagesFilesDirectory(), fileName);
                    }
                    if (file.exists()) file.delete();

                    try (OutputStream outputStream = new FileOutputStream(file)) {
                        if(controlType.equalsIgnoreCase(Control.Signature.toString())){
                            Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

                            double widthRatio = (double) bitmap.getWidth() / getResources().getDisplayMetrics().widthPixels;
                            double heightRatio = (double) bitmap.getHeight() / getResources().getDisplayMetrics().heightPixels;

                            double imageX = shapeContainer.getX() * widthRatio;
                            double imageY = shapeContainer.getY() * heightRatio;

                            double imageWidth = shapeContainer.getWidth() * widthRatio;
                            double imageHeight = shapeContainer.getHeight() * heightRatio;

                            bitmap = Bitmap.createBitmap(bitmap, (int) imageX, (int) imageY, (int)imageWidth, (int) imageHeight);
                            bitmap.compress(Bitmap.CompressFormat.PNG, 80, outputStream);

                        }
                        else {
                            outputStream.write(bytes);
                        }

                        outputStream.flush();
                        outputStream.close();

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                pb_progressBar.setVisibility(View.GONE);
                                takePictureButton.setEnabled(true);
                            }
                        });
                        Intent intent = new Intent(CustomCameraActivity.this, CapturedImagePreviewActivity.class);
                        intent.putExtra(IMAGE_PATH, file.getAbsolutePath());
                        startActivityForResult(intent, IMAGE_PREVIEW);
                    } catch (Exception ex) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                pb_progressBar.setVisibility(View.GONE);
                                takePictureButton.setEnabled(true);
                            }
                        });
                        ex.printStackTrace();
                    }
                }

            };

            CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
            if(manager == null) return;
            CameraCharacteristics characteristics = null;
            try {
                characteristics = manager.getCameraCharacteristics(cameraDevice.getId());
            }
            catch (CameraAccessException e) {
                e.printStackTrace();
            }
            Size[] jpegSizes = null;
            if(characteristics != null) {
                jpegSizes = Objects.requireNonNull(characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)).getOutputSizes(ImageFormat.JPEG);
            }
            int width = 640;
            int height = 480;
            if(jpegSizes != null && 0 < jpegSizes.length) {
                width = jpegSizes[0].getWidth();
                height = jpegSizes[0].getHeight();
            }

            imageReader = ImageReader.newInstance(width, height, ImageFormat.JPEG, 1);
            imageReader.setOnImageAvailableListener(readerListener, mBackgroundHandler);

            createCameraPreview();
        }
        @Override
        public void onDisconnected(@NonNull CameraDevice camera) {
            cameraDevice.close();
        }
        @Override
        public void onError(@NonNull CameraDevice camera, int error) {
            cameraDevice.close();
            cameraDevice = null;
        }
    };


    protected void startBackgroundThread() {
        mBackgroundThread = new HandlerThread("Camera Background");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
    }

    protected void stopBackgroundThread() {
        mBackgroundThread.quitSafely();
        try {
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    protected void takePicture() {
        if(null == cameraDevice)
            return;

        try {
            captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
            captureRequestBuilder.addTarget(imageReader.getSurface());

            //Orientation
            int rotation = getWindowManager().getDefaultDisplay().getRotation();
            captureRequestBuilder.set(CaptureRequest.JPEG_ORIENTATION, ORIENTATIONS.get(rotation));

            final CameraCaptureSession.CaptureCallback captureListener = new CameraCaptureSession.CaptureCallback() {
                @Override
                public void onCaptureCompleted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull TotalCaptureResult result) {
                    super.onCaptureCompleted(session, request, result);
                    createCameraPreview();
                }
            };

            cameraCaptureSessions.capture(captureRequestBuilder.build(), captureListener, mBackgroundHandler);

        } catch (Exception e) {
            showShortToast(e.getMessage());
            e.printStackTrace();
        }
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == IMAGE_PREVIEW) {
            if (resultCode == RESULT_OK) {
                Intent intent = new Intent();
                intent.putExtra(PATH, file.getAbsolutePath());
                setResult(RESULT_OK, intent);
                finish();
            } else {
                if (file!=null && file.exists()) {
                    if (file.delete()) {
                        showShortToast("Capture the image again");
                    }
                }
            }

        }

    }

    protected void createCameraPreview() {
        try {
            SurfaceTexture texture = textureView.getSurfaceTexture();
            assert texture != null;
            texture.setDefaultBufferSize(imageDimension.getWidth(), imageDimension.getHeight());
            Surface surface = new Surface(texture);
            captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            captureRequestBuilder.addTarget(surface);
            List<Surface> outputSurfaces = new ArrayList<>(2);
            outputSurfaces.add(imageReader.getSurface());
            outputSurfaces.add(surface);
            cameraDevice.createCaptureSession(outputSurfaces, new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                    //The camera is already closed
                    if (null == cameraDevice) {
                        return;
                    }
                    // When the session is ready, we start displaying the preview.
                    cameraCaptureSessions = cameraCaptureSession;
                    updatePreview();
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
                    Toast.makeText(CustomCameraActivity.this, "Configuration change", Toast.LENGTH_SHORT).show();
                }
            }, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void openCamera() {
        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        if(manager == null) return;
        try {
            // camera id to use
            String cameraId = manager.getCameraIdList()[0];
            CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);
            StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            assert map != null;
            imageDimension = map.getOutputSizes(SurfaceTexture.class)[0];
            // Add permission for camera and let user grant the permission
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(CustomCameraActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA_PERMISSION);
                return;
            }
            manager.openCamera(cameraId, stateCallback, null);

            takePictureButton.setVisibility(View.VISIBLE);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    protected void updatePreview    () {
        if(null == cameraDevice) {
            showShortToast("null camera device");
            return;
        }
        captureRequestBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
        captureRequestBuilder.set(CaptureRequest.CONTROL_AE_LOCK, false);
        captureRequestBuilder.set(CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER, CameraMetadata.CONTROL_AE_PRECAPTURE_TRIGGER_START);
        captureRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE, CameraMetadata.CONTROL_AE_MODE_ON);
        captureRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER, CameraMetadata.CONTROL_AF_TRIGGER_START);
        captureRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CameraMetadata.CONTROL_AF_MODE_AUTO);
        captureRequestBuilder.set(CaptureRequest.CONTROL_AE_EXPOSURE_COMPENSATION, 6);
        try {
            cameraCaptureSessions.setRepeatingRequest(captureRequestBuilder.build(), null, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void closeCamera() {
        if (null != cameraDevice) {
            cameraDevice.close();
            cameraDevice = null;
        }
        if (null != imageReader) {
            imageReader.close();
            imageReader = null;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                Toast.makeText(CustomCameraActivity.this, "Sorry!!!, you can't use this app without granting permission", Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        startBackgroundThread();
        if (textureView.isAvailable()) {
            openCamera();
        } else {
            textureView.setSurfaceTextureListener(textureListener);
        }
    }

    @Override
    protected void onPause() {
        stopBackgroundThread();
        closeCamera();
        if(!mDisposable.isDisposed()) mDisposable.dispose();
        super.onPause();
    }

}
