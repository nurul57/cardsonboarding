package android.form.avss.prepaidcard.ihmf_form_fill.pojo.controls;


import com.google.gson.annotations.SerializedName;

public class DateTimeControl extends BaseControl {

    // alias of acutalText
    @SerializedName("value")
    private String value;


    public DateTimeControl() {
        super();
        setMarkedObject(true);
    }


    @Override
    public void setData(String actualText) {
        this.value = actualText;
    }

    @Override
    public String getData() {
        return value;
    }
}
