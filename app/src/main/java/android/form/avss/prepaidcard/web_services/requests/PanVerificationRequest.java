package android.form.avss.prepaidcard.web_services.requests;

import com.google.gson.annotations.SerializedName;

public class PanVerificationRequest extends BaseRequest {
    @SerializedName("pcn")
    private String panCardNo;
    @SerializedName("nm")
    private String name;

    public String getPanCardNo() {
        return panCardNo;
    }

    public void setPanCardNo(String panCardNo) {
        this.panCardNo = panCardNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

