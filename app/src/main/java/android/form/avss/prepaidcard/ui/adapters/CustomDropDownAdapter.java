package android.form.avss.prepaidcard.ui.adapters;

import android.content.Context;
import android.form.avss.prepaidcard.R;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.List;

public class CustomDropDownAdapter extends ArrayAdapter<String> {

	private final ArrayList<String> srcList;
	private final ArrayList<String> srcListAll;
	private final ArrayList<String> srcListSuggestion;
	private MyFilter mFilter;


	public CustomDropDownAdapter(Context context, List<String> srcList) {
		super(context, 0, srcList);
		this.srcList = new ArrayList<>(srcList);
		this.srcListAll = new ArrayList<>(srcList);
		this.srcListSuggestion = new ArrayList<>();
	}

	@Override
	public int getCount() {
		return srcList.size();
	}

	@Override
	public String getItem(int position) {
		return srcList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public static class ViewHolder {
		TextView tv_name;
	}

	@NonNull
	@Override
	public View getView(int position, View convertView, @NonNull ViewGroup parent) {
		ViewHolder viewHolder;
		if ( convertView == null ) {
			viewHolder = new ViewHolder();
			LayoutInflater inflater = LayoutInflater.from(parent.getContext());
			convertView = inflater.inflate(R.layout.custom_dropdown_row, parent, false);
			viewHolder.tv_name = ( TextView ) convertView.findViewById(R.id.textView);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		viewHolder.tv_name.setText(srcList.get(position));
		return convertView;
	}

	@NonNull
	@Override
	public Filter getFilter() {
		if (mFilter == null) {
			mFilter = new MyFilter();
		}
		return mFilter;
	}

	class MyFilter extends Filter {
		@Override
		public CharSequence convertResultToString(Object resultValue) {
			return resultValue.toString();
		}

		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			if (constraint != null) {
				srcListSuggestion.clear();
				for (String name : srcListAll) {
					if (name.toLowerCase().startsWith(constraint.toString().toLowerCase())) {
						srcListSuggestion.add(name);
					}
				}
				FilterResults filterResults = new FilterResults();
				filterResults.values = srcListSuggestion;
				filterResults.count = srcListSuggestion.size();
				return filterResults;
			} else {
				return new FilterResults();
			}
		}

		@Override
		protected void publishResults(CharSequence constraint, FilterResults results) {
			if (results == null) {
				notifyDataSetInvalidated();
				return;
			}
			List<String> list = (List<String>) results.values;
			srcList.clear();
			srcList.addAll(list);
			notifyDataSetChanged();
		}
	}
}
