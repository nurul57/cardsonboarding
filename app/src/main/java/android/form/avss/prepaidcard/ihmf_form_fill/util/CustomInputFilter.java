package android.form.avss.prepaidcard.ihmf_form_fill.util;

import android.text.InputFilter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CustomInputFilter implements InputFilter {

    private String regex;

    public CustomInputFilter(String regex) {
        this.regex = regex;
    }


    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        boolean keepOriginal = true;
         StringBuilder sb = new StringBuilder(end - start);
        for (int i = start; i < end; i++) {
            char c = source.charAt(i);
            if (Character.isWhitespace(source.charAt(i))) {
                if (dstart == 0 && !dest.toString().equals("")) {
                    return "";
                }
            } else if (isCharAllowed(c))
                sb.append(c);
            else
                keepOriginal = false;
        }
        if (keepOriginal)
            return null;
        else {
            if (source instanceof Spanned) {
                SpannableString sp = new SpannableString(sb);
                TextUtils.copySpansFrom((Spanned) source, start, sb.length(), null, sp, 0);
                return sp;
            } else {
                return sb;
            }
        }
    }

    private boolean isCharAllowed(char c) {
        Pattern ps = Pattern.compile(regex);
        Matcher ms = ps.matcher(String.valueOf(c));
        return ms.matches();
    }
}
