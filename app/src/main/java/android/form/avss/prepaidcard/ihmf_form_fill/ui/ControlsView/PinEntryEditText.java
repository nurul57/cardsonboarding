
package android.form.avss.prepaidcard.ihmf_form_fill.ui.ControlsView;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.form.avss.prepaidcard.R;
import android.form.avss.prepaidcard.database.MyDBHelper;
import android.form.avss.prepaidcard.ihmf_form_fill.util.CustomInputFilter;
import android.form.avss.prepaidcard.ihmf_form_fill.interfaces.FocusListener;
import android.form.avss.prepaidcard.ihmf_form_fill.interfaces.OnDropDownItemClickListener;
import android.form.avss.prepaidcard.ihmf_form_fill.interfaces.iHMFView;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.controls.SuperTextControl;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.controls.iHMFGroupListener;
import android.form.avss.prepaidcard.ihmf_form_fill.ui.Adapters.DropDownListAdapter;
import android.form.avss.prepaidcard.ihmf_form_fill.util.MySharePreference;
import android.form.avss.prepaidcard.ihmf_form_fill.util.Utility;
import android.form.avss.prepaidcard.ihmf_form_fill.util.ValidationUtils;
import android.form.avss.prepaidcard.ihmf_form_fill.util.iHMFUtility;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.JsonObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class PinEntryEditText extends AppCompatEditText implements iHMFView<SuperTextControl>, View.OnFocusChangeListener {
    private static final String XML_NAMESPACE_ANDROID = "http://schemas.android.com/apk/res/android";
//    private final String TAG = getClass().getSimpleName();

    private JsonObject jsonObject;
    private SuperTextControl mControl;
    private boolean showDateDialog = true;
    private Drawable mErrorDrawable, mDefaultDrawable;

    public static final String DEFAULT_MASK = "\u25CF";

    protected String mMask = null;
    protected StringBuilder mMaskChars = null;
    protected String mSingleCharHint = null;
    protected int mAnimatedType = 0;
    protected float mSpace = 1; //24 dp by default, space between the lines
//    protected float mCharSize;
    protected float mNumChars = 4;
    protected float mTextBottomPadding = 8; //8dp by default, height of the text from our lines
    protected int mMaxLength = 4;
    protected RectF[] mLineCoords;
    protected float[] mCharBottom;
    protected Paint mCharPaint;
    protected Paint mLastCharPaint;
    protected Paint mSingleCharPaint;
    protected Drawable mPinBackground;
    protected Rect mTextHeight = new Rect();
    protected boolean mIsDigitSquare = true;
    private AlertDialog alertDialog;


    protected OnClickListener mClickListener;
    protected OnPinEnteredListener mOnPinEnteredListener = null;

    protected float mLineStroke = 1; //1dp by default
    protected float mLineStrokeSelected = 2; //2dp by default
    protected Paint mLinesPaint;
    protected boolean mAnimate = false;
    protected boolean mHasError = false;
    protected ColorStateList mOriginalTextColors;
    protected int[][] mStates = new int[][]{
            new int[]{android.R.attr.state_selected}, // selected
            new int[]{android.R.attr.state_active}, // error
            new int[]{android.R.attr.state_focused}, // focused
            new int[]{-android.R.attr.state_focused}, // unfocused
    };

    protected int[] mColors = new int[]{
            Color.GREEN,
            Color.RED,
            Color.BLACK,
            Color.GRAY
    };

    protected ColorStateList mColorStates = new ColorStateList(mStates, mColors);
    private DatePickerDialog datePickerDialog;
    private Drawable mFocusBoxDrawable;

    private iHMFGroupListener<String> mGroupListener;
//    private GroupControls mGroup;
    private DropDownListAdapter adapter;
    private MySharePreference mSharePreference;

    public PinEntryEditText(Context context) {
        super(context);
    }


    public PinEntryEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public PinEntryEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public void setMaxLength(final int maxLength) {
        mMaxLength = maxLength;
        mNumChars = maxLength;

        setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLength)});
        setText(null);
        invalidate();
    }

    public void setMask(String mask) {
        mMask = mask;
        mMaskChars = null;
        invalidate();
    }

//    public void setSingleCharHint(String hint) {
//        mSingleCharHint = hint;
//        invalidate();
//    }

    private void init(Context context, AttributeSet attrs) {
        mFocusBoxDrawable = ContextCompat.getDrawable(getContext(), R.drawable.et_background_error);
        setOnFocusChangeListener(this);
        setErrorEnabled(false);
        setMovementMethod(null);
        float multi = context.getResources().getDisplayMetrics().density;
        mLineStroke = multi * mLineStroke;
        mLineStrokeSelected = multi * mLineStrokeSelected;
        mSpace = multi * mSpace; //convert to pixels for our density
        mTextBottomPadding = multi * mTextBottomPadding; //convert to pixels for our density

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.PinEntryEditText, 0, 0);
        try {
            TypedValue outValue = new TypedValue();
            ta.getValue(R.styleable.PinEntryEditText_pinAnimationType, outValue);
            mAnimatedType = outValue.data;
            mMask = ta.getString(R.styleable.PinEntryEditText_pinCharacterMask);
            mSingleCharHint = ta.getString(R.styleable.PinEntryEditText_pinRepeatedHint);
            mLineStroke = ta.getDimension(R.styleable.PinEntryEditText_pinLineStroke, mLineStroke);
            mLineStrokeSelected = ta.getDimension(R.styleable.PinEntryEditText_pinLineStrokeSelected, mLineStrokeSelected);
            mSpace = ta.getDimension(R.styleable.PinEntryEditText_pinCharacterSpacing, mSpace);
            mTextBottomPadding = ta.getDimension(R.styleable.PinEntryEditText_pinTextBottomPadding, mTextBottomPadding);
            mIsDigitSquare = ta.getBoolean(R.styleable.PinEntryEditText_pinBackgroundIsSquare, mIsDigitSquare);
            mPinBackground = ta.getDrawable(R.styleable.PinEntryEditText_pinBackgroundDrawable);
            ColorStateList colors = ta.getColorStateList(R.styleable.PinEntryEditText_pinLineColors);
            if (colors != null) {
                mColorStates = colors;
            }
        } finally {
            ta.recycle();
        }

        mCharPaint = new Paint(getPaint());
        mLastCharPaint = new Paint(getPaint());
        mSingleCharPaint = new Paint(getPaint());
        mLinesPaint = new Paint(getPaint());
        mLinesPaint.setStrokeWidth(mLineStroke);

        TypedValue outValue = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.colorControlActivated,
                outValue, true);
        int colorSelected = outValue.data;
        mColors[0] = colorSelected;

        int colorFocused = isInEditMode() ? Color.GRAY : ContextCompat.getColor(context, R.color.pin_normal);
        mColors[1] = colorFocused;

        int colorUnfocused = isInEditMode() ? Color.GRAY : ContextCompat.getColor(context, R.color.pin_normal);
        mColors[2] = colorUnfocused;

        setBackgroundResource(0);

        mMaxLength = attrs.getAttributeIntValue(XML_NAMESPACE_ANDROID, "maxLength", 4);
        mNumChars = mMaxLength;

        //Disable copy paste
        super.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {
            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }
        });
        // When tapped, move cursor to end of text.
        super.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelection(getText().length());
                if (mClickListener != null) {
                    mClickListener.onClick(v);
                }
            }
        });

        super.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                setSelection(getText().length());
                return true;
            }
        });

        //If input type is password and no mask is set, use a default mask
        if ((getInputType() & InputType.TYPE_TEXT_VARIATION_PASSWORD) == InputType.TYPE_TEXT_VARIATION_PASSWORD && TextUtils.isEmpty(mMask)) {
            mMask = DEFAULT_MASK;
        } else if ((getInputType() & InputType.TYPE_NUMBER_VARIATION_PASSWORD) == InputType.TYPE_NUMBER_VARIATION_PASSWORD && TextUtils.isEmpty(mMask)) {
            mMask = DEFAULT_MASK;
        }

        if (!TextUtils.isEmpty(mMask)) {
            mMaskChars = getMaskChars();
        }

        //Height of the characters, used if there is a background drawable
        getPaint().getTextBounds("|", 0, 1, mTextHeight);

        mAnimate = mAnimatedType > -1;

        setTextColor(getResources().getColor(R.color.iHMF_textColor));

    }


    @Override
    protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect);

        if (hasFocus() && showDateDialog && getControl().isMarkedControl() && getControl().getDataType().equals("date")) {
            clearFocus();
            showDatePickerDialog();
        }

        if(hasFocus() && getControl().isMarkedControl() && !getControl().getDdLabelId().equals("0")){
            clearFocus();
            showDropDownDialog();
        }
    }

    private void showDatePickerDialog() {
        if (datePickerDialog != null) {
            datePickerDialog.show();
            return;
        }

        Calendar calendar = Calendar.getInstance();
        int y = calendar.get(Calendar.YEAR);
        int m = calendar.get(Calendar.MONTH);
        int d = calendar.get(Calendar.DAY_OF_MONTH);
        datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year1, int month1, int dayOfMonth1) {
                SimpleDateFormat format = new SimpleDateFormat(Utility.getFormat(PinEntryEditText.this.getControl().getDateFormat()), Locale.getDefault());
                Date date;
                String dateString = null;
                try {
                    Calendar cal = Calendar.getInstance();
                    cal.set(year1, month1, dayOfMonth1);
                    date = cal.getTime();
                    dateString = format.format(date);
                } catch (Exception e) {
                    Toast.makeText(PinEntryEditText.this.getContext(), "Some error occurred, Please fill manually", Toast.LENGTH_SHORT).show();
                    showDateDialog = false;
                }
                PinEntryEditText.this.setText(dateString);
            }
        }, y, m, d);
        datePickerDialog.show();
    }

    @Override
    public void setInputType(int type) {
        super.setInputType(type);

        if ((type & InputType.TYPE_TEXT_VARIATION_PASSWORD) == InputType.TYPE_TEXT_VARIATION_PASSWORD
                || (type & InputType.TYPE_NUMBER_VARIATION_PASSWORD) == InputType.TYPE_NUMBER_VARIATION_PASSWORD) {
            // If input type is password and no mask is set, use a default mask
            if (TextUtils.isEmpty(mMask)) {
                setMask(DEFAULT_MASK);
            }
        } else {
            // If input type is not password, remove mask
            setMask(null);
        }

    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        mOriginalTextColors = getTextColors();
        if (mOriginalTextColors != null) {
            mLastCharPaint.setColor(mOriginalTextColors.getDefaultColor());
            mCharPaint.setColor(mOriginalTextColors.getDefaultColor());
            mSingleCharPaint.setColor(getCurrentHintTextColor());
        }

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (mControl.isMarkedControl())
            setMeasuredDimension(Math.round(mControl.getScaleWidth()), mControl.getScaleHeight().intValue());
        else
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);

    }

    @Override
    public void setOnClickListener(OnClickListener l) {
        mClickListener = l;
    }

    @Override
    public void setCustomSelectionActionModeCallback(ActionMode.Callback actionModeCallback) {
        throw new RuntimeException("setCustomSelectionActionModeCallback() not supported.");
    }


    @Override
    protected void onDraw(Canvas canvas) {
//        super.onDraw(canvas);

        if (getControl().isEmpty()) return;

        CharSequence text = getFullText();
        int textLength = text.length();

        int width = getWidth() / mMaxLength;
        int height = getHeight();


        float strokeWidth = 0;

        if (mPinBackground instanceof ShapeDrawable)
            strokeWidth = ((ShapeDrawable) mPinBackground).getPaint().getStrokeWidth();
        int initLeft = 0;

        for (int i = 0; i < mNumChars; i++) {
            //If a background for the pin characters is specified, it should be behind the characters.
            if (mPinBackground != null) {
                updateDrawableState(i < textLength, i == textLength);
                mPinBackground.setBounds(initLeft, 0, initLeft + width - getControl().getBoxGap(), height);

                if (i == textLength && i < mNumChars && isFocused()) {
                    mFocusBoxDrawable.setBounds(initLeft, 0, initLeft + width - getControl().getBoxGap(), height);
                    mFocusBoxDrawable.draw(canvas);
                } else
                    mPinBackground.draw(canvas);
            }
//            float middle = mLineCoords[i].left + mCharSize / 2;
            if (textLength > i) {
                float textWidth = getPaint().measureText(text, i, i + 1) / 2;

                int center = (int) ((initLeft - getControl().getBoxGap() + width / 2 - textWidth) + strokeWidth * 2);
                int heightcenter = (int) (3 * (height - strokeWidth * 2) / 4);
                int txtSize = (int) (Math.min(width, height) * 0.6);

                if (!mAnimate || i != textLength - 1) {
                    mCharPaint.setTextSize(txtSize);
                    canvas.drawText(text.toString().toUpperCase(), i, i + 1, center, heightcenter, mCharPaint);
                } else {
                    mLastCharPaint.setTextSize(txtSize);

                    canvas.drawText(text, i, i + 1, center, heightcenter, mLastCharPaint);
                }
            }
//            else if (mSingleCharHint != null) {
//                canvas.drawText(mSingleCharHint, 0, 0, mSingleCharPaint);
//            }
            //The lines should be in frot of the text (because that's how I want it).
            if (mPinBackground == null) {
                updateColorForLines(i <= textLength);
                canvas.drawLine(mLineCoords[i].left, mLineCoords[i].top, mLineCoords[i].right, mLineCoords[i].bottom, mLinesPaint);
            }

            initLeft += width;
        }


        if (getControl().isMandatory()) {

            float w = getPaint().measureText("*");
            float txtSize = getPaint().getTextSize();
            float newTextSize = (float) Math.sqrt(Math.pow(Math.min(getWidth(), getHeight()), 2) / 4);
            getPaint().setTextSize(newTextSize);
            int color = getPaint().getColor();
            getPaint().setColor(Color.RED);
            canvas.drawText("*", getWidth() - getPaddingEnd(), w * 1.5f, getPaint());
            getPaint().setColor(color);
            getPaint().setTextSize(txtSize);
        }


    }


    private CharSequence getFullText() {
        if (TextUtils.isEmpty(mMask)) {
            return getText();
        } else {
            return getMaskChars();
        }
    }

    private StringBuilder getMaskChars() {
        if (mMaskChars == null) {
            mMaskChars = new StringBuilder();
        }
        int textLength = getText().length();
        while (mMaskChars.length() != textLength) {
            if (mMaskChars.length() < textLength) {
                mMaskChars.append(mMask);
            } else {
                mMaskChars.deleteCharAt(mMaskChars.length() - 1);
            }
        }
        return mMaskChars;
    }


    private int getColorForState(int... states) {
        return mColorStates.getColorForState(states, Color.GRAY);
    }

    /**
     * @param hasTextOrIsNext Is the color for a character that has been typed or is
     *                        the next character to be typed?
     */
    protected void updateColorForLines(boolean hasTextOrIsNext) {
        if (mHasError) {
            mLinesPaint.setColor(getColorForState(android.R.attr.state_active));
        } else if (isFocused()) {
            mLinesPaint.setStrokeWidth(mLineStrokeSelected);
            mLinesPaint.setColor(getColorForState(android.R.attr.state_focused));
            if (hasTextOrIsNext) {
                mLinesPaint.setColor(getColorForState(android.R.attr.state_selected));
            }
        } else {
            mLinesPaint.setStrokeWidth(mLineStroke);
            mLinesPaint.setColor(getColorForState(-android.R.attr.state_focused));
        }
    }


    private int[] STATE_ACTIVE = new int[]{android.R.attr.state_active};
    private int[] STATE_FOCUSED = new int[]{android.R.attr.state_focused};
    private int[] STATE_FOCUSED_AND_SELECTED = new int[]{android.R.attr.state_focused, android.R.attr.state_selected};
    private int[] STATE_FOCUSED_AND_CHECKED = new int[]{android.R.attr.state_focused, android.R.attr.state_checked};

    protected void updateDrawableState(boolean hasText, boolean isNext) {
        if (mHasError) {
            mPinBackground.setState(STATE_ACTIVE);
        } else if (isFocused()) {
            mPinBackground.setState(STATE_FOCUSED);
            if (isNext) {
                mPinBackground.setState(STATE_FOCUSED_AND_SELECTED);
            } else if (hasText) {
                mPinBackground.setState(STATE_FOCUSED_AND_CHECKED);
            }
        } else {
            if (hasText) {
                mPinBackground.setState(STATE_FOCUSED_AND_CHECKED);
            } else {
                mPinBackground.setState(STATE_FOCUSED);
            }
        }
    }

    public void setError(boolean hasError) {
        mHasError = hasError;
        invalidate();
    }

    public boolean isError() {
        return mHasError;
    }

    /**
     * Request focus on this PinEntryEditText
     */
    public void focus() {
        requestFocus();
    }


    @Override
    public boolean requestFocus(int direction, Rect previouslyFocusedRect) {
        return super.requestFocus(direction, previouslyFocusedRect);
    }

    @Override
    public void setTypeface(@Nullable Typeface tf) {
        super.setTypeface(tf);
        setCustomTypeface(tf);
    }

    @Override
    public void setTypeface(@Nullable Typeface tf, int style) {
        super.setTypeface(tf, style);
        setCustomTypeface(tf);
    }

    private void setCustomTypeface(@Nullable Typeface tf) {
        if (mCharPaint != null) {
            mCharPaint.setTypeface(tf);
            mLastCharPaint.setTypeface(tf);
            mSingleCharPaint.setTypeface(tf);
            mLinesPaint.setTypeface(tf);
        }
    }

//    public void setPinLineColors(ColorStateList colors) {
//        mColorStates = colors;
//        invalidate();
//    }

    public void setPinBackground(Drawable pinBackground) {
        mPinBackground = pinBackground;
        postInvalidate();
    }


    @Override
    protected void onTextChanged(CharSequence text, final int start, int lengthBefore, final int lengthAfter) {
        setError(false);

        if (mLineCoords == null || !mAnimate) {
            if (mOnPinEnteredListener != null && text.length() == mMaxLength) {
                mOnPinEnteredListener.onPinEntered(text);
            }
            return;
        }

        if (mAnimatedType == -1) {
            invalidate();
            return;
        }
        if (lengthAfter > lengthBefore) {
            if (mAnimatedType == 0) {
                animatePopIn();
            } else {
                animateBottomUp(text, start);
            }
        }
    }


    private void animatePopIn() {
        ValueAnimator va = ValueAnimator.ofFloat(1, getPaint().getTextSize());
        va.setDuration(200);
        va.setInterpolator(new OvershootInterpolator());
        va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
//                mLastCharPaint.setTextSize((Float) animation.getAnimatedValue());
//                PinEntryEditText.this.invalidate();
            }
        });
        if (getText().length() == mMaxLength && mOnPinEnteredListener != null) {
            va.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    mOnPinEnteredListener.onPinEntered(getText());
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                }

                @Override
                public void onAnimationRepeat(Animator animation) {
                }
            });
        }
        va.start();
    }

    private void animateBottomUp(CharSequence text, final int start) {
        mCharBottom[start] = mLineCoords[start].bottom - mTextBottomPadding;
        ValueAnimator animUp = ValueAnimator.ofFloat(mCharBottom[start] + getPaint().getTextSize(), mCharBottom[start]);
        animUp.setDuration(300);
        animUp.setInterpolator(new OvershootInterpolator());
        animUp.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
//                Float value = (Float) animation.getAnimatedValue();
//                mCharBottom[start] = value;
//                PinEntryEditText.this.invalidate();
            }
        });

        mLastCharPaint.setAlpha(255);
        ValueAnimator animAlpha = ValueAnimator.ofInt(0, 255);
        animAlpha.setDuration(300);
        animAlpha.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer value = (Integer) animation.getAnimatedValue();
                mLastCharPaint.setAlpha(value);
            }
        });

        AnimatorSet set = new AnimatorSet();
        if (text.length() == mMaxLength && mOnPinEnteredListener != null) {
            set.addListener(new Animator.AnimatorListener() {

                @Override
                public void onAnimationStart(Animator animation) {
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    mOnPinEnteredListener.onPinEntered(getText());
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
        }
        set.playTogether(animUp, animAlpha);
        set.start();
    }

//    public void setAnimateText(boolean animate) {
//        mAnimate = animate;
//    }
//
//    public void setOnPinEnteredListener(OnPinEnteredListener l) {
//        mOnPinEnteredListener = l;
//    }

    @Override
    public SuperTextControl getControl() {

        if (mControl == null)
            mControl = new SuperTextControl();

        return mControl;
    }


    private FocusListener focusListener;

    @Override
    public void setControl(SuperTextControl control, JsonObject jsonObject, FocusListener focusListener) {
        this.mControl = control;
        this.jsonObject = jsonObject;
        this.focusListener = focusListener;

        getControl();

        setX(mControl.getScaleX());
        setY(mControl.getScaleY());

        getPaint().setTextSize(iHMFUtility.getTextSize(mControl, getResources().getDisplayMetrics()));

        setMaxLength(mControl.getBoxCount());

        setText(mControl.getData());

        setEnabled(!getControl().isLock());

        if (getControl().isMarkedControl()) {

            if(getControl().hasGroup())
                addTextWatcher();

            if(!TextUtils.isEmpty(getControl().getDataType())){
                setDataTypeFilter();
            }
        }


    }


    public void setBoxText(String text) {
        if (getControl().getDataType().equalsIgnoreCase("currency")
                && getControl().getAmountDisplayType().equalsIgnoreCase("Words") && !TextUtils.isEmpty(text)) {
            text = Utility.convert(Integer.parseInt(text));
        }
        setText(text);
    }

    public void addTextWatcher() {
        addTextChangedListener(mTextWatcher);
    }

    public void removeTextWatcher() {
        removeTextChangedListener(mTextWatcher);
    }


    private TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (getControl().hasGroup() && mGroupListener != null)
                mGroupListener.handleGroup(getControl().getId(), getControl().getGroupId(), s.toString());
        }
    };


    private void setDataTypeFilter() {
        switch (getControl().getDataType()) {
            case ValidationUtils.DataType.NUM_CONTINOUS:
            case ValidationUtils.DataType.MOBILE_NUM_WITH_OTP:
            case ValidationUtils.DataType.MOBILE_NUM:
            case ValidationUtils.DataType.AADHAAR_NUM:
            case ValidationUtils.DataType.PINCODE:
            case ValidationUtils.DataType.CURRENCY:
                setInputType(InputType.TYPE_CLASS_NUMBER);
                break;

//            case Validation.Type.EMAIL:
//            case Validation.Type.EMAIL_WITH_OTP:
//                setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
//                break;

            default:
                if(getControl().isCapital()){
                    setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
                }

                if (getControl().isCapital()) {
                    setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
                }

                String regex = getControl().getInputFilterRegex();
                if(!TextUtils.isEmpty(regex)){
                    setFilters(new InputFilter[]{new CustomInputFilter(regex)});
                }
        }



    }


    @Override
    public boolean save() {
        if(getPreference().shouldCheckMandatoryFields() && getControl().isMandatory() && !isDataValid()){
                setErrorDrawable();
                setErrorEnabled(true);
            return false;
        }
        getControl().setData(getFullText().toString());
        if (jsonObject != null) jsonObject.addProperty("actualText", getFullText().toString());

        return true;
    }

    @Override
    public boolean isValid() {
        return !getText().toString().isEmpty();
    }

    @Override
    public MySharePreference getPreference() {
        if(mSharePreference == null){
            mSharePreference = new MySharePreference(getContext());
        }
        return mSharePreference;
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {

        if(getControl().isMarkedControl() && !hasFocus){
            if(getControl().isMandatory() && TextUtils.isEmpty(getText().toString())){
                setErrorDrawable();
                setErrorEnabled(true);
                return;
            }

            if (!TextUtils.isEmpty(getText().toString()) && !isDataValid()) {
                setErrorDrawable();
                setErrorEnabled(true);
                return;
            }
        }

        if(isErrorEnabled()){
            setDefaultDrawable();
            setErrorEnabled(false);
        }


        //listener to move view above keyboard by adding addOnGlobalLayoutListener (jugaad)
        //check implementation in FormFillFragmentForm
        if(focusListener != null) {
            if(hasFocus)
                focusListener.addGlobalLayoutListener(getControl().getScaleY() + getControl().getScaleHeight() + 20);
            else
                focusListener.removeGlobalLayoutListener();
        }

    }


    private void setErrorDrawable() {
        if (mErrorDrawable == null)
            mErrorDrawable = getResources().getDrawable(R.drawable.pin_background_error);

        setPinBackground(mErrorDrawable);
    }

    private void setDefaultDrawable() {
        if (mDefaultDrawable == null)
            mDefaultDrawable = getResources().getDrawable(R.drawable.pin_background_default);

        setPinBackground(mDefaultDrawable);
    }

    @Override
    public void resizeView(int resizeType) {

    }

    @Override
    public void removeOnTouchListener() {

    }

    public void setOnGroupListener(iHMFGroupListener<String> mListener) {
        this.mGroupListener = mListener;
    }

//    public GroupControls getGroup() {
//
//        if (mGroup == null)
//            mGroup = mGroupListener == null ? null : mGroupListener.getGroupControl(getControl().getGroupId());
//
//        return mGroup;
//    }

    @Override
    public View getView() {
        return this;
    }

    public interface OnPinEnteredListener {
        void onPinEntered(CharSequence str);
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    private boolean isDataValid(){
        return ValidationUtils.isDataValid(getText().toString(), getControl().getRegex());
    }

    private boolean errorEnabled;

    private boolean isErrorEnabled() {
        return errorEnabled;
    }

    private void setErrorEnabled(boolean b) {
        this.errorEnabled = b;
    }



    private void showDropDownDialog(){
        if(alertDialog != null){
            alertDialog.show();
            return;
        }

        MyDBHelper myDBHelper = MyDBHelper.getInstance(getContext());
        List<String> dataList = myDBHelper.getDropDownList(getControl().getDdLabelId());

        if(dataList == null || dataList.isEmpty()) {
            Toast.makeText(getContext(), "Drop down data not found", Toast.LENGTH_SHORT).show();
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        @SuppressLint("InflateParams")
        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_drop_down_layout, null);

        EditText et_searchView = (EditText) view.findViewById(R.id.et_search);
        RecyclerView rv_item = (RecyclerView) view.findViewById(R.id.rv_item);
//        TextView tv_title = (TextView) view.findViewById(R.id.tv_title);

        rv_item.setLayoutManager(new LinearLayoutManager(getContext()));

        adapter = new DropDownListAdapter(dataList, new OnDropDownItemClickListener() {
            @Override
            public void onClick(String data) {
                setText(data);
                if(isErrorEnabled()){
                    setDefaultDrawable();
                    setErrorEnabled(false);
                }
                alertDialog.dismiss();
            }
        });

        rv_item.setAdapter(adapter);

        et_searchView.addTextChangedListener(textWatcher);
        builder.setView(view);

        alertDialog = builder.create();

        alertDialog.show();
    }

    private final TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if(adapter != null){
                adapter.getFilter().filter(s.toString());
            }
        }
    };




}
