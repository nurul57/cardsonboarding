package android.form.avss.prepaidcard.ihmf_form_fill.util;

import android.content.Context;
import android.form.avss.prepaidcard.R;
import android.form.avss.prepaidcard.ihmf_form_fill.interfaces.FocusListener;
import android.form.avss.prepaidcard.ihmf_form_fill.interfaces.iHMFView;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.controls.BaseControl;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.controls.Control;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.controls.ControlUtility;
import android.form.avss.prepaidcard.ihmf_form_fill.pojo.controls.TextControl;
import android.graphics.PointF;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.google.gson.JsonObject;


public class iHMFUtility {

    private static final int SCALE_FACTOR = 1;

    public static View getView(ViewGroup viewGroup, JsonObject obj, PointF point, FocusListener focusListener) {

        return getControlView(ControlUtility.get(obj), viewGroup, viewGroup.getContext(), point, obj, focusListener);
    }


    @SuppressWarnings("unchecked")
    private static View getControlView(BaseControl control, ViewGroup viewGroup, Context ctx, PointF point, JsonObject obj,
                                       FocusListener focusListener) {

        if (control == null) return null;

        View view;


        String type = control.getType();
        LayoutInflater inflater = LayoutInflater.from(ctx);
        Integer viewID = null;

        if (Control.TextField.toString().equalsIgnoreCase(type))
            viewID = R.layout.layout_ihmf_edit_text;
        else if (Control.Photo.toString().equalsIgnoreCase(type))
            viewID = R.layout.layout_ihmf_image_view;
        else if (Control.Signature.toString().equalsIgnoreCase(type))
            viewID = R.layout.layout_ihmf_image_view;
        else if (Control.SUPER_TEXT.toString().equalsIgnoreCase(type))
            viewID = R.layout.layout_ihmf_pin_entry_box;
        else if (Control.Boolean.name().equalsIgnoreCase(type))
            viewID = R.layout.layout_ihmf_boolean;
        else if (Control.TextFieldBox.name().equalsIgnoreCase(type))
            viewID = R.layout.layout_ihmf_pin_entry_box;



        if(viewID == null) return null;

        view = inflater.inflate(viewID,viewGroup,false);
        if(control.isMarkedControl())
            view.setId(control.getId());
        else
            view.setId(View.NO_ID);


        control.setRatio(point);

        if(view instanceof iHMFView)
         ((iHMFView) view).setControl(control, obj, focusListener);

        return view;
    }


    public static float getTextSize(TextControl control, DisplayMetrics metrics) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, control.getFontSize() * control.getRatio().y, metrics);
    }


    public static float getScale(float dimen, float scale) {
        return dimen * scale * getScaleFactor();
    }

    public static int getScaleFactor() {
        return SCALE_FACTOR;
    }


    private final static int TEMP_SCALE_FACTOR = 2;

    public static float getTextSize(float fontSize, PointF scale, DisplayMetrics dm) {
        return Math.round(fontSize * scale.y * TEMP_SCALE_FACTOR) / (dm.density);
    }

    /**
     * This is used to clear focus and hide keyboard
     * @param view
     */
    public static void clearAndHideSoftKeyboard(View view) {
        if (view == null) return;
        view.clearFocus();
        hideSoftKeyboard(view);
    }

    /**
     * This is used to hide soft keyboard
     * @param view - which has focus
     */
    public static void hideSoftKeyboard(View view) {
        if (view == null) return;
        InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null)
            imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
    }


    public static void showSoftKeyBoard(View view){
        InputMethodManager inputMethodManager =
                (InputMethodManager)view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if(inputMethodManager == null) return;
        inputMethodManager.toggleSoftInputFromWindow(
                view.getApplicationWindowToken(),
                InputMethodManager.SHOW_FORCED, 0);
    }

}
