package android.form.avss.prepaidcard.ihmf_form_fill.ui.zoomlayout;

import android.content.Context;
import android.form.avss.prepaidcard.ihmf_form_fill.util.iHMFUtility;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.FrameLayout;
import android.widget.Toast;

/**
 * Layout that provides pinch-zooming,draging of content. This view should have exactly one direct child
 * view.
 * <p>
 * Parent touch interception is not handle.
 */
public class ZoomLayout extends FrameLayout implements ScaleGestureDetector.OnScaleGestureListener, ZoomEngine.Listener, ZoomApi {

    private boolean mHasClickableChildren = true;
    private float[] mMatrixValues = new float[9];
    private Matrix mMatrix = new Matrix();
    private RectF mChildRect = new RectF();
    private final String TAG = getClass().getSimpleName();
    public static final float MIN_ZOOM = 0.5f;
    public static final float MAX_ZOOM = 5.0f;

    private ScaleGestureDetector scaleDetector;
    private ViewConfiguration viewConfiguration;
    private final static ZoomLogger LOG = ZoomLogger.create("fdsa");


    int touchSlop = 1;

    private int firstPointerID = -1;
    private int secondPointerID = -1;
    private float scale = 1.0f;


    // Where the finger first  touches the screen
    private float startX = 0f;
    private float startY = 0f;
    private boolean mScale;


    public ZoomLayout(Context context) {
        super(context);
        init(context);
    }

    public ZoomLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ZoomLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }


/*
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {

        boolean handled = false;

        switch (ev.getActionMasked()) {

            case MotionEvent.ACTION_DOWN:


                firstPointerID = ev.getPointerId(ev.getActionIndex());
                startX = child().getX() - ev.getRawX();
                startY = child().getY() - ev.getRawY();


                break;

            case MotionEvent.ACTION_MOVE:


                int currentX =  Math.round(child().getX() - ev.getRawX());



                handled = Math.ceil(currentX - startX) > touchSlop;
                break;

        }


        if (!handled)
            handled = super.onInterceptTouchEvent(ev);


        return handled;
    }
*/


    private ZoomEngine mEngine;

    private void init(Context context) {
        scaleDetector = new ScaleGestureDetector(context, this);
        viewConfiguration = ViewConfiguration.get(context);

        mEngine = new ZoomEngine(context, this, this);


        scale = 1.0f;
        touchSlop = viewConfiguration.getScaledTouchSlop();
        mEngine.setMaxZoom(5.0f, TYPE_ZOOM);
    }


    @Override
    public boolean onScaleBegin(ScaleGestureDetector scaleDetector) {
        return mScale && scaleDetector.getCurrentSpan() > viewConfiguration.getScaledTouchSlop();
    }

    @Override
    public boolean onScale(ScaleGestureDetector scaleDetector) {


        float scaleFactor = scaleDetector.getScaleFactor();

        if (scaleFactor == 0f) return true;

        scale *= scaleFactor;
        scale = Math.max(MIN_ZOOM, Math.min(scale, MAX_ZOOM));
        child().animate().scaleX(scale).scaleY(scale).setDuration(0).start();


        return true;
    }


    public void setScale(float scale) {

        this.scale = Math.max(MIN_ZOOM, Math.min(scale, MAX_ZOOM));

        child().animate().scaleX(this.scale).scaleY(this.scale).setDuration(300).start();

    }


    @Override
    public void onScaleEnd(ScaleGestureDetector scaleDetector) {
    }


    private View child(int i) {
        return getChildAt(i);
    }

    private View child() {
        return getChildAt(0);
    }

/*
    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        boolean handled = false;

        mScale= false;

        switch (ev.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:

                firstPointerID = ev.getPointerId(ev.getActionIndex());
                startX = child().getX() - ev.getRawX();
                startY = child().getY() - ev.getRawY();

                handled = true;


                break;
            case MotionEvent.ACTION_POINTER_DOWN:

                if (secondPointerID == -1) {
                    secondPointerID = ev.getPointerId(ev.getActionIndex());
                }

                handled = true;
                break;

            case MotionEvent.ACTION_MOVE:

                    if (ev.getPointerCount() == 1) {


                        int newX = (int) (ev.getRawX() + startX);
                        int newY = (int) (ev.getRawY() + startY);


                        if(Math.abs(getX() - newX) < touchSlop && Math.abs(getY() -newY) < touchSlop){
                            handled = true;

                            break;
                        }

                        child().animate().x(newX)
                                .y(newY)
                                .setDuration(0)
                                .start();



                    } else if(ev.getPointerCount() == 2) {
                        int tempPointerID = ev.getPointerId(ev.getActionIndex());

                        if (tempPointerID == firstPointerID || tempPointerID == secondPointerID)
                            mScale = true;
                    }



                handled = true;

                break;

            case MotionEvent.ACTION_POINTER_UP:

                    secondPointerID = -1;

                handled = true;
                break;

            case MotionEvent.ACTION_UP:

                firstPointerID = -1;

                iHMFUtility.clearAndHideSoftKeyboard(getFocusedChild());
                clearFocus();
                handled = true;
                break;
        }


        scaleDetector.onTouchEvent(ev);

        if (!handled)
            handled = super.onTouchEvent(ev);

        return handled;


    }
*/

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return mEngine.onInterceptTouchEvent(ev) || (mHasClickableChildren && super.onInterceptTouchEvent(ev));
    }

    boolean mClicked = false;

    @Override
    public boolean onTouchEvent(MotionEvent ev) {

        float downX = 0;
        if (ev.getActionMasked() == MotionEvent.ACTION_DOWN) {

            startX = getX() - ev.getRawX();
            startY = getY() - ev.getRawY();
            downX = ev.getRawX();
            mClicked = false;
        }

        if (ev.getActionMasked() == MotionEvent.ACTION_MOVE) {
            mClicked = Math.abs(getX() - (ev.getRawX() + startX)) < viewConfiguration.getScaledTouchSlop();
            if(getWidth() - downX < 10 ){
                Toast.makeText(getContext(), "Start", Toast.LENGTH_SHORT).show();
            }
        }

        if (ev.getActionMasked() == MotionEvent.ACTION_UP) {

            if (mClicked)
                iHMFUtility.clearAndHideSoftKeyboard(getFocusedChild());

            mClicked = false;
        }

        return mEngine.onTouchEvent(ev) || (mHasClickableChildren && super.onTouchEvent(ev));
    }

    @Override
    public void onUpdate(ZoomEngine helper, Matrix matrix) {
        mMatrix.set(matrix);
        if (mHasClickableChildren) {
            if (getChildCount() > 0) {
                View child = getChildAt(0);

                // child.getMatrix().getValues(mMatrixValues);
                // Log.e(TAG, "values 0:" + Arrays.toString(mMatrixValues));
                // mMatrix.getValues(mMatrixValues);
                // Log.e(TAG, "values 1:" + Arrays.toString(mMatrixValues));

                mMatrix.getValues(mMatrixValues);
                child.setPivotX(0);
                child.setPivotY(0);
                child.setTranslationX(mMatrixValues[Matrix.MTRANS_X]);
                child.setTranslationY(mMatrixValues[Matrix.MTRANS_Y]);
                child.setScaleX(mMatrixValues[Matrix.MSCALE_X]);
                child.setScaleY(mMatrixValues[Matrix.MSCALE_Y]);

                // child.getMatrix().getValues(mMatrixValues);
                // Log.e(TAG, "values 2:" + Arrays.toString(mMatrixValues));
            }
        } else {
            invalidate();
        }

        awakenScrollBars();
    }

    @Override
    public void onIdle(ZoomEngine engine) {

    }

    @Override
    protected int computeHorizontalScrollOffset() {
        return (int) (-1 * mEngine.getPanX() * mEngine.getRealZoom());
    }

    @Override
    protected int computeHorizontalScrollRange() {
        return (int) (mChildRect.width() * mEngine.getRealZoom());
    }

    @Override
    protected int computeVerticalScrollOffset() {
        return (int) (-1 * mEngine.getPanY() * mEngine.getRealZoom());
    }

    @Override
    protected int computeVerticalScrollRange() {
        return (int) (mChildRect.height() * mEngine.getRealZoom());
    }

    @Override
    protected boolean drawChild(Canvas canvas, View child, long drawingTime) {
        boolean result;

        if (!mHasClickableChildren) {
            int save = canvas.save();
            canvas.setMatrix(mMatrix);
            result = super.drawChild(canvas, child, drawingTime);
            canvas.restoreToCount(save);
        } else {
            result = super.drawChild(canvas, child, drawingTime);
        }

        return result;
    }

    //endregion

    //region APIs

    /**
     * Whether the view hierarchy inside has (or will have) clickable children.
     * This is false by default.
     *
     * @param hasClickableChildren whether we have clickable children
     */
    public void setHasClickableChildren(boolean hasClickableChildren) {
        LOG.i("setHasClickableChildren:", "old:", mHasClickableChildren, "new:", hasClickableChildren);
        if (mHasClickableChildren && !hasClickableChildren) {
            // Revert any transformation that was applied to our child.
            if (getChildCount() > 0) {
                View child = getChildAt(0);
                child.setScaleX(1);
                child.setScaleY(1);
                child.setTranslationX(0);
                child.setTranslationY(0);
            }
        }
        mHasClickableChildren = hasClickableChildren;

        // Update if we were laid out already.
        if (getWidth() > 0 && getHeight() > 0) {
            if (mHasClickableChildren) {
                onUpdate(mEngine, mMatrix);
            } else {
                invalidate();
            }
        }
    }


    public void setSize(float width, float height) {

        mChildRect.set(0, 0,
                width,
                height);
        mEngine.setContentSize(mChildRect);
    }


    /**
     * Gets the backing {@link ZoomEngine} so you can access its APIs.
     *
     * @return the backing engine
     */
    public ZoomEngine getEngine() {
        return mEngine;
    }

    //endregion

    //region ZoomApis

    /**
     * Controls whether the content should be over-scrollable horizontally.
     * If it is, drag and fling horizontal events can scroll the content outside the safe area,
     * then return to safe values.
     *
     * @param overScroll whether to allow horizontal over scrolling
     */
    @Override
    public void setOverScrollHorizontal(boolean overScroll) {
        getEngine().setOverScrollHorizontal(overScroll);
    }

    /**
     * Controls whether the content should be over-scrollable vertically.
     * If it is, drag and fling vertical events can scroll the content outside the safe area,
     * then return to safe values.
     *
     * @param overScroll whether to allow vertical over scrolling
     */
    @Override
    public void setOverScrollVertical(boolean overScroll) {
        getEngine().setOverScrollVertical(overScroll);
    }

    /**
     * Controls whether horizontal panning using gestures is enabled.
     *
     * @param enabled true enables horizontal panning, false disables it
     */
    @Override
    public void setHorizontalPanEnabled(boolean enabled) {
        getEngine().setHorizontalPanEnabled(enabled);
    }

    /**
     * Controls whether vertical panning using gestures is enabled.
     *
     * @param enabled true enables vertical panning, false disables it
     */
    @Override
    public void setVerticalPanEnabled(boolean enabled) {
        getEngine().setVerticalPanEnabled(enabled);
    }

    /**
     * Controls whether the content should be overPinchable.
     * If it is, pinch events can change the zoom outside the safe bounds,
     * than return to safe values.
     *
     * @param overPinchable whether to allow over pinching
     */
    @Override
    public void setOverPinchable(boolean overPinchable) {
        getEngine().setOverPinchable(overPinchable);
    }

    /**
     * Controls whether zoom using pinch gesture is enabled or not.
     *
     * @param enabled true enables zooming, false disables it
     */
    @Override
    public void setZoomEnabled(boolean enabled) {
        getEngine().setZoomEnabled(enabled);
    }

    /**
     * Sets the base transformation to be applied to the content.
     * Defaults to {@link #TRANSFORMATION_CENTER_INSIDE} with {@link Gravity#CENTER},
     * which means that the content will be zoomed so that it fits completely inside the container.
     *
     * @param transformation the transformation type
     * @param gravity        the transformation gravity. Might be ignored for some transformations
     */
    @Override
    public void setTransformation(int transformation, int gravity) {
        getEngine().setTransformation(transformation, gravity);
    }

    /**
     * A low level API that can animate both zoom and pan at the same time.
     * Zoom might not be the actual matrix scale, see {@link #getZoom()} and {@link #getRealZoom()}.
     * The coordinates are referred to the content size so they do not depend on current zoom.
     *
     * @param zoom    the desired zoom value
     * @param x       the desired left coordinate
     * @param y       the desired top coordinate
     * @param animate whether to animate the transition
     */
    @Override
    public void moveTo(float zoom, float x, float y, boolean animate) {
        getEngine().moveTo(zoom, x, y, animate);
    }

    /**
     * Pans the content until the top-left coordinates match the given x-y
     * values. These are referred to the content size so they do not depend on current zoom.
     *
     * @param x       the desired left coordinate
     * @param y       the desired top coordinate
     * @param animate whether to animate the transition
     */
    @Override
    public void panTo(float x, float y, boolean animate) {
        getEngine().panTo(x, y, animate);
    }

    /**
     * Pans the content by the given quantity in dx-dy values.
     * These are referred to the content size so they do not depend on current zoom.
     * <p>
     * In other words, asking to pan by 1 pixel might result in a bigger pan, if the content
     * was zoomed in.
     *
     * @param dx      the desired delta x
     * @param dy      the desired delta y
     * @param animate whether to animate the transition
     */
    @Override
    public void panBy(float dx, float dy, boolean animate) {
        getEngine().panBy(dx, dy, animate);
    }

    /**
     * Zooms to the given scale. This might not be the actual matrix zoom,
     * see {@link #getZoom()} and {@link #getRealZoom()}.
     *
     * @param zoom    the new scale value
     * @param animate whether to animate the transition
     */
    @Override
    public void zoomTo(float zoom, boolean animate) {
        getEngine().zoomTo(zoom, animate);
    }

    /**
     * Applies the given factor to the current zoom.
     *
     * @param zoomFactor a multiplicative factor
     * @param animate    whether to animate the transition
     */
    @Override
    public void zoomBy(float zoomFactor, boolean animate) {
        getEngine().zoomBy(zoomFactor, animate);
    }


    @Override
    public void zoomToPivot(float zoom, boolean animate, float x, float y) {
        getEngine().zoomToPivot(zoom, animate, x, y);
    }

    /**
     * Applies a small, animated zoom-in.
     */
    @Override
    public void zoomIn() {
        getEngine().zoomIn();
    }

    /**
     * Applies a small, animated zoom-out.
     */
    @Override
    public void zoomOut() {
        getEngine().zoomOut();
    }

    /**
     * Animates the actual matrix zoom to the given value.
     *
     * @param realZoom the new real zoom value
     * @param animate  whether to animate the transition
     */
    @Override
    public void realZoomTo(float realZoom, boolean animate) {
        getEngine().realZoomTo(realZoom, animate);
    }

    /**
     * Which is the max zoom that should be allowed.
     * If {@link #setOverPinchable(boolean)} is set to true, this can be over-pinched
     * for a brief time.
     *
     * @param maxZoom the max zoom
     * @param type    the constraint mode
     * @see #getZoom()
     * @see #getRealZoom()
     */
    @Override
    public void setMaxZoom(float maxZoom, int type) {
        getEngine().setMaxZoom(maxZoom, type);
    }

    /**
     * Which is the min zoom that should be allowed.
     * If {@link #setOverPinchable(boolean)} is set to true, this can be over-pinched
     * for a brief time.
     *
     * @param minZoom the min zoom
     * @param type    the constraint mode
     * @see #getZoom()
     * @see #getRealZoom()
     */
    @Override
    public void setMinZoom(float minZoom, int type) {
        getEngine().setMinZoom(minZoom, type);
    }

    /**
     * Gets the current zoom value, which can be used as a reference when calling
     * {@link #zoomTo(float, boolean)} or {@link #zoomBy(float, boolean)}.
     * <p>
     * This can be different than the actual scale you get in the matrix, because at startup
     * we apply a base zoom to respect the "center inside" policy.
     * All zoom calls, including min zoom and max zoom, refer to this axis, where zoom is set to 1
     * right after the initial transformation.
     *
     * @return the current zoom
     * @see #getRealZoom()
     */
    @Override
    public float getZoom() {
        return getEngine().getZoom();
    }

    /**
     * Gets the current zoom value, including the base zoom that was eventually applied when
     * initializing to respect the "center inside" policy. This will match the scaleX - scaleY
     * values you get into the {@link Matrix}, and is the actual scale value of the content
     * from its original size.
     *
     * @return the real zoom
     */
    @Override
    public float getRealZoom() {
        return getEngine().getRealZoom();
    }

    /**
     * Returns the current horizontal pan value, in content coordinates
     * (that is, as if there was no zoom at all).
     *
     * @return the current horizontal pan
     */
    @Override
    public float getPanX() {
        return getEngine().getPanX();
    }

    /**
     * Returns the current vertical pan value, in content coordinates
     * (that is, as if there was no zoom at all).
     *
     * @return the current vertical pan
     */
    @Override
    public float getPanY() {
        return getEngine().getPanY();
    }

    /**
     * This is used to move  form page to screen area
     */
    public void moveToScreen() {
        child().animate().x(0).y(0).setDuration(0).start();
    }

    //endregion
}
