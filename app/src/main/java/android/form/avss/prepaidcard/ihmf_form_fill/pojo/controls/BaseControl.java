package android.form.avss.prepaidcard.ihmf_form_fill.pojo.controls;

import android.graphics.PointF;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

public class BaseControl implements Cloneable {

    public transient static final float DEFAULT_HELPTEXT_FONT_SIZE = 14f;
    public transient static final int TRUE = 1;
    public transient static final int FALSE = 0;
    public transient static final float DEFAULT_DIMEN = 0F;
    public transient static final int DEFAULT_INT_DIMEN = 0;


    private transient PointF ratio = new PointF(1f, 1f);

    @SerializedName("id")
    private Integer id;
    @SerializedName("type")
    private String type;
    @SerializedName("x")
    private Number x = DEFAULT_DIMEN;
    @SerializedName("y")
    private Number y = DEFAULT_DIMEN;
    @SerializedName("w")
    private Number w = DEFAULT_DIMEN;
    @SerializedName("h")
    private Number h = DEFAULT_DIMEN;
    @SerializedName("zindex")
    private Float zindex;
    @SerializedName("name")
    private String name;
    @SerializedName("isMarkedObject")
    private Integer isMarkedObject = TRUE;

    @SerializedName("mandatory")
    private Integer isMandatory = FALSE;

    //	==================================
    @SerializedName("helpText")
    private String helpText;

    @SerializedName("helpTextFontSize")
    private Float helpTextFontSize = DEFAULT_HELPTEXT_FONT_SIZE;


    @SerializedName("actualText")
    private String actualText;

    @SerializedName("isLock")
    private String isLock = Boolean.FALSE.toString();


    @SerializedName("controlVisible")
    private Integer controlVisible = TRUE;

    @SerializedName(value = "groupId", alternate = "controlGroupId")
    private String groupId;

    @SerializedName("activity")
    private String activity;


    @SerializedName("assignedUserCode")
    private String assignedUserCode;

    @SerializedName("assignedUserId")
    private String assignedUserId;

    @SerializedName("showMandatory")
    private String showMandatory;

    @SerializedName("profileId")
    private String profileId;

    @SerializedName("fieldLabel")
    private String fieldLabel;

    @SerializedName("subType")
    private String subType;

    @SerializedName("staffProfileId")
    private String staffProfileId;

    @SerializedName("boxH")
    private String boxH;

    @SerializedName("boxW")
    private String boxW;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Float getX() {
        return x.floatValue();
    }

    public void setX(Float x) {
        this.x = x;
    }

    public Float getY() {
        return y.floatValue();
    }

    public void setY(Float y) {
        this.y = y;
    }

    public Float getW() {
        return w.floatValue();
    }

    public void setW(Float w) {
        this.w = w;
    }

    public Float getH() {
        return h.floatValue();
    }

    public void setH(Float h) {
        this.h = h;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getMarkedObject() {
        return isMarkedControl();
    }

    public void setMarkedObject(boolean markedObject) {
        isMarkedObject = markedObject ? TRUE : FALSE;
    }

    public Integer getIsMandatory() {
        return isMandatory;
    }


    public boolean isMandatory() {
        return isMandatory != null && isMandatory == TRUE;
    }


    public void setIsMandatory(Integer isMandatory) {
        this.isMandatory = isMandatory;
    }

    public String getHelpText() {
        return helpText;
    }

    public void setHelpText(String helpText) {
        this.helpText = helpText;
    }

    public Float getHelpTextFontSize() {
        return helpTextFontSize;
    }

    public void setHelpTextFontSize(Float helpTextFontSize) {
        this.helpTextFontSize = helpTextFontSize;
    }


    public String getActualText() {
        return actualText == null ? "" : actualText;
    }

    public void setData(String actualText) {
        this.actualText = actualText;
    }

    public String getData() {
        return actualText;
    }


    public Integer getControlVisible() {
        return controlVisible;
    }

    public boolean isControlVisible() {
        return controlVisible != null && controlVisible == TRUE;
    }

    public void setControlVisible(Integer controlVisible) {
        this.controlVisible = controlVisible;
    }

    public void setControlVisible(boolean controlVisible) {
        this.controlVisible = controlVisible ? TRUE : FALSE;
    }

    public boolean isMarkedControl() {
        return isMarkedObject != null && isMarkedObject == TRUE;
    }


    public PointF getRatio() {
        if (ratio == null)
            ratio = new PointF(1f, 1f);

        return ratio;
    }

    public void setRatio(PointF ratio) {
        this.ratio = ratio;
    }


    public float getScaleX() {
        return getX() * getRatio().x;
    }

    public float getScaleY() {
        return getY() * getRatio().y;
    }

    public Float getScaleWidth() {
        return getW() * getRatio().x;
    }

    public Float getScaleHeight() {
        return getH() * getRatio().y;
    }


    public String getGroupId() {
        return groupId;
    }

    public boolean isLock() {
        return isLock != null && isLock.equalsIgnoreCase("1");
    }

    public boolean hasGroup() {
        return !TextUtils.isEmpty(getGroupId())
                && !TextUtils.isEmpty(getActivity())
                && !getGroupId().equals("0");
    }


    public String getActivity() {
        return activity;
    }


    public boolean isValid() {
        return true;
    }


    public BaseControl clone() throws CloneNotSupportedException {
        return (BaseControl) super.clone();
    }


    @SerializedName("htmlMapper")
    private String htmlMapper;

    public String getMapper() {
        return htmlMapper;
    }

    public boolean hasMapper(){
        return htmlMapper != null && !htmlMapper.isEmpty();
    }


}
