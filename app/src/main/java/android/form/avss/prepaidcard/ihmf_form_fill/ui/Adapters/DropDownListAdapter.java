package android.form.avss.prepaidcard.ihmf_form_fill.ui.Adapters;

import android.content.res.ColorStateList;
import android.form.avss.prepaidcard.R;
import android.form.avss.prepaidcard.ihmf_form_fill.interfaces.OnDropDownItemClickListener;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class DropDownListAdapter extends RecyclerView.Adapter<DropDownListAdapter.CustomViewHolder> implements Filterable {
    private List<String> dataList;
    private List<String> filteredDataList;
    private String charString;
    private OnDropDownItemClickListener listener;


    public DropDownListAdapter(List<String> dataList, OnDropDownItemClickListener listener) {
        this.dataList = dataList;
        this.listener = listener;
        this.filteredDataList = dataList;
    }


    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CustomViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_drop_down, null));
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, int position) {
        holder.tv_item.setText(filteredDataList.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(listener != null) listener.onClick(filteredDataList.get(holder.getAdapterPosition()));
            }
        });

        holder.tv_item.setText(filteredDataList.get(position));

        String fullText = filteredDataList.get(position);

        if ( charString!= null && !charString.isEmpty()) {
            int startPos = fullText.toLowerCase(Locale.US).indexOf(charString.toLowerCase(Locale.US));
            int endPos = startPos + charString.length();

            if (startPos != -1) {
                Spannable spannable = new SpannableString(fullText);
                ColorStateList blueColor = new ColorStateList(new int[][]{new int[]{}}, new int[]{Color.BLUE});
                TextAppearanceSpan highlightSpan = new TextAppearanceSpan(null, Typeface.BOLD, -1, blueColor, null);
                spannable.setSpan(highlightSpan, startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                holder.tv_item.setText(spannable);
            } else {
                holder.tv_item.setText(fullText);
            }
        } else {
            holder.tv_item.setText(fullText);
        }

    }

    @Override
    public int getItemCount() {
        return filteredDataList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredDataList = dataList;
                } else {
                    List<String> filteredList = new ArrayList<>();
                    for (String row : dataList) {
                        if (row.toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    filteredDataList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredDataList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredDataList = (ArrayList<String>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    class CustomViewHolder extends RecyclerView.ViewHolder{

        private TextView tv_item;

        CustomViewHolder(View itemView) {
            super(itemView);
            tv_item = (TextView) itemView.findViewById(R.id.tv_item);
        }
    }




}