package android.form.avss.prepaidcard.ui.custom_classes;

import android.content.Context;
import android.form.avss.prepaidcard.ui.custom_classes.interfaces.MyOnFocusChangeListener;
import android.graphics.Color;
import android.graphics.Rect;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;


public class CustomAutoComplete extends AppCompatAutoCompleteTextView {
	private TextWatcher textWatcher;

	public CustomAutoComplete(Context context) {
		super(context);
		this.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                return event.getKeyCode() == KeyEvent.KEYCODE_SOFT_RIGHT || event.getKeyCode() == KeyEvent.KEYCODE_DPAD_CENTER ||
                        event.getKeyCode() == KeyEvent.KEYCODE_DPAD_UP || event.getKeyCode() == KeyEvent.KEYCODE_DPAD_RIGHT ||
                        event.getKeyCode() == KeyEvent.KEYCODE_DPAD_DOWN || event.getKeyCode() == KeyEvent.KEYCODE_DPAD_LEFT;
            }
        });
		this.setTextSize(14.0f);
		this.setTextColor(Color.BLACK);
		this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getAdapter().isEmpty()) {
                    setDropDownHeight(0);
                } else {
                    showDropDown();
                }
            }
        });
	}

	public CustomAutoComplete(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                return event.getKeyCode() == KeyEvent.KEYCODE_SOFT_RIGHT || event.getKeyCode() == KeyEvent.KEYCODE_DPAD_CENTER ||
                        event.getKeyCode() == KeyEvent.KEYCODE_DPAD_UP || event.getKeyCode() == KeyEvent.KEYCODE_DPAD_RIGHT ||
                        event.getKeyCode() == KeyEvent.KEYCODE_DPAD_DOWN || event.getKeyCode() == KeyEvent.KEYCODE_DPAD_LEFT;
            }
        });
		this.setTextSize(14.0f);
		this.setTextColor(Color.BLACK);
	}

	public CustomAutoComplete(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		this.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                return event.getKeyCode() == KeyEvent.KEYCODE_SOFT_RIGHT || event.getKeyCode() == KeyEvent.KEYCODE_DPAD_CENTER ||
                        event.getKeyCode() == KeyEvent.KEYCODE_DPAD_UP || event.getKeyCode() == KeyEvent.KEYCODE_DPAD_RIGHT ||
                        event.getKeyCode() == KeyEvent.KEYCODE_DPAD_DOWN || event.getKeyCode() == KeyEvent.KEYCODE_DPAD_LEFT;
            }
        });
		this.setTextSize(14.0f);
		this.setTextColor(Color.BLACK);
	}

	@Override
	public boolean enoughToFilter() {
		return true;
	}

	@Override
	protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
		super.onFocusChanged(focused, direction, previouslyFocusedRect);
		if (focused && getAdapter() != null) {
			performFiltering(getText(), 0);
		}
	}

	public void addTextWatcher(final CustomTextInputLayout inputLayout) {
		textWatcher = new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (inputLayout.isErrorEnabled()) {
					inputLayout.setErrorDisabled();
				}
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		};
		this.addTextChangedListener(textWatcher);
	}

	public void setFocusChangeListener(final CustomTextInputLayout textInputLayout) {
		this.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (textInputLayout.isErrorEnabled()) {
                        addTextWatcher(textInputLayout);
                    } else {
                        removeTextChangedListener(textWatcher);
                    }
                    if (isNotEmpty()) {
                        setText("");
                    }
                    setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v1) {
                            if (getAdapter().isEmpty()) {
                                setDropDownHeight(0);
                            } else {
                                showDropDown();
                            }
                        }
                    });
                } else {
                    if (isStringEqualsTo("")) {
                        textInputLayout.setErrorMessage();
                    } else {
                        textInputLayout.setErrorDisabled();
                    }
                    removeTextChangedListener(textWatcher);
                }
            }
        });
	}

	public void setFocusChangeListener(final CustomTextInputLayout textInputLayout, final MyOnFocusChangeListener listener) {
		this.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (textInputLayout.isErrorEnabled()) {
                        addTextWatcher(textInputLayout);
                    } else {
                        removeTextChangedListener(textWatcher);
                    }
                    if (isNotEmpty()) {
                        setText("");
                    }
                    setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v1) {
                            if (!getAdapter().isEmpty()) {
                                showDropDown();
                            } else {
                                setDropDownHeight(0);
                            }
                        }
                    });

                } else {
                    if (isStringEqualsTo("")) {
                        textInputLayout.setErrorMessage();
                    } else {
                        textInputLayout.setErrorDisabled();
                    }
                    removeTextChangedListener(textWatcher);
                    if (listener != null)
                        listener.focusChanged();

                }
            }
        });
	}

	public boolean isEmpty() {
		return !TextUtils.isEmpty(this.getString());
	}

	public String getString() {
		return this.getText().toString().trim();
	}

	public Boolean isStringEqualsTo(String matchString) {
		return this.getString().equalsIgnoreCase(matchString);
	}

	public boolean isNotEmpty() {
		return !TextUtils.isEmpty(this.getString());
	}

	public boolean toLength(int length) {
		return  this.getString().length() == length;
	}

	public void setFieldToEmpty() {
		this.setText("");
	}

	public void setDisabled() {
		setEnabled(false);
	}

	public void setEnabled() {
		setEnabled(true);
	}

	public boolean isFieldEmpty( CustomTextInputLayout inputLayout, String msg ) {
		if (TextUtils.isEmpty(this.getString())) {
			inputLayout.setErrorMessage(msg);
			return false;
		} else {
			inputLayout.setErrorDisabled();
			return true;
		}
	}

	public boolean isFieldEmpty(CustomTextInputLayout inputLayout ) {
		if (TextUtils.isEmpty(this.getString())) {
			inputLayout.setErrorMessage();
			return true;
		} else {
			inputLayout.setErrorDisabled();
			return false;
		}
	}

	public boolean isFieldEmpty() {
		return TextUtils.isEmpty(this.getString());
	}

}
